<?php
#include("config.php");
include("db.php");
/*SE COMENTA PARA NO ENVIAR CORREOS AL CLIENTE NI A LA CONTRATA
include("send_email.php");*/
#include("send_push.php");

global $BASEDIR;

/*Funciones utiles*/
include("utils.php");

/*Manejadores de eventos
include("os.php");
include("mnt.php");
include("insp.php");

Desactivo el limite de memoria*/
ini_set('memory_limit', '-1');

/*___________________________________________________________________________________________
___________________________________________________________________________________________
Ciclo de despacho*/
$db = new MySQL_Database();
$ALIVE 		= TRUE;
$INTERVAL 	= 5000000; /*5s*/
$BENCHMARK  = TRUE;

$cont_id=1;
$fecha_OS= $argv[1];
$fecha_MNT=$argv[1];

Loggear("Iniciando servicio");

/*Marcamos las que usaremos para la actualizacion*/
$db->startTransaction();
Loggear('paso 1 actualizar os ');
$res = $db->ExecuteQuery("
		UPDATE  orden_servicio
		SET orse_pago_relacionado = 2
		WHERE cont_id = $cont_id
		AND (orse_pago_relacionado is null OR orse_pago_relacionado = 0)
		AND orse_fecha_validacion IS NOT NULL 
		AND orse_estado = 'APROBADA'   
		AND orse_tipo IN ('OSGN', 'OSEN', 'OSGU', 'OSEU')  
		AND orse_fecha_validacion >= '2017-07-01 00:00:00' 
		AND orse_fecha_validacion <='$fecha_OS'
	");
if($res['status']==0){
	Loggear('Salio del update de orden de servicio (1)');
	$db->Rollback();
	/*echo $res['error']."\n";*/
	Loggear($res['error']);

	exit(1);
}
Loggear('paso 2 actualizar mnt ');
$res = $db->ExecuteQuery("
		UPDATE  mantenimiento
		SET mant_pago_relacionado = 2
		WHERE cont_id = $cont_id
		AND (mant_pago_relacionado is null OR mant_pago_relacionado = 0)
		AND mant_fecha_validacion IS NOT NULL 
		AND mant_estado = 'APROBADA'     
		AND mant_fecha_validacion >= '2017-07-01 00:00:00' 
		AND mant_fecha_validacion <= '$fecha_MNT'
");
if($res['status']==0){
	Loggear('Salio del update de mantenimiento  (2)');
	$db->Rollback();
	echo $res['error']."\n";
	exit(1);
}
/*Realizamos el INSERT*/
	try{
	    $res = $db->ExecuteQuery("INSERT INTO  os_servicio_pago 
									( 	orse_id
									,	cont_id
									,	ospa_estado_proceso
									,	ospa_monto_capex
									,	ospa_monto_opex
									,	ospa_fecha_aprobacion
									,	ospa_periodo_aprobacion
									,	ospa_codigo_capex
									,	ospa_codigo_opex
									,	ospa_id_item_lpu
									,	ospa_desc_item_lpu
									,	ospa_valor_lpu
									,	ospa_cant_lpu
									,	ospa_zona_movistar
									,	ospa_zona_contrato
									,	ospa_usua_modificacion
									,	ospa_fecha_modificacion
									,	ospa_estado_os
									,	ospa_fecha_programacion
									, 	ospa_zona_cluster
									,	ospa_region
									) 
								SELECT 
									 o.orse_id
									,o.cont_id
									,orse_estado
									,ifnull(ROUND((lpip_precio * prit_cantidad),2), 0) ospa_monto_capex
									,ifnull(ROUND((lpip_precio * prit_cantidad),2),0) ospa_monto_opex
									,orse_fecha_validacion ospa_fecha_aprobacion
									,replace(substring(orse_fecha_validacion,1,7) ,'-','') ospa_periodo_aprobacion
									,ifnull(lpip_sap_capex, '-') ospa_codigo_capex
									,ifnull(lpip_sap_opex,'-') ospa_codigo_opex
									,ifnull(lip.lpip_id, 0) ospa_id_item_lpu
									,ifnull(lpit_nombre,orse_descripcion) ospa_desc_item_lpu
									,ifnull(lpip_precio,0) ospa_valor_lpu
									,ifnull(prit_cantidad,0) ospa_cant_lpu
									,(SELECT 
										zona_nombre
										FROM  rel_zona_emplazamiento 
										INNER JOIN  zona z ON (z.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='MOVISTAR' )
										WHERE rel_zona_emplazamiento.empl_id = o.empl_id
										AND z.cont_id=o.cont_id
										LIMIT 1) AS ospa_zona_movistar
									,(SELECT 
										zona_nombre
										FROM  rel_zona_emplazamiento 
										INNER JOIN  zona z ON (z.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CONTRATO' )
										WHERE rel_zona_emplazamiento.empl_id = o.empl_id
										AND z.cont_id=o.cont_id
										LIMIT 1) AS ospa_zona_contrato
                                    , 1 
									, NOW()
									,'NOPAGADA'
									, orse_fecha_solicitud
									, (SELECT 
										zona_nombre
										FROM  rel_zona_emplazamiento 
										INNER JOIN  zona z ON (z.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CLUSTER' )
										WHERE rel_zona_emplazamiento.empl_id = o.empl_id
										AND z.cont_id=o.cont_id
										LIMIT 1) AS ospa_zona_cluster
									,r.regi_nombre AS ospa_region	
									FROM  orden_servicio o 
									INNER JOIN  emplazamiento e ON (e.empl_id = o.empl_id)
									INNER JOIN  comuna c ON (c.comu_id = e.comu_id)
									INNER JOIN  provincia pr ON (pr.prov_id = c.prov_id)
									INNER JOIN  region r ON (r.regi_id = pr.regi_id)
									LEFT JOIN  presupuesto p ON (p.orse_id = o.orse_id AND p.pres_estado = 'APROBADO' )
									LEFT JOIN  presupuesto_item pi ON (pi.pres_id = p.pres_id)
									LEFT JOIN  lpu_item_precio lip ON (pi.lpip_id = lip.lpip_id)
									LEFT JOIN  lpu_item li ON (lip.lpit_id = li.lpit_id)
									LEFT JOIN  lpu_grupo lg ON (li.lpgr_id = lg.lpgr_id)	
									
									WHERE o.cont_id = $cont_id
									AND o.orse_estado = 'APROBADA'
									AND orse_pago_relacionado = 2
									ORDER BY o.orse_id
								");
		if($res['status']){
			if(0<$res['rows']){

				usleep($INTERVAL);			   
			}
		}else{
			Loggear('Salio del insert ');
			$db->Rollback();
			echo $res['error']."\n";
		}

		Loggear('paso 3 inserto os  ');
		$res = $db->ExecuteQuery("INSERT INTO  mnt_servicio_pago
										( mant_id
										, cont_id
										, mnpa_estado_proceso
										, mnpa_monto_capex
										, mnpa_monto_opex
										, mnpa_fecha_aprobacion
										, mnpa_periodo_aprobacion
										, mnpa_codigo_capex
										, mnpa_codigo_opex
										, mnpa_id_item_lpu
										, mnpa_desc_item_lpu
										, mnpa_valor_lpu
										, mnpa_cant_lpu
										, mnpa_zona_movistar
										, mnpa_zona_contrato
										, mnpa_usua_modificacion
										, mnpa_fecha_modificacion
										, mnpa_estado_mnt
										, mnpa_fecha_programacion
										, mnpa_zona_cluster
										, mnpa_region
										, mnpa_periodo
                                        , mnpa_especialidad
                                        , mnpa_empresa
                                        , mnpa_codigo
                                        , mnpa_emplazamiento
                                        , mnpa_direccion_emplazamiento
                                        , mnpa_clasificacion
										)
									SELECT 
										mant_id
										, m.cont_id
										, mant_estado mnpa_estado_proceso
										, case when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 6 ) then '29.7'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 6 ) then '28.29'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 6 ) then '22.41'
											   when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 12 ) then '29.7'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 12 ) then '28.29'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 12 ) then '22.41'

											   when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 14 ) then '29.7'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 14 ) then '28.29'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 14 ) then '22.41'


											   when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 2 ) then '17.96'
											   when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 2 ) then '17.1'
											   when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 2 ) then '12.45'
											   when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 3 ) then '17.96'
											   when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 3 ) then '17.1'
											   when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 3 ) then '12.45'
											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 2 ) then '7.68'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 2 ) then '6.93'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 2 ) then '6.93'
											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 12 ) then '7.68'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 12 ) then '6.93'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 12 ) then '6.93'
											   
											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 14 ) then '0'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 14 ) then '6.93'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 14 ) then '6.93'
											   
											   else 0 end    mnpa_monto_capex
										, case when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 6 ) then '29.7'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 6 ) then '28.29'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 6 ) then '22.41'
											   when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 12 ) then '29.7'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 12 ) then '28.29'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 12 ) then '22.41'

											   when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 14 ) then '29.7'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 14 ) then '28.29'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 14 ) then '22.41'

											   when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 2 ) then '17.96'
											   when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 2 ) then '17.1'
											   when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 2 ) then '12.45'
											   when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 3 ) then '17.96'
											   when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 3 ) then '17.1'
											   when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 3 ) then '12.45'
											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 2 ) then '7.68'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 2 ) then '6.93'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 2 ) then '6.93'
											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 12 ) then '7.68'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 12 ) then '6.93'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 12 ) then '6.93'

											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 14 ) then '0'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 14 ) then '6.93'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 14 ) then '6.93'

											   else 0 end    mnpa_monto_opex
										, mant_fecha_validacion mnpa_fecha_aprobacion
										, replace(substring(mant_fecha_validacion,1,7) ,'-','')  mnpa_periodo_aprobacion
										, case when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 6 ) then 'Consorcio 1315'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 6 ) then 'Consorcio 1322'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 6 ) then 'Consorcio 1329'
											   when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 12 ) then 'Consorcio 1319'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 12 ) then 'Consorcio 1326'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 12 ) then 'Consorcio 1333'
											   
											   when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 14 ) then 'Consorcio 1319'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 14 ) then 'Consorcio 1326'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 14 ) then 'Consorcio 1333'
											   
											   
											   when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 2 ) then 'Consorcio 1314'
											   when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 2 ) then 'Consorcio 1321'
											   when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 2 ) then 'Consorcio 1328'
											   when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 3 ) then 'Consorcio 1318'
											   when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 3 ) then 'Consorcio 1325'
											   when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 3 ) then 'Consorcio 1332'
											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 2 ) then 'Consorcio 1316'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 2 ) then 'Consorcio 1323'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 2 ) then 'Consorcio 1330'
											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 12 ) then 'Consorcio 1320'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 12 ) then 'Consorcio 1327'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 12 ) then 'Consorcio 1334'           

											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 14 ) then 'Consorcio 1320'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 14 ) then 'Consorcio 1327'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 14 ) then 'Consorcio 1334'           

											   else 0 end mnpa_codigo_capex
										, case when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 6 ) then 'Consorcio 1315'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 6 ) then 'Consorcio 1322'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 6 ) then 'Consorcio 1329'
											   when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 12 ) then 'Consorcio 1319'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 12 ) then 'Consorcio 1326'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 12 ) then 'Consorcio 1333'

											   when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 14 ) then 'Consorcio 1319'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 14 ) then 'Consorcio 1322'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 14 ) then 'Consorcio 1333'

											   when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 2 ) then 'Consorcio 1314'
											   when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 2 ) then 'Consorcio 1321'
											   when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 2 ) then 'Consorcio 1328'
											   when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 3 ) then 'Consorcio 1318'
											   when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 3 ) then 'Consorcio 1325'
											   when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 3 ) then 'Consorcio 1332'
											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 2 ) then 'Consorcio 1316'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 2 ) then 'Consorcio 1323'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 2 ) then 'Consorcio 1330'
											   
											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 12 ) then 'Consorcio 1320'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 12 ) then 'Consorcio 1327'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 12 ) then 'Consorcio 1334'

											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 14 ) then 'Consorcio 1320'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 14 ) then 'Consorcio 1327'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 14 ) then 'Consorcio 1334'
											   
											   
											   else 0 end mnpa_codigo_opex 
										, 0 mnpa_id_item_lpu
										, case when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 6 ) then 'ZONA 1_ENERGIA DE RESPALDO  AB, AC SEMESTRAL'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 6 ) then 'ZONA 2_ENERGIA DE RESPALDO  AB, AC SEMESTRAL'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 6 ) then 'ZONA 3_ENERGIA DE RESPALDO  AB, AC SEMESTRAL'
											   when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 12 ) then 'ZONA 1_ENERGIA DE RESPALDO  BB, BC ANUAL'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 12 ) then 'ZONA 2_ENERGIA DE RESPALDO  BB, BC ANUAL'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 12 ) then 'ZONA 3_ENERGIA DE RESPALDO  BB, BC ANUAL'
											   when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 14 ) then m.mant_descripcion
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 14 ) then m.mant_descripcion
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 14 ) then m.mant_descripcion
											   when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 2 ) then 'ZONA 1_CLIMA AA, AB BIMESTRAL'
											   when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 2 ) then 'ZONA 2_CLIMA AA, AB BIMESTRAL'
											   when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 2 ) then 'ZONA 3_CLIMA AA, AB BIMESTRAL'
											   when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 3 ) then 'ZONA 1_CLIMA BA, BB TRIMESTRAL'
											   when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 3 ) then 'ZONA 2_CLIMA BA, BB TRIMESTRAL'
											   when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 3 ) then 'ZONA 3_CLIMA BA, BB TRIMESTRAL'
											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 2 ) then 'ZONA 1_VISITA TÉCNICA AD BIMESTRAL'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 2 ) then 'ZONA 2_VISITA TÉCNICA AD BIMESTRAL'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 2 ) then 'ZONA 3_VISITA TÉCNICA AD BIMESTRAL'
											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 12 ) then 'ZONA 1_VISITA TÉCNICA BD ANUAL'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 12 ) then 'ZONA 2_VISITA TÉCNICA BD ANUAL'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 12 ) then 'ZONA 3_VISITA TÉCNICA BD ANUAL'           

											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 14 ) then 'ZONA 1_VISITA TÉCNICA BD ANUAL'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 14 ) then 'ZONA 2_VISITA TÉCNICA BD ANUAL'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 14 ) then 'ZONA 3_VISITA TÉCNICA BD ANUAL'           

											   else '' end mnpa_desc_item_lpu
										, case when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 6 ) then '29.7'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 6 ) then '28.29'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 6 ) then '22.41'
											   when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 12 ) then '29.7'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 12 ) then '28.29'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 12 ) then '22.41'											   
											   when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 14 ) then '29.7'
											   when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 14 ) then '28.29'
											   when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 14 ) then '22.41'
											   when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 2 ) then '17.96'
											   when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 2 ) then '17.1'
											   when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 2 ) then '12.45'
											   when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 3 ) then '17.96'
											   when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 3 ) then '17.1'
											   when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 3 ) then '12.45'
											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 2 ) then '7.68'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 2 ) then '6.93'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 2 ) then '6.93'
											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 12 ) then '7.68'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 12 ) then '6.93'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 12 ) then '6.93'
											   when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 14 ) then '0'
											   when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 14 ) then '6.93'
											   when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 14 ) then '6.93'

											   else 0 end mnpa_valor_lpu
										, 1 mnpa_cant_lpu
										, (SELECT 
												zona_nombre
												FROM  rel_zona_emplazamiento 
												INNER JOIN  zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='MOVISTAR' )
												WHERE rel_zona_emplazamiento.empl_id = m.empl_id
												AND zona.cont_id=m.cont_id
												LIMIT 1) AS mnpa_zona_movistar
										,  z_contrato.zona_nombre mnpa_zona_contrato
										, 1 
										, NOW()
										,'NOPAGADA'
										, m.mant_fecha_programada
										, (SELECT 
											zona_nombre
											FROM  rel_zona_emplazamiento 
											INNER JOIN  zona z ON (z.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CLUSTER' )
											WHERE rel_zona_emplazamiento.empl_id = m.empl_id
											AND z.cont_id=m.cont_id
											LIMIT 1) AS mnpa_zona_cluster
										, r.regi_nombre AS mnpa_region	
										, p.peri_nombre
                                        , es.espe_nombre
                                        , em.empr_nombre
                                        , '5000018665' as codigo
                                        , e.empl_nombre
                                        , e.empl_direccion
                                        , cl.clpr_nombre
										
									FROM  mantenimiento m
									INNER JOIN  emplazamiento e ON (e.empl_id = m.empl_id)
									INNER JOIN  comuna c ON (c.comu_id = e.comu_id)
									INNER JOIN  provincia pr ON (pr.prov_id = c.prov_id)
									INNER JOIN  region r ON (r.regi_id = pr.regi_id)									
									INNER JOIN  mantenimiento_periodos mp ON mp.mape_id = m.mape_id
									INNER JOIN  rel_contrato_periodicidad rcp ON rcp.rcpe_id = mp.rcpe_id AND rcp.cont_id = m.cont_id
									INNER JOIN  periodicidad p ON p.peri_id = rcp.peri_id 
									INNER JOIN  especialidad es ON es.espe_id = m.espe_id
									INNER JOIN  empresa em ON em.empr_id = m.empr_id
									INNER JOIN  clasificacion_programacion cl ON cl.clpr_id = m.clpr_id
									INNER JOIN (SELECT 
													z.zona_id
													,zona_nombre
													,empl_id
													,cont_id
												FROM  rel_zona_emplazamiento r
												INNER JOIN  zona z ON (z.zona_id = r.zona_id AND z.zona_tipo='CONTRATO' )
												) AS z_contrato ON z_contrato.empl_id = m.empl_id AND z_contrato.cont_id =m.cont_id
									#INNER JOIN lpu l ON l.cont_id = m.cont_id AND l.lpu_estado = 'ACTIVO'
									#INNER JOIN lpu_grupo lg ON lg.lpu_id= l.lpu_id AND lg.lpgr_nombre = 'MANTENCION PREVENTIVA'
									#INNER JOIN lpu_item li ON li.lpgr_id = lg.lpgr_id 
									#INNER JOIN lpu_item_precio lp ON lp.lpit_id = li.lpit_id AND lp.zona_id = z_contrato.zona_id
									WHERE mant_pago_relacionado =2
									AND m.cont_id = $cont_id
									UNION ALL
					SELECT 
							mant_id
							, m.cont_id
							, mant_estado mnpa_estado_proceso
							, case when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 6 ) then '29.7'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 6 ) then '28.29'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 6 ) then '22.41'
							when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 12 ) then '29.7'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 12 ) then '28.29'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 12 ) then '22.41'
							when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 14 ) then '29.7'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 14 ) then '28.29'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 14 ) then '22.41'
							when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 2 ) then '17.96'
							when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 2 ) then '17.1'
							when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 2 ) then '12.45'
							when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 3 ) then '17.96'
							when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 3 ) then '17.1'
							when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 3 ) then '12.45'
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 2 ) then '7.68'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 2 ) then '6.93'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 2 ) then '6.93'
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 12 ) then '7.68'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 12 ) then '6.93'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 12 ) then '6.93'

							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 14 ) then '0'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 14 ) then '6.93'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 14 ) then '6.93'

							else 0 end    mnpa_monto_capex
							, case when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 6 ) then '29.7'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 6 ) then '28.29'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 6 ) then '22.41'
							when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 12 ) then '29.7'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 12 ) then '28.29'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 12 ) then '22.41'
							when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 14 ) then '29.7'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 14 ) then '28.29'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 14 ) then '22.41'
							when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 2 ) then '17.96'
							when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 2 ) then '17.1'
							when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 2 ) then '12.45'
							when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 3 ) then '17.96'
							when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 3 ) then '17.1'
							when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 3 ) then '12.45'
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 2 ) then '7.68'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 2 ) then '6.93'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 2 ) then '6.93'
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 12 ) then '7.68'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 12 ) then '6.93'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 12 ) then '6.93'
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 14 ) then '0'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 14 ) then '6.93'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 14 ) then '6.93'

							else 0 end    mnpa_monto_opex
							, mant_fecha_validacion mnpa_fecha_aprobacion
							, replace(substring(mant_fecha_validacion,1,7) ,'-','')  mnpa_periodo_aprobacion
							, case when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 6 ) then 'Consorcio 1315'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 6 ) then 'Consorcio 1322'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 6 ) then 'Consorcio 1329'
							when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 12 ) then 'Consorcio 1319'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 12 ) then 'Consorcio 1326'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 12 ) then 'Consorcio 1333'
							when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 14 ) then 'Consorcio 1319'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 14 ) then 'Consorcio 1326'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 14 ) then 'Consorcio 1333'
							when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 2 ) then 'Consorcio 1314'
							when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 2 ) then 'Consorcio 1321'
							when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 2 ) then 'Consorcio 1328'
							when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 3 ) then 'Consorcio 1318'
							when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 3 ) then 'Consorcio 1325'
							when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 3 ) then 'Consorcio 1332'
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 2 ) then 'Consorcio 1316'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 2 ) then 'Consorcio 1323'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 2 ) then 'Consorcio 1330'
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 12 ) then 'Consorcio 1320'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 12 ) then 'Consorcio 1327'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 12 ) then 'Consorcio 1334'           
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 14 ) then 'Consorcio 1320'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 14 ) then 'Consorcio 1327'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 14 ) then 'Consorcio 1334'           

							else 0 end mnpa_codigo_capex
							, case when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 6 ) then 'Consorcio 1315'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 6 ) then 'Consorcio 1322'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 6 ) then 'Consorcio 1329'
							when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 12 ) then 'Consorcio 1319'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 12 ) then 'Consorcio 1326'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 12 ) then 'Consorcio 1333'
							when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 14 ) then 'Consorcio 1319'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 14 ) then 'Consorcio 1322'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 14 ) then 'Consorcio 1333'
							when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 2 ) then 'Consorcio 1314'
							when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 2 ) then 'Consorcio 1321'
							when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 2 ) then 'Consorcio 1328'
							when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 3 ) then 'Consorcio 1318'
							when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 3 ) then 'Consorcio 1325'
							when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 3 ) then 'Consorcio 1332'
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 2 ) then 'Consorcio 1316'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 2 ) then 'Consorcio 1323'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 2 ) then 'Consorcio 1330'
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 12 ) then 'Consorcio 1320'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 12 ) then 'Consorcio 1327'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 12 ) then 'Consorcio 1334'
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 14 ) then 'Consorcio 1320'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 14 ) then 'Consorcio 1327'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 14 ) then 'Consorcio 1334'

							else 0 end mnpa_codigo_opex 
							, 0 mnpa_id_item_lpu
							, case when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 6 ) then 'ZONA 1_ENERGIA DE RESPALDO  AB, AC SEMESTRAL'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 6 ) then 'ZONA 2_ENERGIA DE RESPALDO  AB, AC SEMESTRAL'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 6 ) then 'ZONA 3_ENERGIA DE RESPALDO  AB, AC SEMESTRAL'
							when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 12 ) then 'ZONA 1_ENERGIA DE RESPALDO  BB, BC ANUAL'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 12 ) then 'ZONA 2_ENERGIA DE RESPALDO  BB, BC ANUAL'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 12 ) then 'ZONA 3_ENERGIA DE RESPALDO  BB, BC ANUAL'
							when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 14 ) then m.mant_descripcion
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 14 ) then m.mant_descripcion
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 14 ) then m.mant_descripcion
							when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 2 ) then 'ZONA 1_CLIMA AA, AB BIMESTRAL'
							when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 2 ) then 'ZONA 2_CLIMA AA, AB BIMESTRAL'
							when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 2 ) then 'ZONA 3_CLIMA AA, AB BIMESTRAL'
							when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 3 ) then 'ZONA 1_CLIMA BA, BB TRIMESTRAL'
							when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 3 ) then 'ZONA 2_CLIMA BA, BB TRIMESTRAL'
							when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 3 ) then 'ZONA 3_CLIMA BA, BB TRIMESTRAL'
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 2 ) then 'ZONA 1_VISITA TÉCNICA AD BIMESTRAL'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 2 ) then 'ZONA 2_VISITA TÉCNICA AD BIMESTRAL'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 2 ) then 'ZONA 3_VISITA TÉCNICA AD BIMESTRAL'
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 12 ) then 'ZONA 1_VISITA TÉCNICA BD ANUAL'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 12 ) then 'ZONA 2_VISITA TÉCNICA BD ANUAL'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 12 ) then 'ZONA 3_VISITA TÉCNICA BD ANUAL'           
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 14 ) then 'ZONA 1_VISITA TÉCNICA BD ANUAL'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 14 ) then 'ZONA 2_VISITA TÉCNICA BD ANUAL'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 14 ) then 'ZONA 3_VISITA TÉCNICA BD ANUAL'           

							else '' end mnpa_desc_item_lpu
							, case when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 6 ) then '29.7'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 6 ) then '28.29'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 6 ) then '22.41'
							when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 12 ) then '29.7'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 12 ) then '28.29'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 12 ) then '22.41' 

						    when (es.espe_id = 1 and z_contrato.zona_id =1 and p.peri_id = 14 ) then '29.7'
							when (es.espe_id = 1 and z_contrato.zona_id =2 and p.peri_id = 14 ) then '28.29'
							when (es.espe_id = 1 and z_contrato.zona_id =3 and p.peri_id = 14 ) then '22.41'
							when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 2 ) then '17.96'
							when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 2 ) then '17.1'
							when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 2 ) then '12.45'
							when (es.espe_id = 2 and z_contrato.zona_id =1 and p.peri_id = 3 ) then '17.96'
							when (es.espe_id = 2 and z_contrato.zona_id =2 and p.peri_id = 3 ) then '17.1'
							when (es.espe_id = 2 and z_contrato.zona_id =3 and p.peri_id = 3 ) then '12.45'
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 2 ) then '7.68'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 2 ) then '6.93'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 2 ) then '6.93'
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 12 ) then '7.68'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 12 ) then '6.93'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 12 ) then '6.93'
							when (es.espe_id = 14 and z_contrato.zona_id =1 and p.peri_id = 14 ) then '0'
							when (es.espe_id = 14 and z_contrato.zona_id =2 and p.peri_id = 14 ) then '6.93'
							when (es.espe_id = 14 and z_contrato.zona_id =3 and p.peri_id = 14 ) then '6.93'

							else 0 end mnpa_valor_lpu
							, 1 mnpa_cant_lpu
							, (SELECT 
							zona_nombre
							FROM  rel_zona_emplazamiento 
							INNER JOIN  zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='MOVISTAR' )
							WHERE rel_zona_emplazamiento.empl_id = m.empl_id
							AND zona.cont_id=m.cont_id
							LIMIT 1) AS mnpa_zona_movistar
							,  z_contrato.zona_nombre mnpa_zona_contrato
							, 1 
							, NOW()
							,'NOPAGADA'
							, m.mant_fecha_programada
							, (SELECT 
							zona_nombre
							FROM  rel_zona_emplazamiento 
							INNER JOIN  zona z ON (z.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CLUSTER' )
							WHERE rel_zona_emplazamiento.empl_id = m.empl_id
							AND z.cont_id=m.cont_id
							LIMIT 1) AS mnpa_zona_cluster
							, r.regi_nombre AS mnpa_region               
							, p.peri_nombre
							, es.espe_nombre
							, em.empr_nombre
							, 5000018665 as codigo
							, e.empl_nombre
							, e.empl_direccion
							,  '' as clpr_nombre

							FROM  mantenimiento m
							INNER JOIN  emplazamiento e ON (e.empl_id = m.empl_id)
							INNER JOIN  comuna c ON (c.comu_id = e.comu_id)
							INNER JOIN  provincia pr ON (pr.prov_id = c.prov_id)
							INNER JOIN  region r ON (r.regi_id = pr.regi_id)    
							INNER JOIN  mantenimiento_periodos mp ON mp.mape_id = m.mape_id
							INNER JOIN  rel_contrato_periodicidad rcp ON rcp.rcpe_id = mp.rcpe_id AND rcp.cont_id = m.cont_id
							INNER JOIN  periodicidad p ON p.peri_id = rcp.peri_id 
							INNER JOIN  especialidad es ON es.espe_id = m.espe_id
							INNER JOIN  empresa em ON em.empr_id = m.empr_id
							INNER JOIN (SELECT 
							z.zona_id
							,zona_nombre
							,empl_id
							,cont_id
							FROM  rel_zona_emplazamiento r
							INNER JOIN  zona z ON (z.zona_id = r.zona_id AND z.zona_tipo='CONTRATO' )
							) AS z_contrato ON z_contrato.empl_id = m.empl_id AND z_contrato.cont_id =m.cont_id
							#INNER JOIN lpu l ON l.cont_id = m.cont_id AND l.lpu_estado = 'ACTIVO'
							#INNER JOIN lpu_grupo lg ON lg.lpu_id= l.lpu_id AND lg.lpgr_nombre = 'MANTENCION PREVENTIVA'
							#INNER JOIN lpu_item li ON li.lpgr_id = lg.lpgr_id 
							#INNER JOIN lpu_item_precio lp ON lp.lpit_id = li.lpit_id AND lp.zona_id = z_contrato.zona_id
							where m.cont_id = 1
							AND m.clpr_id = 0
							AND mant_pago_relacionado =2"
								);
		if($res['status']){
			if(0<$res['rows']){
				usleep($INTERVAL);			   
			}
		}
		Loggear('paso 4 inserto mnt  ');
		/*Marcamos las procesadas*/
		$res = $db->ExecuteQuery("
			UPDATE orden_servicio
			SET orse_pago_relacionado = 1 
			WHERE cont_id = $cont_id 
			AND orse_pago_relacionado = 2 
		");
		if($res['status']==0){
			Loggear('Salio del update de orden de servicio (3)');
			$db->Rollback();
			echo $res['error']."\n";
			exit(3);
		}

		$res = $db->ExecuteQuery("
			UPDATE mantenimiento
			SET mant_pago_relacionado = 1 
			WHERE cont_id = $cont_id
			AND mant_pago_relacionado = 2
			");
		if($res['status']==0){
			Loggear('Salio del update de mantenimiento (4)');
			$db->Rollback();
			echo $res['error']."\n";
			exit(3);
		}
		$db->Commit();
		exit(0);
		echo "Procedimiento finalizado exitosamente\n";
		
	}catch (Exception $e) {
		Loggear("Excepción:  ".$e->getMessage(),LOG_ERR);
	}
	
	Loggear('paso 5 paso todo bien');
	Loggear("Finalizando servicio");
	exit(0);
?>