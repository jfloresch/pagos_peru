<?php
date_default_timezone_set('America/Santiago');

class MySQL_Database{
	var $lastQuery = "";
	function MySQL_Database($DB_HOST,$DB_USER,$DB_PASS,$DB_NAME){
		$this->pdb = mysql_connect($DB_HOST,$DB_USER,$DB_PASS);
		if(!$this->pdb){
			echo mysql_error();
		}
			
		if(!mysql_select_db($DB_NAME)){
			echo mysql_error();									
		}		
		setlocale(LC_ALL,"es_CL.UTF-8"); 
        mysql_query('SET CHARACTER SET utf8');
	}				
	
	function ExecuteQuery($szQuery){
        $tResult 			 = array();
		$tResult['status']   = 0;
		$tResult['rows'] 	 = 0;
		$tResult['data']	 = array();
		$tResult['error']	 = "";
		$this->lastQuery     = $szQuery;
		$szQuery = trim($szQuery);			

		$tDbResult = mysql_query($szQuery,$this->pdb);
		if($tDbResult){											
			$tResult['status'] = 1;		
			if(strpos($szQuery,"INTO OUTFILE")===FALSE){			
				if(strpos($szQuery,"SELECT")===0 || strpos($szQuery,"SHOW")===0){
					$tResult['rows'] = mysql_num_rows($tDbResult);
					if($tRow = mysql_fetch_assoc($tDbResult)){	
						do{
							array_push($tResult['data'],$tRow);
						}
						while($tRow = mysql_fetch_assoc($tDbResult));						
					}					
				}
				else{
					$tResult['data'][0]['status'] = $tResult['status'];
					$tResult['data'][0]['id']     = mysql_insert_id($this->pdb);
					$tResult['rows']              = mysql_affected_rows($this->pdb);			
				}
			}							
		}	
		else{
			$tResult['error'] = mysql_error($this->pdb);
		}				
		return $tResult;
	}
	 
	 function ExecuteFastQuery($szQuery){
		$tDbResult = mysql_query($szQuery,$this->pdb);			
		return $tDbResult;
	}
	
	 
	 function startTransaction(){
	 		$szQuery = "START TRANSACTION";	
			return $this->ExecuteQuery($szQuery);	
	 }
	 		 
	 function Commit(){
	 		$szQuery = "COMMIT";	
			return $this->ExecuteQuery($szQuery);	
	 }
	 function Rollback(){
	 		$szQuery = "ROLLBACK";	
			return $this->ExecuteQuery($szQuery);	
	 }		
}


//Obtener formularios
$DB_HOST1 = 'siom.cebramjynokd.us-west-2.rds.amazonaws.com';
$DB_USER1 = 'SiomMaster';
$DB_PASS1 = 'Pass.784.OM';
$DB_NAME1 = 'SIOM2.4-prod';

$DB_HOST2 = "siom.cebramjynokd.us-west-2.rds.amazonaws.com";
$DB_USER2 = "SiomMaster";
$DB_PASS2 = "Pass.784.OM";
$DB_NAME2 = "SIOM2.4-devel";

$FORMS   = array(24);

$db1 = new MySQL_Database($DB_HOST1,$DB_USER1,$DB_PASS1,$DB_NAME1);
$db2 = new MySQL_Database($DB_HOST2,$DB_USER2,$DB_PASS2,$DB_NAME2);


$db2->startTransaction();

foreach ($FORMS as $form_id) {
	echo "Moviendo formulario $form_id\n";

	$res = $db1->ExecuteQuery("SELECT * FROM formulario WHERE form_id=$form_id");
	if(!$res['status']){
		$db2->Rollback();
		die($res['error']."\n");
	}
	if($res['rows']==0){
		continue;
	}
	
	print_r($res);

	$form = $res['data'][0];

	$form['form_id']=0;
	$insert_data = array();
	foreach ($form as $key => $value) {
		array_push($insert_data,$key."='".$value."'");
	}
	$insert_data = implode(",",$insert_data);

	$res = $db2->ExecuteQuery("INSERT INTO formulario SET ".$insert_data);
	if(!$res['status']){
		$db2->Rollback();
		die($res['error']."\n");
	}
	$new_form_id = $res['data'][0]['id'];


	$res = $db1->ExecuteQuery("SELECT * FROM formulario_grupo WHERE form_id=$form_id");
	if(!$res['status']){
		$db2->Rollback();
		die($res['error']."\n");
	}

	$grupos = $res['data'];

	foreach($grupos as $row){
		$fogr_id 		= $row['fogr_id'];

		echo "\t\t Moviendo formulario_grupo $fogr_id\n";

		$row['fogr_id'] = 0;
		$row['form_id'] = $new_form_id;
		$insert_data = array();
		foreach ($row as $key => $value) {
			if($key=='fogr_orden' && $value==''){
				array_push($insert_data,$key."=0");
			}
			else if($key=='fogr_estado' && $value==''){
				array_push($insert_data,$key."='ACTIVO'");
			}
			else{
				array_push($insert_data,$key."='".$value."'");
			}
		}
		$insert_data = implode(",",$insert_data);

		//echo $insert_data."\n";

		$res = $db2->ExecuteQuery("INSERT INTO formulario_grupo SET ".$insert_data);
		if(!$res['status']){
			$db2->Rollback();
			die($res['error']."\n");
		}
		$new_fogr_id = $res['data'][0]['id'];	



		$res = $db1->ExecuteQuery("SELECT * FROM formulario_item WHERE fogr_id=$fogr_id");
		if(!$res['status']){
			$db2->Rollback();
			die($res['error']."\n");
		}
		$items = $res['data'];

		foreach($items as $row2){
			$foit_id 		= $row2['foit_id'];
			echo "\t\t\t Moviendo formulario_item $foit_id\n";

			$row2['foit_id'] = 0;
			$row2['fogr_id'] = $new_fogr_id;

			$insert_data = array();
			foreach ($row2 as $key => $value) {
				if($key=='foit_orden' && $value==''){
					array_push($insert_data,$key."=0");
				}
				else if($key=='foit_estado' && $value==''){
					array_push($insert_data,$key."='ACTIVO'");
				}
				else{
					array_push($insert_data,$key."='".$value."'");
				}
			}
			$insert_data = implode(",",$insert_data);


			$res = $db2->ExecuteQuery("INSERT INTO formulario_item SET ".$insert_data);
			if(!$res['status']){
				$db2->Rollback();
				die($res['error']."\n");
			}
		}
	}
}

$db2->Commit();
echo "OK!\n";
?>