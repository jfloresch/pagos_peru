<?php
class Upload
{
	const UPLOAD_DIR = "repo/";

	public static function UploadFile($file,$filename) {
		try {
			if (!isset($file['error']) || is_array($file['error'])) {
			    return array("status"=>false,"error"=>'Parametros inválidos');
			}

			switch ($file['error']) {
			    case UPLOAD_ERR_OK:
			        break;
			    case UPLOAD_ERR_NO_FILE:
			        return array("status"=>false,"error"=>'No se ha enviado archivo');
			    case UPLOAD_ERR_INI_SIZE:
			    case UPLOAD_ERR_FORM_SIZE: 
			        return array("status"=>false,"error"=>'Archivo excede tamaño máximo');
			    default:
			        return array("status"=>false,"error"=>'Error desconocido');
			}
			/*
			if ($files['size'] > 1000000) {
			    throw new RuntimeException('Exceeded filesize limit.');
			}
			*/
			/*
			if(0<count($mimetipes)){
				$finfo = new finfo(FILEINFO_MIME_TYPE);
				if (false === $ext = array_search( $finfo->file($files['tmp_name']),$mimetipes)
				    array(
				        'jpg' => 'image/jpeg',
				        'png' => 'image/png',
				        'gif' => 'image/gif',
				    ),
				    true
				)) {
				    throw new RuntimeException('Invalid file format.');
				}
			}
			*/

			$res = Upload::GenerateFilename($filename);

			if (!move_uploaded_file($file['tmp_name'],$res['abspath'])){
			   return array("status"=>false,"error"=>'Error al mover el archivo');
			}

			return array("status"=>true,"data"=>array("filename"=>$res['relpath']));
		} 
		catch (RuntimeException $e) {
			return array("status"=>false,"error"=>$e->getMessage());
		}
	}

	public static function GenerateFilename($filename) {
		$relpath  = Upload::UPLOAD_DIR.rand(0,255)."/".rand(0,255);
		$abspath  = realpath(__DIR__ . "/..")."/".$relpath;
 
		if(!file_exists($abspath)){
			mkdir($abspath,0775,true); 
		}
		return array("relpath"=>$relpath."/".$filename,"abspath"=>$abspath."/".$filename);
	}
}

//print_r(Upload::GenerateFilename("algo.ogo"));
?>
