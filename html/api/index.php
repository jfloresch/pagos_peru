<?php

function JSONResponse($a){
    header('Cache-Control: no-cache, must-revalidate');
    header('Content-type: application/json');
    echo json_encode($a);
}

function CreateZip($files = array(),$destination = '',$overwrite = true) {
    if(file_exists($destination) && !$overwrite) { return false; }
    $valid_files = array();
    if(is_array($files)) {
        foreach($files as $file) {
            if(file_exists($file)) {
                $valid_files[] = $file;
            }
        }
    }
    
    if(count($valid_files)) {
        $zip = new ZipArchive();
        if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
            return false;
        }
    
        foreach($valid_files as $file) {
            $zip->addFile($file,basename($file));
        }
        $zip->close();
        return file_exists($destination);
    }
    else
    {
        return false;
    }
}

function logIt($line,$message){
    syslog(LOG_ERR,"[SIOM] ".__FILE__.":$line $message");
}


header('Content-type: application/json');

include("../rest/config.php");
//include("cache.php");
//include("../rest/db.php");
include("export.php");

global $DEF_CONFIG;

//$db = new MySQL_Database();
//$cache = new Cache();

$DEFAULT_TIMEZONE=$DEF_CONFIG['app_timezone'];

date_default_timezone_set("America/Santiago");

switch($_GET['q']){
    case 'GenerateExcel':{
        session_start();
        if(isset($_SESSION['user_id'])){
            $type   = $_GET['tipo'];
            $params = $_GET['params'];
            
            Export::GenerateExcel($type,$params);
        }
        else{
            echo("{\"status\":0,\"error\":\"Acceso denegado\"}");
        }
        break;
    }

    case 'GeneratePDF':{
        session_start();
        if(isset($_SESSION['user_id'])){
            $type   = $_GET['tipo'];
            $params = $_GET['params'];
            
            Export::GeneratePDF($type,$params);
        }
        else{
            echo("{\"status\":0,\"error\":\"Acceso denegado\"}");
        }
        break;
    }
}
?>