<?php

error_reporting(E_ALL ^ E_DEPRECATED);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Santiago');

class Export
{
    const CREATOR = 'SIOM';
    const TITLE   = '';
    const DESCRIPTION = '';

    public static function GenerateExcel($type,$params) {
		require_once dirname(__FILE__).'/PHPExcel/Classes/PHPExcel.php';
		require_once dirname(__FILE__).'/export_siom.php';

		$creator     = isset($params['creator'])?($params['creator']):(self::CREATOR); 
		$title 		 = isset($params['title'])?($params['title']):(self::TITLE); 
		$description = isset($params['description'])?($params['description']):(self::DESCRIPTION);
		$filename    = isset($params['filename'])?($params['filename']):("archivo.xlsx"); 

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator($creator)
									 ->setLastModifiedBy($creator)
									 ->setTitle($title)
									 ->setDescription($description);
		
		switch($type){
			case "SIOM_OS_PRESUPUESTO":{
				ExportSIOM::GenerateExcelPresupuesto($objPHPExcel,$params);
				break;
			}
			default:{
				$objPHPExcel->setActiveSheetIndex(0)
		                    ->setCellValue('A1', 'Tipo de documento no reconocido ($type)');
			}
		}
						 
		Export::_SendHeaders("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",$filename);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
    }


    public static function GeneratePDF($type,$params) {
    	require('lightncandy/lightncandy.php');
    	require_once("dompdf/dompdf_config.inc.php");

		switch($type){
			case "SIOM_OS_INFORME":{
				$cont_id = $params['data']['cont_id'];
				$orse_id = $params['data']['orse_id'];
				$info_id = $params['data']['info_id'];


				$url  = $_SERVER['HTTP_REFERER']."rest/contrato/$cont_id/os/informe/$orse_id/validar/$info_id";
				$data = Export::_GetDataFromRest($url);

				try{
					ini_set('display_errors', FALSE);
					$renderer = include("templates/os_informe.php");
					$html = $renderer(array("data"=>$data));
				} catch (Exception $e) {
				   $html = "<h1>Error generando informe</h1><h3>".$e->getMessage()."</h3>";
				}
				
				$dompdf = new DOMPDF();
			    $dompdf->load_html($html);
			    $dompdf->set_base_path(realpath(__DIR__ . '/..'));
			    $dompdf->set_paper("letter","portrait");
			    $dompdf->render();

				$filename = "os_".$orse_id."_informe_".$info_id.".pdf";

			    Export::_SendHeaders("application/pdf",$filename);
			    $dompdf->stream($filename);

				break;
			}
			case "SIOM_MNT_INFORME":{
				$cont_id = $params['data']['cont_id'];
				$mant_id = $params['data']['mant_id'];
				$info_id = $params['data']['info_id'];


				$url  = $_SERVER['HTTP_REFERER']."rest/contrato/$cont_id/mnt/informe/$mant_id/validar/$info_id";
				$data = Export::_GetDataFromRest($url);

				try{
					ini_set('display_errors', FALSE);
					$renderer = include("templates/mnt_informe.php");
					$html = $renderer(array("data"=>$data));
				} catch (Exception $e) {
				   $html = "<h1>Error generando informe</h1><h3>".$e->getMessage()."</h3>";
				}
				
				$dompdf = new DOMPDF();
			    $dompdf->load_html($html);
			    $dompdf->set_base_path(realpath(__DIR__ . '/..'));
			    $dompdf->set_paper("letter","portrait");
			    $dompdf->render();

				$filename = "mnt_".$mant_id."_informe_".$info_id.".pdf";

			    Export::_SendHeaders("application/pdf",$filename);
			    $dompdf->stream($filename);

				break;
			}
			default:{
				
			}
		}
					
    }

    private static function _GetDataFromRest($route){
    	$ch = curl_init($route); 
    	curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt( $ch, CURLOPT_COOKIE,session_id()); 
		$response = curl_exec($ch); 
		curl_close($ch);
		return json_decode($response,true);
    }


    private static function _SendHeaders($content_type,$filename){
    	header('Content-Type: '.$content_type);
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header('Set-Cookie: fileDownload=true; path=/');
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); 			// Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); 			// HTTP/1.1
		header ('Pragma: public');								// HTTP/1.0
    }
}
?>
