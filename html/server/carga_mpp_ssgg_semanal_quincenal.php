 <?php
include("db.php");

global $DEF_CONFIG;

//Cargar el primer día del mes en que empieza el período
$FECHA      = isset($argv[1])?$argv[1]:date("Y-m-d");

$FORM_INGRESO 		   = isset($DEF_CONFIG['mnt']['formIngreso'])?$DEF_CONFIG['mnt']['formIngreso']:1; //id de formulario de ingreso
$FORM_SALIDA           = isset($DEF_CONFIG['mnt']['formSalida'])?$DEF_CONFIG['mnt']['formSalida']:4; //id de formulario de salida
$ESTADO_CANCELADA      = isset($DEF_CONFIG['mnt']['estadoMPPCancelado'])?$DEF_CONFIG['mnt']['estadoMPPCancelado']:'NO REALIZADO'; //estado en que queda MPP al cancelar

echo 'FECHA: '.$FECHA."\n";
echo 'FORM_INGRESO: '.$FORM_INGRESO."\n";
echo 'FORM_SALIDA '.$FORM_SALIDA."\n";
echo 'ESTADO_CANCELADA '.$ESTADO_CANCELADA."\n";

//____________________________________________________________________________________
//Obtener período(s)
function ObtenerPeriodos($db,$fecha){
	echo 'Dentro de Funcion ObtenerPeriodos'."\n";
	echo 'fecha : '.$fecha."\n";
    $contracts = array();
    $res = $db->ExecuteQuery("SELECT
								cont_id,
								cont_nombre,
								DATE(cont_fecha_inicio) AS cont_fecha_inicio
							FROM contrato
							WHERE cont_estado='ACTIVO'
							AND cont_id =18");
    if($res['status']){
        if(0<$res['rows']){
            $contracts = $res['data'];
			print_r ($res['data']);
        }
        else{
            return array("status"=>false,"error"=>"No se han definido contratos");
        }
    }
    else{
        return $res;
    }


    $data = array("add"=>array());
    foreach($contracts as $c){
        $cont_id = $c['cont_id'];

        $periods = array();
        $res = $db->ExecuteQuery("SELECT 
									p.peri_id,
									p.peri_nombre,
									p.peri_meses,
									p.peri_dias,
									r.rcpe_id,
									r.rcpe_dias_apertura,
									r.rcpe_dias_post_cierre
								FROM
								periodicidad p
								INNER JOIN rel_contrato_periodicidad r ON (r.cont_id=$cont_id
																		AND	 p.peri_id = r.peri_id  
																		AND  r.rcpe_participa_mpp='1')
								WHERE p.peri_meses = 0");
        if($res['status']){
            if(0<$res['rows']){
                $periods = $res['data'];
            } else {
                continue;
            }
        } else {
            return $res;
        }

		$i = 0;
        foreach($periods AS $p){
			echo "FOR ==> $i \n";
			$fecha_inicio = $fecha;
			$fecha_inicio = new DateTime($fecha);
			$fecha_termino = clone $fecha_inicio;
			$fecha_termino->add(new DateInterval("P".$p['peri_dias']."D"));

			echo 'fecha_inicio ' .$fecha_inicio->format('Y-m-d')."\n";
			echo 'fecha_termino '.$fecha_termino->format('Y-m-d')."\n";
			array_push($data['add'],array("cont_id"=>$cont_id,
											"peri_id"=>$p['peri_id'],
											"peri_nombre"=>$p['peri_nombre'],
											"peri_mes"=>$p['peri_meses'],
											"peri_dias"=>$p['peri_dias'],
											"rcpe_id"=>$p['rcpe_id'],
											"rcpe_dias_apertura"=>$p['rcpe_dias_apertura'],
											"rcpe_dias_post_cierre"=>$p['rcpe_dias_post_cierre'],									
											"fecha_inicio"=>$fecha_inicio->format('Y-m-d'),
											"fecha_termino"=>$fecha_termino->format('Y-m-d')));
			$i++;
        }
    }
    return array("status"=>true,"data"=>$data);
}

//____________________________________________________________________________________
//Cargar MPPs
function CargarMPPs($db,$cont_id,$peri_id,$peri_mes,$fecha_inicio,$fecha_termino,$fecha_programada,$form_ingreso,$form_salida,$rcpe_id,$rcpe_dias_apertura,$rcpe_dias_post_cierre){
	echo 'Dentro del Metodo CargarMPPs'."\n";
	echo 'cont_id'.$cont_id."\n";
	echo 'peri_id'.$peri_id."\n";
	echo 'peri_mes'.$peri_mes."\n";
	echo 'fecha_inicio'.$fecha_inicio."\n";
	echo 'fecha_termino'.$fecha_termino."\n";
	echo "fecha_programada".$fecha_programada->format('Y-m-d')."\n";
	echo 'form_ingreso'.$form_ingreso."\n";
	echo 'form_salida'.$form_salida."\n";
	echo 'rcpe_id'.$rcpe_id."\n";
	echo 'rcpe_dias_apertura'.$rcpe_dias_apertura."\n";
	echo 'rcpe_dias_post_cierre'.$rcpe_dias_post_cierre."\n";
	
    //Cargar lista de empresas
    $empresas = array();
    $res = $db->ExecuteQuery("SELECT
                                    zona_id,
                                    empr_id
                                FROM rel_contrato_empresa
                                WHERE cont_id=$cont_id 
								AND coem_tipo='CONTRATISTA' 
								AND coem_participa_mpp=1");
    if($res['status']){
        if(0<$res['rows']){
            foreach($res['data'] as $row){
                if($row['zona_id']==NULL || $row['zona_id']==""){
                    $empresas["%"] = $row['empr_id'];
                }
                else{
                    $empresas[$row['zona_id']] = $row['empr_id'];
                }
            }
        }
        else{
            return array("status"=>false,"error"=>"No se han definido empresa contratistas participantes de MPP en contrato $cont_id");
        }
    }
    else{
        return $res;
    }



    //formularios asociados
    $forms = array();
    $res = $db->ExecuteQuery("	SELECT clpr_id
										,espe_id
										,form_id
                                FROM mantenimiento_definicion_formulario 
								WHERE cont_id='$cont_id' 
								AND peri_id='$peri_id' 
								AND peri_mes='$peri_mes'
								AND madf_estado='ACTIVO'");
    if($res['status']){
        if(0<$res['rows']){
            foreach($res['data'] as $row){
                if(!isset($forms[$row['clpr_id']][$row['espe_id']])){
                    $forms[$row['clpr_id']][$row['espe_id']] = array();
                }
                array_push($forms[$row['clpr_id']][$row['espe_id']],$row['form_id']);
            }
        }
        else{
            return array("status"=>false,"error"=>"Periodo $peri_id sin formulario definidos");
        }
    }
    else{
        return $res;
    }

    //emplazamientos para el período
    $emplazamientos = array();
    $clpr_ids = implode("','",array_keys($forms));
	
	$query_emplazamiento = "SELECT emplazamiento.empl_id
									,clas_id
									,rel_contrato_emplazamiento.clpr_id
									,empl_nombre
									,zona.zona_id
							FROM emplazamiento
							INNER JOIN rel_contrato_emplazamiento ON (rel_contrato_emplazamiento.empl_id=emplazamiento.empl_id)
							INNER JOIN clasificacion_programacion ON (clasificacion_programacion.clpr_id=rel_contrato_emplazamiento.clpr_id 
																	AND clasificacion_programacion.clpr_activo='ACTIVO')
							INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.empl_id=emplazamiento.empl_id)
							INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id 
											AND zona.cont_id =rel_contrato_emplazamiento.cont_id
											AND zona.zona_tipo='CONTRATO')
							WHERE emplazamiento.empl_estado='ACTIVO' 
							AND rel_contrato_emplazamiento.cont_id=$cont_id 
							AND rel_contrato_emplazamiento.rece_mpp=1  
							AND rel_contrato_emplazamiento.clpr_id IN ('$clpr_ids')  
                            ";
							
	echo "query emplazamiento ==> " .$query_emplazamiento. "\n";
	$res = $db->ExecuteQuery($query_emplazamiento);
    if($res['status']){
        if(0<$res['rows']){
            $emplazamientos	= $res['data'];
        }
        else{
            return array("status"=>false,"error"=>"Sin emplazamientos definidos");
        }
    }
    else{
        return $res;
    }

	echo "***********Generando Insert de mantenimiento_periodos************* \n";
/* SACAR ESTE COMENTARIO */
    //crear mantenimiento período..
    $res = $db->ExecuteQuery("INSERT INTO mantenimiento_periodos SET
                             rcpe_id='$rcpe_id',
                             mape_fecha_pre_apertura = DATE_ADD('$fecha_inicio', INTERVAL - $rcpe_dias_apertura DAY),
                             mape_fecha_inicio = '$fecha_inicio',
                             mape_fecha_cierre = '$fecha_termino',
                             mape_fecha_post_cierre = DATE_ADD('$fecha_termino', INTERVAL $rcpe_dias_post_cierre DAY),
                             mape_estado = 'PROCESANDO'");
    if(!$res['status']){
        return $res;
    }
    $mape_id = $res['data'][0]['id'];


    //Generando MPPs
    $mpps_creados=0;
	echo 'Generando MPPs'."\n";
    foreach ($emplazamientos as $emplazamiento) {
		
        $empl_nombre 	= $emplazamiento['empl_nombre'];
        $empl_id 		= $emplazamiento['empl_id'];
        $clas_id 		= $emplazamiento['clas_id'];
        $clpr_id 		= $emplazamiento['clpr_id'];
        $zona_id 		= $emplazamiento['zona_id'];
        $empr_id 		= NULL;

        if(isset($empresas[$zona_id])){
            $empr_id = $empresas[$zona_id];
        } else{
            $empr_id = $empresas["%"];
        }

        if($empr_id==NULL){
            continue;
        }

		echo 'empl_nombre : '.$empl_nombre."\n";
		echo 'empl_id : '.$empl_id."\n";
        echo 'clas_id : '.$clas_id."\n";
        echo 'clpr_id : '.$clpr_id."\n";
        echo 'zona_id : '.$zona_id."\n";
        echo 'empr_id : '.$empr_id."\n";
        foreach($forms[$clpr_id] as $espe_id => $f){

/*
			//Obtener ultima fecha de realización o fecha programada
			$query_utlfecha = "SELECT  IF(mant_fecha_ejecucion,DATEDIFF(DATE(mant_fecha_ejecucion),mape_fecha_inicio),DATEDIFF(DATE(mant_fecha_programada),mape_fecha_inicio)) AS days
									FROM mantenimiento
									INNER JOIN mantenimiento_periodos ON (mantenimiento.mape_id=mantenimiento_periodos.mape_id)
									WHERE mantenimiento.empl_id='$empl_id' 
									AND	mantenimiento.espe_id='$espe_id' 
									AND	mantenimiento_periodos.rcpe_id='$rcpe_id'
									ORDER BY mantenimiento.mape_id DESC LIMIT 1";
			#echo "query ultfecha=> " .$query_utlfecha. "\n";						
            $res = $db->ExecuteQuery($query_utlfecha);
            if(!$res['status']){
                return $res;
            }
            if(0<$res['rows']){
                $days = $res['data'][0]['days'];

                $fi = new DateTime($fecha_inicio);
                $ft = new DateTime($fecha_termino);

                $fi->add(new DateInterval("P".$days."D"));
                if($fi < $ft){
                    $fecha_programada = $fi->format('Y-m-d');
                }
                else{
                    $fecha_programada = $ft->format('Y-m-d');
                }
            }
*/

	echo "************Generando Insert de mantenimiento******************* \n";
/* SACAR ESTE COMENTARIO*/

            //crear mantenimiento..
			$fecha_programada_st = $fecha_programada->format('Y-m-d');
            $res = $db->ExecuteQuery("INSERT INTO mantenimiento SET
                                     empr_id='$empr_id',
                                     cont_id='$cont_id',
                                     mape_id='$mape_id',
                                     empl_id='$empl_id',
                                     espe_id='$espe_id',
                                     mant_fecha_programada='$fecha_programada_st 00:00:00',
                                     clas_id='$clas_id',
                                     clpr_id='$clpr_id'");
            if(!$res['status']){
                return $res;
            }
            $mant_id = $res['data'][0]['id'];

            $values = "($mant_id,$form_ingreso),";
            foreach ($f as $form_id) {
                $values .= "($mant_id,$form_id),";
            }
            $values .= "($mant_id,$form_salida)";

            //crear mantemineito formularios..
            $res = $db->ExecuteQuery("INSERT INTO rel_mantenimiento_formulario (mant_id,form_id) VALUES $values");
            if(!$res['status']){
                return $res;
            }			

            $mpps_creados++;
        }
    }

    return array("status"=>true,"mpps"=>$mpps_creados);
}

//____________________________________________________________________________________
//Cancelar MPPs
function CancelarMPPs($db,$cont_id,$peri_id,$fecha_inicio, $rcpe_id, $estado){
	echo 'Dentro del Metodo CancelarMPPs'."\n";
	echo 'cont_id : '.$cont_id."\n";
	echo 'peri_id : '.$peri_id."\n";
	echo 'fecha_inicio : '.$fecha_inicio."\n";
	echo 'estado : '.$estado."\n";
	echo 'rcpe_id : '.$rcpe_id."\n";
	
	
    //Buscar mantenimiento_periodo
    $mape_ids=array();
    $res = $db->ExecuteQuery("SELECT mape_id
                              FROM mantenimiento_periodos
                              WHERE rcpe_id='$rcpe_id'
							  AND mape_fecha_post_cierre < '$fecha_inicio'
							");
    if($res['status']){
        if(0<$res['rows']){
            foreach($res['data'] as $row){
                array_push($mape_ids,$row['mape_id']);
            }
        } else {
            return array("status"=>false,"error"=>"No se ha encontrado mantenimiento_periodo");
        }
    } else {
        return $res;
    }

    foreach ($mape_ids as $mape_id) {
		echo 'Dentro del for mape_id : '.$mape_id."\n";
		
		echo "Actualizando tabla mantenimiento_periodos \n";
        $res = $db->ExecuteQuery("UPDATE mantenimiento_periodos SET mape_estado='CERRADO' WHERE mape_id='$mape_id'");
        if(!$res['status']){
            return $res;
        }
		
		echo "Actualizando tabla tarea \n";
		$res = $db->ExecuteQuery("UPDATE tarea t
									SET t.tare_estado='ANULADA'
                                WHERE t.tare_modulo = 'MNT'
								AND t.tare_id_relacionado IN (SELECT mnt.mant_id 
																FROM mantenimiento mnt
																WHERE mnt.mape_id='$mape_id' 
																AND mnt.mant_estado NOT IN ('APROBADA','RECHAZADA','ANULADA','NO REALIZADO')
																AND mnt.mant_id NOT IN (
																		SELECT info.info_id_relacionado 
																		FROM informe info 
																		WHERE info.info_modulo='MNT' 
																		AND info.info_id_relacionado = mnt.mant_id
																		AND info.info_estado in ('SINVALIDAR', 'PREAPROBADO')
																	)
															)
								AND tare_estado NOT IN ('CREADA','DESPACHADA')
		");
		if(!$res['status']){
            return $res;
        }

		echo "Actualizando tabla mantenimiento \n";
        $res = $db->ExecuteQuery("UPDATE mantenimiento mnt
                                  SET mnt.mant_estado='$estado'
                                  WHERE mnt.mape_id='$mape_id' 
								  AND mnt.mant_estado NOT IN ('APROBADA','RECHAZADA','ANULADA','NO REALIZADO')
								  AND mnt.mant_id NOT IN (select info.info_id_relacionado from informe info where info.info_modulo='MNT' 
																AND info.info_id_relacionado = mnt.mant_id
																AND info.info_estado in ('SINVALIDAR', 'PREAPROBADO')
														 )
		");
        if(!$res['status']){
            return $res;
        }
    }
    return array("status"=>true);
}

//____________________________________________________________________________________
//MAIN
	echo 'MAIN \n';
	$db = new MySQL_Database();
	$res = ObtenerPeriodos($db,$FECHA);
	if($res['status']){
		$db->startTransaction();

		$mpp_total = 0;
		$periodos_a_cargar   = $res['data']['add'];
		//$periodos_a_cancelar = $res['data']['del'];

		foreach ($periodos_a_cargar as $periodo) {
			$rcpe_id= $periodo['rcpe_id'];
			$fecha_inicio = $periodo['fecha_inicio'];
			
			//Obtener ultima fecha de realización o fecha programada
			$query_utlfecha =   "SELECT 
								IF('$fecha_inicio'> mape_fecha_post_cierre, 1,0) cierre
								FROM  mantenimiento_periodos 
								WHERE rcpe_id=$rcpe_id
								AND mape_estado = 'PROCESANDO'
								ORDER BY mape_id DESC LIMIT 1";
			echo "query ultfecha=> " .$query_utlfecha. "\n";						
            $res = $db->ExecuteQuery($query_utlfecha);
			if(!$res['status']){
                return $res;
            }
			
			#Si cierre = 1; corresponde cancelar y hacer carga si cierre=0 no se hace nada cierre=2 no hay datos cargados
			$cierre=2;
			if (0<$res['rows']) {
				$cierre= $res['data'][0]['cierre'];
			}
			if (0==$cierre){
				echo "No corresponde realizar carga para periodo ==>".$periodo['peri_nombre']."\n";
			}
			else {
			
				echo "Cancelando período ".$periodo['peri_nombre']."\n";
				$res = CancelarMPPs($db, 
								  $periodo['cont_id'],
								  $periodo['peri_id'],
								  $periodo['fecha_inicio'],
								  $periodo['rcpe_id'],
								  $ESTADO_CANCELADA);
				if(!$res['status']){
					echo $res['error']."\n";
				}



				echo "Cargando período ".$periodo['peri_nombre']." (".$periodo['peri_mes'].") a iniciar en ".$periodo['fecha_inicio']." al ".$periodo['fecha_termino']."\n";

				$fecha_inicio = new DateTime($periodo['fecha_inicio']);
				$fecha_programada = clone $fecha_inicio;
				
				echo "fecha_inicio : ".$fecha_inicio->format('Y-m-d')."\n";
				echo "fecha_programada : ".$fecha_programada->format('Y-m-d')."\n";
				$res = CargarMPPs($db,
								  $periodo['cont_id'],
								  $periodo['peri_id'],
								  $periodo['peri_mes'],
								  $periodo['fecha_inicio'],
								  $periodo['fecha_termino'],
								  $fecha_programada,
								  $FORM_INGRESO,
								  $FORM_SALIDA,
								  $periodo['rcpe_id'],
								  $periodo['rcpe_dias_apertura'],
								  $periodo['rcpe_dias_post_cierre']);
				if(!$res['status']){
					//$db->Rollback();
					//$db->Disconnect();
					echo $res['error']."\n";
					//echo "Advertencia: No se han cargado ni cancelado MPPs\n\n";
					//exit(1);
				} else{
					$mpp = isset($res['mpps'])?$res['mpps']:0;
					echo $mpp." MPPs cargados\n\n";
					$mpp_total += intval($mpp);
				}
			}
		}


		$db->Commit();//$db->Rollback();
		echo $mpp_total." MPPs cargados en total\n\n";
	}
	else{
		echo $res['error'];
	}

	$db->Disconnect();
	exit(0);
?>
