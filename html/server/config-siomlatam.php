<?php

//________________________________________________________________
//Configuración DB
$DB_HOST = 'localhost';
$DB_USER = 'siom2latam';
$DB_PASS = 'latam.siom3';
$DB_NAME = 'siom2latam';

//________________________________________________________________
//Directorio servicio
$BASEDIR = "/srv/siomlatam.movistar.cl/";

//________________________________________________________________
//Configuración  Google
$MAPS_API_KEY = "AIzaSyDgIW6FV0U1KopabHOYOdsUPqgQoL0yqQU";
$PUSH_API_KEY = "AIzaSyCyCS3-ZVsDEza5VtfwHBsi_nEixsit4es";
$PUSH_URL     = "https://android.googleapis.com/gcm/send";

//________________________________________________________________
//Configuración aplicación
$DEF_CONFIG = array(
	"app_timezone" => "-03:00",
    "geofence_enabled" => false,
    "max_file_upload" => 20971520, //20 M
	"os"=>array(
    		"colorStatus" => array(
    			"OSGU"=>array("warning_offset"=>3), //en horas
    			"OSGN"=>array("warning_offset"=>24),
    			"OSEU"=>array("warning_offset"=>3),
    			"OSEN"=>array("warning_offset"=>24),
    			"OSI" =>array("warning_offset"=>24)
    		),
    		"minDescription"=>0,//10,
            "formularios"=>array(
                "1"=>"Ingreso a sitio",
                "2"=>"Situación encontrada",
                "3"=>"Subida de servicio",
                "4"=>"Salida de sitio",
                ),
			"formulario_ingreso_id" => 1,
			"formulario_salida_id" => 4			
    	),
    "mnt"=>array(
            "minDescription"=>0,
            "formIngreso"=>1,
            "formSalida"=>4,
            "diasFechaProgramada"=>15,
            "estadoMPPCancelado"=>"NO REALIZADO"
        ),
    "insp"=>array(
            "subeReparos"=>42,
            "subeRechazada"=>43,
            "formIngreso"=>1,
            "formSalida"=>4,
        ),
    "sla"=>array(
            "idFormularioSubidaServicio"=>3
        )
);

//________________________________________________________________
//Configuracion de log
$LOGS_CONFIG = array(
    'rootLogger' => array(
        'level' => "ERROR",
        'appenders' => array('default'),
    ),
    'appenders' => array(
        'default' => array(
            'class' => 'LoggerAppenderDailyFile',
            'layout' => array(
                'class' => 'LoggerLayoutSimple'
            ),
            'params' => array(
                'file' => $BASEDIR.'logs/siom_%s.log',
                'datePattern' => 'Ymd'
            )
        )
    )
);

?>