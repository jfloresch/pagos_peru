<?php

//________________________________________________________________
//Configuración DB
$DB_HOST = 'siom.cebramjynokd.us-west-2.rds.amazonaws.com';
$DB_USER = 'SiomMaster';
$DB_PASS = 'Pass.784.OM';
$DB_NAME = 'SIOM2.7-prod';

//________________________________________________________________
//Directorio servicio
$BASEDIR = "/srv/testing.siomred.cl/";

//________________________________________________________________
//Configuración Google
$MAPS_API_KEY = "AIzaSyDCP_5NyMrEdKOlsuVW69YrPr2ZmQMz7FY";
$PUSH_API_KEY = "AIzaSyDRXU7VKXl6kkd9PuaUOi3931bX5Wt-K8A";
$PUSH_URL     = "https://android.googleapis.com/gcm/send";

//________________________________________________________________
//Configuración aplicación
$DEF_CONFIG = array(
	"app_timezone" => "-03:00",
    "geofence_enabled" => false,
    "max_file_upload" => 20971520, //20 M
	"os"=>array(
    		"colorStatus" => array(
    			"OSGU"=>array("warning_offset"=>3), //en horas
    			"OSGN"=>array("warning_offset"=>24),
    			"OSEU"=>array("warning_offset"=>3),
    			"OSEN"=>array("warning_offset"=>24),
    			"OSI" =>array("warning_offset"=>24)
    		),
    		"minDescription"=>0,//10,
            "formularios"=>array(
                "1"=>"Ingreso a sitio",
                "2"=>"Situación encontrada",
                "3"=>"Subida de servicio",
                "4"=>"Salida de sitio",
                ),
			"formulario_ingreso_id" => 1,
			"formulario_salida_id" => 4	
    	),
    "mnt"=>array(
            "minDescription"=>0,
            "formIngreso"=>1,
            "formSalida"=>4,
            "diasFechaProgramada"=>15,
            "estadoMPPCancelado"=>"NO REALIZADO"
        ),
    "insp"=>array(
            "subeReparos"=>42,
            "subeRechazada"=>43,
            "formIngreso"=>1,
            "formSalida"=>4,
        ),
    "sla"=>array(
            "idFormularioSubidaServicio"=>3
        )
);

//________________________________________________________________
//Configuracion de log
$LOGS_CONFIG = array(
    'rootLogger' => array(
        //'level' => "ERROR",
        'appenders' => array('default'),
    ),
    'appenders' => array(
        'default' => array(
            'class' => 'LoggerAppenderDailyFile',
            'layout' => array(
                'class' => 'LoggerLayoutPattern',
                'params' => array(
                    'conversionPattern' => '%date{d.m.Y H:i:s,u} [%-5level] %msg%n'
                )
            ),
            'params' => array(
                'file' => $BASEDIR.'logs/siom_%s.log',
                'datePattern' => 'Ymd'
            )
        )
    )
);


?>