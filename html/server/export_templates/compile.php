<?php
require('../../../libs/lightncandy/lightncandy.php');

if(count($argv)<2){
	echo "Debe indicar template\n";
	exit(0);
}

$template = file_get_contents($argv[1]);
$filename = basename($argv[1],".hb");


$phpTempl = LightnCandy::compile($template, 
			Array(
    			'flags' => LightnCandy::FLAG_HANDLEBARSJS|LightnCandy::FLAG_SPVARS|LightnCandy::FLAG_ERROR_EXCEPTION|LightnCandy::FLAG_PARENT,
    			'helpers' => Array(
    				'empty' => function ($arg1) {
						    		if(is_array($arg1)){
						    			$arg1 = $arg1[0];
						    		}
						    		if($arg1==""){
						    			return "N/D";
						    		}
						            return $arg1;
						       	},
					'boolean' => function ($arg1) {
						    		if(is_array($arg1)){
						    			$arg1 = $arg1[0];
						    		}
						    		if($arg1==""){
						    			return "N/D";
						    		}
						            return ($arg1=="0")?("NO"):("SI");
						       	},
					'uppercase'  => function ($arg1) {
						    		if(is_array($arg1)){
						    			$arg1 = $arg1[0];
						    		}
						            return mb_strtoupper($arg1);
						         },
					'aggregatorLabel' => function($arg){
									$opciones = $arg[0];
									$indice   = $arg[1];

									$label = "";
							        if(isset($opciones) && isset($opciones['labels']) && $indice < count($opciones['labels'])){
							        	$label = $opciones['labels'][$indice];
							        }
							        return $label;
								}
				),
				'hbhelpers' => Array(
					'compare' =>  function ($arg1,$arg2, $options){
									if ($arg1==$arg2) {
						                return $options['fn']();
						            } else {
						                return $options['inverse']();
						            }
								  },
					'isEven' =>  function ($arg,$options){
									if ($arg % 2 == 0) {
						                return $options['fn']();
						            }
								  },
					'isOdd' =>  function ($arg,$options){
									if ($arg % 2 != 0) {
						                return $options['fn']();
						            }
								  },
				    'notAggregated' =>  function ($arg,$options){
									if (!isset($arg['aggregated']) || $arg['aggregated']==false) {
						                return $options['fn']();
						            } else if(isset($options['inverse'])){
						                return $options['inverse']();
						            }
								  },
					'isImage' =>  function ($arg,$options){
									if ($arg && is_array($arg)) {
						                return $options['fn']();
						            } else {
						                return $options['inverse']();
						            }
								  }
    			)
   	 		)
);

file_put_contents($filename.".php",$phpTempl);
?>
