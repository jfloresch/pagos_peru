<?php

$FORMULARIO_MEDICION= 194;
$ITEM_RESP_MEDICION=11821;
$USUARIO_VALIDADOR_PEX= 2123;//USER : "syspex"

function ObtenerResponsablesOS($db,$orse_id){
   $res = $db->ExecuteQuery("SELECT
                               orden_servicio.usua_creador AS creador,
                               orden_servicio_asignacion.usua_creador AS despachador
                             FROM 
                               orden_servicio
                             LEFT JOIN orden_servicio_asignacion ON (orden_servicio.orse_id=orden_servicio_asignacion.orse_id)
                             WHERE
                                 orden_servicio.orse_id=$orse_id
                             ORDER BY oras_fecha_asignacion DESC
                             LIMIT 1");
   if($res['status']==0){
      return $res;
   }

   $responsables = array("creador"=>$res['data'][0]['creador'],
                         "despachador"=>$res['data'][0]['despachador'],
                         "gestores"=>array(),
                         "despachadores"=>array());

   $res = $db->ExecuteQuery("SELECT DISTINCT
                               usuario.usua_id,
                               perf_nombre
                             FROM 
                               orden_servicio
                             INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.empl_id=orden_servicio.empl_id)
                             INNER JOIN rel_contrato_usuario_responsabilidad ON (rel_contrato_usuario_responsabilidad.zona_id=rel_zona_emplazamiento.zona_id)
                             INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.recu_id=rel_contrato_usuario_responsabilidad.recu_id)
                             INNER JOIN usuario ON (usuario.usua_id=rel_contrato_usuario.usua_id  AND usuario.usua_estado='ACTIVO')
                             INNER JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id)
                             INNER JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id AND (perfil.perf_nombre='GESTOR' OR (perfil.perf_nombre='DESPACHADOR' AND usuario.empr_id=orden_servicio.empr_id)))
                             WHERE
                                 orse_id=$orse_id");
   if($res['status']==0){
      return $res;
   }

   foreach($res['data'] as $row) {
      if($row['perf_nombre']=="GESTOR"){
         if($row['usua_id'] != $responsables["creador"]){
            $responsables["gestores"][] = $row['usua_id'];
         }
      }
      else if($row['perf_nombre']=="DESPACHADOR"){
         if($row['usua_id'] != $responsables["despachador"]){
            $responsables["despachadores"][] = $row['usua_id'];
         }
      } 
   }

   return array("status"=>1,"data"=>$responsables);
}


function ObtenerInformacionOS($db,$orse_id){
  $res = $db->ExecuteQuery("SELECT 
                                    os.orse_id,
                                    os.orse_tipo,
                                    os.orse_descripcion,
                                    esp.espe_nombre, 
                                    sesp.sube_nombre, 
                                    emp.empl_nombre, 
                                    emp.empl_nemonico,
                                    emp.empl_direccion,
                                    e.empr_nombre, 
                                    c.comu_nombre, 
                                    r.regi_nombre,
                                    u.usua_nombre
                            FROM 
                                    orden_servicio os
                                    , empresa e
                                    , subespecialidad sesp
                                    , especialidad esp
                                    , emplazamiento emp
                                    , comuna c
                                    , region r
                                    , provincia p
                                    , usuario u
                            WHERE
                                    os.orse_id = $orse_id 
                                    AND os.empr_id=e.empr_id
                                    AND os.empl_id=emp.empl_id
                                    AND os.sube_id=sesp.sube_id
                                    AND sesp.espe_id=esp.espe_id
                                    AND emp.comu_id=c.comu_id
                                    AND p.prov_id = c.prov_id
                                    AND r.regi_id = p.regi_id
                                    AND u.usua_id = os.usua_creador");
  if($res['status'] == 0) {
      Loggear("Error al obtener información de OS: ".$res['error'],LOG_ERR);
      return "";
  }

  return "            
   Nº OS: <b>$orse_id</b><br>
   Empresa: <b>".$res['data'][0]['empr_nombre']."</b><br>
   Tipo de OS: <b>".$res['data'][0]['orse_tipo']."</b><br>
   Alarma: <b>".$res['data'][0]['sube_nombre']."</b><br>
   Especialidad: <b>".$res['data'][0]['espe_nombre']."</b><br>
   Descripci&oacute;n: <b>".$res['data'][0]['orse_descripcion']."</b><br>
   Creador: <b>".$res['data'][0]['usua_nombre']."</b><br><br>

   Emplazamiento: <b>".$res['data'][0]['empl_nombre']."</b><br>
   Nem&oacute;nico: <b>".$res['data'][0]['empl_nemonico']."</b><br>
   Direcci&oacute;n: <b>".$res['data'][0]['empl_direccion']."</b><br>
   Comuna: <b>".$res['data'][0]['comu_nombre']."</b><br>
   Región: <b>".$res['data'][0]['regi_nombre']."</b>";
}


function OSAJustarResponsable($db,$orse_id){
  $res = $db->ExecuteQuery("SELECT
                                  tare_tipo
                              FROM
                                  tarea
                              WHERE 
                              tare_modulo='OS' AND tare_estado IN ('CREADA','DESPACHADA') 
                              AND tare_id_relacionado = $orse_id
                              ");
  if($res['status'] == 0) {
      Loggear("Error al tareas pendientes de OS: ".$res['error'],LOG_ERR);
  }

  $orse_responsable = "CONTRATISTA";
  $tareas_movistar  = array("VALIDAR_INFORME", "VALIDAR_OS", "VALIDAR_PRESUPUESTO", "VALIDAR_SOLICITUD_CAMBIO","VALIDAR_SOLICITUD_INFORME");
  foreach ($res['data'] AS $row) {
      if(in_array($row['tare_tipo'],$tareas_movistar)){
          $orse_responsable = "MOVISTAR";
          break;
      }
  }

  $res = $db->ExecuteQuery("UPDATE orden_servicio SET orse_responsable='$orse_responsable' WHERE orse_id=$orse_id");
  if($res['status'] == 0) {
      Loggear("Error al actualizar responsable de OS: ".$res['error'],LOG_ERR);
  }
}

//________________________________________________________________________________
//________________________________________________________________________________
function OSCreada($db,$responsables,$orse_id){
   
   if($responsables['despachadores']==0){  		
		//EnviarNotificacion($db,$usua_creador,"SYSTEM","SIN_DESPACHADOR",0,){
		return array("status"=>false,"error"=>"No se ha asignado un usuario despachador para OS $orse_id");
	}

	$res = EnviarTarea($db,$responsables['despachadores'],'OS','ASIGNAR',$orse_id);
	if($res['status']==0){
		return $res;
	}

   //Actualizar orden de servicio
	return $db->ExecuteQuery("UPDATE orden_servicio SET orse_estado='ASIGNANDO' WHERE orse_id=$orse_id");
}


function OSAnulada($db,$responsables,$orse_id){
  $usua_asignado = 0;
  $res = $db->ExecuteQuery("SELECT DISTINCT
                             usua_id
                            FROM 
                            orden_servicio_asignacion
                            INNER JOIN rel_orden_servicio_asignacion_usuario ON (orden_servicio_asignacion.oras_id=rel_orden_servicio_asignacion_usuario.oras_id)
                            WHERE
                            orse_id=$orse_id AND roau_tipo='JEFECUADRILLA' AND oras_estado='ACTIVO'");
  if($res['status']==0){
      return $res;
   }

  if(0<$res['rows']){
    $usua_asignado = $res['data'][0]['usua_id'];
    return EnviarPush($db,$usua_asignado,"UPDATE_ASSIGNMENT","Se ha anulado OS Nº ".$orse_id);
  }
  return array("status"=>1);
}



function OSAsignada($db,$responsables,$orse_id,$oras_id){

	//Obtener usuario asignado
	$usua_asignado = 0;
	$res = $db->ExecuteQuery("SELECT 
								usua_id 
							   FROM rel_orden_servicio_asignacion_usuario 
							   WHERE oras_id=$oras_id AND roau_tipo='JEFECUADRILLA'");
	if($res['status']==0){
   		return $res;
   }

	if($res['rows']==0){
		return array("status"=>false,"error"=>"No se ha asignado un usuario jefe de cuadrilla para OS $orse_id");
	}
	$usua_asignado = $res['data'][0]['usua_id'];

	$res = EnviarTarea($db,$usua_asignado,'OS','VISITAR_SITIO',$orse_id);
	if($res['status']==0){
		return $res;
	}

	//actualizar estado
	$res = $db->ExecuteQuery("UPDATE orden_servicio SET orse_estado='ASIGNADA' WHERE orse_id=$orse_id");
	if($res['status']==0){
		return $res;
	}

	return EnviarPush($db,$usua_asignado,"UPDATE_ASSIGNMENT","Se le ha asignado OS Nº ".$orse_id);
}

function OSAsignacionCancelada($db,$responsables,$orse_id,$oras_id){
  //Obtener usuario asignado
  $usua_asignado = 0;
  $res = $db->ExecuteQuery("SELECT 
                usua_id 
                 FROM rel_orden_servicio_asignacion_usuario 
                 WHERE oras_id=$oras_id AND roau_tipo='JEFECUADRILLA'");
  if($res['status']==0){
      return $res;
   }

  if($res['rows']==0){
    return array("status"=>false,"error"=>"No se ha asignado un usuario jefe de cuadrilla para OS $orse_id");
  }
  $usua_asignado = $res['data'][0]['usua_id'];


  //cancelar tarea
  $res = $db->ExecuteQuery("UPDATE tarea SET tare_estado='CANCELADA' 
                            WHERE tare_modulo='OS' AND tare_tipo='VISITAR_SITIO' AND tare_id_relacionado='$orse_id' AND usua_id='$usua_asignado'");
  if($res['status']==0){
    return $res;
  }

  return EnviarPush($db,$usua_asignado,"UPDATE_ASSIGNMENT","Se ha cancelado su asignación a OS Nº ".$orse_id);
}

function OSPresupuestoAgregado($db,$responsables,$orse_id,$pres_id){
	include_once("export.php");

	//Generar excel 
	$res = ExcelOSPresupuesto($db,$orse_id,$pres_id);
	if(!$res['status']){
		return $res;
	}

	$res = EnviarTarea($db,$responsables['creador'],'OS','VALIDAR_PRESUPUESTO',$orse_id,'{\"pres_id\":'.$pres_id.'}');
	if($res['status']==0){
		return $res;
	}

	return $res;
}


function OSPresupuestoValidado($db,$responsables,$orse_id,$pres_id,$pres_estado){
  if($pres_estado=="PREAPROBADO"){
      $res = $db->ExecuteQuery("SELECT
                                    info_id
                                FROM
                                    informe
                                WHERE info_modulo='OS' AND info_id_relacionado=$orse_id AND info_estado='PREAPROBADO'");
    if(!$res['status']){
        return $res;
    }
    if(0<$res['rows']){
      $res = EnviarTarea($db,$responsables['creador'],'OS','VALIDAR_OS',$orse_id);
      return $res;
    }
  }

	return array("status"=>1);
}

function OSVisitaEjecutando($db,$responsables,$orse_id){
	$res = $db->ExecuteQuery("INSERT INTO emplazamiento_visita SET 
                              empl_id = (SELECT empl_id FROM orden_servicio WHERE orden_servicio.orse_id=$orse_id),
                              emvi_modulo = 'OS',
                              emvi_id_relacionado = $orse_id,
                              emvi_fecha_ingreso = NOW(),
                              emvi_estado = 'ACTIVO'");
  if($res['status']==0){
    return $res;
  }

  return array("status"=>1);
}

function OSInformeAgregado($db,$responsables,$orse_id,$info_id){
	include_once("export.php");

  $res = $db->ExecuteQuery("UPDATE emplazamiento_visita SET 
                            emvi_fecha_salida = NOW(),
                            emvi_estado = 'NOACTIVO'
                            WHERE emvi_modulo = 'OS' AND emvi_id_relacionado = $orse_id");
  if($res['status']==0){
    return $res;
  }

	//Generar informe en pdf 
	$res = PDFOSInforme($db,$orse_id,$info_id);
	if(!$res['status']){
		return $res;
	}

    //
    $res = $db->ExecuteQuery("SELECT v.fova_valor
                                FROM 
                                tarea t
                                , rel_tarea_formulario_respuesta rtfr 
                                , formulario_respuesta r
                                , formulario_valor v
                                WHERE t.tare_id_relacionado = $orse_id
                                AND t.tare_modulo = 'OS'
                                AND t.tare_id = rtfr.tare_id
                                AND r.fore_id = rtfr.fore_id
                                AND r.form_id = $FORMULARIO_MEDICION
                                AND v.fore_id =r.fore_id
                                AND v.foit_id =$ITEM_RESP_MEDICION
                               ;");
    $valor_respuesta = "NO";
    if($res['status']==0){
        if($res['rows']>0){
             $valor_respuesta = $res['data'][0]['fova_valor'];
        }
    }
    if($valor_respuesta == "NO"){
        $res = $db->ExecuteQuery("UPDATE informe
                                    SET info_estado = 'APROBADO'
                                    , info_observacion = ' Aprobado automaticamente'
                                    , usua_validador= $USUARIO_VALIDADOR_PEX
                                    , info_fecha_validacion = now()
                                    WHERE info_id_relacionado =$orse_id
                                    AND info_modulo = 'OS';");
        if($res['status']==0){
            return $res;
        }

        $res = $db->ExecuteQuery("UPDATE orden_servicio
                                    SET orse_estado = 'APROBADA'
                                    , orse_comentario = ' Aprobado automaticamente'
                                    , usua_validador= $USUARIO_VALIDADOR_PEX
                                    , orse_fecha_validacion = now()
                                    WHERE orse_id =$orse_id;");
        if($res['status']==0){
            return $res;
        }

        $res = $db->ExecuteQuery("INSERT INTO evento SET 
                                      even_modulo='OS',
                                      even_evento='OS_VALIDADA',
                                      even_id_relacionado='$orse_id',
                                      even_fecha=NOW(),
                                      even_estado='DESPACHADO',
                                      /*even_datos='$datos',*/
                                      usua_creador='$USUARIO_VALIDADOR_PEX' 
                                        ");
        if($res['status']==0){
            return $res;
        }
        $res = EnviarTarea($db,$responsables['creador'],'OS','VALIDAR_OS',$orse_id,'{\"info_id\":'.$info_id.'}');
        if($res['status']==0){
            return $res;
        }
    }
    else {
        $res = EnviarTarea($db,$responsables['creador'],'OS','VALIDAR_INFORME',$orse_id,'{\"info_id\":'.$info_id.'}');
        if($res['status']==0){
            return $res;
        }
    }
 
	return $res;
}


function OSInformeValidado($db,$responsables,$orse_id,$info_id,$info_estado){
  if($info_estado=="PREAPROBADO"){
    $res = $db->ExecuteQuery("SELECT
                                  orse_tipo 
                              FROM
                                  orden_servicio
                              WHERE orse_id=$orse_id");
    if(!$res['status']){
        return $res;
    }
    $orse_tipo = $res['data'][0]['orse_tipo'];

    if(in_array($orse_tipo,array("OSEU","OSEN","OSI"))){
      $res = $db->ExecuteQuery("SELECT
                                    pres_id 
                                FROM
                                    presupuesto
                                WHERE orse_id=$orse_id AND pres_estado='PREAPROBADO'");
      if(!$res['status']){
          return $res;
      }
      if(0<$res['rows']){
        $res = EnviarTarea($db,$responsables['creador'],'OS','VALIDAR_OS',$orse_id);
        return $res;
      }
    }
    else{
      $res = EnviarTarea($db,$responsables['creador'],'OS','VALIDAR_OS',$orse_id);
      return $res;
    }
  }

	return array("status"=>1);
}

function OSSolicitudCambio($db,$responsables,$orse_id,$razon){
	
	$res = EnviarTarea($db,$responsables['creador'],'OS','VALIDAR_SOLICITUD_CAMBIO',$orse_id,'{\"razon\":\"'.$razon.'\"}');
	return $res;
}

function OSSolicitudCambioValidada($db,$responsables,$orse_id,$orse_tipo_anterior,$orse_tipo_actual,$orse_solicitud_cambio){
	
   //Si la solicitud fue aceptada y la orse tipo nueva admite presupuesto, bajar tarea de VALIDAR_OS...
   if($orse_solicitud_cambio=="APROBADA"){
      if(in_array($orse_tipo_actual,array("OSEU","OSEN","OSI"))){
        $res = $db->ExecuteQuery("UPDATE tarea SET tare_estado='CANCELADA' 
                                  WHERE tare_modulo='OS' AND tare_tipo='VALIDAR_OS' AND tare_id_relacionado='$orse_id'");
        if($res['status']==0){
          return $res;
        }
      }
   }


   return array("status"=>1);
}

function OSSolicitudInforme($db,$responsables,$orse_id,$razon){
	
   $res = EnviarTarea($db,$responsables['creador'],'OS','VALIDAR_SOLICITUD_INFORME',$orse_id,'{\"razon\":\"'.$razon.'\"}');
   return $res;
}

function OSSolicitudInformeValidada($db,$responsables,$orse_id,$orse_solicitud_informe_web){
	
   return array("status"=>1);
}

function OSFinalizada($db,$responsables,$orse_id){

    $res = EnviarTarea($db,$responsables['creador'],'OS','VALIDAR_OS',$orse_id);
    return $res;
}

function OSValidada($db,$responsables,$orse_id,$orse_estado){
  //Agregar a inventario informe(s) aprobado(s)
  if($orse_estado=='APROBADA'){
    //Obtener informe
    $res = $db->ExecuteQuery("SELECT info_id FROM informe 
                              WHERE
                                info_modulo='OS' AND info_id_relacionado='$orse_id' AND info_estado='APROBADO'");

    if($res['status']==0){
      return $res;
    }

    foreach ($res['data'] as $row) {
      $res = AgregarInventario($db,$row['info_id']);
      if($res['status']==0){
        Loggear("Error al agregar a inventario informe ".$row['info_id'].": ".$res['error'],LOG_ERR);
      }
    }
  }

   return array("status"=>1);
}

/* Deja una OS en estado 'ANULADA' */
Flight::route('GET|POST /orse/@orse_id:[0-9]+/anulada', function($orse_id){
    $out = array();
    $out['status'] = 1;
    
    $dbo = new MySQL_Database();

    $query = "UPDATE orden_servicio SET orse='ANULADA' where orse_id=".$orse_id;
    
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $query1 = "DELETE from sla_orden_servicio where orse_id=".$orse_id;

    $res = $dbo->ExecuteQuery($query1);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    Flight::json($out);
});

/* Deja una OS en estado 'CREADA' */
Flight::route('GET|POST /orse/@orse_id:[0-9]+/creada', function($orse_id){
    $out = array();
    $out['status'] = 1;
    
    $dbo = new MySQL_Database();

    $query = "UPDATE orden_servicio SET orse_estado='CREADA' where orse_id=".$orse_id;
    
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $query1 = "DELETE from tarea where tare_id_relacionado=".$orse_id;

    $res = $dbo->ExecuteQuery($query1);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $query2 = "DELETE from evento where even_id_relacionado=".$orse_id;

    $res = $dbo->ExecuteQuery($query2);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    Flight::json($out);
});

#$id = $res["data"][0]["id"];

/* Deja una OS en estado 'ASIGNADA' */
Flight::route('GET|POST /orse/@orse_id:[0-9]+/asignada', function($orse_id){
    $out = array();
    $out['status'] = 1;
    
    $dbo = new MySQL_Database();

    $query = "UPDATE orden_servicio SET orse_estado='ASIGNADA' where orse_id=".$orse_id;
    
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $query1 = "SELECT oras_id from orden_servicio_asignacion where orse_id=".$orse_id;

    $res = $dbo->ExecuteQuery($query1);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $maas_id = $res['data'][0]['oras_id'];

    $query2 = "DELETE from evento where even_id_relacionado=".$orse_id;

    $res = $dbo->ExecuteQuery($query2);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $query3 = ("UPDATE tarea SET
                                tare_estado='CREADA'
                              WHERE
                                  tare_id_relacionado='$orse_id' 
                                  AND tare_tipo='ASIGNAR'");
    $res = $dbo->ExecuteQuery($query3);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $query4 = "INSERT into evento(even_modulo, even_evento, even_id_relacionado, even_fecha, even_estado, even_datos, even_comentario, usua_creador)values('OS','ASIGNADA',$orse_id,NOW(),'ATENDIDO','{\"oras_id\":$oras_id}',1)";

    $res = $dbo->ExecuteQuery($query4);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    Flight::json($out);
});

/*Validando */
Flight::route('GET|POST /orse/@orse_id:[0-9]+/validando', function($orse_id){
    $out = array();
    $out['status'] = 1;
    
    $dbo = new MySQL_Database();

    $query = "UPDATE orden_servicio SET orse_estado='VALIDANDO' where orse_id=".$orse_id;
    
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    Flight::json($out);
});

/*Aprobada */
Flight::route('GET|POST /orse/@orse_id:[0-9]+/aprobada', function($orse_id){
    $out = array();
    $out['status'] = 1;
    
    $dbo = new MySQL_Database();

    $query = "UPDATE orden_servicio SET orse_estado='APROBADA' where orse_id=".$orse_id;
    
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    Flight::json($out);
});

/*Rechazada */
Flight::route('GET|POST /orse/@orse_id:[0-9]+/rechazada', function($orse_id){
    $out = array();
    $out['status'] = 1;
    
    $dbo = new MySQL_Database();

    $query = "UPDATE orden_servicio SET orse_estado='RECHAZADA' where orse_id=".$orse_id;
    
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    Flight::json($out);
});

/*Finalizada */
Flight::route('GET|POST /orse/@orse_id:[0-9]+/finalizada', function($orse_id){
    $out = array();
    $out['status'] = 1;
    
    $dbo = new MySQL_Database();

    $query = "UPDATE orden_servicio SET orse_estado='FINALIZADA' where orse_id=".$orse_id;
    
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    Flight::json($out);
});

/*No realizado */
Flight::route('GET|POST /orse/@orse_id:[0-9]+/norealizado', function($orse_id){
    $out = array();
    $out['status'] = 1;
    
    $dbo = new MySQL_Database();

    $query = "UPDATE orden_servicio SET orse_estado='NO REALIZADO' where orse_id=".$orse_id;
    
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    Flight::json($out);
});

?>