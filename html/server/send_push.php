<?php

include_once("config.php");

function SendPush($regs,$message){
    Loggear("HERE SENDPUSH");
    global $PUSH_API_KEY,$PUSH_URL;

    $res = array("status"=>true);
    $API_KEY = $PUSH_API_KEY;
    $URL     = $PUSH_URL;

    Loggear("RES: ".$res);
    Loggear("API_KEY: ".$API_KEY);
    Loggear("URL: ".$URL);
    Loggear("REGS:".$regs);

    $fields = array(
        'registration_ids' => $regs,
        'data' => $message
    );
    $headers = array(
        'Authorization: key=' . $API_KEY,
        'Content-Type: application/json'
    );

    // Open connection
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, $URL);
    curl_setopt( $ch, CURLOPT_POST, true);
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields));

    Loggear("ch:".$ch);
    // Execute post
    $result      = curl_exec($ch);
    $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    //echo $result." [".$http_status."]\n";
    
    Loggear("Result:".$result);
    Loggear("STATUS:".$http_status);
    
    if($result===FALSE){
        $res = array("status"=>false,"error"=>"Error al enviar push");
        return $res;
    }
    if($http_status!=200){
        $res = array("status"=>false,"error"=>"Error al enviar push código ".$http_status);
        return $res;
    }

    $gcm_res  = json_decode($result,true);
    $status   = $gcm_res["success"]==count($regs);

    if(!$status){
        print_r($gcm_res);
        $res = array("status"=>false,"error"=>$gcm_res["results"][0]["error"]);
    }
    return $res;
}
?>