<?php

global $BASEDIR;

include($BASEDIR."../libs/SwiftMailer/lib/swift_required.php");
date_default_timezone_set("America/Santiago");

//Enviador de emails
//From: (array) email desde el cual se envía, formato array('alguncorreo@visualprogress.cl' => 'Algún nombre');
//To: (string|array) email a enviar, puede ser un arreglo de emails.
//Title: (string) titulo del correo
//Message: (string) mensaje del correo
//Attachments: (string|array) adjuntos a enviar
//Options: (array) opciones adicionales (host, port,..)

function SendEmail($from,$to,$title,$message,$attachments=null,$options=null){
    $res = array("status"=>false,"error"=>"","warning"=>"");

    //Validacion
    if(!$from){
        $res['error'] = "Campo 'from' no puede ser vacio";
        return $res;
    }
    else{

    }

    $valids_to = array();
    if(is_array($to)){
        foreach($to as $email){
            if(Swift_Validate::email($email)){
                 array_push($valids_to,$email);             
            }
            else{
                $res['warning'] .= "$email no es válido, se descartará\n";
            }
        } 
    }
    else{
        if(Swift_Validate::email($to)){
             array_push($valids_to,$to);             
        }
        else{
            $res['warning'] .= "$to no es válido, se descartará\n";
        }
    }

    if(count($valids_to)==0){
        $res['error'] = "No se ha ingreso campos 'to' válidos";
        return $res;
    }

    try{
        $host = ($options != null && isset($options['host']))?($options['host']):('localhost');
        $port = ($options != null && isset($options['port']))?($options['port']):(25);

        $transport = Swift_SmtpTransport::newInstance($host,$port);
        $mailer    = Swift_Mailer::newInstance($transport);
        $email     = Swift_Message::newInstance($title)
                      ->setFrom($from)
                      ->setTo($valids_to)
                      ->setBody($message,'text/html');

        if($attachments!=null){
            if(is_array($attachments)){
                foreach ($attachments as $attachment) {
                    $email->attach(Swift_Attachment::fromPath($attachment));
                }
            }
            else{
                $email->attach(Swift_Attachment::fromPath($attachments));
            }
        }
                                                       
        $result = $mailer->send($email);
        if($result){
            $res['status'] = true;
        }
        else{
            $res['error'] = "Error al enviar correo";
        }
    }
    catch(Exception $e){ 
        $res['error'] = $e->getMessage();
    }
    return $res; 
}
    
?>