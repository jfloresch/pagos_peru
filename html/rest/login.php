<?php
    header("Cache-Control: no-cache, no-store, must-revalidate");
    header("Pragma: no-cache"); 
    header("Expires: 0"); 
    clearstatcache();
    clearstatcache("siom.js");

    Flight::route('POST /login', function(){
        global $DEF_CONFIG;
        global $PERFILES;
        global $BASEDIR;
        $db = new MySQL_Database();
        require $BASEDIR."../libs/password.php";
        $user = mysql_real_escape_string($_POST['user']);
        $pass = mysql_real_escape_string($_POST['pass']); 

        if(''== $user || '' == $pass ){
            $out['error']   = "Usuario inválido";
            Flight::json($out);
            return;
        }

       $res = $db->ExecuteQuery("SELECT 
                                        usua_id,
                                        usua_nombre,
                                        empresa.empr_id,
                                        empr_nombre ,
                                        usua_crypt_pass 
                                FROM usuario
                                    INNER JOIN empresa 
                                        ON (empresa.empr_id = usuario.empr_id) 
                                WHERE usua_login='$user' 
                                    AND usua_acceso_web='1' 
                                    AND usua_estado='ACTIVO'");
        if($res['status']) {
            if(0<$res['rows']) {
                $usuario = $res['data'][0];
                $usua_id = $usuario['usua_id'];
                $crypt = $res['data'][0]['usua_crypt_pass'];
                if(password_verify($pass, $crypt)) {
                    global $res;
                    $res = $db->ExecuteQuery("SELECT c.cont_id,
                                                     c.cont_nombre
                                              FROM contrato c
                                                    INNER JOIN rel_contrato_usuario rel ON (rel.cont_id=c.cont_id AND rel.usua_id=$usua_id  
                                                AND rel.recu_estado='ACTIVO')");
                    if(!$res['status']){
                        Flight::Log($res['error']);
                        Flight::json(array("status"=>false,"error"=>$res['error']));
                        return;
                    }  
                    $contratos = $res['data'];

                    $res = $db->ExecuteQuery("SELECT
                                                perf_nombre
                                            FROM
                                                rel_usuario_perfil
                                            INNER JOIN perfil ON (rel_usuario_perfil.perf_id=perfil.perf_id
															  AND perfil.perf_modulo_sistemico in ('TODAS','FINANCIERO'))
                                            WHERE usua_id=$usua_id order by rup_id ASC");
                    if(!$res['status']){
                        Flight::Log($res['error']);
                        Flight::json(array("status"=>false,"error"=>$res['error']));
                        return;
                    }  
                    $usuario['usua_cargo'] = array();
                    $perfil = array();
                    if (  0 < $res['rows'] ) {
                        foreach($res['data'] as $row){
                            if(isset($PERFILES[$row['perf_nombre']])){
                                $perfil = array_merge($perfil,$PERFILES[$row['perf_nombre']]);
                            }
                            array_push($usuario['usua_cargo'],$row['perf_nombre']);
                        }
                    }
                    $_SESSION['user_id']    = $usuario['usua_id'];
                    $_SESSION['user_name']  = $usuario['usua_nombre'];
                    $_SESSION['usua_cargo'] = $usuario['usua_cargo'];
                    $_SESSION['empr_id']    = $usuario['empr_id'];
                    $_SESSION['empr_nombre']  = $usuario['empr_nombre'];
                    $_SESSION['perfil']     = $perfil;
                    $_SESSION['logged']     = true;
                    $_SESSION['config']     = $DEF_CONFIG;
                    $_SESSION['contracts']  = $contratos;
                    $_SESSION['sections']   = array();
                    $_SESSION['cont_id']    = $contratos[0]['cont_id']; 
                    $_SESSION['LAST_ACTIVITY'] = time();
                    $secciones_menu = array();
					
/*                    if (0 < $res['rows']) {
                        foreach($res['data'] as $row){
                            if("SYS_FINANCIERO"==$row['perf_nombre'] || "ADM_FINANCIERO"==$row['perf_nombre'] ){
*/                                array_push($secciones_menu,
                                        array("link"=>"#/pago/regresar/resumen" ,"section"=>"Resumen Costeo" ),     
                                        array("link"=>"#/pago/presupuesto/perfil" ,"section"=>"Ingreso Presupuesto"),
                                        array("link"=>"#/pago/costeo/generar" ,"section"=>"Generar Costeo Pago")
                                );
/*                            }else {
                                if("VALIDADOR_COSTEO"==$row['perf_nombre'] || "VALIDADOR_ACTA"==$row['perf_nombre']  ) {
                                    array_push($secciones_menu,
                                        array("link"=>"#/pago/regresar/resumen" ,"section"=>"Resumen Costeo")
                                    );
                                }
                                if("VALIDADOR_PRESUPUESTO"==$row['perf_nombre']) {
                                    array_push($secciones_menu,
                                        array("link"=>"#/pago/presupuesto/perfil" ,"section"=>"Ingreso Presupuesto" )
                                    );
                                }
                            }
                        }
                                        }*/
                    array_push($_SESSION['sections'],array("link"=>"#/pago","section"=>"Pago","sections"=>$secciones_menu));                
                   $res = array("status"=>true,
                                 "user_id"=>$_SESSION['user_id'],
                                 "user_name"=>$_SESSION['user_name'],
                                 "user_level"=>$_SESSION['usua_cargo'],
                                 "profile"=>$_SESSION['perfil'],
                                 "client"=>$_SESSION['empr_nombre'],
                                 "config"=>$_SESSION['config'],
                                 "sections"=>$_SESSION['sections'],
                                 "contracts"=>$_SESSION['contracts'],
                                 "contrato"=>$_SESSION['cont_id']);
                } else {
                    $res = array("status"=>0,"error"=>"Usuario inv�lido");
                }
            } else {
                $res = array("status"=>0,"error"=>"Usuario inv�lido");
            }
        }
        Flight::json($res);
    });

    Flight::route('GET /islogged', function(){
        if(isset($_SESSION['user_id'])){
            Flight::json(array("status"=>true,
                                    "logged"=>true,
                                    "user_id"=>$_SESSION['user_id'],
                                    "user_name"=>$_SESSION['user_name'],
                                    "user_level"=>$_SESSION['usua_cargo'],
                                    "profile"=>$_SESSION['perfil'],
                                    "client"=>$_SESSION['empr_nombre'],
                                    "config"=>$_SESSION['config'],
                                    "sections"=>$_SESSION['sections'],
                                    "contracts"=>$_SESSION['contracts'],
                                    "contrato"=>$_SESSION['cont_id']));
        } else {
            Flight::json(array("status"=>true,"logged"=>0));
        }
    });

    Flight::route('POST /logout', function(){
        $_SESSION = array();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
        session_destroy();
        Flight::json(array("status"=>1));
    });
?>