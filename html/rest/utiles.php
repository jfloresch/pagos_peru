<?php

Flight::route('GET /core/comuna/list(/@provincia:[0-9]+)', function($provincia) {
    $query = "SELECT * FROM comuna ".((is_null($provincia))?"":"WHERE prov_id=".$provincia)." ORDER BY comu_nombre ASC";
    $dbo = new MySQL_Database();
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

Flight::route('GET /core/provincia/list(/@region:[0-9]+)', function($region){
    $query = "SELECT * FROM provincia ".((is_null($region))?"":"WHERE regi_id=".$region)." ORDER BY prov_nombre ASC";
    $dbo = new MySQL_Database();
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

Flight::route('GET /core/region/list(/@pais:[0-9]+)', function($pais){
    $query = "SELECT * FROM region ".((is_null($pais))?"":"WHERE pais_id=".$pais)." ORDER BY regi_orden ASC";
    $dbo = new MySQL_Database();
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

Flight::route('GET /core/pais/list', function(){
    $query = "SELECT * FROM pais ORDER BY pais_nombre ASC";
    $dbo = new MySQL_Database();
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});


?>