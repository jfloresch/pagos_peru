<?php
    include_once("../server/config.php");
    include("../server/send_email.php");
    include("../server/utils.php");
    include("config.php");
    include("utils.php");
    include("send_email.php");
    global $DB_NAME;

    Flight::route('GET /contrato/@cont_id:[0-9]+/ingresoLinea', function($cont_id){

        $out = array();
        $out['status'] = 1;
        $db = new MySQL_Database();
        $anio_actual =date ("Y");
        $proveedor_nombre = "SELECT empr_nombre
                             FROM  rel_contrato_empresa rcem,  empresa empr
                             WHERE empr.empr_id = rcem.empr_id
                                AND cont_id = $cont_id
                                AND rcem.coem_tipo = 'CONTRATISTA'";
        $proveedor = $db->ExecuteQuery($proveedor_nombre);
        if ($proveedor['status'] == 0) {
            Flight::json(array("status" => 0, "error"=>$proveedor['error']));
            return;
        }
        
        $sociedad_codigo = "SELECT soci_codigo,soci_nombre FROM sociedad";
        $sociedad = $db->ExecuteQuery($sociedad_codigo);
        if ($sociedad['status'] == 0) {
            Flight::json(array("status" => 0, "error"=>$sociedad['error']));
            return;
        }
        $tipo_linea = $db->ExecuteQuery( "SHOW COLUMNS FROM pago_presupuesto where field= 'papr_tipo'" );
        if ($tipo_linea['status'] == 0) {
            Flight::json(array("status" => 0, "error" => "No se encontraron tipos de lineas presupuestarias"));
            return;
        }

        preg_match("/^enum\(\'(.*)\'\)$/", $tipo_linea['data'][0]['Type'], $matches);
        explode("','", $matches[1]);
        $out['tipo_linea'] = explode("','", $matches[1]);
        $out['proveedor'] = $proveedor['data']; 
        $out['sociedad'] = $sociedad['data']; 
        $out['empresa'] = $sociedad['data'];
        $out['anio_actual']=$anio_actual;
        Flight::json($out);
    });

    Flight::route('GET /pago/pago_suplementario', function(){
        $out = array();
        $out['status'] = 1;
        Flight::json($out);
    });

    Flight::route('GET /pago/generarActa', function(){
        $out = array();
        $out['status'] = 1;
        Flight::json($out);
    });

    /* Esta funcion se utiliza para poder ingresar los nuevos presupuestos en la tabla de pagos */
    Flight::route('POST /pago_presupuesto/add/contrato/@cont_id:[0-9]+',function($cont_id){

        $out = array();
        $out['status'] = 1;
        $dbo = new MySQL_Database();
        $pago = array_merge($_POST);
        $anio=$pago['pago_anio'];
        $empresa=$pago['pago_empresa'];
        $sociedad =$pago['pago_sociedad'];
        $nombre_servicio=$pago['pago_servicio'];
        $proveedor=$pago['pago_proveedor'];
        $monto=$pago['pago_monto'];
        $moneda =  $GLOBALS['tipo_moneda'];
        $papr_tipo = $pago['tipo_linea'];

        if( isset($pago['pago_anio']) && ""!=$pago['pago_anio']){
            $anio=$pago['pago_anio'];
        }else{
            $anio = date("Y");
        }
        
        /*if(0>=$pago_anio){*/
            $res = $dbo->ExecuteQuery("INSERT INTO pago_presupuesto
                                                (   cont_id,
                                                    papr_empresa,
                                                    papr_sociedad,
                                                    papr_nombre_servicio,
                                                    papr_proveedor,
                                                    papr_anio,
                                                    papr_unidad,
                                                    papr_monto,
                                                    papr_fecha_creacion,
                                                    papr_tipo,
                                                    papr_usua_actualizacion  
                                                )
                                      VALUES(   $cont_id,
                                                '$empresa',
                                                '$sociedad',
                                                '$nombre_servicio',
                                                '$proveedor',
                                                $anio,
                                                '$moneda',
                                                $monto,
                                                NOW(),
                                                '$papr_tipo',
                                                papr_usua_actualizacion
                                                )"
            );
            if(0==$res['status']){
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }
            Flight::json($res);
    });

    /*
   Esta funcion se utiliza para subir archivos 
    */                  
    Flight::route('POST /pago_costeo_ver/contrato/@cont_id:[0-9]+/archivo/add',function($cont_id){

        $out = array();
        $out['status'] = 1;
        $db = new MySQL_Database();
        $pago = array_merge($_POST);
        $usuario_creador = $_SESSION['user_id'];
        $copa_id=$pago['copa_id_valor'];
        $db->startTransaction();
        //Subida de archivos
        if (isset($_FILES["archivos"])) { 
            $files = array(); 
            foreach($_FILES["archivos"] as $key1 => $value1) {
                foreach($value1 as $key2 => $value2) {
                    $files[$key2][$key1] = $value2; 
                }
            }
            $descriptions = json_decode($_POST['archivos_descripciones'],true);

            for($i=0; $i<count($files); $i++){
                $nombre      = $files[$i]['name'];
                $descripcion = $descriptions[$i];
                $filename = date('ymdHis')."_os_".$copa_id."_".str_replace(" ","_",$nombre);
                $resFile = Upload::UploadFile($files[$i],$filename); 
                if( !$resFile['status'] ){
                    $db->Rollback();
                    Flight::json(array("status" => 0, "error" => $resFile['error']));
                    return;
                }
                
                $query = "INSERT INTO repositorio_pago (repo_tipo_doc,repo_nombre,repo_descripcion,repo_tabla,repo_id_relacionado,repo_ruta,repo_fecha_creacion,repo_data,usua_creador)
                            VALUES(
                                'DOCUMENTO',
                                '".$nombre."',
                                '".$descripcion."',
                                'orden_servicio',
                                $copa_id,
                                '".$resFile['data']['filename']."',
                                NOW(),
                                NULL,
                                $usuario_creador)";
                $resUpload = $db->ExecuteQuery($query);
                if( $resUpload['status']==0 ) {
                    $db->Rollback();
                    Flight::json(array("status" => 0, "error" => $resUpload['error']));
                    return;
                }            
            }
        }
        
        $db->Commit();
        
        Flight::json($out);
    });

    /*
    Realiza un select para poder visualizar  el pago_detalle_filtro y se visualiza en lista_detalle_pago
    =============================================================
    =============================================================
    ================================================================
    Tenemos que cambiar esto una vez que presentemos por que no se puede hacer esto siompago esta en duro y debe ser una variable automatica con db.php de rest
    =============================================================
    =============================================================
    =============================================================
    =============================================================
    */
    Flight::route('GET /contrato/@cont_id:[0-9]+/pago/resumen/filtros', function($cont_id){
        $out = array();
        $out['status'] = 1;
        $anio =date ("Y");
        $out['pafi_anio'] = $anio;
        Flight::json($out);
    });

    Flight::route('GET /contrato/@cont_id:[0-9]+/periodo/@periodo/actividad/@actividad_valor/costeover', function($cont_id,$periodo,$actividad){
        $db = new MySQL_Database();
        $out = array();
        $out['status'] = 1;

        $copa_tipo_ot = $db->ExecuteQuery("SELECT copa_tipo_ot
                                            ,CASE WHEN (copa_tipo_ot='$actividad') THEN 'selected'
                                            ELSE '' END copa_tipo_ot_select 
                                            FROM costeo_pago 
                                            WHERE copa_periodo=$periodo
                                            AND copa_tipo_ot = '$actividad' 
                                        ");
        if ($copa_tipo_ot['status'] == 0) {
            Flight::json(array("status" => 0, "error" => "No se encontraron tipos de lineas presupuestarias"));
            return;
        }
        foreach ($copa_tipo_ot['data'] as $data) {
            $out['copa_tipo_ot_select']=$data['copa_tipo_ot_select'];
        }
       
        $out['periodo']=$periodo;
        $out['actividad']= $actividad;
        Flight::json($out);
    });
    /*Filtro de generar costeo visualiza los selectpicker*/
    Flight::route('GET /contrato/@cont_id:[0-9]+/pago/costeo', function($cont_id){
        $out = array();
        $out['status'] = 1;

        $db = new MySQL_Database();
        $res = $db->ExecuteQuery("  SELECT copa_periodo  
                                    FROM costeo_pago
                                    ORDER BY  copa_periodo DESC  
                                    LIMIT 1 ");
        if ($res['status'] == 0) {
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
        $periodo = $res['data'][0]['copa_periodo'];  
        $xperiodo =$periodo +1; 
        $out['periodo']= $xperiodo;

        $res = $db->ExecuteQuery("SELECT regi_id
                                        , regi_nombre 
                                    FROM  contrato c
                                    , pais p
                                    , region r 
                                    WHERE c.cont_id=$cont_id 
                                    AND p.pais_id = c.pais_id
                                    AND r.pais_id = p.pais_id
                                    ORDER BY regi_orden ASC");
        if ($res['status'] == 0) {
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
        $out['regiones'] = $res['data'];    
     
        $zona ="SELECT zona_id
                     , zona_nombre 
                FROM  zona 
                WHERE cont_id= $cont_id 
                AND zona_estado = 'ACTIVO' 
                AND zona_tipo='CONTRATO'";
                                    
        $res = $db->ExecuteQuery($zona);
        if ($res['status'] == 0){
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
             
        $out['zona_contrato'] = $res['data'];

       $zona ="SELECT zona_id
                     , zona_nombre 
                FROM  zona 
                WHERE cont_id= $cont_id 
                AND zona_estado = 'ACTIVO' 
                AND zona_tipo='MOVISTAR'";
        $res = $db->ExecuteQuery($zona);
        if ($res['status'] == 0) {
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
        $out['zona_movistar'] = $res['data'];

        $zona ="SELECT zona_id
                     , zona_nombre 
                FROM  zona 
                WHERE cont_id= $cont_id 
                AND zona_estado = 'ACTIVO' 
                AND zona_tipo='CLUSTER'";
        
        $res = $db->ExecuteQuery($zona);
        if ($res['status'] == 0) {
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
        $out['zona_cluster'] = $res['data'];
        Flight::json($out);
    });
    /*ingresar hem y derivada  filtro */
    Flight::route('GET /contrato/@cont_id:[0-9]+/pago/ingreso/hemyderivada/copa_id/@copa_id', function($cont_id,$copa_id){
        $out = array();
        $out['status'] = 1;

        $db = new MySQL_Database();

         /* Recuperar el periodo actual+1 para el costeo a ingresar*/
        $query = "SELECT distinct papr_id,papr_tipo
                    FROM pago_presupuesto papr
                    ,costeo_pago copa
                    WHERE papr.papr_estado  = 'ACTIVO'
                        AND papr.papr_id = copa.papr_id_capex
                            OR papr.papr_id = copa.papr_id_opex
                        AND copa_id = $copa_id
                        AND papr.cont_id = $cont_id;";
        $res = $db->ExecuteQuery($query);
        if ( 0==$res['status'] ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        $out['pago_presupuesto'] = $res['data'];
        /* Recuperar el periodo actual+1 para el costeo a ingresar*/
        $query = "SELECT copa_periodo as periodo 
                         ,papr_id_capex
                         ,papr_id_opex
                         ,copa_tipo_ot
                         ,copa_monto_capex
                         ,copa_monto_opex
                         ,copa_monto_total
                  FROM costeo_pago
                  WHERE cont_id = $cont_id
                  AND copa_id=$copa_id";
        $res = $db->ExecuteQuery($query);
        if ( 0==$res['status'] ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        
        $periodo = $res['data'][0]['periodo'];
        $out['periodo'] = $periodo;
        $res = $db->ExecuteQuery("SELECT regi_id
                                        , regi_nombre 
                                    FROM  contrato c
                                    , pais p
                                    , region r 
                                    WHERE c.cont_id=$cont_id 
                                    AND p.pais_id = c.pais_id
                                    AND r.pais_id = p.pais_id
                                    ORDER BY regi_orden ASC");
        if ($res['status'] == 0) {
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
        $out['regiones'] = $res['data'];    
     
        $zona ="SELECT zona_id
                     , zona_nombre 
                FROM  zona 
                WHERE cont_id= $cont_id 
                AND zona_estado = 'ACTIVO' 
                AND zona_tipo='CONTRATO'";                            
        $res = $db->ExecuteQuery($zona);
        if ($res['status'] == 0){
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
        $out['zona_contrato'] = $res['data'];

        $zona ="SELECT zona_id
                     , zona_nombre 
                FROM  zona 
                WHERE cont_id= $cont_id 
                AND zona_estado = 'ACTIVO' 
                AND zona_tipo='MOVISTAR'";
        $res = $db->ExecuteQuery($zona);
        if ($res['status'] == 0) {
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
        $out['zona_movistar'] = $res['data'];

        $zona ="SELECT zona_id
                     , zona_nombre 
                FROM  zona 
                WHERE cont_id= $cont_id 
                AND zona_estado = 'ACTIVO' 
                AND zona_tipo='CLUSTER'";
        $res = $db->ExecuteQuery($zona);
        if ($res['status'] == 0) {
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
        $out['zona_cluster'] = $res['data'];
        $out['copa_id']=$copa_id;
        Flight::json($out);
    });
    /*visualizar lupa de hemyderivada*/
    Flight::route('GET|POST /contrato/@cont_id/detalle/hemyderivada', function($cont_id){
        $out = array();
        $out['status'] = 1;
	/**JRFC**/
        $db = new MySQL_Database();
        $pago = array_merge($_GET,$_POST);
        $actividad = $pago['copa_tipo_ot'];

        //MEJORA VULNERABILIDAD INI
        $copa_id= mysql_real_escape_string($pago['copa_id']);
        if ("OS"==$actividad ||"LMT"==$actividad ) {
            $query1 = " SELECT distinct(hem_id) AS hem
                            ,derivada_id AS derivada
                            ,ospa_tipo_cambio AS tipo_cambio
                            ,ospa_observacion AS observacion
                        FROM os_servicio_pago
                        WHERE  copa_id = '$copa_id'";
            $res = $db->ExecuteQuery($query1);
            if( 0==$res['status'] ){
                $db->Rollback();
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }
        }else{
            $query2 = "SELECT distinct(hem_id) AS hem
                            ,derivada_id AS derivada
                            ,mnpa_tipo_cambio AS tipo_cambio
                            ,mnpa_observacion AS observacion
                        FROM mnt_servicio_pago
                        WHERE  copa_id = '$copa_id'";
	        //MEJORA VULNERABILIDAD FIN
            $res = $db->ExecuteQuery($query2);
            if( 0==$res['status'] ){
                $db->Rollback();
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }
        }
        $out['hemyderivada'] = $res['data'];
        Flight::json($out);
    });

    /*visualiza la data del filtro de  editar costeo */
    Flight::route('GET /contrato/@cont_id:[0-9]+/pago/costeo/copa_id/@copa_id', function($cont_id,$copa_id){
        $out = array();
        $out['status'] = 1;

	    /**JRFC**/
        $db = new MySQL_Database();
        /* Recuperar el periodo actual+1 para el costeo a ingresar*/
        $query = "SELECT copa_periodo as periodo 
                         ,papr_id_capex
                         ,papr_id_opex
                         ,copa_tipo_ot
                         ,copa_monto_capex
                         ,copa_monto_opex
                         ,copa_monto_total
                         ,copa_id
                  FROM costeo_pago
                  WHERE cont_id = $cont_id
                  AND copa_id=$copa_id";
        $res = $db->ExecuteQuery($query);
        if ( 0==$res['status'] ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }

        $periodo = $res['data'][0]['periodo'];
        $out['copa_id']=$copa_id;

        $out['periodo'] = $periodo;
        $res = $db->ExecuteQuery("SELECT regi_id
                                        , regi_nombre 
                                    FROM  contrato c
                                    , pais p
                                    , region r 
                                    WHERE c.cont_id=$cont_id 
                                    AND p.pais_id = c.pais_id
                                    AND r.pais_id = p.pais_id
                                    ORDER BY regi_orden ASC");
        if ($res['status'] == 0) {
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
        $out['regiones'] = $res['data'];    
     
        $zona ="SELECT zona_id
                     , zona_nombre 
                FROM  zona 
                WHERE cont_id= $cont_id 
                AND zona_estado = 'ACTIVO' 
                AND zona_tipo='CONTRATO'"; 
        $res = $db->ExecuteQuery($zona);
        if ($res['status'] == 0){
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
        $out['zona_contrato'] = $res['data'];

        $zona ="SELECT zona_id
                     , zona_nombre 
                FROM  zona 
                WHERE cont_id= $cont_id 
                AND zona_estado = 'ACTIVO' 
                AND zona_tipo='MOVISTAR'";
        $res = $db->ExecuteQuery($zona);
        if ($res['status'] == 0) {
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
        $out['zona_movistar'] = $res['data'];

        $zona ="SELECT zona_id
                     , zona_nombre 
                FROM  zona 
                WHERE cont_id= $cont_id 
                AND zona_estado = 'ACTIVO' 
                AND zona_tipo='CLUSTER'";    
        $res = $db->ExecuteQuery($zona);
        if ($res['status'] == 0) {
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
        $out['zona_cluster'] = $res['data'];
        Flight::json($out);
    });

    /*Visualiza la pantalla de asignar usuario costeo*/
    Flight::route('GET /contrato/@cont_id:[0-9]+/pago/asignar/usuario/validador/costeo/copa_id/@copa_id', function($cont_id,$copa_id){
        $out = array();
        $out['status'] = 1;
        $dbo = new MySQL_Database();

        $usuariovalidadores = $dbo->ExecuteQuery("  
                        SELECT concat('(',e.empr_nombre,')-',us.usua_nombre) as usua_nombre
                        , us.usua_id
                        ,p.perf_nombre
                        FROM  usuario us 
                        , rel_contrato_usuario rcu
                        , rel_usuario_perfil rup
                        , perfil p
                        , empresa e
                        , rel_costeo_usuario_validador rcuv
                        WHERE us.usua_estado = 'ACTIVO'
                        AND rcu.cont_id =$cont_id
                        AND rcu.usua_id=us.usua_id
                        AND rup.usua_id = us.usua_id
                        AND rup.perf_id = p.perf_id
                        AND rcu.usua_id not in (1)
                        AND p.perf_modulo_sistemico= 'FINANCIERO'
                        AND p.perf_nombre in ('VALIDADOR_COSTEO')
                        AND e.empr_id= us.empr_id
                        AND rcuv.copa_id = $copa_id
                        AND rcuv.usua_id = us.usua_id
                        ORDER BY e.empr_nombre, us.usua_nombre");  
        if( 0== $usuariovalidadores['status']){
            Flight::json(array("status"=>0, "error"=>$usuariovalidadores['error']));
            return;
        } 
        $out['usuariosvalidadores'] =$usuariovalidadores['data']; 
        $usuario = $dbo->ExecuteQuery("  
                        SELECT concat('(',e.empr_nombre,')-',us.usua_nombre) as usua_nombre
                              , us.usua_id
                            ,p.perf_nombre
                        FROM  usuario us 
                        , rel_contrato_usuario rcu
                        , rel_usuario_perfil rup
                        , perfil p
                        , empresa e
                        WHERE us.usua_estado = 'ACTIVO'
                        AND rcu.cont_id =$cont_id
                        AND rcu.usua_id=us.usua_id
                        AND rup.usua_id = us.usua_id
                        AND rup.perf_id = p.perf_id
                        AND rcu.usua_id not in (1)
                        AND p.perf_modulo_sistemico= 'FINANCIERO'
                        AND p.perf_nombre in ('VALIDADOR_COSTEO')
                        AND e.empr_id= us.empr_id
                        ORDER BY e.empr_nombre, us.usua_nombre");   
        if( 0== $usuario['status']){
            Flight::json(array("status"=>0, "error"=>$usuario['error']));
            return;
        } 
        $out['validadores'] = $usuario['data']; 
        $out['copa_id'] =$copa_id;
        Flight::json($out);
    });

     /*Visualiza la pantalla de asignar usuario acta*/
    Flight::route('GET /contrato/@cont_id:[0-9]+/pago/asignar/usuario/validador/acta/copa_id/@copa_id', function($cont_id,$copa_id){
        $out = array();
        $out['status'] = 1;
        $dbo = new MySQL_Database();
        $usuariovalidadores = $dbo->ExecuteQuery("  
                        SELECT concat('(',e.empr_nombre,')-',us.usua_nombre) as usua_nombre
                        , us.usua_id
                        ,p.perf_nombre
                        FROM  usuario us 
                        , rel_contrato_usuario rcu
                        , rel_usuario_perfil rup
                        , perfil p
                        , empresa e
                        , rel_costeo_usuario_validador rcuv
                        WHERE us.usua_estado = 'ACTIVO'
                        AND rcu.cont_id =$cont_id
                        AND rcu.usua_id=us.usua_id
                        AND rup.usua_id = us.usua_id
                        AND rup.perf_id = p.perf_id
                        AND rcu.usua_id not in (1)
                        AND p.perf_modulo_sistemico= 'FINANCIERO'
                        AND p.perf_nombre in ('VALIDADOR_ACTA')
                        AND rcuv.rcuv_tipo_validacion in ('VALIDADOR_ACTA')
                        AND e.empr_id= us.empr_id
                        AND rcuv.copa_id = $copa_id
                        AND rcuv.usua_id = us.usua_id
                        ORDER BY e.empr_nombre, us.usua_nombre");  
        if( 0== $usuariovalidadores['status']){
            Flight::json(array("status"=>0, "error"=>$usuariovalidadores['error']));
            return;
        } 
        $out['usuariosvalidadores'] =$usuariovalidadores['data']; 

        $usuario = $dbo->ExecuteQuery("  
                        SELECT concat('(',e.empr_nombre,')-',us.usua_nombre)as usua_nombre
                              , us.usua_id
                            ,p.perf_nombre
                        FROM  usuario us 
                        , rel_contrato_usuario rcu
                        , rel_usuario_perfil rup
                        , perfil p
                        , empresa e
                        WHERE us.usua_estado = 'ACTIVO'
                        AND rcu.cont_id =$cont_id
                        AND rcu.usua_id=us.usua_id
                        AND rup.usua_id = us.usua_id
                        AND rup.perf_id = p.perf_id
                        AND rcu.usua_id not in (1)
                        AND p.perf_modulo_sistemico= 'FINANCIERO'
                        AND p.perf_nombre in ('VALIDADOR_ACTA')
                        AND e.empr_id= us.empr_id
                        ORDER BY e.empr_nombre, us.usua_nombre
                    ");   

        if( 0== $usuario['status']){
            Flight::json(array("status"=>0, "error"=>$usuario['error']));
            return;
        } 
        $out['validadores'] = $usuario['data']; 
        $out['copa_id'] =$copa_id;
        Flight::json($out);
    });


    /*agrega los validadores costeo*/
    Flight::route('POST /contrato/@cont_id:[0-9]+/asignar/validador/costeo/copa_id/@copa_id/add', function($cont_id,$copa_id){
        $out = array();
        $out['status'] = 1;
        $dbo = new MySQL_Database();
        $dbo->startTransaction();
        $validadores = array();
        $i = 0;
        
        if(is_array($_POST['validadores'])){
            foreach($_POST['validadores'] as $a){
               
                array_push($validadores,"(".$copa_id.",".$a.",'VALIDADOR_COSTEO')");
                $i=$i+1;
            }
            $validadores = implode(",",$validadores);
        }else{
            $validadores = "(".$copa_id.",".$_POST['validadores'].",'VALIDADOR_COSTEO')";
        }

        $res = $dbo->ExecuteQuery("INSERT INTO rel_costeo_usuario_validador
                                    (copa_id
                                    ,usua_id
                                    ,rcuv_tipo_validacion)
                                    VALUES
                                    $validadores");
        if( $res['status']==0 ){
            $dbo->Rollback();
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        $estado = $dbo->ExecuteQuery("  UPDATE costeo_pago 
                                        SET copa_estado ='PEND_VALIDAR_COSTEO' 
                                        WHERE  copa_id = $copa_id");
        if( $estado['status']==0 ){
            $dbo->Rollback();
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }

        $dbo->Commit();
        $titulo = "Validar costeo Nª ".$copa_id;
        $tipo="VALIDADOR_COSTEO";
        $resEmail = EnviarEmail($dbo,$copa_id,$titulo,$tipo);
                        if($resEmail['status']==0){
                            Loggear("Error al enviar notificacion: ".$resEmail['error'],LOG_ERR);
                            Loggear($dbo->lastQuery);
                        }
        Flight::json($res);
    });

    /*Agrega los validadores acta*/
    Flight::route('POST /contrato/@cont_id:[0-9]+/asignar/validador/acta/copa_id/@copa_id/add', function($cont_id,$copa_id){
        $out = array();
        $out['status'] = 1;
        $dbo = new MySQL_Database();
        $dbo->startTransaction();
        $validadores = array();
        $i = 0;       
        if(is_array($_POST['validadores'])){
            foreach($_POST['validadores'] as $a){
                array_push($validadores,"(".$copa_id.",".$a.",'VALIDADOR_ACTA')");
                $i=$i+1;
            }
            $validadores = implode(",",$validadores);
        }else{
            $validadores = "(".$copa_id.",".$_POST['validadores'].",'VALIDADOR_ACTA')";
        }

        $res = $dbo->ExecuteQuery("INSERT INTO rel_costeo_usuario_validador
                                    (copa_id
                                    ,usua_id
                                    ,rcuv_tipo_validacion)
                                    VALUES
                                    $validadores");
        if( $res['status']==0 ){
            $dbo->Rollback();
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }

        $estado = $dbo->ExecuteQuery("  UPDATE costeo_pago 
                                        SET copa_estado ='PEND_VALIDAR_ACTA' 
                                        WHERE  copa_id = $copa_id");
        if( $estado['status']==0 ){
            $dbo->Rollback();
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        $dbo->Commit();
        $titulo = "Validar Acta Nª ".$copa_id;
        $tipo="VALIDADOR_ACTA";
        $resEmail = EnviarEmail($dbo,$copa_id,$titulo,$tipo);
                        if($resEmail['status']==0){
                            Loggear("Error al enviar notificacion: ".$resEmail['error'],LOG_ERR);
                            Loggear($dbo->lastQuery);
                        }
        Flight::json($res);
    });

    /* Pantalla principal Resumen Costeo admin */
    Flight::route('POST /contrato/@cont_id:[0-9]+/pago/resumen/filtro(/@page:[0-9]+)', function($cont_id,$page){

        $out = array();
        $out['status'] = 1;
        $dbo = new MySQL_Database();
        $usuario = $_SESSION['user_id'];
        $anio =date ("Y");
        $mes = date ("m", strtotime("-1 month"));
        $filtro = array_merge($_GET, $_POST);
        $filtrosQuery = "";
        if( isset($filtro['pafi_anio']) && ""!=$filtro['pafi_anio']){
            $anio= mysql_real_escape_string($filtro['pafi_anio']);
        }

        /* Si valor de pafi_mes es 00, significa que la búsqueda tiene que realizar por el año completo */
        if( isset($filtro['pafi_mes']) && ""!=$filtro['pafi_mes']){
            $mes = mysql_real_escape_string($filtro['pafi_mes']);
            //MEJORA VULNERABILIDAD INI
            $filtrosQuery = " AND copa_periodo='".$anio.$mes."'";
            if("00"==$mes){
                $filtrosQuery = " AND copa_periodo>='".$anio."01' AND copa_periodo<='".$anio."12' ";
            }
	    //MEJORA VULNERABILIDAD FIN
        }
        $filtrosQuery .= " ORDER BY cp.copa_id DESC";
        $out['pafi_anio'] = $anio;
       
        $res = $dbo->ExecuteQuery("SELECT    cp.copa_id
                                            ,cp.copa_fecha_creacion
                                            ,cp.copa_usua_creacion
                                            ,cp.copa_monto_capex
                                            ,cp.copa_monto_opex
                                            ,cp.copa_monto_total
                                            ,cp.copa_tipo_ot
                                            ,cp.copa_fecha_ult_modificacion
                                            ,cp.copa_usua_actualizacion
                                            ,cp.copa_periodo
                                            ,cp.cont_id
                                            ,cp.copa_derivada
                                            ,cp.copa_hem
                                            ,cp.copa_tipo_cambio
                                            ,cp.copa_estado
                                            ,CASE 
                                                WHEN r.rcuv_tipo_validacion = 'VALIDADOR_COSTEO'
                                                AND cp.copa_estado in ('PEND_VALIDAR_COSTEO','VALIDANDO_COSTEO')
                                                AND r.rcuv_fecha_validacion IS  NULL THEN ''
                                             ELSE 'none' END  copa_validar_costeo
                                            ,CASE 
                                                WHEN r.rcuv_tipo_validacion = 'VALIDADOR_ACTA'
                                                AND cp.copa_estado in ('PEND_VALIDAR_ACTA','VALIDANDO_ACTA')
                                                AND rcuv_fecha_validacion IS  NULL THEN ''
                                             ELSE 'none' END  copa_validar_acta
                                            ,IFNULL(cp.copa_observacion, '') as copa_observacion
                                            ,CASE WHEN (cp.copa_estado='CREADO') THEN ''
                                            ELSE 'none' END copa_visibilidad_asignar_validador_costeo
                                             ,CASE WHEN (cp.copa_estado='COSTEO_VALIDADO') THEN ''
                                            ELSE 'none' END copa_visibilidad_asignar_validador_acta
                                            ,cp.copa_cantidad_total
                                            ,ppc.papr_id papr_id_capex
                                            ,ppc.papr_nombre_servicio papr_nombre_servicio_capex
                                            ,ppc.papr_tipo papr_tipo_capex
                                            ,ppo.papr_id papr_id_opex
                                            ,ppo.papr_nombre_servicio papr_nombre_servicio_opex
                                            ,ppo.papr_tipo papr_tipo_opex 
                                            ,r.usua_id
                                            ,r.rcuv_fecha_validacion
                                    FROM costeo_pago cp
                                    LEFT JOIN rel_costeo_usuario_validador r ON r.copa_id = cp.copa_id
                                                                              AND r.usua_id = $usuario
                                                                              AND rcuv_fecha_validacion IS  NULL
                                    LEFT JOIN pago_presupuesto ppc ON cp.papr_id_capex = ppc.papr_id 
                                                                  AND cp.cont_id = ppc.cont_id
                                                                  AND ppc.papr_estado = 'ACTIVO'
                                    LEFT JOIN pago_presupuesto ppo ON cp.papr_id_opex = ppo.papr_id 
                                                                  AND cp.cont_id = ppo.cont_id
                                                                  AND ppo.papr_estado = 'ACTIVO'
                                    WHERE cp.cont_id=$cont_id" .$filtrosQuery);
        if( 0== $res['status']){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        } 
        $out['monto'] = $res['data']; 
        Flight::json($out);        
    });

    /* EMB ocupar para visualizar en resumen de costeo sólo los que esten cerrados*/ 
    Flight::route('POST /contrato/@cont_id:[0-9]+/pago/resumen/cerrado/filtro(/@page:[0-9]+)', function($cont_id,$page){

        $out = array();
        $out['status'] = 1;
        $dbo = new MySQL_Database();
        $anio =date ("Y");
        $mes = date ("m", strtotime("-1 month"));
        $usuario = $_SESSION['user_id'];
        $filtro = array_merge($_GET, $_POST);
        $filtrosQuery = " AND copa_estado= 'APROBADO' ";
        if( isset($filtro['pafi_anio']) && ""!=$filtro['pafi_anio']){
            $anio= mysql_real_escape_string($filtro['pafi_anio']);
        }

        /* Si valor de pafi_mes es 00, significa que la búsqueda tiene que realizar por el año completo */
        if( isset($filtro['pafi_mes']) && ""!=$filtro['pafi_mes']){
            $mes = mysql_real_escape_string($filtro['pafi_mes']);
	        //MEJORA VULNERABILIDAD INI
            if("00"==$mes){
                $filtrosQuery = " AND copa_periodo>='".$anio."01' AND copa_periodo<='".$anio."12'";
            }else{
                $filtrosQuery = " AND copa_periodo='".$anio.$mes."'";
            }
	    //MEJORA VULNERABILIDAD FIN
        }
        $out['pafi_anio'] = $anio;
        /*$periodo = $anio.$mes;*/
        $res = $dbo->ExecuteQuery("SELECT    cp.copa_id as copa_id 
                                            ,cp.copa_fecha_creacion
                                            ,cp.copa_usua_creacion
                                            ,cp.copa_monto_capex
                                            ,cp.copa_monto_opex
                                            ,cp.copa_monto_total
                                            ,cp.copa_tipo_ot
                                            ,cp.copa_fecha_ult_modificacion
                                            ,cp.copa_usua_actualizacion
                                            ,cp.copa_periodo
                                            ,cp.cont_id
                                            ,cp.copa_derivada
                                            ,cp.copa_hem
                                            ,cp.copa_tipo_cambio
                                            ,cp.copa_estado
                                            ,'none' AS  copa_validar_costeo
                                            ,'none' AS copa_validar_acta  
                                            ,cp.copa_cantidad_total
                                            ,pp.papr_id
                                            ,pp.papr_nombre_servicio
                                            ,pp.papr_tipo
                                            ,IFNULL(cp.copa_observacion, '') as copa_observacion
                                    FROM costeo_pago cp
                                    LEFT JOIN pago_presupuesto pp ON cp.papr_id_capex = pp.papr_id 
                                            AND cp.cont_id = pp.cont_id
                                            AND  pp.papr_estado = 'ACTIVO'
                                    LEFT JOIN pago_presupuesto ppo ON cp.papr_id_opex = ppo.papr_id 
                                            AND cp.cont_id = ppo.cont_id
                                            AND  ppo.papr_estado = 'ACTIVO'
                                    WHERE cp.cont_id=$cont_id" .$filtrosQuery. 
                                    "  UNION
                                    SELECT   cp.copa_id as copa_id 
                                            ,cp.copa_fecha_creacion
                                            ,cp.copa_usua_creacion
                                            ,cp.copa_monto_capex
                                            ,cp.copa_monto_opex
                                            ,cp.copa_monto_total
                                            ,cp.copa_tipo_ot
                                            ,cp.copa_fecha_ult_modificacion
                                            ,cp.copa_usua_actualizacion
                                            ,cp.copa_periodo
                                            ,cp.cont_id
                                            ,cp.copa_derivada
                                            ,cp.copa_hem
                                            ,cp.copa_tipo_cambio
                                            ,cp.copa_estado
                                            ,CASE 
                                                WHEN r.rcuv_tipo_validacion = 'VALIDADOR_COSTEO'
                                                AND cp.copa_estado in ('PEND_VALIDAR_COSTEO','VALIDANDO_COSTEO')
                                                AND r.rcuv_fecha_validacion IS  NULL THEN ''
                                             ELSE 'none' END  copa_validar_costeo
                                            ,CASE 
                                                WHEN r.rcuv_tipo_validacion = 'VALIDADOR_ACTA'
                                                AND cp.copa_estado in ('PEND_VALIDAR_ACTA','VALIDANDO_ACTA')
                                                AND rcuv_fecha_validacion IS  NULL THEN ''
                                             ELSE 'none' END  copa_validar_acta
                                            ,cp.copa_cantidad_total
                                            ,pp.papr_id
                                            ,pp.papr_nombre_servicio
                                            ,pp.papr_tipo
                                            ,IFNULL(cp.copa_observacion, '') as copa_observacion
                                    FROM costeo_pago cp
                                    INNER JOIN rel_costeo_usuario_validador r ON r.copa_id = cp.copa_id
                                                                              AND r.usua_id = $usuario
                                                                              AND rcuv_fecha_validacion IS  NULL 
                                    LEFT JOIN pago_presupuesto pp ON cp.papr_id_capex = pp.papr_id
                                                                        AND cp.cont_id = pp.cont_id
                                                                        AND pp.papr_estado='ACTIVO'
                                    LEFT JOIN pago_presupuesto ppo ON cp.papr_id_opex = ppo.papr_id 
                                                                        AND cp.cont_id = ppo.cont_id
                                                                        AND ppo.papr_estado='ACTIVO'
                                    WHERE cp.cont_id=$cont_id
                                    ORDER BY copa_id DESC;"
                                );
        if( 0== $res['status']){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        $out['monto'] = $res['data'];
        Flight::json($out);
    });
    
    /*Inserta derivada y hem */
    /*EMB Esta funcion deviera llamarse actualizar Costeo Pago"*/
    Flight::route('GET|POST /contrato/@cont_id:[0-9]+/pago/actualizar/presupuesto', function($cont_id){

        $out = array();
        $out['status'] = 1;
        $db = new MySQL_Database();
        $pago = array_merge($_POST);

        $periodo='null';
        if(isset($pago['periodo']) && ""!=$pago['periodo']){
            $periodo=$pago['periodo'];
        }
    
        $copa_id='null';
        if(isset($pago['copa_id']) && ""!=$pago['copa_id']){
            $copa_id=$pago['copa_id'];     
        }
             
        $observacion='';
        if(isset($pago['observacion']) && ""!=$pago['observacion']){
            $observacion=$pago['observacion']; 
        }

        $db->startTransaction();
        $query = " SELECT copa_tipo_ot
                    FROM costeo_pago
                    WHERE copa_id = $copa_id";
        $res = $db->ExecuteQuery($query);
        if( 0==$res['status'] ){
            $db->Rollback();
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        $actividad=$res['data'][0]['copa_tipo_ot'];
		$query = "  UPDATE  costeo_pago
                    SET     
                        copa_estado = 'APROBADO'
                        ,copa_periodo = $periodo
                        ,copa_observacion = '$observacion'
                    WHERE   copa_id = $copa_id";
        $res = $db->ExecuteQuery($query);
        if( 0==$res['status'] ){
            $db->Rollback();
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
		$db->commit();
		
		$db->startTransaction();
		
        if ("OS"==$actividad) {
            $query = " UPDATE os_servicio_pago osp
                        SET osp.ospa_estado_os = 'PAGADA'
                        WHERE osp.copa_id = $copa_id";
            $res = $db->ExecuteQuery($query);
            if( 0==$res['status'] ){
                $db->Rollback();
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }
        }else{
            $query = " UPDATE mnt_servicio_pago msp
                        SET msp.mnpa_estado_mnt = 'PAGADA'
                        WHERE msp.copa_id = $copa_id";
            $res = $db->ExecuteQuery($query);
            if( 0==$res['status'] ){
                $db->Rollback();
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }
        }
        $query ="  SELECT  copa_monto_capex*IFNULL(copa_tipo_cambio, 1) as sum_capex
                            ,copa_monto_opex*IFNULL(copa_tipo_cambio, 1) as sum_opex
                            ,papr_id_opex
                            ,papr_id_capex
                    FROM costeo_pago
                    WHERE copa_id = $copa_id";
        $res = $db->ExecuteQuery($query);
        if( 0==$res['status'] ){
            $db->Rollback();
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        $sum_capex=$res['data'][0]['sum_capex'];
        $sum_opex=$res['data'][0]['sum_opex'];
        $papr_id_opex=$res['data'][0]['papr_id_opex'];
        $papr_id_capex=$res['data'][0]['papr_id_capex'];

		if ( 0<$papr_id_capex) {
			$query = "UPDATE pago_presupuesto pp
					   SET pp.papr_saldo = pp.papr_monto - (IFNULL(pp.papr_consumido,0) + $sum_capex)
					   WHERE pp.papr_id = $papr_id_capex
					  ";
			$res = $db->ExecuteQuery($query);
			if( 0==$res['status'] ){
				$db->Rollback();
				Flight::json(array("status"=>0, "error"=>$res['error']));
				return;
			}
		}
		if ( 0<$papr_id_opex) {
			$query = "UPDATE pago_presupuesto pp
					   SET pp.papr_saldo = pp.papr_monto - (IFNULL(pp.papr_consumido,0) + $sum_opex)
					   WHERE pp.papr_id = $papr_id_opex
					  ";
			$res = $db->ExecuteQuery($query);
			if( 0==$res['status'] ){
				$db->Rollback();
				Flight::json(array("status"=>0, "error"=>$res['error']));
				return;
			}
		}
		$db->commit();
		$db->startTransaction();
		if ( 0<$papr_id_capex) {
			$query = "UPDATE pago_presupuesto pp
						SET pp.papr_consumido = IFNULL(pp.papr_consumido,0) + $sum_capex
						WHERE pp.papr_id = $papr_id_capex
					";
			$res = $db->ExecuteQuery($query);
			if( 0==$res['status'] ){
				$db->Rollback();
				Flight::json(array("status"=>0, "error"=>$res['error']));
				return;
			}
		}
		if ( 0<$papr_id_opex) {
			$query = "UPDATE pago_presupuesto pp
						SET pp.papr_consumido = IFNULL(pp.papr_consumido,0) + $sum_opex
						WHERE pp.papr_id = $papr_id_opex
					";
			$res = $db->ExecuteQuery($query);
			if( 0==$res['status'] ){
				$db->Rollback();
				Flight::json(array("status"=>0, "error"=>$res['error']));
				return;
			}
		}

		$db->commit();
        Flight::json($out);
    });

    /*Inserta  observacion */
    /*EMB Esta funcion deviera llamarse actualizar Costeo Pago"*/
    Flight::route('GET|POST /contrato/@cont_id:[0-9]+/pago/actualizar/observacion', function($cont_id){
        $out = array();
        $out['status'] = 1;
        $db = new MySQL_Database();
        $pago = array_merge($_POST);
       
        $copa_id='null';
        if(isset($pago['copa_id']) && ""!=$pago['copa_id']){
            $copa_id=$pago['copa_id'];     
        }
        
        $observacion='';
        if(isset($pago['observacion']) && ""!=$pago['observacion']){
            $observacion=$pago['observacion']; 
        }

        $query = " UPDATE  costeo_pago
                    SET     copa_observacion = '$observacion'
                    WHERE   copa_id = $copa_id";
        $res = $db->ExecuteQuery($query);
        if( 0==$res['status'] ){
            $db->Rollback();
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
       Flight::json($out);
    });


    /*Elimina el presupuesto seleccionado */
     Flight::route('POST /contrato/@cont_id:[0-9]+/pago/presupuesto/eliminar', function($cont_id){

        $out = array();
        $out['status'] = 1;
        $db = new MySQL_Database();
        $pago = array_merge($_POST);
        $usuario = $_SESSION['user_id'];
        $papr_id='null';
        if(isset($pago['papr_id']) && ""!=$pago['papr_id']){
            $papr_id=$pago['papr_id'];  
        }
        if(isset($pago['consumido']) && ""!=$pago['consumido']){
            $consumido=$pago['consumido'];  
        }  
        $db->startTransaction();
        $Eliminarpresupuesto = " UPDATE pago_presupuesto
                                SET papr_estado = 'INACTIVO'
                                ,papr_usua_actualizacion = $usuario
                                ,papr_fecha_actualizacion = NOW()
                                where papr_id= $papr_id";
        $res = $db->ExecuteQuery($Eliminarpresupuesto);
        if( 0==$res['status'] ){
            $db->Rollback();
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        /*posiblemente tengamos que recalcular el saldo y el consumido de la linea presupuestaria FAZ*/
        $db->Commit();
        Flight::json($out); 
     });

    /*Actualiza la lista de las ordenes de servicio del periodo seleccionado*/
    Flight::route('POST /contrato/@cont_id:[0-9]+/pago/actividad/@actividad/copa_id/@copa_id_valor/ot/@id_ot/eliminar', function($cont_id,$actividad,$copa_id_valor,$id_ot){

        $out = array();
        $out['status'] = 1;
        $db = new MySQL_Database();
        $pago = array_merge($_POST);

        $monto_total='null';
        if(isset($pago['monto_total']) && ""!=$pago['monto_total']){
            $monto_total=$pago['monto_total'];  
        }

        $capex_ot='null';
        if(isset($pago['capex_ot']) && ""!=$pago['capex_ot']){
            $capex_ot=$pago['capex_ot'];  
        }

        $opex_ot='null';
        if(isset($pago['opex_ot']) && ""!=$pago['opex_ot']){
            $opex_ot=$pago['opex_ot'];  
        }
        $db->startTransaction();
        if ("OS"==$actividad) {
            $query1 =" UPDATE os_servicio_pago osp
                    SET osp.copa_id= NULL
                    ,osp.ospa_seleccion_codigo = NULL
                    WHERE osp.orse_id = $id_ot";
            $res = $db->ExecuteQuery($query1);
            if( 0==$res['status'] ){
                $db->Rollback();
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }
        }else{
            $query2 = " UPDATE mnt_servicio_pago msp
                        SET msp.copa_id= NULL
                        ,msp.mnpa_seleccion_codigo =NULL
                        WHERE msp.mant_id = $id_ot
                      ";
            $res = $db->ExecuteQuery($query2);
            if( 0==$res['status'] ){
                $db->Rollback();
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }
        }
        $query3 = "UPDATE costeo_pago 
                    SET copa_cantidad_total = copa_cantidad_total -1
                    ,copa_monto_capex = copa_monto_capex - $capex_ot
                    ,copa_monto_opex = copa_monto_opex -$opex_ot
                    ,copa_monto_total = copa_monto_total- $monto_total
                    where copa_id = $copa_id_valor
                ";

        $res = $db->ExecuteQuery($query3);
        if( 0==$res['status'] ){
            $db->Rollback();
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        /*posiblemente tengamos que recalcular el saldo y el consumido de la linea presupuestaria FAZ*/
        $db->Commit();

        Flight::json($out); 

    });
    /*se utiliza para actualizar el presupuesto admin*/
    Flight::route('POST /contrato/@cont_id:[0-9]+/pago/presupuesto/actualizar/admin', function($cont_id){

        $out = array();
        $out['status'] = 1;
        $db = new MySQL_Database();
        $pago = array_merge($_POST);

        $papr_id=$pago['papr_id'];
        $empresa=$pago['empresa'];  
        $sociedad=$pago['sociedad'];
        $nombre_servicio=$pago['nombre_servicio'];  
        $papr_tipo=$pago['papr_tipo']; 
        $proveedor=$pago['proveedor']; 
        $monto=$pago['monto'];  

        $monto_total_capex="SELECT ROUND(sum(copa_monto_capex * ifnull(copa_tipo_cambio,1)),2) monto_total_capex
                        FROM costeo_pago 
                        WHERE papr_id_capex = $papr_id";
        $monto_total_capex = $db->ExecuteQuery($monto_total_capex);
        if( 0==$monto_total_capex['status'] ){
            Flight::json(array("status"=>0, "error"=>$monto_total_capex['error']));
            return;
        }
        $monto_capex = $monto_total_capex['data'][0]['monto_total_capex'];

        $monto_total_opex="SELECT ROUND(sum(copa_monto_opex * ifnull(copa_tipo_cambio,1)),2) monto_total_opex
                            FROM costeo_pago WHERE papr_id_opex=$papr_id";
        $monto_total_opex = $db->ExecuteQuery($monto_total_opex);
        if( 0==$monto_total_opex['status'] ){
            Flight::json(array("status"=>0, "error"=>$monto_total_opex['error']));
            return;
        }
        $monto_opex = $monto_total_opex['data'][0]['monto_total_opex'];
        $monto_total_capex_opex = $monto_opex + $monto_capex;

        $monto_consumido="UPDATE  pago_presupuesto
                                SET papr_empresa = '$empresa'
                                ,papr_sociedad =$sociedad
                                ,papr_nombre_servicio = '$nombre_servicio'
                                ,papr_tipo='$papr_tipo'
                                ,papr_proveedor = '$proveedor'
                                ,papr_monto = REPLACE(REPLACE('$monto','.',''),',','.')
                                ,papr_consumido=REPLACE(REPLACE('$monto_total_capex_opex','.',''),',','.')
                                ,papr_saldo = REPLACE(REPLACE('$monto','.',''),',','.') - REPLACE(REPLACE('$monto_total_capex_opex','.',''),',','.')
                        WHERE papr_id = $papr_id
                        AND papr_estado = 'ACTIVO'";
        $monto = $db->ExecuteQuery($monto_consumido);
        if( 0==$monto['status'] ){
            Flight::json(array("status"=>0, "error"=>$monto['error']));
            return;
        }
        Flight::json($out);   
    });

    /*se utiliza para visualizar la data de presupuesto admin*/
    Flight::route('POST /contrato/@cont_id:[0-9]+/pago/presupuesto/lista/admin', function($cont_id){

        $out = array();
        $out['status'] = 1;
        $db = new MySQL_Database();
        $anio =date ("Y"); 
        $query = " SELECT
                    papr_id, 
                    cont_id,
                    papr_empresa empresa,
                    papr_sociedad sociedad,
                    papr_nombre_servicio nombre_servicio,
                    papr_proveedor proveedor,
                    papr_anio anio,
                    papr_monto monto,
                    papr_consumido consumido,
                    papr_saldo saldo,
                    papr_fecha_creacion fecha_creacion,
                    papr_tipo
                    FROM pago_presupuesto
                    WHERE cont_id = $cont_id
                    AND papr_anio = $anio
                    AND papr_estado='ACTIVO'";
        $res = $db->ExecuteQuery($query);
        if( 0==$res['status'] ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        $out['pago_pre_tabla'] = $res['data']; 
        Flight::json($out);   
    });
     
    /*revisar 1
    Se utiliza para visualizar pago_presupuesto_tabla y pago_presupuesto, extrae todos los datos de la tabla */
     Flight::route('POST /contrato/@cont_id:[0-9]+/pago/presupuesto/lista', function($cont_id){

        $out = array();
        $out['status'] = 1;
        $db = new MySQL_Database();
        $anio =date ("Y");
        $query = "SELECT
                    papr_id, 
                    cont_id,
                    papr_empresa empresa,
                    papr_sociedad sociedad,
                    papr_nombre_servicio nombre_servicio,
                    papr_proveedor proveedor,
                    papr_anio anio,
                    papr_monto monto,
                    papr_consumido consumido,
                    papr_saldo saldo,
                    papr_fecha_creacion fecha_creacion,
                    papr_tipo
                  FROM pago_presupuesto
                  WHERE cont_id = $cont_id
                    AND papr_anio = $anio
                    AND papr_estado = 'ACTIVO'";
        $res = $db->ExecuteQuery($query);
        if( 0==$res['status'] ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        $out['pago_pre_tabla'] = $res['data'];
        Flight::json($out);
    });

    /* 
    EMB esto parece que no va que hay que eliminar
    */
    /*Detalle Ordenes de servicio por periodo  al darle click en monto comprometido se visualiza esto */
    Flight::route('GET|POST /pago/detalle_costeo_periodo/contrato/@cont_id:[0-9]+/periodo/@periodo:[0-9]+', function($cont_id,$periodo){
        $moneda =  $GLOBALS['tipo_moneda'];
        $out = array();
        $out['status'] = 1;
        $db = new MySQL_Database();
        
        $query = " SELECT  id_ot
                      , tipo_ot
                      , capex_ot
                      , opex_ot
                      , fec_aprobacion_ot
                      , hem_id
                      , cont_id
                      , periodo_pago_ot
                FROM  (
                    SELECT
                             orse_id  id_ot
                            , 'OS'  tipo_ot
                            , SUM(ospa_monto_capex) capex_ot
                            , SUM(ospa_monto_opex)  opex_ot
                            , ospa_fecha_aprobacion fec_aprobacion_ot
                            , hem_id
                            , cont_id
                            , ospa_periodo_aprobacion_aprobacion periodo_pago_ot
                    FROM  os_servicio_pago
                    WHERE cont_id = $cont_id
                        AND ospa_periodo_aprobacion = $periodo
                        AND copa_id IS NOT  null
                    GROUP BY
                            orse_id
                            ,'OS'
                            ,'$moneda'
                            ,ospa_fecha_aprobacion
                            ,hem_id
                            ,cont_id
                            ,ospa_periodo_aprobacion
                UNION
                    SELECT
                            mant_id
                            ,'MNT'
                            ,mnpa_monto_capex
                            ,mnpa_monto_opex
                            ,mnpa_fecha_aprobacion
                            ,hem_id
                            ,cont_id
                            ,mnpa_periodo_aprobacion periodo_pago_ot
                    FROM  mnt_servicio_pago
                    WHERE cont_id = $cont_id
                    AND mnpa_periodo_aprobacion =$periodo
                    AND copa_id IS NOT  null
                ) pago";
        $res = $db->ExecuteQuery($query);
        if( 0==$res['status'] ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        $out['periodo'] = $periodo;
        $out['pago_os'] = $res['data'];
        Flight::json($out);
    });

    /*muestra costeo pago  */
     Flight::route('GET|POST /contrato/@cont_id:[0-9]+/pago/generar/costeo/@actividad(/@page:[0-9]+)', function($cont_id,$actividad,$page){
        $moneda =  $GLOBALS['tipo_moneda'];
        $db = new MySQL_Database();
        $out = array();
        $results_by_page = 40;
        $out['status'] = 1;
        $page=null;    
        $pago = array_merge($_GET,$_POST);

        /* recupero los filtros a aplicar en la consulta */
        $seleccion_filtros=" ";
        $seleccion_region = "0";
        
        if ("OS"==$actividad || "LMT"==$actividad) {
            if(isset($pago['region']) && "0"!=$pago['region']){
                if(is_array($pago['region'])){
                    $seleccion_region = implode("','", $pago['region']);
                    $seleccion_filtros.=" AND ospa_region in('".$seleccion_region."')";  
                } else {
                    $seleccion_region = $pago['region'];
                    $seleccion_filtros.=" AND ospa_region =('".$seleccion_region."')";
                }
            }
            if(isset($pago['zmovistar']) && "0"!=$pago['zmovistar']){
                if(is_array($pago['zmovistar'])){
                    $seleccion_zmovistar = implode("','",$pago['zmovistar']);
                    $seleccion_filtros.=" AND ospa_zona_movistar in('".$seleccion_zmovistar."')";  
                }else{
                    $seleccion_zmovistar = $pago['zmovistar'];
                    $seleccion_filtros.=" AND ospa_zona_movistar ='".$seleccion_zmovistar."'";    
                }
            }
            if(isset($pago['zcontrato']) && "0"!=$pago['zcontrato']){
                if(is_array($pago['zcontrato'])){
                    $seleccion_zcontrato = implode("','", $pago['zcontrato']);
                    $seleccion_filtros.=" AND ospa_zona_contrato in('".$seleccion_zcontrato ."')";  
                } else {
                    $seleccion_zcontrato = $pago['zcontrato'];
                    $seleccion_filtros.=" AND ospa_zona_contrato = '".$seleccion_zcontrato."'";
                }
            }
            if(isset($pago['zcluster']) && "0"!=$pago['zcluster']){
                if(is_array($pago['zcluster'])){
                    $seleccion_zcluster = implode("','", $pago['zcluster']);
                    $seleccion_filtros.=" AND ospa_zona_cluster in('".$seleccion_zcluster."')";    
                } else {
                    $seleccion_zcluster =  $pago['zcluster'];
                    $seleccion_filtros.=" AND ospa_zona_cluster ='".$seleccion_zcluster."'";   
                }
            }
            if(isset($pago['f_ini_programada']) && "0"!=$pago['f_ini_programada']  && ""!=$pago['f_ini_programada']){
                $fecha_ini=$pago['f_ini_programada'];
                $fecha_ini=date('Y-m-d H:i:s',strtotime($fecha_ini));
                $seleccion_filtros.=" AND ospa_fecha_aprobacion >='".$fecha_ini."'";    
            }
            if(isset($pago['f_fin_programada']) && "0"!=$pago['f_fin_programada'] && ""!=$pago['f_fin_programada']){
                $fecha_fin = $pago['f_fin_programada'];
                $fecha_fin=date('Y-m-d 23:59:59', strtotime($fecha_fin));
                $seleccion_filtros.=" AND ospa_fecha_aprobacion <='".$fecha_fin."'";    
            }
            if(isset($pago['numero_ot']) && "0"!=$pago['numero_ot'] && ""!=$pago['numero_ot']){
                $seleccion_filtros.=" AND orse_id =".$pago['numero_ot']."";    
            }
        }else{
            if(isset($pago['region']) && "0"!=$pago['region']){
                if(is_array($pago['region'])){
                    $seleccion_region = implode("','", $pago['region']);
                    $seleccion_filtros.=" AND mnpa_region in('".$seleccion_region."')";  

                } else {
                $seleccion_region = $pago['region'];
                    $seleccion_filtros.=" AND mnpa_region ='".$seleccion_region."'";  
                }
            }
            if(isset($pago['zmovistar']) && "0"!=$pago['zmovistar']){
                if(is_array($pago['zmovistar'])){
                    $seleccion_zmovistar = implode("','",$pago['zmovistar']);
                    $seleccion_filtros.=" AND mnpa_zona_movistar in('".$seleccion_zmovistar."')";  
                }else{
                    $seleccion_zmovistar = $pago['zmovistar'];
                    $seleccion_filtros.=" AND mnpa_zona_movistar ='".$seleccion_zmovistar ."'";  
                }
            }
            if(isset($pago['zcontrato']) && "0"!=$pago['zcontrato']){
                if(is_array($pago['zcontrato'])){
                    $seleccion_zcontrato = implode("','", $pago['zcontrato']);
                    $seleccion_filtros.=" AND mnpa_zona_contrato in('".$seleccion_zcontrato."')"; 
                }else{
                    $seleccion_zcontrato = $pago['zcontrato'];
                    $seleccion_filtros.="AND mnpa_zona_contrato ='".$seleccion_zcontrato."'"; 
                }
            }
            if(isset($pago['zcluster']) && "0"!=$pago['zcluster']){
                if(is_array($pago['zcluster'])){
                    $seleccion_zcontrato = implode("','", $pago['zcluster']);
                    $seleccion_filtros.=" AND mnpa_zona_cluster in('".$seleccion_zcontrato."')"; 
                }else{
                    $seleccion_zcontrato = $pago['zcluster'];
                    $seleccion_filtros.=" AND mnpa_zona_cluster ='".$seleccion_zcontrato."'"; 
                }
               
            }
            if(isset($pago['f_ini_programada']) && "0"!=$pago['f_ini_programada']  && ""!=$pago['f_ini_programada']){
                $fecha_ini=$pago['f_ini_programada'];
                $fecha_ini=date('Y-m-d H:i:s',strtotime($fecha_ini));
                $seleccion_filtros.=" AND mnpa_fecha_aprobacion>='".$fecha_ini."'";    
            }
            if(isset($pago['f_fin_programada']) && "0"!=$pago['f_fin_programada'] && ""!=$pago['f_fin_programada']){
                $fecha_fin = $pago['f_fin_programada'];
                $fecha_fin=date('Y-m-d 23:59:59', strtotime($fecha_fin));
                $seleccion_filtros.=" AND mnpa_fecha_aprobacion <='".$fecha_fin."'";    
            }
            if(isset($pago['numero_ot']) && "0"!=$pago['numero_ot'] && ""!=$pago['numero_ot']){
                $seleccion_filtros.=" AND mant_id =".$pago['numero_ot']."";    
            }

        }
            
        /* Recuperar el periodo actual+1 para el costeo a ingresar3*/
        $query = "SELECT MAX(copa_periodo) as periodo 
                  FROM costeo_pago
                  WHERE cont_id = $cont_id
                  AND copa_tipo_ot='$actividad'";
        $res = $db->ExecuteQuery($query);
        if ( 0==$res['status'] ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        
        if ( 0<=$res['rows'] ){
            $periodo = $res['data'][0]['periodo'];
            if (null == $periodo){
                $periodo= date('Ym');
            }
        }
        
        $periodoMes = substr($periodo,4,2);
        if(12==$periodoMes){
            $periodoAnyo = intval(substr($periodo, 0, 4));
            $periodoAnyo = $periodoAnyo+1;
            $periodo = $periodoAnyo."01";
        }else{
            $periodo = intval($periodo)+1;
        }

        $periodoAnyo=substr($periodo,0,4);
        $linea_presupuestaria_capex = "SELECT papr_id, 
                                        papr_nombre_servicio papr_nombre_servicio
                                        FROM pago_presupuesto
                                        WHERE cont_id = $cont_id
                                        AND papr_anio = $periodoAnyo
                                        AND papr_estado = 'ACTIVO'
                                        AND papr_tipo in('CAPEX','AMBAS')";
        $lineaCapex = $db->ExecuteQuery($linea_presupuestaria_capex);
        if( 0==$lineaCapex['status'] ){
            Flight::json(array("status"=>0, "error"=>$lineaCapex['error']));
            return;
        }
        $out['linea_presupuestocapex'] = $lineaCapex['data'];  

        $linea_presupuestaria_opex = "SELECT papr_id, 
                                        papr_nombre_servicio papr_nombre_servicio
                                        FROM pago_presupuesto
                                        WHERE cont_id = $cont_id
                                        AND papr_anio = $periodoAnyo
                                        AND papr_estado = 'ACTIVO'
                                        AND papr_tipo in('OPEX','AMBAS') ";
        $lineaOpex = $db->ExecuteQuery($linea_presupuestaria_opex);
        if( 0==$lineaOpex['status'] ){
            Flight::json(array("status"=>0, "error"=>$lineaOpex['error']));
            return;
        }
        
        $out['linea_presupuestoopex'] = $lineaOpex['data'];
        $zona ="SELECT zona_id, zona_nombre 
                                    FROM  zona 
                                    WHERE cont_id= $cont_id 
                                    AND zona_estado = 'ACTIVO' 
                                    AND zona_tipo='CONTRATO'";
                                    
        $res = $db->ExecuteQuery($zona);
        if ($res['status'] == 0){
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
                 
        $out['zona_contrato'] = $res['data'];

        $res = $db->ExecuteQuery("SELECT zona_id, zona_nombre 
                                    FROM  zona 
                                    WHERE cont_id= $cont_id
                                    AND zona_estado = 'ACTIVO' 
                                    AND zona_tipo='MOVISTAR'");
        if ($res['status'] == 0) {
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
        $out['zona_movistar'] = $res['data'];
        
        $res = $db->ExecuteQuery("SELECT zona_id, zona_nombre 
                                    FROM  zona 
                                    WHERE cont_id= $cont_id
                                    AND zona_estado = 'ACTIVO' 
                                    AND zona_tipo='CLUSTER'");
        if ($res['status'] == 0) {
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
        $out['zona_cluster'] = $res['data'];
        
        $res = $db->ExecuteQuery("SELECT regi_id
                                        , regi_nombre 
                                        , CASE WHEN (regi_nombre in ('$seleccion_region')) THEN 'Selected'
                                          ELSE '' END as regi_selected
                                    FROM  contrato c
                                        , pais p
                                        , region r 
                                    WHERE c.cont_id = $cont_id 
                                        AND p.pais_id = c.pais_id
                                        AND r.pais_id = p.pais_id
                                    ORDER BY regi_orden ASC");
        if ($res['status'] == 0) {
            Flight::json(array("status" => 0, "error"=>$res['error']));
            return;
        }
        $out['regiones'] = $res['data'];    

        /*EMB solo de paso, la variable hay que leerla del hb
        $actividad="OS";*/
    
        if("MNT"==$actividad){
            $query = "
                SELECT  SQL_CALC_FOUND_ROWS
                        id_ot
                        , tipo_ot
                        , monto_total
                        , capex_ot
                        , opex_ot
                        , tipo_moneda
                        , fecha_aprobacion_ot
                        , hem_id
                        , cont_id
                        , periodo_pago_ot
                FROM  (
                        SELECT
                            mant_id as id_ot
                           ,'MNT' AS tipo_ot
                           ,0 as monto_total
                           , ROUND(mnpa_monto_capex,2) AS capex_ot
                           , ROUND(mnpa_monto_opex,2) AS opex_ot
                           ,'$moneda' AS tipo_moneda
                           ,mnpa_fecha_aprobacion AS fecha_aprobacion_ot
                           ,hem_id
                           ,cont_id
                           ,mnpa_periodo_aprobacion as periodo_pago_ot
                        FROM  mnt_servicio_pago
                        WHERE cont_id = $cont_id
                        AND copa_id IS null
                        $seleccion_filtros
                        order by id_ot asc
                ) pago ";
                /* EMB hay que arreglar paso de variables*/
                #LIMIT $results_by_page OFFSET ".(($page-1)*$results_by_page);
        }else{
        #if("OS"==$actividad){
                
            $query = "
                SELECT  SQL_CALC_FOUND_ROWS
                        id_ot
                        , tipo_ot
                        , monto_total
                        , capex_ot
                        , opex_ot
                        , tipo_moneda
                        , fecha_aprobacion_ot
                        , hem_id
                        , cont_id
                        , periodo_pago_ot
                FROM  (
                        SELECT
                            orse_id AS id_ot
                            , 'OS' AS tipo_ot
                            , 0 AS monto_total
                            , ROUND(SUM(ospa_monto_capex),2) AS capex_ot
                            , ROUND(SUM(ospa_monto_opex),2) AS opex_ot
                            ,'$moneda' AS tipo_moneda
                            ,ospa_fecha_aprobacion AS fecha_aprobacion_ot
                            ,hem_id
                            ,cont_id
                            ,ospa_periodo_aprobacion periodo_pago_ot
                        FROM  os_servicio_pago
                        WHERE cont_id = $cont_id
                        AND copa_id IS null
                        $seleccion_filtros
                        GROUP BY
                            orse_id
                            , 'OS'
                            ,'$moneda'
                            ,ospa_fecha_aprobacion
                            ,hem_id
                            ,cont_id
                            ,ospa_periodo_aprobacion
                            order by id_ot asc
                    ) pago";
                    /* EMB hay que arreglar paso de variables*/
                    #LIMIT $results_by_page OFFSET ".(($page-1)*$results_by_page);
        }
        $res = $db->ExecuteQuery($query);
        $out['pago_os_periodo'] = $res['data'];
        if( 0==$res['status'] ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        if($page!=null){
            $res = $db->ExecuteQuery("SELECT FOUND_ROWS() as total");
            if(!$res['status']){
                return $res;
            }
            $out['pagina'] = intval($page);
            $out['total'] = intval($res['data'][0]['total']);
            $out['paginas'] = ceil($out['total']/$results_by_page);
        }
        $out['periodo'] = $periodo;
        $out['actividad'] = $actividad;
        Flight::json($out);
    });

    /*detalle costeo generar filtro */
    Flight::route('GET|POST /contrato/@cont_id:[0-9]+/pago/costeo/filtro/@actividad/actividad', function($cont_id,$actividad){

        $out = array();
        $out['status'] = 1;       
        Flight::json($out);
    });
    /*detalle  ingreso hem y derivada*/                       
    Flight::route('GET|POST /contrato/@cont_id:[0-9]+/pago/ingreso/hemyderivada/@copa_id/copa_id(/@page:[0-9]+)', function($cont_id,$copa_id){
    
        $results_by_page = 40;
        $out = array();
        $out['status'] = 1;
        $db = new MySQL_Database();
        $pago = array_merge($_GET,$_POST);
        $page=null;
        /* Recuperar el periodo actual+1 para el costeo a ingresar3*/
        $query = "SELECT copa_periodo as periodo 
                         ,papr_id_capex
                         ,papr_id_opex
                         ,copa_tipo_ot
                         ,copa_monto_capex
                         ,copa_monto_opex
                         ,copa_monto_total
                  FROM costeo_pago
                  WHERE cont_id = $cont_id
                  AND copa_id=$copa_id";
        $res = $db->ExecuteQuery($query);
        if ( 0==$res['status'] ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        $copa_monto_capex= $res['data'][0]['copa_monto_capex'];
        $copa_monto_opex= $res['data'][0]['copa_monto_opex'];
        $copa_monto_total= $res['data'][0]['copa_monto_total'];

        $out['copa_monto_capex']=$copa_monto_capex;
        $out['copa_monto_opex']=$copa_monto_opex;
        $out['copa_monto_total']=$copa_monto_total;
        $periodo = $res['data'][0]['periodo'];
        $actividad=$res['data'][0]['copa_tipo_ot'];
        /* recupero los filtros a aplicar en la consulta */
        $seleccion_filtros=" ";
        $seleccion_region = "0";
        if ("OS"==$actividad || "LMT"==$actividad) {
            if(isset($pago['region']) && "0"!=$pago['region']){
                if(is_array($pago['region'])){
                    $seleccion_region = implode("','", $pago['region']);
                    $seleccion_filtros.=" AND ospa_region in('".$seleccion_region."')";  
                }else{
                    $seleccion_region = $pago['region'];
                    $seleccion_filtros.=" AND ospa_region ='".$seleccion_region."'";
                }
            }
            if(isset($pago['zmovistar']) && "0"!=$pago['zmovistar']){
                if(is_array($pago['zmovistar'])){
                    $seleccion_zmovistar = implode("','", $pago['zmovistar']);
                    $seleccion_filtros.=" AND ospa_zona_movistar in('".$seleccion_zmovistar ."') ";    
                }else{
                    $seleccion_zmovistar = $pago['zmovistar'];
                    $seleccion_filtros.=" AND ospa_zona_movistar ='".$seleccion_zmovistar ."' "; 
                }
            }
            if(isset($pago['zcontrato']) && "0"!=$pago['zcontrato']){
                if(is_array($pago['zcontrato'])){
                    $seleccion_zcontrato = implode("','", $pago['zcontrato']);
                    $seleccion_filtros.=" AND ospa_zona_contrato in('".$seleccion_zcontrato."') ";
                }else{
                    $seleccion_zcontrato =  $pago['zcontrato'];
                    $seleccion_filtros.=" AND ospa_zona_contrato ='".$seleccion_zcontrato."' ";
                }    
            }
            if(isset($pago['zcluster']) && "0"!=$pago['zcluster']){
                if(is_array($pago['zcluster'])){
                    $seleccion_zcluster = implode("','", $pago['zcluster']);
                    $seleccion_filtros.=" AND ospa_zona_cluster in('".$seleccion_zcluster."')";  
                }else{
                    $seleccion_zcluster =  $pago['zcluster'];
                    $seleccion_filtros.=" AND ospa_zona_cluster ='".$seleccion_zcluster."'";
                }  
            }
            if(isset($pago['numero_ot']) && "0"!=$pago['numero_ot'] && ""!=$pago['numero_ot']){
                $seleccion_filtros.=" AND orse_id =".$pago['numero_ot']."";    
            }
            if(isset($pago['lpresupustaria']) && "0"!=$pago['lpresupustaria']){
                if(is_array($pago['lpresupustaria'])){
                    $lpresupustaria = implode("','", $pago['lpresupustaria']);
                    $seleccion_filtros.=" AND ospa_seleccion_codigo in('".$lpresupustaria."')";  
                }else{
                $lpresupustaria = $pago['lpresupustaria'];
                    $seleccion_filtros.=" AND ospa_seleccion_codigo ='".$lpresupustaria."'";
                }
            }


        }else{
            if(isset($pago['region']) && "0"!=$pago['region']){
                if(is_array($pago['region'])){
                    $seleccion_region = implode("','", $pago['region']);
                    $seleccion_filtros.=" AND mnpa_region in('". $seleccion_region."') ";  
                }else{
                    $seleccion_region = $pago['region'];
                    $seleccion_filtros.=" AND mnpa_region ='".$seleccion_region."' ";  
                }
            }
            if(isset($pago['zmovistar']) && "0"!=$pago['zmovistar']){
                if(is_array($pago['zmovistar'])){
                    $seleccion_zmovistar= implode("','", $pago['zmovistar']);
                    $seleccion_filtros.=" AND mnpa_zona_movistar in('".$seleccion_zmovistar."') ";  
                }else{
                    $seleccion_zmovistar = $pago['zmovistar'];
                    $seleccion_filtros.=" AND mnpa_zona_movistar ='".$seleccion_zmovistar."' ";  
                }
            }
            if(isset($pago['zcontrato']) && "0"!=$pago['zcontrato']){
                if(is_array($pago['zcontrato'])){
                    $seleccion_zcontrato = implode("','", $pago['zcontrato']);
                    $seleccion_filtros.=" AND mnpa_zona_contrato in('".$seleccion_zcontrato."')"; 
                }else{
                    $seleccion_zcontrato =  $pago['zcontrato'];
                    $seleccion_filtros.=" AND mnpa_zona_contrato ='".$seleccion_zcontrato."' "; 
                }
            }
            if(isset($pago['zcluster']) && "0"!=$pago['zcluster']){
                if(is_array($pago['zcluster'])){
                    $seleccion_zcontrato = implode("','", $pago['zcluster']);
                    $seleccion_filtros.=" AND mnpa_zona_cluster in('".$seleccion_zcontrato."')"; 
                }else{
                    $seleccion_zcontrato = $pago['zcluster'];
                    $seleccion_filtros.=" AND mnpa_zona_cluster ='".$seleccion_zcontrato."'"; 
                }
            }
           
            if(isset($pago['numero_ot']) && "0"!=$pago['numero_ot'] && ""!=$pago['numero_ot']){
                $seleccion_filtros.=" AND mant_id =".$pago['numero_ot']."";    
            }
            if(isset($pago['lpresupustaria']) && "0"!=$pago['lpresupustaria']){
                 if(is_array($pago['lpresupustaria']))
                {
                    $lpresupustaria = implode("','", $pago['lpresupustaria']);
                    $seleccion_filtros.=" AND mnpa_seleccion_codigo in('".$lpresupustaria."')";  
                }else{
                $lpresupustaria = $pago['lpresupustaria'];
                    $seleccion_filtros.=" AND mnpa_seleccion_codigo ='".$lpresupustaria."'";
                }
            }
        }
        if("OS"==$actividad || "LMT"==$actividad){       
            $query = "
                  SELECT  SQL_CALC_FOUND_ROWS
                    id
                    ,id_ot
                    ,monto
                    ,desc_item_lpu
                    ,hem_id
                    ,derivada_id
                    ,tipo_cambio 
                    ,observacion
                    ,copa_id
                FROM  (
                        SELECT
                            ospa_id AS id
                            ,orse_id AS id_ot
                            ,ospa_monto_capex as monto
                            ,ospa_desc_item_lpu as desc_item_lpu
                            ,hem_id
                            ,derivada_id
                            ,ospa_tipo_cambio  as tipo_cambio
                            ,ospa_observacion as observacion
                            ,copa_id 
                        FROM  os_servicio_pago
                        WHERE cont_id = $cont_id
                        AND copa_id = $copa_id
                         $seleccion_filtros
                    ) pago order by id_ot asc;" ;
                    /* EMB hay que arreglar paso de variables*/
                    #LIMIT $results_by_page OFFSET ".(($page-1)*$results_by_page);
        }
        else {
            $query = "
                 SELECT  SQL_CALC_FOUND_ROWS
                    id
                    ,id_ot
                    ,monto
                    ,desc_item_lpu
                    ,hem_id
                    ,derivada_id
                    ,tipo_cambio 
                    ,observacion
                    ,copa_id
                FROM  (
                        SELECT
                            mnpa_id as id
                            ,mant_id AS id_ot
                            ,mnpa_monto_capex as monto
                            ,mnpa_desc_item_lpu as desc_item_lpu
                            ,hem_id
                            ,derivada_id
                            ,mnpa_tipo_cambio as tipo_cambio
                            ,mnpa_observacion as observacion
                            ,copa_id
                        FROM  mnt_servicio_pago
                        WHERE cont_id = $cont_id
                        AND copa_id = $copa_id
                         $seleccion_filtros
                    ) pago order by id_ot asc; ";
                /* EMB hay que arreglar paso de variables*/
                #LIMIT $results_by_page OFFSET ".(($page-1)*$results_by_page);
        }
        $res = $db->ExecuteQuery($query);
        $out['pago_os_periodo'] = $res['data'];
        if( 0==$res['status'] ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }

        if($page!=null){
            $res = $db->ExecuteQuery("SELECT FOUND_ROWS() as total");
            if(!$res['status']){
                return $res;
            }
            $out['pagina'] = intval($page);
            $out['total'] = intval($res['data'][0]['total']);
            $out['paginas'] = ceil($out['total']/$results_by_page);
        }
        $out['periodo'] = $periodo;
        $out['copa_id']=$copa_id;
        $out['actividad'] = $actividad;
        
        
        
        Flight::json($out);
    });


    /*detalle costeo generar costeo por actividad filtro */
    Flight::route('GET|POST /contrato/@cont_id:[0-9]+/pago/generar/costeo/filtro/@actividad/actividad/periodo/@periodo', function($cont_id,$actividad,$periodo){
        $moneda =  $GLOBALS['tipo_moneda'];
        $results_by_page = 40;
        $out = array();
        $out['status'] = 1;
        $db = new MySQL_Database();
        $page=null;

        $query = "SELECT copa_periodo as periodo 
                        ,copa_id
                        ,papr_id_capex
                        ,papr_id_opex
                        ,copa_tipo_ot
                        ,copa_monto_capex
                        ,copa_monto_opex
                        ,copa_monto_total
                  FROM costeo_pago
                  WHERE cont_id = $cont_id
                  AND copa_periodo=$periodo
                  AND copa_tipo_ot = '$actividad'";

        $res = $db->ExecuteQuery($query);
        if ( 0==$res['status'] ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }

        $copa_monto_capex= $res['data'][0]['copa_monto_capex'];
        $copa_monto_opex= $res['data'][0]['copa_monto_opex'];
        $copa_monto_total= $res['data'][0]['copa_monto_total'];
        $copa_id= $res['data'][0]['copa_id'];
        $periodo = $res['data'][0]['periodo'];
        $periodoAnyo = substr($periodo, 0, 4);
        $linea_seleccionada_capex =$res['data'][0]['papr_id_capex'];
        $linea_seleccionada_opex =$res['data'][0]['papr_id_opex'];
        $actividad=$res['data'][0]['copa_tipo_ot'];
        $linea_presupuestaria_capex = "SELECT papr_id, 
                                        papr_nombre_servicio papr_nombre_servicio,
                                        CASE WHEN (papr_id='$linea_seleccionada_capex') THEN 'selected'
                                        ELSE '' END papr_id_seleccionada
                                 FROM pago_presupuesto
                                 WHERE cont_id = $cont_id
                                 AND papr_anio = $periodoAnyo
                                 AND papr_tipo in ('CAPEX', 'AMBAS')
                                 AND papr_estado = 'ACTIVO'";
        $lineaCapex = $db->ExecuteQuery($linea_presupuestaria_capex);
        if( 0==$lineaCapex['status'] ){
            Flight::json(array("status"=>0, "error"=>$lineaCapex['error']));
            return;
        }
        $out['linea_presupuestocapex'] = $lineaCapex['data'];
        $linea_presupuestaria_opex = "SELECT papr_id, 
                                        papr_nombre_servicio,
                                        CASE WHEN (papr_id='$linea_seleccionada_opex') THEN 'selected'
                                        ELSE '' END papr_id_seleccionada
                                 FROM pago_presupuesto
                                 WHERE cont_id = $cont_id
                                 AND papr_anio = $periodoAnyo
                                 AND papr_tipo in ('OPEX', 'AMBAS')
                                 AND papr_estado = 'ACTIVO' ";
        $lineaopex = $db->ExecuteQuery($linea_presupuestaria_opex);
        if( 0==$lineaopex['status'] ){
            Flight::json(array("status"=>0, "error"=>$lineaopex['error']));
            return;
        } 

        $out['linea_presupuestoopex'] = $lineaopex['data'];


        if('OS'==$actividad || 'LMT'==$actividad)
        {
            $orden_servicio= "SELECT  SQL_CALC_FOUND_ROWS
                                 precio_lpu 
                                ,cantidad_lpu 
                                ,actividad_ot
                                ,orse_id 
                                , id_ot
                                , tipo_ot
                                , monto_total_capex_opex
                                , capex_ot
                                , checkedCapex
                                , opex_ot
                                , checkedOpex
                                , tipo_moneda
                                , fecha_aprobacion_ot
                                , hem_id
                                , cont_id
                                , periodo_pago_ot
                                ,checkedPenalidad
                    FROM  (
                            SELECT
                                 ospa_valor_lpu AS precio_lpu   
                                ,ospa_cant_lpu AS cantidad_lpu  
                                ,ospa_desc_item_lpu AS actividad_ot
                                ,IF (ospa_penalidad='SI', 'Checked', '') AS checkedPenalidad
                                ,orse_id
                                , ospa_id AS id_ot
                                , 'OS' AS tipo_ot
                                /*, TRUNCATE(ospa_monto_capex+ospa_monto_opex,3) AS monto_total*/
                                , IF (ospa_seleccion_codigo='CAPEX', ROUND(ospa_monto_capex,2), IF (ospa_seleccion_codigo='OPEX',ROUND(ospa_monto_opex,2), 0 ) ) AS monto_total_capex_opex
                                , ROUND(ospa_monto_capex,2) AS capex_ot
                                , IF (ospa_seleccion_codigo='CAPEX', 'Checked', '') AS checkedCapex
                                , ROUND(ospa_monto_opex,2) AS opex_ot
                                , IF (ospa_seleccion_codigo='OPEX', 'Checked', '') AS checkedOpex
                                ,'$moneda' AS tipo_moneda
                                ,ospa_fecha_aprobacion AS fecha_aprobacion_ot
                                ,hem_id
                                ,cont_id
                                ,ospa_periodo_aprobacion periodo_pago_ot
                            FROM  os_servicio_pago
                            WHERE cont_id = $cont_id
                            AND copa_id = $copa_id
                          
                            /*UNION ALL
                            SELECT
                                orse_id AS id_ot
                                , 'OS' AS tipo_ot
                                , 0 AS monto_total
                                , TRUNCATE(ospa_monto_capex,3) AS capex_ot
                                , '' AS checkedCapex
                                , TRUNCATE(ospa_monto_opex,3) AS opex_ot
                                , '' AS checkedOpex
                                ,'$moneda' AS tipo_moneda
                                ,ospa_fecha_aprobacion AS fecha_aprobacion_ot
                                ,hem_id
                                ,cont_id
                                ,ospa_periodo_aprobacion periodo_pago_ot
                            FROM  os_servicio_pago
                            WHERE cont_id =$cont_id
                            AND copa_id IS null*/
                        ) pago
            ";
        }else {
            $orden_servicio= "SELECT  SQL_CALC_FOUND_ROWS
                             precio_lpu 
                            ,cantidad_lpu 
                            ,actividad_ot
                            ,orse_id
                            , id_ot
                            , tipo_ot
                            , monto_total_capex_opex
                            , capex_ot
                            , checkedCapex
                            , opex_ot
                            , checkedOpex
                            , tipo_moneda
                            , fecha_aprobacion_ot
                            , hem_id
                            , cont_id
                            , periodo_pago_ot
                            ,checkedPenalidad
                FROM  (
                        SELECT
                             mnpa_valor_lpu AS precio_lpu   
                            ,mnpa_cant_lpu AS cantidad_lpu  
                            ,mnpa_desc_item_lpu AS actividad_ot
                            ,IF (mnpa_penalidad='SI', 'Checked', '') AS checkedPenalidad
                            ,mant_id AS orse_id
                            ,mnpa_id AS id_ot
                            , 'MNT' AS tipo_ot
                            /*, TRUNCATE(ospa_monto_capex+ospa_monto_opex,3) AS monto_total*/
                            , IF (mnpa_seleccion_codigo='CAPEX', ROUND(mnpa_monto_capex,2), IF (mnpa_seleccion_codigo='OPEX',ROUND(mnpa_monto_opex,2), 0 ) ) AS monto_total_capex_opex
                            , ROUND(mnpa_monto_capex,2) AS capex_ot
                            , IF (mnpa_seleccion_codigo='CAPEX', 'Checked', '') AS checkedCapex
                            , ROUND(mnpa_monto_opex,2) AS opex_ot
                            , IF (mnpa_seleccion_codigo='OPEX', 'Checked', '') AS checkedOpex
                            ,'$moneda' AS tipo_moneda
                            ,mnpa_fecha_aprobacion AS fecha_aprobacion_ot
                            ,hem_id
                            ,cont_id
                            ,mnpa_periodo_aprobacion periodo_pago_ot
                        FROM  mnt_servicio_pago
                        WHERE cont_id = $cont_id
                        and copa_id =$copa_id
                       
                       
                    ) pago";
        }
        $res = $db->ExecuteQuery($orden_servicio);
        $out['pago_os_periodo'] = $res['data'];
        if( 0==$res['status'] ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        if($page!=null){
            $res = $db->ExecuteQuery("SELECT FOUND_ROWS() as total");
            if(!$res['status']){
                return $res;
            }
            $out['pagina'] = intval($page);
            $out['total'] = intval($res['data'][0]['total']);
            $out['paginas'] = ceil($out['total']/$results_by_page);
        }
        $out['periodo'] = $periodo;
        $out['actividad'] = $actividad;
        $out['copa_monto_capex']=$copa_monto_capex;
        $out['copa_monto_opex']=$copa_monto_opex;
        $out['copa_monto_total']=$copa_monto_total;
        $out['copa_id']=$copa_id;
        Flight::json($out);
    });

    /* EMB - Utilizar para editar */
    /*detalle costeo generar filtro */                          
    Flight::route('GET|POST /contrato/@cont_id:[0-9]+/pago/costeo/editar/generar/@copa_id/copa_id(/@page:[0-9]+)', function($cont_id,$copa_id){
        $moneda =  $GLOBALS['tipo_moneda'];
        $results_by_page = 40;
        $out = array();
        $out['status'] = 1;
        $db = new MySQL_Database();
        $pago = array_merge($_GET,$_POST);
        $page=null;
        /* Recuperar el periodo actual+1 para el costeo a ingresar3*/
        $query = "SELECT copa_periodo as periodo 
                         ,papr_id_capex
                         ,papr_id_opex
                         ,copa_tipo_ot
                         ,copa_monto_capex
                         ,copa_monto_opex
                         ,copa_monto_total
                  FROM costeo_pago
                  WHERE cont_id = $cont_id
                  AND copa_id=$copa_id";
        $res = $db->ExecuteQuery($query);
        if ( 0==$res['status'] ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        $copa_monto_capex= $res['data'][0]['copa_monto_capex'];
        $copa_monto_opex= $res['data'][0]['copa_monto_opex'];
        $copa_monto_total= $res['data'][0]['copa_monto_total'];

        $out['copa_monto_capex']=$copa_monto_capex;
        $out['copa_monto_opex']=$copa_monto_opex;
        $out['copa_monto_total']=$copa_monto_total;

        $periodo = $res['data'][0]['periodo'];
        $periodoAnyo = substr($periodo, 0, 4);
        $linea_seleccionada_capex =$res['data'][0]['papr_id_capex'];
        $linea_seleccionada_opex =$res['data'][0]['papr_id_opex'];
        
        $actividad=$res['data'][0]['copa_tipo_ot'];


        /* recupero los filtros a aplicar en la consulta */
        $seleccion_filtros=" ";
        $seleccion_region = "0";
        
        if ("OS"==$actividad || "LMT"==$actividad) {
            if(isset($pago['region']) && "0"!=$pago['region']){
                if(is_array($pago['region'])){
                    $seleccion_region = implode("','", $pago['region']);
                    $seleccion_filtros.=" AND ospa_region in('".$seleccion_region."')";  
                }else{
                $seleccion_region = $pago['region'];
                    $seleccion_filtros.=" AND ospa_region ='".$seleccion_region."'";
                }
            }
            if(isset($pago['zmovistar']) && "0"!=$pago['zmovistar']){
                if(is_array($pago['zmovistar'])){
                    $seleccion_zmovistar = implode("','", $pago['zmovistar']);
                    $seleccion_filtros.=" AND ospa_zona_movistar in('".$seleccion_zmovistar ."') ";    
                }else{
                    $seleccion_zmovistar = $pago['zmovistar'];
                    $seleccion_filtros.=" AND ospa_zona_movistar ='".$seleccion_zmovistar ."' "; 
                }
            }
            if(isset($pago['zcontrato']) && "0"!=$pago['zcontrato']){
                if(is_array($pago['zcontrato'])){
                    $seleccion_zcontrato = implode("','", $pago['zcontrato']);
                    $seleccion_filtros.=" AND ospa_zona_contrato in('".$seleccion_zcontrato."') ";
                }else{
                    $seleccion_zcontrato =  $pago['zcontrato'];
                    $seleccion_filtros.=" AND ospa_zona_contrato ='".$seleccion_zcontrato."' ";
                }    
            }
            if(isset($pago['zcluster']) && "0"!=$pago['zcluster']){
                if(is_array($pago['zcluster'])){
                    $seleccion_zcluster = implode("','", $pago['zcluster']);
                    $seleccion_filtros.=" AND ospa_zona_cluster in('".$seleccion_zcluster."')";  
                }else{
                    $seleccion_zcluster =  $pago['zcluster'];
                    $seleccion_filtros.=" AND ospa_zona_cluster ='".$seleccion_zcluster."'";
                }  
            }
            if(isset($pago['f_ini_programada']) && "0"!=$pago['f_ini_programada']  && ""!=$pago['f_ini_programada']){
                $fecha_ini=$pago['f_ini_programada'];
                $fecha_ini=date('Y-m-d H:i:s',strtotime($fecha_ini));
                $seleccion_filtros.=" AND ospa_fecha_aprobacion >='".$fecha_ini."'";    
            }
            if(isset($pago['f_fin_programada']) && "0"!=$pago['f_fin_programada'] && ""!=$pago['f_fin_programada']){
                $fecha_fin = $pago['f_fin_programada'];
                $fecha_fin=date('Y-m-d 23:59:59', strtotime($fecha_fin));
                $seleccion_filtros.=" AND ospa_fecha_aprobacion <='".$fecha_fin."'";    
            }
            if(isset($pago['numero_ot']) && "0"!=$pago['numero_ot'] && ""!=$pago['numero_ot']){
                $seleccion_filtros.=" AND orse_id =".$pago['numero_ot']."";    
            }
        }else{
            if(isset($pago['region']) && "0"!=$pago['region']){
                if(is_array($pago['region'])){
                    $seleccion_region = implode("','", $pago['region']);
                    $seleccion_filtros.=" AND mnpa_region in('". $seleccion_region."') ";  
                }else{
                    $seleccion_region = $pago['region'];
                    $seleccion_filtros.=" AND mnpa_region ='".$seleccion_region."' ";  
                }
            }
            if(isset($pago['zmovistar']) && "0"!=$pago['zmovistar']){
                if(is_array($pago['zmovistar'])){
                    $seleccion_zmovistar= implode("','", $pago['zmovistar']);
                    $seleccion_filtros.=" AND mnpa_zona_movistar in('".$seleccion_zmovistar."') ";  
                }else{
                    $seleccion_zmovistar = $pago['zmovistar'];
                    $seleccion_filtros.=" AND mnpa_zona_movistar ='".$seleccion_zmovistar."' ";  
                }
            }
            if(isset($pago['zcontrato']) && "0"!=$pago['zcontrato']){
                if(is_array($pago['zcontrato'])){
                    $seleccion_zcontrato = implode("','", $pago['zcontrato']);
                    $seleccion_filtros.=" AND mnpa_zona_contrato in('".$seleccion_zcontrato."')"; 
                }else{
                    $seleccion_zcontrato =  $pago['zcontrato'];
                    $seleccion_filtros.=" AND mnpa_zona_contrato ='".$seleccion_zcontrato."' "; 
                }
            }
            if(isset($pago['zcluster']) && "0"!=$pago['zcluster']){
                if(is_array($pago['zcluster'])){
                    $seleccion_zcontrato = implode("','", $pago['zcluster']);
                    $seleccion_filtros.=" AND mnpa_zona_cluster in('".$seleccion_zcontrato."')"; 
                }else{
                    $seleccion_zcontrato = $pago['zcluster'];
                    $seleccion_filtros.=" AND mnpa_zona_cluster ='".$seleccion_zcontrato."'"; 
                }
            }
            if(isset($pago['f_ini_programada']) && "0"!=$pago['f_ini_programada']  && ""!=$pago['f_ini_programada']){
                $fecha_ini=$pago['f_ini_programada'];
                $fecha_ini=date('Y-m-d H:i:s',strtotime($fecha_ini));
               
                $seleccion_filtros.=" AND mnpa_fecha_aprobacion >='".$fecha_ini."'";    
            }
            if(isset($pago['f_fin_programada']) && "0"!=$pago['f_fin_programada'] && ""!=$pago['f_fin_programada']){
                $fecha_fin = $pago['f_fin_programada'];
                $fecha_fin=date('Y-m-d 23:59:59', strtotime($fecha_fin));
                $seleccion_filtros.=" AND mnpa_fecha_aprobacion <='".$fecha_fin."'";    
            }
            if(isset($pago['numero_ot']) && "0"!=$pago['numero_ot'] && ""!=$pago['numero_ot']){
                $seleccion_filtros.=" AND mant_id =".$pago['numero_ot']."";    
            }
        }
        
        $linea_presupuestaria_capex = "SELECT papr_id, 
                                        papr_nombre_servicio papr_nombre_servicio,
                                        CASE WHEN (papr_id='$linea_seleccionada_capex') THEN 'selected'
                                        ELSE '' END papr_id_seleccionada
                                 FROM pago_presupuesto
                                 WHERE cont_id = $cont_id
                                 AND papr_anio = $periodoAnyo
                                 AND papr_estado = 'ACTIVO'
                                 AND papr_tipo in ('CAPEX', 'AMBAS')";
        $lineaCapex = $db->ExecuteQuery($linea_presupuestaria_capex);
        if( 0==$lineaCapex['status'] ){
            Flight::json(array("status"=>0, "error"=>$lineaCapex['error']));
            return;
        }
        $out['linea_presupuestocapex'] = $lineaCapex['data'];
        $linea_presupuestaria_opex = "SELECT papr_id, 
                                        papr_nombre_servicio papr_nombre_servicio,
                                        CASE WHEN (papr_id='$linea_seleccionada_opex') THEN 'selected'
                                        ELSE '' END papr_id_seleccionada
                                 FROM pago_presupuesto
                                 WHERE cont_id = $cont_id
                                 AND papr_anio = $periodoAnyo
                                 AND papr_estado = 'ACTIVO'
                                 AND papr_tipo in ('OPEX', 'AMBAS')
                                ";
        $lineaopex = $db->ExecuteQuery($linea_presupuestaria_opex);
        if( 0==$lineaopex['status'] ){
            Flight::json(array("status"=>0, "error"=>$lineaopex['error']));
            return;
        }
        $out['linea_presupuestoopex'] = $lineaopex['data'];
        if("OS"==$actividad || "LMT"==$actividad){
            $query = "
                SELECT  SQL_CALC_FOUND_ROWS
                        id_ot
                        , tipo_ot
                        , monto_total
                        , capex_ot
                        , checkedCapex
                        , opex_ot
                        , checkedOpex
                        , tipo_moneda
                        , fecha_aprobacion_ot
                        , hem_id
                        , cont_id
                        , periodo_pago_ot
                FROM  (
                        SELECT
                            orse_id AS id_ot
                            , 'OS' AS tipo_ot
                            , ROUND(SUM(ospa_monto_capex+ospa_monto_opex),2) AS monto_total
                            , ROUND(SUM(ospa_monto_capex),2) AS capex_ot
                            , IF (ospa_seleccion_codigo='CAPEX', 'Checked', '') AS checkedCapex
                            , ROUND(SUM(ospa_monto_opex),2) AS opex_ot
                            , IF (ospa_seleccion_codigo='OPEX', 'Checked', '') AS checkedOpex
                            ,'$moneda' AS tipo_moneda
                            ,ospa_fecha_aprobacion AS fecha_aprobacion_ot
                            ,hem_id
                            ,cont_id
                            ,ospa_periodo_aprobacion periodo_pago_ot
                        FROM  os_servicio_pago
                        WHERE cont_id = $cont_id
                        AND copa_id = $copa_id
                        $seleccion_filtros
                        GROUP BY
                            orse_id
                            , 'OS'
                            ,'$moneda'
                            ,ospa_fecha_aprobacion
                            ,hem_id
                            ,cont_id
                            ,ospa_periodo_aprobacion
                        UNION ALL
                        SELECT
                            orse_id AS id_ot
                            , 'OS' AS tipo_ot
                            , 0 AS monto_total
                            , ROUND(SUM(ospa_monto_capex),2) AS capex_ot
                            , '' AS checkedCapex
                            , ROUND(SUM(ospa_monto_opex),2) AS opex_ot
                            , '' AS checkedOpex
                            ,'$moneda' AS tipo_moneda
                            ,ospa_fecha_aprobacion AS fecha_aprobacion_ot
                            ,hem_id
                            ,cont_id
                            ,ospa_periodo_aprobacion periodo_pago_ot
                        FROM  os_servicio_pago
                        WHERE cont_id = $cont_id
                        AND copa_id IS null
                        $seleccion_filtros
                        GROUP BY
                            orse_id
                            , 'OS'
                            ,'$moneda'
                            ,ospa_fecha_aprobacion
                            ,hem_id
                            ,cont_id
                            ,ospa_periodo_aprobacion
                    ) pago order by id_ot asc" ;
                    /* EMB hay que arreglar paso de variables*/
                    #LIMIT $results_by_page OFFSET ".(($page-1)*$results_by_page);
        }
        else {
            $query = "
                SELECT  SQL_CALC_FOUND_ROWS
                        id_ot
                        , tipo_ot
                        , monto_total
                        , capex_ot
                        , checkedCapex
                        , opex_ot
                        , checkedOpex
                        , tipo_moneda
                        , fecha_aprobacion_ot
                        , hem_id
                        , cont_id
                        , periodo_pago_ot
                FROM  (
                        SELECT
                            mant_id as id_ot
                           ,'MNT' AS tipo_ot
                           ,ROUND(IF (mnpa_seleccion_codigo= 'OPEX',(mnpa_monto_opex),(mnpa_monto_capex)) ,2) AS monto_total
                           ,ROUND(mnpa_monto_capex,2) AS capex_ot
                           ,IF (mnpa_seleccion_codigo='CAPEX', 'Checked', '') AS checkedCapex
                           ,ROUND(mnpa_monto_opex,2) AS opex_ot
                           ,IF (mnpa_seleccion_codigo='OPEX', 'Checked', '') AS checkedOpex
                           ,'$moneda' AS tipo_moneda
                           ,mnpa_fecha_aprobacion AS fecha_aprobacion_ot
                           ,hem_id
                           ,cont_id
                           ,mnpa_periodo_aprobacion as periodo_pago_ot
                        FROM  mnt_servicio_pago
                        WHERE cont_id = $cont_id
                        AND copa_id = $copa_id
                        $seleccion_filtros
                        UNION ALL
                        SELECT
                            mant_id as id_ot
                           ,'MNT' AS tipo_ot
                           ,0 as monto_total
                           ,ROUND(mnpa_monto_capex,2) AS capex_ot
                           ,'' AS checkedCapex
                           ,ROUND(mnpa_monto_opex,2)AS opex_ot
                           , '' AS checkedOpex
                           ,'$moneda' AS tipo_moneda
                           ,mnpa_fecha_aprobacion AS fecha_aprobacion_ot
                           ,hem_id
                           ,cont_id
                           ,mnpa_periodo_aprobacion as periodo_pago_ot
                        FROM  mnt_servicio_pago
                        WHERE cont_id = $cont_id
                        AND copa_id IS null
                        $seleccion_filtros
                ) pago  order by id_ot asc  ";
                /* EMB hay que arreglar paso de variables*/
                #LIMIT $results_by_page OFFSET ".(($page-1)*$results_by_page);
        }
        $res = $db->ExecuteQuery($query);
        $out['pago_os_periodo'] = $res['data'];
        if( 0==$res['status'] ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }

        if($page!=null){
            $res = $db->ExecuteQuery("SELECT FOUND_ROWS() as total");
            if(!$res['status']){
                return $res;
            }
            $out['pagina'] = intval($page);
            $out['total'] = intval($res['data'][0]['total']);
            $out['paginas'] = ceil($out['total']/$results_by_page);
        }
        $out['periodo'] = $periodo;
        $out['actividad'] = $actividad;
        Flight::json($out);
    });

    /*
    Descargar a excel  Resumen presupuesto
    */
    Flight::route('GET|POST /contrato/@cont_id:[0-9]+/pago/detalle/resumen/descargar', function($cont_id){
        setlocale(LC_ALL,"es_ES");
        $db = new MySQL_Database();
        $query ="  SELECT
                        papr_anio AS A�o
                        ,papr_empresa AS Empresa
                        ,papr_sociedad  AS Sociedad
                        ,papr_nombre_servicio  AS Nombre_Servicio
                        ,papr_tipo AS Tipo_Linea
                        ,papr_proveedor AS Poveedor
                        ,papr_monto AS CLP
                        ,papr_consumido AS Consumido
                        ,papr_saldo  AS Saldo 
                    FROM pago_presupuesto
                    WHERE cont_id = $cont_id
                    AND papr_estado = 'ACTIVO'";
        $res = $db->ExecuteQuery($query);
        if(0==$res['status']){
            echo $res['error'];
            return;
        }
        if(0==$res['rows']){
            echo "No hay datos disponibles";
            return;
        }

        $delimiter = ";";
        $filename = "ResumenPresupuesto_".date("dmY").".csv";
        header('Content-Type: application; charset=utf-8');
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Set-Cookie: fileDownload=true; path=/');
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header ('Cache-Control: cache, must-revalidate');
        header ('Pragma: public');

        $f = fopen('php://output', 'w');
        fprintf ($f, chr (0xEF) .chr (0xBB) .chr (0xBF));
        fputcsv($f, array_keys($res['data'][0]), $delimiter);

        foreach ($res['data'] as $data) {
            fputcsv($f, $data, $delimiter);
        }
        exit;

    });

    /*
    Descargar a excel  de pagina principal
    */
    Flight::route('GET|POST /contrato/@cont_id:[0-9]+/pago/detalle/descargar', function($cont_id){

        $db = new MySQL_Database();
        $query ="   SELECT  distinct(cp.copa_id)as Numero_Costeo
                            ,cp.copa_periodo AS Periodo
                            ,cp.copa_tipo_ot AS Tipo
                            ,cp.copa_estado  AS  Estado
                            ,cp.copa_cantidad_total AS Cantidad
                            ,REPLACE(cp.copa_monto_total,'.',',') AS Monto_Total
                            ,REPLACE(cp.copa_monto_capex,'.',',') AS  Monto_Capex
                            ,REPLACE(cp.copa_monto_opex,'.',',') AS Monto_Opex
                            ,osp.derivada_id AS Derivada
                            ,osp.hem_id AS Hem
                            ,REPLACE(osp.ospa_tipo_cambio,'.',',') AS Tipo_Cambio
                            ,IFNULL(osp.ospa_observacion , '') AS Observacion
                    FROM costeo_pago cp,os_servicio_pago osp 
                    where cp.copa_id = osp.copa_id
                    union all
                    SELECT  distinct(cp.copa_id)as Numero_Costeo
                            ,cp.copa_periodo AS Periodo
                            ,cp.copa_tipo_ot AS Tipo
                            ,cp.copa_estado  AS  Estado
                            ,cp.copa_cantidad_total AS Cantidad
                            ,REPLACE(cp.copa_monto_total,'.',',') AS Monto_Total
                            ,REPLACE(cp.copa_monto_capex,'.',',') AS  Monto_Capex
                            ,REPLACE(cp.copa_monto_opex,'.',',') AS Monto_Opex
                            ,msp.derivada_id AS Derivada
                            ,msp.hem_id AS Hem
                            ,REPLACE(msp.mnpa_tipo_cambio,'.',',') AS Tipo_Cambio
                            ,IFNULL(msp.mnpa_observacion , '') AS Observacion
                    FROM costeo_pago cp,mnt_servicio_pago msp 
                    where cp.copa_id = msp.copa_id";
        $res = $db->ExecuteQuery($query);
        if(0==$res['status']){
            echo $res['error'];
            return;
        }

        if(0==$res['rows']){
            echo "No hay datos disponibles";
            return;
        }

        $delimiter = ";";
        $filename = "ResumenCosteo_".date("dmY").".csv";
        header('Content-Type: application; charset=ISO-8859-1');
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Set-Cookie: fileDownload=true; path=/');
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header ('Cache-Control: cache, must-revalidate');
        header ('Pragma: public');

        $f = fopen('php://output', 'w');
        fprintf ($f, chr (0xEF) .chr (0xBB) .chr (0xBF));
        fputcsv($f, array_keys($res['data'][0]), $delimiter);

        foreach ($res['data'] as $data) {
            fputcsv($f, $data, $delimiter);
        }
        exit;

    });

    /*
    Descargar a excel  de pagina principal cerrada
    */
    Flight::route('GET|POST /contrato/@cont_id:[0-9]+/pago/detalle/cerrada/descargar', function($cont_id){

        $db = new MySQL_Database();
        $anio =date ("Y");
        $mes = date ("m");
        
        $filtro = array_merge($_GET, $_POST);
        
        $filtrosQuery = " AND copa_estado= 'APROBADO' ";
        if( isset($filtro['pafi_anio']) && ""!=$filtro['pafi_anio']){
            $anio= mysql_real_escape_string($filtro['pafi_anio']);
        }

        $usuario = $_SESSION['user_id'];

        /* Si valor de pafi_mes es 00, significa que la búsqueda tiene que realizar por el año completo */
        if( isset($filtro['pafi_mes']) && ""!=$filtro['pafi_mes']){
            $mes = mysql_real_escape_string($filtro['pafi_mes']);
            
            if("00"==$mes){
                $filtrosQuery = " AND copa_periodo>=".$anio."01 AND copa_periodo<=".$anio."12";
            }else{
                $filtrosQuery = " AND copa_periodo=".$anio.$mes;
            }
        }
        $query ="SELECT 
                     cp.copa_id as Numero_Costeo
                    ,cp.copa_periodo AS Periodo
                    ,cp.copa_tipo_ot AS Tipo
                    ,cp.copa_estado  AS  Estado
                    ,cp.copa_cantidad_total AS Cantidad
                    ,REPLACE(cp.copa_monto_total,'.',',') AS Monto_Total
                    ,REPLACE(cp.copa_monto_capex,'.',',') AS  Monto_Capex
                    ,REPLACE(cp.copa_monto_opex,'.',',') AS Monto_Opex
                    ,cp.copa_derivada AS Derivada
                    ,cp.copa_hem AS Hem
                    ,REPLACE(cp.copa_tipo_cambio,'.',',') AS Tipo_Cambio
                    ,IFNULL(cp.copa_observacion, '') AS Observacion 
            FROM costeo_pago cp
            LEFT JOIN pago_presupuesto pp ON cp.papr_id_capex = pp.papr_id 
                    AND cp.cont_id = pp.cont_id
                    AND  pp.papr_estado = 'ACTIVO'
            LEFT JOIN pago_presupuesto ppo ON cp.papr_id_opex = ppo.papr_id 
                    AND cp.cont_id = ppo.cont_id
                    AND  ppo.papr_estado = 'ACTIVO'
                    WHERE cp.cont_id=$cont_id" .$filtrosQuery. 
            "  UNION
            SELECT 
                    cp.copa_id as Numero_Costeo
                    ,cp.copa_periodo AS Periodo
                    ,cp.copa_tipo_ot AS Tipo
                    ,cp.copa_estado  AS  Estado
                    ,cp.copa_cantidad_total AS Cantidad
                    ,REPLACE(cp.copa_monto_total,'.',',') AS Monto_Total
                    ,REPLACE(cp.copa_monto_capex,'.',',') AS  Monto_Capex
                    ,REPLACE(cp.copa_monto_opex,'.',',') AS Monto_Opex
                    ,cp.copa_derivada AS Derivada
                    ,cp.copa_hem AS Hem
                    ,REPLACE(cp.copa_tipo_cambio,'.',',') AS Tipo_Cambio
                    ,IFNULL(cp.copa_observacion, '') AS Observacion 
                    FROM costeo_pago cp
            INNER JOIN rel_costeo_usuario_validador r ON r.copa_id = cp.copa_id
                                                      AND r.usua_id = $usuario
            LEFT JOIN pago_presupuesto pp ON cp.papr_id_capex = pp.papr_id
                                                AND cp.cont_id = pp.cont_id
                                                AND pp.papr_estado='ACTIVO'
            LEFT JOIN pago_presupuesto ppo ON cp.papr_id_opex = ppo.papr_id 
                                                AND cp.cont_id = ppo.cont_id
                                                AND ppo.papr_estado='ACTIVO'
            WHERE cp.cont_id=$cont_id
            AND cp.copa_estado in ('PEND_VALIDAR')";

        $res = $db->ExecuteQuery($query);

        if(0==$res['status']){
            echo $res['error'];
            return;
        }

        if(0==$res['rows']){
            echo "No hay datos disponibles";
            return;
        }

        $delimiter = ";";
        $filename = "ResumenCosteo_".date("dmY").".csv";
        header('Content-Type: application; charset=ISO-8859-1');
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Set-Cookie: fileDownload=true; path=/');
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header ('Cache-Control: cache, must-revalidate');
        header ('Pragma: public');

        $f = fopen('php://output', 'w');
        fprintf ($f, chr (0xEF) .chr (0xBB) .chr (0xBF));
        fputcsv($f, array_keys($res['data'][0]), $delimiter);

        foreach ($res['data'] as $data) {
            fputcsv($f, $data, $delimiter);
        }
        exit;
    });

    /*
      Detalle Ordenes de servicio por periodo
      costeo periodo  se ingresa a travez de resumen el href 
    */

    Flight::route('GET|POST /contrato/@cont_id:[0-9]+/usuario/@usuario/periodo/@periodo:[0-9]+/actividad/@actividad/copa/@copa_id/ver/filtro(/@page:[0-9]+)', function($cont_id,$usuario,$periodo,$actividad,$copa_id,$page){
        $moneda =  $GLOBALS['tipo_moneda'];
        $results_by_page = 40;
        $out = array();
        $out['status'] = 1;
        $db = new MySQL_Database();
        $out['copa_id'] = $copa_id;
        $out['periodo'] = $periodo;
        $out['actividad'] = $actividad;
        $out['pagina'] = $page;

        if(!isset($periodo) || ""==$periodo){
            $periodo =date ("Ym");
        }
        if('0'==$copa_id){
           $query="SELECT copa_id
                FROM costeo_pago
                WHERE copa_tipo_ot='$actividad'
                AND copa_periodo = $periodo";
            $res = $db->ExecuteQuery($query);
            if( 0==$res['status'] ){
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }
            $copa_id = $res['data'][0]['copa_id'];
            $out['copa_id']=$copa_id;
        }
        
        $query="SELECT copa_estado
                FROM costeo_pago
                WHERE copa_id=$copa_id";
        $res = $db->ExecuteQuery($query);
        if( 0==$res['status'] ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }

        $user = "SELECT copa_id
                       ,rcuv_fecha_validacion
                       ,rcuv_tipo_validacion
                 FROM rel_costeo_usuario_validador 
                 WHERE usua_id=$usuario
                AND rcuv_fecha_validacion is null
                 AND copa_id=$copa_id";

        $res_usuarios = $db->ExecuteQuery($user);
        if( 0==$res_usuarios['status'] ){
            Flight::json(array("status"=>0, "error"=>$res_usuarios['error']));
            return;
        }         
        
        $out['botonera_costeo_visible'] = "none";
        if( 0<$res_usuarios['rows'] ){
            if (("PEND_VALIDAR_COSTEO"==$res['data'][0]['copa_estado'] || 
                "VALIDANDO_COSTEO"==$res['data'][0]['copa_estado'])&&
                "VALIDADOR_COSTEO"==$res_usuarios['data'][0]['rcuv_tipo_validacion'] ){
                if($copa_id == $res_usuarios['data'][0]['copa_id'] ){         
                    if(""==$res_usuarios['data'][0]['rcuv_fecha_validacion']){
                        $out['botonera_costeo_visible'] = "";
                    }
                }
            }
        }

        $out['botonera_acta_visible'] = "none";
        if( 0<$res_usuarios['rows'] ){
            if (("PEND_VALIDAR_ACTA"==$res['data'][0]['copa_estado'] || 
                "VALIDANDO_ACTA"==$res['data'][0]['copa_estado'])&& 
                "VALIDADOR_ACTA"==$res_usuarios['data'][0]['rcuv_tipo_validacion'] ){
                if($copa_id == $res_usuarios['data'][0]['copa_id'] ){         
                    $out['botonera_acta_visible'] = "";
                }
            }
        }
        $perfiles = $_SESSION['usua_cargo'];
        $boton_eliminar = " ";
        if (in_array("SYS_ADMIN", $perfiles)) {
            $boton_eliminar  = "none";
        }
        
        $out['display_eliminar'] = $boton_eliminar ;
        $out['botonera_os_visible'] = "none";
        $out['botonera_mnt_visible'] = "none";
        
        if("OS"==$actividad || "LMT"==$actividad ){
            $out['botonera_os_visible'] = "";
            $query="SELECT SQL_CALC_FOUND_ROWS
                            id_ot
                            ,tipo_ot
                            ,seleccion_ot
                            ,monto_total
                            ,capex_ot
                            ,opex_ot
                            ,tipo_moneda
                            ,fecha_aprobacion_ot
                            ,hem_ot
                            ,derivada_ot
                            ,cont_id
                            ,periodo_pago_ot
                            ,estado_ot
                            ,sap_lpu
                            ,item_lpu
                            ,valor_lpu
                            ,cant_lpu
                            ,subtotal_lpu
                            ,mes_ejecucion
                            ,zona_movistar
                            ,zona_contrato                          
                    FROM  (
                            SELECT
                                orse_id AS id_ot
                                ,'OS' AS tipo_ot
                                ,ospa_seleccion_codigo AS seleccion_ot
                                ,CASE WHEN ( (select count(1) 
                                        from  rel_usuario_perfil r
                                        ,  perfil p
                                        where r.usua_id = $usuario
                                        and p.perf_id = r.perf_id
                                        and ( p.perf_nombre = 'SYS_FINANCIERO' or 'ADM_FINANCIERO')
                                        )  >= 1 ) then ''
                                ELSE 'none' end  as display_eliminar
                                ,if (ospa_seleccion_codigo='CAPEX',ROUND(ospa_monto_capex,2),0) 
                                + if (ospa_seleccion_codigo='OPEX',ROUND(ospa_monto_opex,2),0) AS monto_total
                                ,if (ospa_seleccion_codigo='CAPEX',ROUND(ospa_monto_capex,2),0)  AS capex_ot
                                ,if (ospa_seleccion_codigo='OPEX',ROUND(ospa_monto_opex,2),0) AS opex_ot
                                ,'$moneda' AS tipo_moneda
                                ,ospa_fecha_aprobacion AS fecha_aprobacion_ot
                                ,hem_id AS hem_ot
                                ,derivada_id AS derivada_ot
                                ,o.cont_id
                                ,ospa_periodo_aprobacion AS periodo_pago_ot
                                ,ospa_estado_proceso  AS estado_ot
                                ,if (ospa_seleccion_codigo='CAPEX',ospa_codigo_capex,if(ospa_seleccion_codigo='OPEX',ospa_codigo_opex,'')) AS sap_lpu
                                ,ospa_desc_item_lpu as item_lpu
                                ,ospa_valor_lpu as valor_lpu
                                ,ospa_cant_lpu as cant_lpu
                                ,if (ospa_seleccion_codigo='CAPEX',ROUND(ospa_monto_capex,2),if (ospa_seleccion_codigo='OPEX',ROUND(ospa_monto_opex,2),0)) AS subtotal_lpu
                                ,ospa_periodo_aprobacion as mes_ejecucion
                                ,ospa_zona_movistar as zona_movistar
                                ,ospa_zona_contrato as zona_contrato                                
                            FROM  os_servicio_pago o
                            WHERE o.cont_id = $cont_id
                            AND o.copa_id in (
                                    SELECT c.copa_id FROM costeo_pago c
                                                    WHERE c.cont_id = $cont_id
                                                    AND c.copa_periodo =$periodo
                                                    AND c.copa_tipo_ot='$actividad'
                                                    #AND c.copa_tipo_ot='OS'
                            )
                    ) pago
                    LIMIT $results_by_page OFFSET ".(($page-1)*$results_by_page);

        }else{
            $out['botonera_mnt_visible'] = "";
            $query="SELECT SQL_CALC_FOUND_ROWS
                            id_ot
                            ,tipo_ot
                            ,seleccion_ot
                            ,monto_total
                            ,capex_ot
                            ,opex_ot
                            ,tipo_moneda
                            ,fecha_aprobacion_ot
                            ,hem_ot
                            ,derivada_ot
                            ,cont_id
                            ,periodo_pago_ot
                            ,estado_ot
                            ,sap_lpu
                            ,item_lpu
                            ,valor_lpu
                            ,cant_lpu
                            ,subtotal_lpu
                            ,mes_ejecucion
                            ,zona_movistar
                            ,zona_contrato
                        FROM (
                            SELECT
                                mant_id AS id_ot
                                ,'MNT' AS tipo_ot
                                ,mnpa_seleccion_codigo AS seleccion_ot
                                ,CASE WHEN ( (select count(1) 
                                        from  rel_usuario_perfil r
                                        ,  perfil p
                                        where r.usua_id = $usuario
                                        and p.perf_id = r.perf_id
                                        and ( p.perf_nombre = 'SYS_FINANCIERO' or 'ADM_FINANCIERO')
                                        )  >= 1 ) then ''
                                ELSE 'none' end  as display_eliminar
                                ,if (mnpa_seleccion_codigo='CAPEX',ROUND(mnpa_monto_capex,2) ,0) + if (mnpa_seleccion_codigo='OPEX',ROUND(mnpa_monto_opex,2),0) AS monto_total
                                ,if (mnpa_seleccion_codigo='CAPEX',ROUND(mnpa_monto_capex,2) ,0) AS capex_ot
                                ,if (mnpa_seleccion_codigo='OPEX',ROUND(mnpa_monto_opex,2),0) AS opex_ot
                                ,'$moneda' AS tipo_moneda
                                ,mnpa_fecha_aprobacion AS fecha_aprobacion_ot
                                ,hem_id AS hem_ot
                                ,derivada_id AS derivada_ot
                                ,m.cont_id
                                ,mnpa_periodo_aprobacion AS periodo_pago_ot
                                ,mnpa_estado_mnt AS estado_ot
                                ,if (mnpa_seleccion_codigo='CAPEX',mnpa_codigo_capex,if(mnpa_seleccion_codigo='OPEX',mnpa_codigo_opex,'')) AS sap_lpu 
                                ,mnpa_desc_item_lpu AS item_lpu
                                ,mnpa_valor_lpu AS valor_lpu
                                ,mnpa_cant_lpu AS cant_lpu
                                ,if (mnpa_seleccion_codigo='CAPEX',ROUND(mnpa_monto_capex,2),if (mnpa_seleccion_codigo='OPEX',ROUND(mnpa_monto_opex,2),0)) AS subtotal_lpu
                                ,mnpa_periodo_aprobacion as mes_ejecucion
                                ,mnpa_zona_movistar AS zona_movistar
                                ,mnpa_zona_contrato AS zona_contrato
                            FROM mnt_servicio_pago m
                            WHERE m.cont_id = $cont_id
                            AND m.copa_id in (
                                SELECT c.copa_id FROM costeo_pago c
                                                    WHERE c.cont_id = $cont_id
                                                    AND c.copa_periodo =$periodo
                                                    AND c.copa_tipo_ot='MNT'
                            )
                            GROUP BY mant_id
                                ,'MNT'
                                ,mnpa_seleccion_codigo
                                ,'$moneda'
                                ,mnpa_fecha_aprobacion
                                ,hem_id
                                ,derivada_id
                                ,m.cont_id
                                ,mnpa_periodo_aprobacion
                                ,mnpa_estado_mnt
                        ) pago
                        LIMIT $results_by_page OFFSET ".(($page-1)*$results_by_page);
        }
        $res = $db->ExecuteQuery($query);
        $out['pago_os_periodo'] = $res['data'];
        if( 0==$res['status'] ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }

        if($page!=null){
            $res = $db->ExecuteQuery("SELECT FOUND_ROWS() as total");
            if(!$res['status']){
                return $res;
            }
            $out['pagina'] = intval($page);
            $out['total'] = intval($res['data'][0]['total']);
            $out['paginas'] = ceil($out['total']/$results_by_page);
        }
        Flight::json($out);
    });

    /*
    Descargar a excel  todas las os pagadas del periodo seleccionado
    */
    Flight::route('POST /contrato/@cont_id:[0-9]+/pago/periodo/@periodo:[0-9]+/actividad/@actividad/costeo/ver/descargar', function($cont_id,$periodo,$actividad){
        $db = new MySQL_Database();
        $query = "  SELECT
                            orse_id
                            ,ospa_estado_proceso
                            ,if(ospa_seleccion_codigo ='CAPEX' ,ospa_codigo_capex,if(ospa_seleccion_codigo ='OPEX',ospa_codigo_opex,'')) as codigo_sap
							,ospa_id_item_lpu
                            ,ospa_desc_item_lpu
                            ,REPLACE(ospa_valor_lpu,'.',',') as valor_lpu
                            ,REPLACE(ospa_cant_lpu ,'.',',') as cant_lpu
                            ,if(ospa_seleccion_codigo = 'CAPEX',REPLACE(ROUND(ospa_monto_capex,2),'.',','),REPLACE(ROUND(ospa_monto_opex,2),'.',',')) as ospa_monto
                            ,ospa_fecha_aprobacion
                            ,ospa_periodo_aprobacion
                            ,ospa_zona_movistar
                            ,ospa_zona_contrato
							,ospa_region
                    FROM
                            costeo_pago c
                            , os_servicio_pago o
                    WHERE c.cont_id = $cont_id
                    AND c.copa_periodo = $periodo
                    AND c.copa_tipo_ot ='$actividad'
                    AND c.copa_id = o.copa_id";

        $res = $db->ExecuteQuery($query);
        if(0==$res['status']){
            echo $res['error'];
            return;
        }

        if(0==$res['rows']){
            echo "No hay datos disponibles";
            return;
        }

        $delimiter = ";";
        $filename = "CosteoOS_".date("dmY").".csv";
        header('Content-Type: application; charset=ISO-8859-1');
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Set-Cookie: fileDownload=true; path=/');
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header ('Cache-Control: cache, must-revalidate');
        header ('Pragma: public');
        $f = fopen('php://output', 'w');
        fprintf ($f, chr (0xEF) .chr (0xBB) .chr (0xBF));
        fputcsv($f, array_keys($res['data'][0]), $delimiter);

        foreach ($res['data'] as $data) {
            fputcsv($f, $data, $delimiter);
        }
        exit;

    });
    /* Descargar a excel  todos los  mnt pagados del periodo seleccionado */
    Flight::route('POST /contrato/@cont_id:[0-9]+/pago/periodo/@periodo:[0-9]+/actividad/@actividad/costeo/ver/mnt/descargar', function($cont_id,$periodo,$actividad){

        $db = new MySQL_Database();
        $query = "  SELECT
                            mant_id AS N_MNT
                            ,mnpa_periodo AS Periodo_MNT
                            ,mnpa_especialidad AS Especialidad_MNT
                            ,mnpa_empresa AS ECC_MNT
                            ,mnpa_fecha_programacion AS Fecha_programada_MNT 
                            ,mnpa_fecha_aprobacion AS Fecha_finalizacion_MNT
                            ,mnpa_estado_proceso AS Estado_proceso_MNT
                            ,mnpa_estatus_pago AS Status_pago
                            ,mnpa_codigo AS Codigo
                            ,if(mnpa_seleccion_codigo ='CAPEX' ,mnpa_codigo_capex,if(mnpa_seleccion_codigo ='OPEX',mnpa_codigo_opex,'')) as codigo_sap
                            ,if(mnpa_seleccion_codigo = 'CAPEX',REPLACE(ROUND(mnpa_monto_capex,2),'.',','),REPLACE(ROUND(mnpa_monto_opex,2),'.',',')) as mnt_monto
                            ,mnpa_emplazamiento AS Nombre_EMP
                            ,mnpa_direccion_emplazamiento AS Direccion_EMP
                            ,mnpa_clasificacion  AS Clasificacion_MNT
                            ,mnpa_zona AS Zona_movistar 
                            ,mnpa_zona_movistar AS Zona_de_contrato_CIM
                            ,mnpa_zona_contrato AS Zona
							/*,mnpa_id_item_lpu
                            ,mnpa_desc_item_lpu
							,REPLACE(mnpa_valor_lpu,'.',',') as valor_lpu
                            ,REPLACE(mnpa_cant_lpu ,'.',',') as cant_lpu
                            
                            ,mnpa_periodo_aprobacion
                            ,mnpa_zona_movistar
                            ,mnpa_zona_contrato*/
                    FROM
                            costeo_pago c
                            , mnt_servicio_pago m
                    WHERE c.cont_id = $cont_id
                            AND c.copa_periodo = $periodo
                            AND c.copa_id = m.copa_id";
        $res = $db->ExecuteQuery($query);
        if(0==$res['status']){
            echo $res['error'];
            return;
        }

        if(0==$res['rows']){
            echo "No hay datos disponibles";
            return;
        }

        $delimiter = ";";
        $filename = "CosteoMNT_".date("dmY").".csv";
        header('Content-Type: application; charset=ISO-8859-1');
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Set-Cookie: fileDownload=true; path=/');
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header ('Cache-Control: cache, must-revalidate');
        header ('Pragma: public');
        $f = fopen('php://output', 'w');
        fprintf ($f, chr (0xEF) .chr (0xBB) .chr (0xBF));
        fputcsv($f, array_keys($res['data'][0]), $delimiter);

        foreach ($res['data'] as $data) {
            fputcsv($f, $data, $delimiter);
        }
        exit;

    });

    /*
    Descarga todo lo de la pagina detalle os periodo
    */
    Flight::route('GET|POST /pago/descargar/detalle/costeo/contrato/@cont_id:[0-9]+/periodo/@periodo:[0-9]+/lista', function($cont_id,$periodo){

        $db = new MySQL_Database();
        $query = "  SELECT
                        ospa_id, orse_id
                        ,ospa_monto_capex
                        ,ospa_monto_opex
                        ,ospa_fecha_aprobacion
                        ,hem_id
                        ,cont_id
                        ,ospa_periodo_aprobacion
                    FROM
                        os_servicio_pago
                    where
                        cont_id = $cont_id
                    AND ospa_periodo_aprobacion = $periodo ";
        $res = $db->ExecuteQuery($query);
        if(0==$res['status']){
            echo $res['error'];
            return;
        }

        if(0==$res['rows']){
            echo "No hay datos disponibles";
            return;
        }

        $delimiter = ";";
        $filename = "OS_".date("dmY").".csv";
        header('Content-Type: application; charset=ISO-8859-1');
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Set-Cookie: fileDownload=true; path=/');
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header ('Cache-Control: cache, must-revalidate');
        header ('Pragma: public');
        $f = fopen('php://output', 'w');
        fprintf ($f, chr (0xEF) .chr (0xBB) .chr (0xBF));
        fputcsv($f, array_keys($res['data'][0]), $delimiter);

        foreach ($res['data'] as $data) {
            fputcsv($f, $data, $delimiter);
        }
        exit;
    });

    /*valida ACTA de la  ot en el resumen costeo   */
    Flight::route('GET|POST /contrato/@cont_id:[0-9]+/periodo/@periodo:[0-9]+/copa_id/@copa_id:[0-9]+/actividad/@actividad/validar', function($cont_id,$periodo,$copa_id,$actividad){
        $dbo = new MySQL_Database(); 
        $usua_validador  = $_SESSION['user_id'];
        $res = array();
        $dbo->startTransaction();
        
        if('OS'== $actividad){
            $validar_os = "UPDATE os_servicio_pago 
                           SET ospa_estado_os= 'ENPROCESO' 
                           WHERE  copa_id = $copa_id";
            $res = $dbo->ExecuteQuery($validar_os);

            if(0 == $res['status']){
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
            }
        }else{
            $validar_mnt = "UPDATE mnt_servicio_pago 
                            SET mnpa_estado_mnt= 'ENPROCESO' 
                            WHERE  copa_id =$copa_id";
            $res = $dbo->ExecuteQuery($validar_mnt);

            if(0 == $res['status']){
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
            }
        }

        $costeo_validador= "UPDATE rel_costeo_usuario_validador  
                            SET rcuv_fecha_validacion = NOW()
                            WHERE copa_id=$copa_id
                            AND  usua_id= $usua_validador";
        $res = $dbo->ExecuteQuery($costeo_validador);

        if(0 == $res['status']){
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        
        $costeo_cuenta_validador= " SELECT count(1) usua_id
                                    FROM rel_costeo_usuario_validador  
                                    WHERE copa_id=$copa_id
                                    AND  rcuv_fecha_validacion IS NULL";
        $res = $dbo->ExecuteQuery($costeo_cuenta_validador);
        if(0 == $res['status']){
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        $estado ='VALIDANDO_ACTA' ;
        if(0 == $res['rows']|| 0 == $res['data'][0]['usua_id'] ){
            $estado ='ACTA_VALIDADO' ;
            $titulo = "Acta Nª ".$copa_id." Validado";
            $tipo="ACTA_VALIDADO";
           
            $resEmail = EnviarEmailDefecto($dbo,$copa_id,$titulo,$tipo);
            if(0 == $resEmail['status']){
                Loggear("Error al enviar notificacion: ".$resEmail['error'],LOG_ERR);
                Loggear($dbo->lastQuery);
            }
        }
        $validar_costeo= "  UPDATE costeo_pago 
                            SET copa_estado='$estado' 
                                , copa_usua_actualizacion = '$usua_validador'
                                , copa_fecha_ult_modificacion = NOW()
                            WHERE copa_id =$copa_id";
        $res = $dbo->ExecuteQuery($validar_costeo);

        if(0 == $res['status']){
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        
        $dbo->commit();
        Flight::json(array("status"=>true,"Validacion realizada correctamente."));

    });  

    /*valida COSTEO de la  ot en el resumen costeo   */
    Flight::route('GET|POST /contrato/@cont_id:[0-9]+/periodo/@periodo:[0-9]+/copa_id/@copa_id:[0-9]+/actividad/@actividad/validar/costeo', function($cont_id,$periodo,$copa_id,$actividad){
        $dbo = new MySQL_Database(); 
        $usua_validador  = $_SESSION['user_id'];
        $res = array();
        $dbo->startTransaction();
        
        if('OS'== $actividad){
            $validar_os = "
                   UPDATE os_servicio_pago 
                   SET ospa_estado_os= 'ENPROCESO' 
                   WHERE  copa_id = $copa_id";
            $res = $dbo->ExecuteQuery($validar_os);

            if(0 == $res['status']){
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
            }
        }else{
            $validar_mnt = "    UPDATE mnt_servicio_pago 
                                SET mnpa_estado_mnt= 'ENPROCESO' 
                                WHERE  copa_id =$copa_id";
            $res = $dbo->ExecuteQuery($validar_mnt);

            if(0 == $res['status']){
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
            }
        }

        $costeo_validador= "    UPDATE rel_costeo_usuario_validador  
                                SET rcuv_fecha_validacion = NOW()
                                WHERE copa_id=$copa_id
                                AND  usua_id= $usua_validador";
        $res = $dbo->ExecuteQuery($costeo_validador);

        if(0 == $res['status']){
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        
        $costeo_cuenta_validador= " SELECT count(1) usua_id
                                    FROM rel_costeo_usuario_validador  
                                    WHERE copa_id=$copa_id
                                    AND  rcuv_fecha_validacion IS NULL";
        $res = $dbo->ExecuteQuery($costeo_cuenta_validador);
        if(0 == $res['status']){
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        $estado ='VALIDANDO_COSTEO' ;
        if(0 == $res['rows']|| 0 == $res['data'][0]['usua_id'] ){
            $estado ='COSTEO_VALIDADO' ;
            $titulo = "Costeo Nª ".$copa_id." Validado";
            $tipo="COSTEO_VALIDADO";
            $resEmail = EnviarEmailDefecto($dbo,$copa_id,$titulo,$tipo);
            if(0 == $resEmail['status']){
                Loggear("Error al enviar notificacion: ".$resEmail['error'],LOG_ERR);
                Loggear($dbo->lastQuery);
            }      
        }
        $validar_costeo= "  UPDATE costeo_pago 
                            SET copa_estado='$estado' 
                                , copa_usua_actualizacion = '$usua_validador'
                                , copa_fecha_ult_modificacion = NOW()
                            WHERE copa_id =$copa_id";

        $res = $dbo->ExecuteQuery($validar_costeo);

        if(0 == $res['status']){
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }

        $dbo->commit();
        Flight::json(array("status"=>true,"Validacion realizada correctamente."));

    }); 

    /*valida costeo en pago_costeo_ver  */
    Flight::route('GET|POST /contrato/@cont_id:[0-9]+/periodo/@periodo:[0-9]+/copa_id/@copa_id:[0-9]+/actividad/@actividad/validar/costeo', function($cont_id,$periodo,$copa_id,$actividad){
        $dbo = new MySQL_Database(); 
        $usua_validador  = $_SESSION['user_id'];
        $res = array();
        $dbo->startTransaction();
        
        if('OS'==$actividad){
            $validar_os = " UPDATE os_servicio_pago 
                            SET ospa_estado_os= 'ENPROCESO' 
                            WHERE  copa_id = $copa_id";
            $res = $dbo->ExecuteQuery($validar_os);
            if(0 == $res['status']){
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
            }
        }else{
            $validar_mnt = "    UPDATE mnt_servicio_pago 
                                SET mnpa_estado_mnt= 'ENPROCESO' 
                                WHERE  copa_id =$copa_id";
            $res = $dbo->ExecuteQuery($validar_mnt);

            if(0 == $res['status']){
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
            }
        }

        $costeo_validador= "    UPDATE rel_costeo_usuario_validador  
                                SET rcuv_fecha_validacion = NOW()
                                WHERE copa_id=$copa_id
                                AND  usua_id= $usua_validador";
        $res = $dbo->ExecuteQuery($costeo_validador);
        if(0 == $res['status']){
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        
        $costeo_cuenta_validador= " SELECT count(1) usua_id
                                FROM rel_costeo_usuario_validador  
                                WHERE copa_id=$copa_id
                                AND  rcuv_fecha_validacion IS NULL
                                AND rcuv_tipo_validacion='VALIDADOR_COSTEO'";
        $res = $dbo->ExecuteQuery($costeo_cuenta_validador);
        if(0 == $res['status']){
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        $estado ='VALIDANDO_COSTEO' ;
        if(0 == $res['rows']|| 0 == $res['data'][0]['usua_id'] ){
            $estado ='COSTEO_VALIDADO' ;
            $titulo = "Acta Nª ".$copa_id." Validado";
            $tipo="COSTEO_VALIDADO";
            $resEmail = EnviarEmailDefecto($dbo,$copa_id,$titulo,$tipo);
            if(0==$resEmail['status']){
                Loggear("Error al enviar notificacion: ".$resEmail['error'],LOG_ERR);
                Loggear($dbo->lastQuery);
            }
        }
        $validar_costeo= "  UPDATE costeo_pago 
                            SET copa_estado='$estado' 
                                , copa_usua_actualizacion = '$usua_validador'
                                , copa_fecha_ult_modificacion = NOW()
                            WHERE copa_id =$copa_id";
        $res = $dbo->ExecuteQuery($validar_costeo);

        if(0 == $res['status']){
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }

        $dbo->commit();
        Flight::json(array("status"=>true,"Validacion realizada correctamente."));

    });  

      /*Rechaza  acta de la  ot en el html pago_costeo_ver de los validadores   */
    Flight::route('GET|POST /contrato/@cont_id:[0-9]+/periodo/@periodo:[0-9]+/copa_id/@copa_id:[0-9]+/actividad/@actividad/rechazar', function($cont_id,$periodo,$copa_id,$actividad){
        $dbo = new MySQL_Database(); 
        $usua_validador  = $_SESSION['user_id'];
        $res = array();
        $dbo->startTransaction();
        if('OS'==$actividad){
            $validar_os = "
                   UPDATE os_servicio_pago 
                   SET ospa_estado_os= 'ENPROCESO' 
                   WHERE  copa_id = $copa_id";
            $res = $dbo->ExecuteQuery($validar_os);

            if(0==$res['status']){
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
            }
        }else{
            $validar_mnt = "
                   UPDATE mnt_servicio_pago 
                   SET mnpa_estado_mnt= 'ENPROCESO' 
                   WHERE  copa_id =$copa_id";
            $res = $dbo->ExecuteQuery($validar_mnt);
            if(0==$res['status']){
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
            }
        }

        $costeo_validador= "UPDATE rel_costeo_usuario_validador  
                            SET rcuv_fecha_validacion = NOW()
                            WHERE copa_id=$copa_id
                                AND  usua_id= $usua_validador";
        $res = $dbo->ExecuteQuery($costeo_validador);
        if(0==$res['status']){
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        $costeo_cuenta_validador= " SELECT count(1) usua_id
                                    FROM rel_costeo_usuario_validador  
                                    WHERE copa_id=$copa_id
                                        AND  rcuv_fecha_validacion IS NULL";
        $res = $dbo->ExecuteQuery($costeo_cuenta_validador);
        if(0==$res['status']){
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        $estado ='VALIDANDO' ;
        if(0==$res['rows']|| 0==$res['data'][0]['usua_id'] ){
            $estado ='VALIDADO' ;
        }
        $validar_costeo= " UPDATE costeo_pago 
                            SET copa_estado='$estado' 
                                , copa_usua_actualizacion = '$usua_validador'
                                , copa_fecha_ult_modificacion = NOW()
                            WHERE copa_id =$copa_id
                                AND copa_tipo_ot = '$actividad'";
        $res = $dbo->ExecuteQuery($validar_costeo);
        if(0==$res['status']){
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        $dbo->commit();
        Flight::json(array("status"=>true,"Validacion realizada correctamente."));

    });
   
    /*  Guarda detalle os periodo  del boton generar Costeo */
    Flight::route('POST /contrato/@cont_id:[0-9]+/pago/costeo/add', function($cont_id){

        $db = new MySQL_Database();
        $pago = $_POST;
        $usua_creador  = $_SESSION['user_id'];
        $res = array();
        
        $periodo=$pago['periodo'];
        $actividad=$pago['actividad'];
        $linea_presupuesto_opex=$pago['lineaopex'];     
        $linea_presupuesto_capex=$pago['lineacapex'];
        $monto_total=$pago['monto_total_cabecera'];
        $monto_total_capex=$pago['monto_total_capex'];
        $monto_total_opex=$pago['monto_total_opex'];
        $db->startTransaction();
        $existencia = " SELECT count(1)
                            copa_periodo 
                        FROM 
                            costeo_pago 
                        WHERE cont_id = $cont_id
                        AND copa_periodo =$periodo
                        AND copa_tipo_ot='$actividad'";

        $resExistencia = $db->ExecuteQuery($existencia);
        if(0 == $resExistencia['status']){
            $db->Rollback();
            Flight::json(array("status" => 0, "error" => $resExistencia['error']));
            return;
        }
        if(null == $monto_total_capex || "" == $monto_total_capex){
            $monto_total_capex = 0;
        }
        if(null == $monto_total_opex ||"" == $monto_total_opex){
            $monto_total_opex=0;
        }

        $query = "";
        $queryCopaId="";
        if(0<$resExistencia['data'][0]['copa_periodo']){
            $query = " UPDATE costeo_pago 
                        SET 
                            copa_fecha_ult_modificacion = NOW()
                            ,copa_usua_actualizacion = $usua_creador
                            ,copa_monto_total = REPLACE(REPLACE('$monto_total','.',''),',','.')
                            ,copa_monto_total_sin_descuento = REPLACE(REPLACE('$monto_total','.',''),',','.')
                            ,copa_monto_capex = copa_monto_capex + REPLACE(REPLACE('$monto_total_capex','.',''),',','.')
                            ,copa_monto_opex = copa_monto_opex+ REPLACE(REPLACE('$monto_total_opex','.',''),',','.')
                            ,papr_id_capex = $linea_presupuesto_capex
                            ,papr_id_opex = $linea_presupuesto_opex
                        WHERE cont_id = $cont_id
                        AND copa_periodo = $periodo
                        AND copa_tipo_ot='$actividad'";

            $queryCopaId =" SELECT copa_id
                            FROM costeo_pago
                            WHERE cont_id = $cont_id
                            AND copa_periodo = $periodo
                            AND copa_tipo_ot='$actividad'";
        }else{
            $query = "  INSERT INTO costeo_pago
                                    (copa_fecha_creacion
                                    ,copa_usua_creacion
                                    ,copa_monto_total_sin_descuento
                                    ,copa_monto_total
                                    ,copa_monto_capex
                                    ,copa_monto_opex
                                    ,copa_periodo
                                    ,cont_id
                                    ,copa_estado
                                    ,copa_tipo_ot
                                    ,papr_id_capex
                                    ,papr_id_opex)
                        VALUES
                                    (NOW()
                                    ,$usua_creador
                                    ,REPLACE(REPLACE('$monto_total','.',''),',','.')
                                    ,REPLACE(REPLACE('$monto_total','.',''),',','.')
                                    , REPLACE(REPLACE('$monto_total_capex','.',''),',','.')
                                    , REPLACE(REPLACE('$monto_total_opex','.',''),',','.')
                                    ,$periodo
                                    ,$cont_id
                                    ,'CREADO'
                                    ,'$actividad'
                                    ,$linea_presupuesto_capex
                                    ,$linea_presupuesto_opex)";

            $queryCopaId =" SELECT copa_id
                            FROM costeo_pago
                            WHERE cont_id =$cont_id
                            AND copa_periodo =$periodo
                            AND copa_tipo_ot ='$actividad'";
        }

        $res = $db->ExecuteQuery($query);
        if(0 == $res['status']){
            $db->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }

        $resCopaId = $db->ExecuteQuery($queryCopaId);
        if(0 == $resCopaId['status']){
            $db->Rollback();
            Flight::json(array("status" => 0, "error" => $resCopaId['error']));
            return;
        }

        $copa_id = $resCopaId['data'][0]['copa_id'];
        $str_id_ot=$pago['str_id_ot'];
        $str_capex_ot=$pago['str_capex_ot'];
        $str_check_capex_value=$pago['str_check_capex_value'];  
        $str_opex_ot=$pago['str_opex_ot'];
        $str_check_opex_value=$pago['str_check_opex_value'];
    
        if (0<strlen($str_id_ot)) {
            
            $str_id_ot=$pago['str_id_ot'];
            $array_str_id_ot= explode(";", $str_id_ot);
            
            $str_capex_ot=$pago['str_capex_ot'];
                        
            $str_check_capex_value=$pago['str_check_capex_value'];
            $array_str_check_capex_value = explode(";", $str_check_capex_value);
            
            $str_opex_ot=$pago['str_opex_ot'];
            $str_check_opex_value=$pago['str_check_opex_value'];
            $array_str_check_opex_value = explode(";", $str_check_opex_value);
            
        }
        
        /*saco el numero de elementos dentro del string*/
        $longitud = count($array_str_id_ot);
        $cant_reg=0;
        /*
        foreach ($id_ot as $v_id_ot) {
        Se descarta el primier elemento por ser un ";"
        */
        for($i=1; $i<$longitud; $i++) {

            $query="";
            $orden_id =$array_str_id_ot[$i];            
            if('SI' == $array_str_check_capex_value[$i] || 'SI' == $array_str_check_opex_value[$i]){
                if('SI' == $array_str_check_capex_value[$i]){
                   /* $copa_monto_total = $copa_monto_total+$opex_ot[$i];
                    $monto_opex=$monto_opex+$opex_ot[$i];*/
                    if ('OS' == $actividad || 'LMT' == $actividad) {
                        $query = "  UPDATE  os_servicio_pago
                                    SET  copa_id = $copa_id
                                        ,ospa_estado_os = 'ENPROCESO'
                                        ,ospa_seleccion_codigo='CAPEX'
                                    WHERE orse_id = $orden_id";
                    }else{
                        $query = "
                                    UPDATE  mnt_servicio_pago
                                    SET copa_id =$copa_id
                                        ,mnpa_estado_mnt = 'ENPROCESO'
                                        ,mnpa_seleccion_codigo='CAPEX'
                                    WHERE mant_id = $orden_id";
                    }
                }
                if('SI' == $array_str_check_opex_value[$i]){
                    /*$copa_monto_total = $copa_monto_total+$capex_ot[$i];
                      $monto_capex=$monto_capex+$capex_ot[$i];*/
                    if ('OS' == $actividad || 'LMT' == $actividad) {
                        $query = "  UPDATE  os_servicio_pago
                                    SET copa_id = $copa_id
                                        ,ospa_estado_os= 'ENPROCESO'
                                        ,ospa_seleccion_codigo='OPEX'
                                    WHERE orse_id = $orden_id";
                    }else{
                        $query = "  UPDATE  mnt_servicio_pago
                                    SET copa_id =$copa_id
                                        ,mnpa_estado_mnt= 'ENPROCESO'
                                        ,mnpa_seleccion_codigo='OPEX'
                                    WHERE mant_id =$orden_id";
                    }
                }

                $res = $db->ExecuteQuery($query);
                if(0 == $res['status']){
                    $db->Rollback();
                    Flight::json(array("status" => 0, "error" => $res['error']));
                    return;
                }
                $cant_reg++;    
            }
            
        }
        /*Extrae la cantidad de ot registradas en el costeo */
        if('OS' == $actividad || 'LMT' == $actividad){
            $monto_total_ot= "  SELECT COUNT(distinct(orse_id)) AS cantidad_reg
                                FROM os_servicio_pago 
                                WHERE copa_id = $copa_id";

            $res = $db->ExecuteQuery($monto_total_ot);
            if(0 == $res['status']){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
            }
        }else{
            $monto_total_ot= "  SELECT COUNT(distinct(mant_id)) AS cantidad_reg
                                FROM mnt_servicio_pago 
                                WHERE copa_id = $copa_id";

            $res = $db->ExecuteQuery($monto_total_ot);
            if(0 == $res['status']){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
            }
        }

        $monto_total_ot = $res['data'][0]['cantidad_reg'];

        /*actualiza la cantidad de ot en el costeo */
        $queryMontoTotal= " UPDATE costeo_pago 
                            SET 
                                copa_cantidad_total = $monto_total_ot
                            WHERE cont_id = $cont_id
                            AND copa_periodo = $periodo
                            AND copa_tipo_ot='$actividad'";
        $res = $db->ExecuteQuery($queryMontoTotal);  
        if(0 == $res['status']){
            $db->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        /*Extrae y actualiza el monto sin descuento*/ 
     	if("OS" == $actividad || "LMT" == $actividad)
     	{
	       	$query = "SELECT ROUND(SUM(ospa_monto_capex),3 ) AS  monto_total_capex
					FROM os_servicio_pago 
					WHERE copa_id = $copa_id
					AND ospa_seleccion_codigo ='CAPEX'
                    AND ospa_penalidad = 'NO'";
			$res = $db->ExecuteQuery($query);  
			if(0 == $res['status']){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
       	 	}		
       	 	$monto_total_capex = $res['data'][0]['monto_total_capex'];	
       	 	if(null == $monto_total_capex || "" == $monto_total_capex || 0 == $monto_total_capex ){
            	$monto_total_capex = 0;
        	}
			
			$res = "SELECT ROUND(SUM(ospa_monto_opex),3 ) AS monto_total_opex
					FROM os_servicio_pago 
					WHERE copa_id = $copa_id
                    AND ospa_seleccion_codigo ='OPEX'
                    AND ospa_penalidad = 'NO'";

			$res = $db->ExecuteQuery($res);
			if(0 == $res['status']){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
       	 	}
       	 	$monto_total_opex = $res['data'][0]['monto_total_opex'];	
 			if(null == $monto_total_opex || "" == $monto_total_opex || 0 == $monto_total_opex ){
            	$monto_total_opex = 0;
        	}		

        	$actualizar_monto="	UPDATE costeo_pago
        						SET copa_monto_total_sin_descuento = $monto_total_capex + $monto_total_opex
                                , copa_monto_total = $monto_total_capex + $monto_total_opex
        						WHERE copa_id = $copa_id
        						";

        	$res = $db->ExecuteQuery($actualizar_monto);
        	if(0 == $res['status']){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
       	 	}
		}else{
			$res = "SELECT 	ROUND(SUM(mnpa_monto_capex),3 ) AS  monto_total_capex
					FROM 	mnt_servicio_pago 
					WHERE 	copa_id = $copa_id
                    AND     mnpa_seleccion_codigo = 'CAPEX'
                    AND     mnpa_penalidad= 'NO'";

			$res = $db->ExecuteQuery($res);
			if(0 == $res['status']){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
       	 	}
			$monto_total_capex = $res['data'][0]['monto_total_capex'];	
       	 	if(null == $monto_total_capex || "" == $monto_total_capex || 0 == $monto_total_capex ){
            	$monto_total_capex = 0;
        	}

			$res = "	SELECT 	ROUND(SUM(mnpa_monto_opex),3 )AS monto_total_opex
						FROM 	mnt_servicio_pago 
						WHERE 	copa_id = $copa_id
                        AND     mnpa_seleccion_codigo = 'OPEX'
                        AND     mnpa_penalidad = 'NO'"	;

			$res = $db->ExecuteQuery($res);
			if(0 == $res['status']){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
       	 	}		
			$monto_total_opex = $res['data'][0]['monto_total_opex'];	
 			if(null == $monto_total_opex || "" == $monto_total_opex || 0 == $monto_total_opex ){
            	$monto_total_opex = 0;
        	}
        	$actualizar_monto="	UPDATE costeo_pago
        						SET copa_monto_total_sin_descuento = $monto_total_capex + $monto_total_opex
                                , copa_monto_total = $monto_total_capex + $monto_total_opex
        						WHERE copa_id = $copa_id
        						";

        	$res = $db->ExecuteQuery($actualizar_monto);
        	if(0 == $res['status']){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
       	 	}
		}	

        $res['actividad']=$actividad;
        $res['copa_id']=$copa_id;
        $db->commit();
        Flight::json($res);
    });
    
     /* 
        Guarda hem y derivada para un costeo
    */
    Flight::route('POST /contrato/@cont_id:[0-9]+/pago/hemyderivada/add', function($cont_id){
       
        $db = new MySQL_Database();
        $pago = $_POST;
        $res = array();

        $actividad=$pago['actividad']; 
        $db->startTransaction();
        $str_id_ot=$pago['id_ot'];  

        if (0<strlen($str_id_ot)) {
      
            $str_id_ot=$pago['id_ot'];
            $array_str_id_ot= explode(",", $str_id_ot);

            $str_imputderivada=$pago['imputderivada'];
            $array_str_imputderivada = explode(",", $str_imputderivada);
                        
            $str_imputhem=$pago['imputhem'];
            $array_str_imputhem = explode(",",$str_imputhem);
            
            $str_imputtipocambio=$pago['imputtipocambio'];
            $array_str_imputtipocambio = explode(",",$str_imputtipocambio);
            
            $str_imputobservacion=$pago['observacion'];
            $array_str_imputobservacion= explode(",",$str_imputobservacion);            
        }
        
        /* Saco el numero de elementos dentro del string*/
        $longitud = count($array_str_id_ot);
        
        /* 
        Se descarta el primier elemento por ser un ";"*/
        if('OS'==$actividad ||'LMT'==$actividad )
        {
            for($i=0; $i<$longitud; $i++) {
             
                $query="";
                    
                $array_str_id_ot[$i]= str_replace('[', "", $array_str_id_ot[$i]);
                $array_str_id_ot[$i]= str_replace('"', " ", $array_str_id_ot[$i]);
                $array_str_id_ot[$i]= str_replace(']', "", $array_str_id_ot[$i]);
                if(strlen($array_str_id_ot[$i])<=0){
                    $array_str_id_ot[$i]="NULL";
                }
                $array_str_imputderivada[$i]= str_replace('[', "", $array_str_imputderivada[$i]);
                $array_str_imputderivada[$i]= str_replace('"', "", $array_str_imputderivada[$i]);
                $array_str_imputderivada[$i]= str_replace(']', "", $array_str_imputderivada[$i]);
                if(strlen($array_str_imputderivada[$i])<=0){
                    $array_str_imputderivada[$i]="NULL";
                }

                $array_str_imputhem[$i]= str_replace('[', "", $array_str_imputhem[$i]);
                $array_str_imputhem[$i]= str_replace('"', "", $array_str_imputhem[$i]);
                $array_str_imputhem[$i]= str_replace(']', "", $array_str_imputhem[$i]);
                if(strlen($array_str_imputhem[$i])<=0){
                    $array_str_imputhem[$i]="NULL";
                }

                $array_str_imputtipocambio[$i]= str_replace('[', "", $array_str_imputtipocambio[$i]);
                $array_str_imputtipocambio[$i]= str_replace('"', "", $array_str_imputtipocambio[$i]);
                $array_str_imputtipocambio[$i]= str_replace(']', "", $array_str_imputtipocambio[$i]);
                if(strlen($array_str_imputtipocambio[$i])<=0){
                    $array_str_imputtipocambio[$i]="NULL";
                }

                $array_str_imputobservacion[$i]= str_replace('[', "", $array_str_imputobservacion[$i]);
                $array_str_imputobservacion[$i]= str_replace('"', "", $array_str_imputobservacion[$i]);
                $array_str_imputobservacion[$i]= str_replace(']', "", $array_str_imputobservacion[$i]);
                if(strlen($array_str_imputobservacion[$i])<=0){
                    $observacion=",ospa_observacion =NULL";
                }else{
                    $observacion=",ospa_observacion ='".$array_str_imputobservacion[$i]."'";
                }   
                $query = "  UPDATE  os_servicio_pago 
                            SET hem_id =  $array_str_imputhem[$i]
                            ,derivada_id =  $array_str_imputderivada[$i]
                            ,ospa_tipo_cambio =  $array_str_imputtipocambio[$i]
                            $observacion
                            WHERE ospa_id = $array_str_id_ot[$i]";
                $res = $db->ExecuteQuery($query);  
                if(0==$res['status']){
                    $db->Rollback();
                    Flight::json(array("status" => 0, "error" => $res['error']));
                    return;
                }
                   
            }
        }else   {
           for($i=0; $i<$longitud; $i++) {
             $query="";
                    
                $array_str_id_ot[$i]= str_replace('[', "", $array_str_id_ot[$i]);
                $array_str_id_ot[$i]= str_replace('"', " ", $array_str_id_ot[$i]);
                $array_str_id_ot[$i]= str_replace(']', "", $array_str_id_ot[$i]);
                if(strlen($array_str_id_ot[$i])<=0)
                {
                    $array_str_id_ot[$i]="NULL";
                }
                $array_str_imputderivada[$i]= str_replace('[', "", $array_str_imputderivada[$i]);
                $array_str_imputderivada[$i]= str_replace('"', "", $array_str_imputderivada[$i]);
                $array_str_imputderivada[$i]= str_replace(']', "", $array_str_imputderivada[$i]);
                if(strlen($array_str_imputderivada[$i])<=0)
                {
                    $array_str_imputderivada[$i]="NULL";
                }

                $array_str_imputhem[$i]= str_replace('[', "", $array_str_imputhem[$i]);
                $array_str_imputhem[$i]= str_replace('"', "", $array_str_imputhem[$i]);
                $array_str_imputhem[$i]= str_replace(']', "", $array_str_imputhem[$i]);
                if(strlen($array_str_imputhem[$i])<=0)
                {
                    $array_str_imputhem[$i]="NULL";
                }

                $array_str_imputtipocambio[$i]= str_replace('[', "", $array_str_imputtipocambio[$i]);
                $array_str_imputtipocambio[$i]= str_replace('"', "", $array_str_imputtipocambio[$i]);
                $array_str_imputtipocambio[$i]= str_replace(']', "", $array_str_imputtipocambio[$i]);
                if(strlen($array_str_imputtipocambio[$i])<=0)
                {
                    $array_str_imputtipocambio[$i]="NULL";
                }

                $array_str_imputobservacion[$i]= str_replace('[', "", $array_str_imputobservacion[$i]);
                $array_str_imputobservacion[$i]= str_replace('"', "", $array_str_imputobservacion[$i]);
                $array_str_imputobservacion[$i]= str_replace(']', "", $array_str_imputobservacion[$i]);
               if(strlen($array_str_imputobservacion[$i])<=0)
                {
                    $observacion=",mnpa_observacion =NULL";
                }else{
                    $observacion=",mnpa_observacion ='".$array_str_imputobservacion[$i]."'";
                }   
                    
                $query = "  UPDATE  mnt_servicio_pago 
                            SET hem_id = $array_str_imputhem[$i]
                            ,derivada_id = $array_str_imputderivada[$i]
                            ,mnpa_tipo_cambio = $array_str_imputtipocambio[$i]
                            ,mnpa_observacion = '$array_str_imputobservacion[$i]'
                            WHERE mnpa_id = $array_str_id_ot[$i]";

                $res = $db->ExecuteQuery($query);  
                if(0==$res['status']){
                    $db->Rollback();
                    Flight::json(array("status" => 0, "error" => $res['error']));
                    return;
                }
            }
        }                     
        $db->commit(); 
        Flight::json(array("status"=>true,"Se ingresaron los datos correctamente"));   
    });

    /* 
    Guarda detalle os periodo  del boton generar Costeo
    */
    Flight::route('POST /contrato/@cont_id:[0-9]+/pago/costeo/actividad/add', function($cont_id){

        $db = new MySQL_Database();
        $pago = $_POST;
        $res = array();

        $copa_id=$pago['copa_id'];
        $periodo=$pago['periodo'];
        $actividad=$pago['actividad'];
        $monto_total_cabecera=$pago['monto_total_cabecera'];
        $monto_total_cabecera = str_replace(",", ".", $monto_total_cabecera);   
        $db->startTransaction();

        $existencia = " SELECT count(1)
                            copa_periodo 
                        FROM 
                            costeo_pago 
                        WHERE cont_id = $cont_id
                        AND copa_periodo =$periodo
                        AND copa_tipo_ot='$actividad'";
        $res = $db->ExecuteQuery($existencia);
        if(0==$res['status']){
            $db->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        $str_id_ot=$pago['str_id_ot'];      
        if (0<strlen($str_id_ot)) {
            $str_id_ot=$pago['str_id_ot'];
            $array_str_id_ot= explode(";", $str_id_ot);
            
            $str_check_penalidad_value=$pago['str_check_penalidad_value'];
            $array_str_check_penalidad_value = explode(";", $str_check_penalidad_value);
                        
            $str_monto_total=$pago['str_monto_total'];
            $array_str_monto_total = explode(";",$str_monto_total);
            
            $str_cantidad_lpu=$pago['str_cantidad_lpu'];
            $array_str_cantidad_lpu = explode(";",$str_cantidad_lpu);

            $str_cantidad_lpu=$pago['str_cantidad_lpu'];
            $array_str_cantidad_lpu = explode(";",$str_cantidad_lpu);
            
        }
        
        /* Saco el numero de elementos dentro del string*/
        $longitud = count($array_str_id_ot);
        $cant_reg=0;

        /* Se descarta el primier elemento por ser un ";"*/
        if('MNT'==$actividad)        {
            for($i=1; $i<$longitud; $i++) {
                $query="";
                
                if("SI"==$array_str_check_penalidad_value[$i]){
                    $query = "  UPDATE  mnt_servicio_pago 
                                SET copa_id = $copa_id
                                    ,mnpa_monto_capex = if (mnpa_seleccion_codigo= 'CAPEX',REPLACE(REPLACE('$array_str_monto_total[$i]','.',''),',','.'),mnpa_monto_capex )
                                    ,mnpa_monto_opex=if(mnpa_seleccion_codigo='OPEX',REPLACE(REPLACE('$array_str_monto_total[$i]','.',''),',','.'),mnpa_monto_opex)
                                    ,mnpa_estado_mnt = 'ENPROCESO'
                                    ,mnpa_cant_lpu =REPLACE(REPLACE('$array_str_cantidad_lpu[$i]','.',''),',','.')
                                    ,mnpa_penalidad = '$array_str_check_penalidad_value[$i]'
                                WHERE mnpa_id = $array_str_id_ot[$i]";

                    $res = $db->ExecuteQuery($query);  
                    if(0==$res['status']){
                        $db->Rollback();
                        Flight::json(array("status" => 0, "error" => $res['error']));
                        return;
                    }
                    $cant_reg++;    
                }      
            }
            $queryCapexOpex=   "SELECT   SUM(IF( mnpa_seleccion_codigo= 'CAPEX', mnpa_monto_capex, 0)) AS suma_monto_capex
    									 ,SUM(IF( mnpa_seleccion_codigo= 'OPEX',  mnpa_monto_opex, 0)) AS suma_monto_opex
    									FROM mnt_servicio_pago
    									WHERE copa_id = $copa_id
    									GROUP BY copa_id ";
        } else {
            for($i=1; $i<$longitud; $i++) {
                $query=""; 
                if("SI"==$array_str_check_penalidad_value[$i]){
                    $query = "  UPDATE  os_servicio_pago 
                                SET copa_id = $copa_id
                                    ,ospa_monto_capex = if (ospa_seleccion_codigo= 'CAPEX',REPLACE(REPLACE('$array_str_monto_total[$i]','.',''),',','.'),ospa_monto_capex )
                                    ,ospa_monto_opex=if(ospa_seleccion_codigo='OPEX',REPLACE(REPLACE('$array_str_monto_total[$i]','.',''),',','.'),ospa_monto_opex)
                                    ,ospa_estado_os = 'ENPROCESO'
                                    ,ospa_cant_lpu =REPLACE(REPLACE('$array_str_cantidad_lpu[$i]','.',''),',','.')
                                    ,ospa_penalidad = '$array_str_check_penalidad_value[$i]'
                                WHERE ospa_id = $array_str_id_ot[$i]";
                    $res = $db->ExecuteQuery($query);  
                    if(0==$res['status']){
                        $db->Rollback();
                        Flight::json(array("status" => 0, "error" => $res['error']));
                        return;
                    }
                    $cant_reg++;    
                }      
            }
            $queryCapexOpex=   "SELECT   SUM(IF( ospa_seleccion_codigo= 'CAPEX', ospa_monto_capex, 0)) AS suma_monto_capex
                                        ,SUM(IF( ospa_seleccion_codigo= 'OPEX',  ospa_monto_opex, 0)) AS suma_monto_opex
                                        FROM os_servicio_pago
                                        WHERE copa_id = $copa_id
                                        GROUP BY copa_id ";
        }

        $res = $db->ExecuteQuery($queryCapexOpex);  
        if(0==$res['status']){
            $db->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        $CAPEX=$res['data'][0]['suma_monto_capex'];
        $OPEX=$res['data'][0]['suma_monto_opex'];    
        $queryMontoTotal= " UPDATE costeo_pago 
                            SET  copa_monto_capex = ROUND($CAPEX,2)
                            ,copa_monto_opex =ROUND($OPEX,2)
                            ,copa_monto_total =ROUND($CAPEX,2)+ROUND($OPEX,2)
                            WHERE  copa_id = $copa_id";

        $res = $db->ExecuteQuery($queryMontoTotal);  
        if(0==$res['status']){
            $db->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        $db->commit(); 
        Flight::json(array("status"=>true,"Se ingresaron los datos correctamente"));   
    });
                                        
    Flight::route('GET|POST /contrato/@cont_id:[0-9]+/periodo/@periodo:[0-9]+/pago/copa/@copa_id/acta/mnt', function($cont_id,$periodo,$copa_id){
        global $BASEDIR;
        setlocale(LC_ALL,"es_ES");
        $usua_creador  = $_SESSION['user_id'];
        require_once($BASEDIR."../libs/lightncandy/lightncandy.php");
        require_once($BASEDIR."../libs/dompdf/dompdf_config.inc.php");
        $dbo = new MySQL_Database();
        $out = array();
        $out['copa_id'] = $copa_id;

        $res = $dbo->ExecuteQuery("SELECT repo_tabla
                                    FROM repositorio_pago 
                                    WHERE repo_tipo_doc = 'ACTA' 
                                    AND repo_descripcion = $periodo
                                    AND repo_nombre = 'MNT' ;
                                ");
        if(0==$res['status']){
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }

        if(0==$res['rows']){
            $estado="";
        } else {
            $estado = $res['data'][0]['repo_tabla'];
        }
        if('ACTA_VALIDADO'== $estado || 'APROBADO'== $estado){
            $res = $dbo->ExecuteQuery("SELECT
                                repo_nombre,
                                repo_ruta
                               FROM
                               repositorio_pago
                               WHERE 
                                repo_descripcion = '$periodo'
                           
                                AND repo_nombre = 'MNT'
                               ORDER BY repo_fecha_creacion DESC
                               LIMIT 1
                ");
                if($res['status']){
                  if(0<$res['rows']){
                    $ruta     =  $res['data'][0]['repo_ruta'];
                    $ext      =  pathinfo($ruta, PATHINFO_EXTENSION);
                    $filename =  $res['data'][0]['repo_nombre'].".".$ext;

                    if (file_exists("../".$ruta)) {
                        switch($ext){
                            case "xls":{
                                $contentType = "application/vnd.ms-excel";
                                break;
                            }
                            default:{
                                $contentType = "application/octet-stream";
                            }
                        }

                        header('Content-Type: '.$contentType);
                        header('Content-Disposition: attachment; filename="'.$filename.'"');
                        header('Cache-Control: max-age=0');
                        header('Cache-Control: max-age=1');
                        header('Set-Cookie: fileDownload=true; path=/');
                        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      // Date in the past
                        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                        header ('Cache-Control: cache, must-revalidate');       // HTTP/1.1
                        header ('Pragma: public');                // HTTP/1.0
                        readfile("../".$ruta);
                        exit;
                    } else {
                        echo "Archivo no existe ($filename)";
                    }
                  } else {
                      echo "No hay archivos asociados a $tabla (ID: $tabla_id)";
                  }
                } else {
                    echo $res['error'];
                }

        }else {
            ini_set('display_errors', TRUE);
            try{
                $filename = "../reportes/".date("dmyHis")."_ActaMNT.pdf";
                $fecha=date('d-m-Y');
                $out['fecha'] = $fecha;
                $res = $dbo->ExecuteQuery("SELECT cont_nombre FROM  contrato WHERE cont_id = $cont_id");
                if(0==$res['status']){
                    Flight::json(array("status" => 0, "error" => $res['error']));
                    return;
                }
                $out['contrato'] = $res['data'][0]['cont_nombre'];

                $res = $dbo->ExecuteQuery(" SELECT e.empr_nombre
                                            FROM     empresa e
                                                    , rel_contrato_empresa r
                                            WHERE r.cont_id = $cont_id
                                            AND r.empr_id = e.empr_id
                                            AND r.coem_tipo = 'CONTRATISTA'");
                if(0==$res['status']){
                    Flight::json(array("status" => 0, "error" => $res['error']));
                    return;
                }
                $out['empresa'] = $res['data'][0]['empr_nombre'];
                $res = $dbo->ExecuteQuery("SELECT
                            copa_id
                            ,copa_estado
                            ,copa_monto_total
                            ,copa_monto_total_sin_descuento
                            ,copa_periodo_penalidad
                            , CASE
                                    WHEN substring(copa_periodo,5,2) = '01' THEN 'Enero del'
                                    WHEN substring(copa_periodo,5,2) = '02' THEN 'Febrero del'
                                    WHEN substring(copa_periodo,5,2) = '03' THEN 'Marzo del'
                                    WHEN substring(copa_periodo,5,2) = '04' THEN 'Abril del'
                                    WHEN substring(copa_periodo,5,2) = '05' THEN 'Mayo del'
                                    WHEN substring(copa_periodo,5,2) = '06' THEN 'Junio del'
                                    WHEN substring(copa_periodo,5,2) = '07' THEN 'Julio del'
                                    WHEN substring(copa_periodo,5,2) = '08' THEN 'Agosto del'
                                    WHEN substring(copa_periodo,5,2) = '09' THEN 'Septiembre del'
                                    WHEN substring(copa_periodo,5,2) = '10' THEN 'Octubre del'
                                    WHEN substring(copa_periodo,5,2) = '11' THEN 'Noviembre del'
                                    ELSE 'Diciembre del' END mes
                            , substring(copa_periodo,1,4) ano
                            , CASE WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='12' THEN 'Diciembre del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='01' THEN 'Enero del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='02' THEN 'Febrero del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='03' THEN 'Marzo del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='04' THEN 'Abril del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='05' THEN 'Mayo del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='06' THEN 'Junio del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='07' THEN 'Julio del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='08' THEN 'Agosto del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='09' THEN 'Septiembre del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='10' THEN 'Octubre del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='11' THEN 'Noviembre del'
                            END mesAnterior
                            ,substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),1,4) anoAnterior
                            FROM costeo_pago
                            WHERE copa_periodo = $periodo
                            AND cont_id =$cont_id
                            AND copa_tipo_ot='MNT'");
                if(0==$res['status']){
                    Flight::json(array("status" => 0, "error" => $res['error']));
                    return;
                }
                $copa_periodo_penalidad="";
                if(isset($res['data'][0]['copa_periodo_penalidad'])){
                    $copa_periodo_penalidad =$res['data'][0]['copa_periodo_penalidad'];
                }
                
                $copa_monto_total =$res['data'][0]['copa_monto_total'];
                $copa_monto_total_sin_descuento = $res['data'][0]['copa_monto_total_sin_descuento'];
                $copa_id = $res['data'][0]['copa_id'];  
                $copa_estado = $res['data'][0]['copa_estado'] ;
                $estadoVal="display:none";
                $lista_validador = "";
                if (0<$res['rows']) {
                    if (
                        "PEND_VALIDAR_ACTA"==$copa_estado ||
                        "APROBADO"==$copa_estado||
                        "ACTA_VALIDADO"==$copa_estado ) {
                        $estadoVal="display: ";

                        $res_usuarios = $dbo->ExecuteQuery(" SELECT r.usua_id
                                                        ,u.usua_nombre
                                                        ,r.rcuv_fecha_validacion
                                                        ,e.empr_nombre
                                                FROM rel_costeo_usuario_validador r
                                                ,  usuario u
                                                , empresa e
                                                WHERE r.copa_id=$copa_id
                                                AND r.usua_id = u.usua_id
                                                AND r.rcuv_tipo_validacion = 'VALIDADOR_ACTA'
                                                AND r.rcuv_fecha_validacion IS NOT NULL
                                                AND e.empr_id = u.empr_id");
                        if(0==$res_usuarios['status']){
                            Flight::json(array("status" => 0, "error" => $res_usuarios['error']));
                            return;
                        }
                        if(0<$res_usuarios['rows']){
                            foreach($res_usuarios['data'] AS $row){
                                $lista_validador = $lista_validador.' <tr><td align="center"> '.$row['usua_nombre'].' </td><td align="center"> '.$row['empr_nombre'].' </td><td align="center"> '.$row['rcuv_fecha_validacion'].' </td></tr>' ;
                            }

                        }
                        
                    }
                }

                $out['estadoVal']=$estadoVal;
                $out['lista_validador']=$lista_validador;
                $out['mes']=$res['data'][0]['mes'];
                $out['ano']=$res['data'][0]['ano'];
                $out['mesAnterior']=$res['data'][0]['mesAnterior'];
                $out['anoAnterior']=$res['data'][0]['anoAnterior'];
                $ano=$res['data'][0]['ano'];

                $res = $dbo->ExecuteQuery("SELECT count(1) AS cantidad from (
                                    select mant_id
                                    from mnt_servicio_pago m
                                    ,costeo_pago c
                                    where m.cont_id = $cont_id
                                    AND c.cont_id = m.cont_id
                                    AND c.copa_id =m.copa_id
                                    AND c.copa_periodo = $periodo
                                    AND c.copa_tipo_ot='MNT'
                            ) a");
                if(0==$res['status']){
                    Flight::json(array("status" => 0, "error" => $res['error']));
                    return;
                }
                $out['cantidad']=$res['data'][0]['cantidad'];
    			
    			$res = $dbo->ExecuteQuery("SELECT count(1) AS penalidad from (
                                    select mant_id
                                    from mnt_servicio_pago m
                                    ,costeo_pago c
                                    where m.cont_id = $cont_id
                                    AND c.cont_id = m.cont_id
                                    AND c.copa_id =m.copa_id
                                    AND c.copa_periodo = $periodo
                                    AND c.copa_tipo_ot='MNT'
    								AND m.mnpa_penalidad = 'SI'
                            ) a");
                if(0==$res['status']){
                    Flight::json(array("status" => 0, "error" => $res['error']));
                    return;
                      
                }
    		    $penalidad = 'No corresponde cursar penalidad';
                
    			if (0<$res['data'][0]['penalidad']){
                    $monto_penalidad = $copa_monto_total_sin_descuento-$copa_monto_total;
                    $monto_penalidad = number_format($monto_penalidad, 2, ',', '.');
    				$penalidad = 'Corresponde cursar penalidad según medición de SLA del periodo  '.$copa_periodo_penalidad.'  '.$ano.'  por un total de '.$monto_penalidad.' '.$moneda;
    			}
    			
                $out['penalidad']=$penalidad;
              
                #$penalidad = 'Corresponde cursar penalidad según medición de SLA del periodo'+$mesAnterior+'-'+$mes+'  '+$ano+'por un total de '+$monto_penalidad+' UF'
                #Cantidad total de registros por mes de aprobacion de MNT
                $res = $dbo->ExecuteQuery("SELECT count(1) cant
                                        , CASE  WHEN substring(mnpa_periodo_aprobacion,5,2) = '01' THEN 'Enero del'
                                                WHEN substring(mnpa_periodo_aprobacion,5,2) = '02' THEN 'Febrero del'
                                                WHEN substring(mnpa_periodo_aprobacion,5,2) = '03' THEN 'Marzo del'
                                                WHEN substring(mnpa_periodo_aprobacion,5,2) = '04' THEN 'Abril del'
                                                WHEN substring(mnpa_periodo_aprobacion,5,2) = '05' THEN 'Mayo del'
                                                WHEN substring(mnpa_periodo_aprobacion,5,2) = '06' THEN 'Junio del'
                                                WHEN substring(mnpa_periodo_aprobacion,5,2) = '07' THEN 'Julio del'
                                                WHEN substring(mnpa_periodo_aprobacion,5,2) = '08' THEN 'Agosto del'
                                                WHEN substring(mnpa_periodo_aprobacion,5,2) = '09' THEN 'Septiembre del'
                                                WHEN substring(mnpa_periodo_aprobacion,5,2) = '10' THEN 'Octubre del'
                                                WHEN substring(mnpa_periodo_aprobacion,5,2) = '11' THEN 'Noviembre del'
                                                WHEN substring(mnpa_periodo_aprobacion,5,2) = '12' THEN 'Diciembre del'
                                            END mes_periodo_aprobacion
                                        ,substring(mnpa_periodo_aprobacion,1,4) ano_periodo_aprobacion
                                    FROM (
                                        SELECT mant_id
                                            , mnpa_periodo_aprobacion
                                        FROM mnt_servicio_pago m
                                        ,costeo_pago c
                                        WHERE c.cont_id = $cont_id
                                        AND c.cont_id = m.cont_id
                                        AND c.copa_id =m.copa_id
                                        AND c.copa_periodo = $periodo
                                        AND c.copa_tipo_ot='MNT'
                                        GROUP BY m.mant_id
                                    ) a
                                    GROUP BY mnpa_periodo_aprobacion
                ");
                if(0==$res['status']){
                    Flight::json(array("status" => 0, "error" => $res['error']));
                    return;
                }
                $out['AprobadasTotal']="0";
                $out['AprobadasMes']="";
                
                $total=0;
                foreach($res['data'] AS $row){
                    $out['AprobadasMes'] = $out['AprobadasMes'].' '.$row['cant'].' de '.$row['mes_periodo_aprobacion'].' '.$row['ano_periodo_aprobacion'].',';
                    $total = $total + $row['cant'];
                }
                $out['AprobadasTotal']=$total;

                $res = $dbo->ExecuteQuery(" SELECT copa_monto_total
                                                ,copa_monto_total_sin_descuento
                                            FROM costeo_pago
                                            WHERE copa_periodo = $periodo
                                            AND cont_id =$cont_id
                                            and copa_tipo_ot='MNT'");
                if(0==$res['status']){
                    Flight::json(array("status" => 0, "error" => $res['error']));
                    return;
                }
               
                $copa_monto_total=$res['data'][0]['copa_monto_total'];
                $copa_monto_total = number_format($copa_monto_total, 2, ',', '.');
                $out['total']=$copa_monto_total;
                $copa_monto_total_sin_descuento=$res['data'][0]['copa_monto_total_sin_descuento'];
                $copa_monto_total_sin_descuento = number_format($copa_monto_total_sin_descuento, 2, ',', '.');
                $out['monto_total_sin_descuento']=$copa_monto_total_sin_descuento;

                $ruta=substr($filename,3); 
        
                $query = "INSERT INTO repositorio_pago (
                                            repo_tipo_doc
                                            ,repo_nombre
                                            ,repo_descripcion
                                            ,repo_tabla
                                            ,repo_id_relacionado
                                            ,repo_ruta
                                            ,repo_fecha_creacion
                                            ,repo_data,usua_creador)
                                VALUES(
                                    'ACTA',
                                    'MNT',
                                    '$periodo',
                                    '$copa_estado',
                                    $copa_id,
                                    '$ruta',
                                    NOW(),
                                    NULL,
                                   $usua_creador
                            )";                                  

                    $res = $dbo->ExecuteQuery($query);
                    if(0==$res['status']){
                        Flight::json(array("status" => 0, "error" => $res['error']));
                        return;
                    }                
            
                include("templates/acta.php");

                //CORRECCION LECTURA FOTOS DINAMICAS / MONEDA PARAMETRIZADA - JRFC - INICIO
                global $BASEDIR;
                $out['basedir']=$BASEDIR;
                global $TIPOMONEDA;
                $out['tipo_moneda']=$TIPOMONEDA;
                //CORRECCION LECTURA FOTOS DINAMICAS / MONEDA PARAMETRIZADA - JRFC - FIN

                $html = renderActa($out);
                $dompdf = new DOMPDF();
                $dompdf->load_html($html);
                $dompdf->set_base_path(realpath(__DIR__ . '/..'));
                $dompdf->set_paper("letter","portrait");
                $dompdf->render();
                file_put_contents($filename,$dompdf->output());
                header('Content-Type:application/pdf');
                header('Content-Disposition: attachment; filename="'.$filename.'";');
                header('Set-Cookie: fileDownload=true; path=/');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                readfile($filename);
            }
            catch (Exception $e) {
               return array("status"=>false,"error"=>"Error al generar pdf: ".$e->getMessage());
            }
            exit;
        
        }
    });

    Flight::route('GET|POST /contrato/@cont_id:[0-9]+/periodo/@periodo:[0-9]+/pago/copa/@copa_id:[0-9]+/acta/os', function($cont_id,$periodo,$copa_id){
        global $BASEDIR;
        setlocale(LC_ALL,"es_ES");
    
        require_once($BASEDIR."../libs/lightncandy/lightncandy.php");
        require_once($BASEDIR."../libs/dompdf/dompdf_config.inc.php");
        $dbo = new MySQL_Database();
        $usuario_creador = $_SESSION['user_id'];
        $out['copa_id'] = $copa_id;
        
        $res = $dbo->ExecuteQuery("SELECT repo_tabla
                                    FROM repositorio_pago 
                                    WHERE repo_tipo_doc = 'ACTA' 
                                    AND repo_descripcion = $periodo
                                    AND repo_id_relacionado =$copa_id");
        if(0==$res['status']){
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }

        if(0==$res['rows']){
            $estado="";
        } else {
            $estado = $res['data'][0]['repo_tabla'];
        }
        if('ACTA_VALIDADO'== $estado || 'APROBADO'== $estado){

             $res = $dbo->ExecuteQuery("SELECT
                                repo_nombre,
                                repo_ruta
                               FROM
                                repositorio
                               WHERE 
                                repo_descripcion = '$periodo'
                                AND repo_id_relacionado = $copa_id
                               ORDER BY repo_fecha_creacion DESC
                               LIMIT 1
            ");
            if($res['status']){
                if(0<$res['rows']){
                    $ruta     =  $res['data'][0]['repo_ruta'];
                    $ext      =  pathinfo($ruta, PATHINFO_EXTENSION);
                    $filename =  $res['data'][0]['repo_nombre'].".".$ext;

                    if (file_exists("../".$ruta)) {
                        switch($ext){
                            case "xls":{
                                $contentType = "application/vnd.ms-excel";
                                break;
                            }
                            default:{
                                $contentType = "application/octet-stream";
                            }
                        }

                        header('Content-Type: '.$contentType);
                        header('Content-Disposition: attachment; filename="'.$filename.'"');
                        header('Cache-Control: max-age=0');
                        header('Cache-Control: max-age=1');
                        header('Set-Cookie: fileDownload=true; path=/');
                        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      // Date in the past
                        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                        header ('Cache-Control: cache, must-revalidate');       // HTTP/1.1
                        header ('Pragma: public');                // HTTP/1.0

                        readfile("../".$ruta);
                        exit;
                    } else {
                        echo "Archivo no existe ($filename)";
                    }
                } else {
                    echo "No hay archivos asociados a $tabla (ID: $tabla_id)";
                }
            } else {
                echo $res['error'];
            }

        }else {

            ini_set('display_errors', TRUE);
            try{
                    //$filename = date("dmyHis")."_ActaOS.pdf";
                    $filename = "../reportes/".date("dmyHis")."_ActaOS.pdf";

                    $fecha=date('d-m-Y');
                    $out['fecha'] = $fecha;

                    $res = $dbo->ExecuteQuery("SELECT cont_nombre FROM  contrato WHERE cont_id = $cont_id");
                    if(0==$res['status']){
                        Flight::json(array("status" => 0, "error" => $res['error']));
                        return;
                    }
                    $out['contrato'] = $res['data'][0]['cont_nombre'];

                    $res = $dbo->ExecuteQuery(" SELECT e.empr_nombre
                                            FROM     empresa e
                                                    , rel_contrato_empresa r
                                            WHERE r.cont_id = $cont_id
                                            AND r.empr_id = e.empr_id
                                            AND r.coem_tipo = 'CONTRATISTA'");

                    if(0==$res['status']){
                        Flight::json(array("status" => 0, "error" => $res['error']));
                        return;
                    }
                    $out['empresa'] = $res['data'][0]['empr_nombre'];

                    $res = $dbo->ExecuteQuery("SELECT
                             copa_tipo_ot
                            ,copa_estado
                            ,copa_monto_total
                            ,copa_monto_total_sin_descuento
                            ,copa_periodo_penalidad
                            , CASE
                                    WHEN substring(copa_periodo,5,2) = '01' THEN 'Enero del'
                                    WHEN substring(copa_periodo,5,2) = '02' THEN 'Febrero del'
                                    WHEN substring(copa_periodo,5,2) = '03' THEN 'Marzo del'
                                    WHEN substring(copa_periodo,5,2) = '04' THEN 'Abril del'
                                    WHEN substring(copa_periodo,5,2) = '05' THEN 'Mayo del'
                                    WHEN substring(copa_periodo,5,2) = '06' THEN 'Junio del'
                                    WHEN substring(copa_periodo,5,2) = '07' THEN 'Julio del'
                                    WHEN substring(copa_periodo,5,2) = '08' THEN 'Agosto del'
                                    WHEN substring(copa_periodo,5,2) = '09' THEN 'Septiembre del'
                                    WHEN substring(copa_periodo,5,2) = '10' THEN 'Octubre del'
                                    WHEN substring(copa_periodo,5,2) = '11' THEN 'Noviembre del'
                                    ELSE 'Diciembre del' END mes
                            , substring(copa_periodo,1,4) ano
                            , CASE WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='12' THEN 'Diciembre del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='01' THEN 'Enero del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='02' THEN 'Febrero del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='03' THEN 'Marzo del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='04' THEN 'Abril del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='05' THEN 'Mayo del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='06' THEN 'Junio del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='07' THEN 'Julio del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='08' THEN 'Agosto del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='09' THEN 'Septiembre del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='10' THEN 'Octubre del'
                                   WHEN substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),6,2) ='11' THEN 'Noviembre del'
                            END mesAnterior
                            ,substring(date_sub(CONCAT(substring(copa_periodo,1,4),'-',substring(copa_periodo,5,2),'-01') , INTERVAL 1 MONTH),1,4) anoAnterior
                            FROM costeo_pago
                            WHERE copa_periodo = $periodo
                            AND cont_id =$cont_id
                            AND copa_id = $copa_id");

                    if(0==$res['status']){
                        Flight::json(array("status" => 0, "error" => $res['error']));
                        return;
                    }

                    $copa_periodo_penalidad="";
                    if(isset($res['data'][0]['copa_periodo_penalidad']))
                    {
                        $copa_periodo_penalidad =$res['data'][0]['copa_periodo_penalidad'];
                    }
                    $copa_monto_total =$res['data'][0]['copa_monto_total'];
                    $copa_monto_total_sin_descuento = $res['data'][0]['copa_monto_total_sin_descuento'];
                    $copa_tipo_ot=$res['data'][0]['copa_tipo_ot'];
                    $copa_estado=$res['data'][0]['copa_estado'];
                    $estadoVal="display:none";
                    $lista_validador = "";
                    if (0<$res['rows']) {
                        if (
                            "PEND_VALIDAR_ACTA"==$copa_estado ||
                            "APROBADO"==$copa_estado||
                            "ACTA_VALIDADO"==$copa_estado) {
                            $estadoVal="display: ";

                           
                            $res_usuarios = $dbo->ExecuteQuery(" SELECT r.usua_id
                                                            ,u.usua_nombre
                                                        ,r.rcuv_fecha_validacion
                                                        ,e.empr_nombre
                                                FROM rel_costeo_usuario_validador r
                                                ,  usuario u
                                                , empresa e
                                                WHERE r.copa_id=$copa_id
                                                AND r.usua_id = u.usua_id
                                                AND r.rcuv_tipo_validacion = 'VALIDADOR_ACTA'
                                                AND r.rcuv_fecha_validacion IS NOT NULL
                                                AND e.empr_id = u.empr_id");
                            if(0==$res_usuarios['status']){
                                Flight::json(array("status" => 0, "error" => $res_usuarios['error']));
                                return;
                            }
                            if(0<$res_usuarios['rows']){
                                foreach($res_usuarios['data'] AS $row){
                                    $lista_validador = $lista_validador.' <tr><td align="center"> '.$row['usua_nombre'].' </td><td align="center"> '.$row['empr_nombre'].' </td><td align="center"> '.$row['rcuv_fecha_validacion'].' </td></tr>' ;
                                }
                            }                        
                        }
                    }
                    
                    $out['estadoVal']=$estadoVal;
                    $out['lista_validador']=$lista_validador;
                    $out['mes']=$res['data'][0]['mes'];
                    $out['ano']=$res['data'][0]['ano'];
                    $out['mesAnterior']=$res['data'][0]['mesAnterior'];
                    $out['anoAnterior']=$res['data'][0]['anoAnterior'];
                    $ano=$res['data'][0]['ano'];
                    $res = $dbo->ExecuteQuery("SELECT count(1) AS cantidad from (
                                    select orse_id
                                    from os_servicio_pago m
                                    ,costeo_pago c
                                    where m.cont_id = $cont_id
                                    AND c.cont_id = m.cont_id
                                    AND  c.copa_id = $copa_id 
                                    AND c.copa_id =m.copa_id
                                    AND c.copa_periodo = $periodo
                            ) a");
                    if(0==$res['status']){
                        Flight::json(array("status" => 0, "error" => $res['error']));
                        return;
                    }
                    $out['cantidad']=$res['data'][0]['cantidad'];
        			
    			    $res = $dbo->ExecuteQuery("SELECT count(1) AS penalidad from (
                                    select orse_id
                                    from os_servicio_pago m
                                    ,costeo_pago c
                                    where m.cont_id = $cont_id
                                    AND c.cont_id = m.cont_id
                                    AND c.copa_id = $copa_id 
                                    AND c.copa_id =m.copa_id
                                    AND c.copa_periodo = $periodo
    								AND m.ospa_penalidad = 'SI'
                            ) a");
                    if(0==$res['status']){
                        Flight::json(array("status" => 0, "error" => $res['error']));
                        return;
                    }
                    $monto_penalidad = $copa_monto_total-$copa_monto_total_sin_descuento;
			        $penalidad = 'No corresponde cursar penalidad';
        			if (0<$res['data'][0]['penalidad']){
        				$monto_penalidad = $copa_monto_total_sin_descuento-$copa_monto_total;
                        $monto_penalidad = number_format($monto_penalidad, 2, ',', '.');
                        $penalidad = 'Corresponde cursar penalidad según medición de SLA del periodo  '.$copa_periodo_penalidad.'  '.$ano.'  por un total de '.$monto_penalidad.' '.$moneda;
        			}
			
                    $out['penalidad']=$penalidad;

                    #Cantidad total de registros por mes de aprobacion de OS
                    $res = $dbo->ExecuteQuery("SELECT count(1) cant
                                    , CASE  WHEN substring(ospa_periodo_aprobacion,5,2) = '01' THEN 'Enero del'
                                            WHEN substring(ospa_periodo_aprobacion,5,2) = '02' THEN 'Febrero del'
                                            WHEN substring(ospa_periodo_aprobacion,5,2) = '03' THEN 'Marzo del'
                                            WHEN substring(ospa_periodo_aprobacion,5,2) = '04' THEN 'Abril del'
                                            WHEN substring(ospa_periodo_aprobacion,5,2) = '05' THEN 'Mayo del'
                                            WHEN substring(ospa_periodo_aprobacion,5,2) = '06' THEN 'Junio del'
                                            WHEN substring(ospa_periodo_aprobacion,5,2) = '07' THEN 'Julio del'
                                            WHEN substring(ospa_periodo_aprobacion,5,2) = '08' THEN 'Agosto del'
                                            WHEN substring(ospa_periodo_aprobacion,5,2) = '09' THEN 'Septiembre del'
                                            WHEN substring(ospa_periodo_aprobacion,5,2) = '10' THEN 'Octubre del'
                                            WHEN substring(ospa_periodo_aprobacion,5,2) = '11' THEN 'Noviembre del'
                                            WHEN substring(ospa_periodo_aprobacion,5,2) = '12' THEN 'Diciembre del'
                                        END mes_periodo_aprobacion
                                    ,substring(ospa_periodo_aprobacion,1,4) ano_periodo_aprobacion
                                FROM (
                                    SELECT orse_id, ospa_periodo_aprobacion
                                    FROM os_servicio_pago m
                                    ,costeo_pago c
                                    WHERE c.cont_id = $cont_id
                                    AND c.cont_id = m.cont_id
                                    AND c.copa_id = $copa_id
                                    AND c.copa_id =m.copa_id
                                    AND c.copa_periodo = $periodo
                                    GROUP BY m.orse_id
                                ) a
                                GROUP BY ospa_periodo_aprobacion
                            ");
                    if(0==$res['status']){
                        Flight::json(array("status" => 0, "error" => $res['error']));
                        return;
                    }
                    $out['AprobadasTotal']="0";
                    $out['AprobadasMes']="";

                    $total=0;
                    foreach($res['data'] AS $row){
                        $out['AprobadasMes'] = $out['AprobadasMes'].' '.$row['cant'].' de '.$row['mes_periodo_aprobacion'].' '.$row['ano_periodo_aprobacion'].',';
                        $total = $total + $row['cant'];
                    }
                    $out['AprobadasTotal']=$total;


                    $res = $dbo->ExecuteQuery(" SELECT copa_monto_total
                                                    ,copa_monto_total_sin_descuento
                                                FROM costeo_pago
                                                WHERE copa_periodo = $periodo
                                                AND cont_id =$cont_id
                                                AND copa_id = $copa_id");
                    if(0==$res['status']){
                        Flight::json(array("status" => 0, "error" => $res['error']));
                        return;
                    }

                    $copa_monto_total=$res['data'][0]['copa_monto_total'];
                    $copa_monto_total = number_format($copa_monto_total, 2, ',', '.');
                    $out['total']=$copa_monto_total;
                    $copa_monto_total_sin_descuento=$res['data'][0]['copa_monto_total_sin_descuento'];
                    $copa_monto_total_sin_descuento = number_format($copa_monto_total_sin_descuento, 2, ',', '.');
                    $out['monto_total_sin_descuento']=$copa_monto_total_sin_descuento;
                    $ruta=substr($filename,3); 

                    $query = "INSERT INTO repositorio_pago (
                                            repo_tipo_doc
                                            ,repo_nombre
                                            ,repo_descripcion
                                            ,repo_tabla
                                            ,repo_id_relacionado
                                            ,repo_ruta
                                            ,repo_fecha_creacion
                                            ,repo_data,usua_creador)
                                VALUES(
                                    'ACTA',
                                    '$copa_tipo_ot',
                                    '$periodo',
                                    '$copa_estado',
                                    $copa_id,
                                    '$ruta',
                                    NOW(),
                                    NULL,
                                   $usuario_creador
                            )";

                    $res = $dbo->ExecuteQuery($query);
                    if(0==$res['status']){
                        Flight::json(array("status" => 0, "error" => $res['error']));
                        return;
                    }                

                    //CORRECCION LECTURA FOTOS DINAMICAS / MONEDA PARAMETRIZADA - JRFC - INICIO
                    global $BASEDIR;
                    $out['basedir']=$BASEDIR;
                    global $TIPOMONEDA;
                    $out['tipo_moneda']=$TIPOMONEDA;
                    //CORRECCION LECTURA FOTOS DINAMICAS / MONEDA PARAMETRIZADA - JRFC - FIN
                   
                    include("templates/acta_os.php");
                    $html = renderActa($out);
                    $dompdf = new DOMPDF();
                    $dompdf->load_html($html);
                    $dompdf->set_base_path(realpath(__DIR__ . '/..'));
                    $dompdf->set_paper("letter","portrait");
                    $dompdf->render();
                    file_put_contents($filename,$dompdf->output());
                    header('Content-Type:application/pdf');
                    header('Content-Disposition: attachment; filename="'.$filename.'";');
                    header('Set-Cookie: fileDownload=true; path=/');
                    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                    header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
                    header('Cache-Control: cache, must-revalidate');
                    header('Pragma: public');
                    readfile($filename);
                }
            catch (Exception $e) {
                return array("status"=>false,"error"=>"Error al generar pdf: ".$e->getMessage());
            }
            exit;
        }
    });
       
    Flight::route('GET /costeo/contrato/@contract/visualizar/documentacion/periodo/@periodo/costeo/@copa_id', function($cont_id,$periodo,$copa_id){

        $out = array();
        $out['status'] = 1;
        $db = new MySQL_Database();
        $documentacion = "
                        SELECT   repo_id
                                ,repo_nombre
                                ,repo_descripcion
                                ,repo_id_relacionado
                        FROM repositorio_pago 
                        WHERE repo_id_relacionado = $copa_id
                        AND repo_tipo_doc = 'DOCUMENTO'
                        ";
        $documentacion = $db->ExecuteQuery($documentacion);      
        $out['documentacion'] = $documentacion['data']; 
        $out['copa_id'] = $copa_id; 
        Flight::json($out);

    });
    Flight::route('GET|POST /costeo/documentacion/id_reporte/@repo_id/descargar', function($repo_id){
        global $BASEDIR;
        setlocale(LC_ALL,"es_ES");
        require_once($BASEDIR."../libs/lightncandy/lightncandy.php");
        require_once($BASEDIR."../libs/dompdf/dompdf_config.inc.php");
        $dbo = new MySQL_Database();
      
        $out = array();

           
        $res = $dbo->ExecuteQuery("SELECT   repo_nombre
                                                ,repo_ruta
                                    FROM repositorio_pago 
                                    WHERE repo_id = $repo_id
        ");
        if($res['status']){
            if(0<$res['rows']){
                $ruta     =  $res['data'][0]['repo_ruta'];
                $ext      =  pathinfo($ruta, PATHINFO_EXTENSION);
                $filename =  $res['data'][0]['repo_nombre'].".".$ext;
                if (file_exists("../".$ruta)) {
                    switch($ext){
                        case "xls":{
                            $contentType = "application/vnd.ms-excel";
                            break;
                        }
                        default:{
                           $contentType = "application/octet-stream";
                        }
                    }

                    header('Content-Type: '.$contentType);
                    header('Content-Disposition: attachment; filename="'.$filename.'"');
                    header('Cache-Control: max-age=0');
                    header('Cache-Control: max-age=1');
                    header('Set-Cookie: fileDownload=true; path=/');
                    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      // Date in the past
                    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                    header ('Cache-Control: cache, must-revalidate');       // HTTP/1.1
                    header ('Pragma: public');                // HTTP/1.0

                    readfile("../".$ruta);
                    exit;
                } else {
                    echo "Archivo no existe ($filename)";
                }
            } else {
                echo "No hay archivos asociados";
            }
        } else {
        echo $res['error'];
        }    
    });


    Flight::route('GET /pago/tipos_monedas', function(){
        $out = array();
        $out[0] = $GLOBALS['tipo_moneda'];
        $out[1] = $GLOBALS['tipo_moneda_pago'];
        Flight::json($out);
    });
?>