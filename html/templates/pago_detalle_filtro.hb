<script src="js/bootstrap-table.js" type="text/javascript" charset="utf-8"></script>    
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<script src="js/bootstrap-select.min.js" type="text/javascript" charset="utf-8"></script>
<form class="form-horizontal siom-form-tiny" role="form" action="#/pago/resumen/filtro" method="POST" id="pago-resumen-filtro">
   <div class="col-md-12">
      <div class="row" style="/*margin-left: 5px; */margin-top:20px;">
         <div class="col-md-4">
            <fieldset class="siom-fieldset" style="padding-bottom:0px; margin-top:0px; margin-left:10px; shape-margin:10px;  ">
               <legend>Seleccione fecha</legend>
               <div class="col-md-12">
                  <div class="form-group" style="display:block">
                     <div class="col-sm-5">
                        <button type="button" class="btn btn-primary btn-default" id="Limpiarfiltro">
                           <span class="glyphicon" aria-hidden="true"></span> Limpiar Filtros
                        </button>
                     </div>
                     <div class="col-md-7">
                        <button type="button" class="btn btn-primary btn-default" id="BuscarResumenPago" data-tipo="OS">Buscar</button>
                     </div>
                  </div>
                  <hr />
                  <div class="form-group" style="display:block">
                     <label for="id_papre_seleccion_contrato" title='Año'  class="col-sm-5 control-label">Año</label>
                     <div class="col-md-7">
                        <input type="text" id="pafi_anio" name="pafi_anio" maxlength="4" value="{{data.pafi_anio}}" />
                        
                     </div>
                  </div>
                  <br />
                  <div class="form-group" style="display:block">
                     <label for="pafi_mes" title='mes del contrato solicitado' class="col-sm-5 control-label">Mes</label>
                     <div class="col-md-7">
                        <select class="selectpicker" data-width="100%"  name="pafi_mes">
                           <option value="00">TODOS</option>
                           <option value="01">ENERO</option>
                           <option value="02">FEBRERO</option>
                           <option value="03">MARZO</option>
                           <option value="04">ABRIL</option>
                           <option value="05">MAYO</option>
                           <option value="06">JUNIO</option>
                           <option value="07">JULIO</option>
                           <option value="08">AGOSTO</option>
                           <option value="09">SEPTIEMBRE</option>
                           <option value="10">OCTUBRE</option>
                           <option value="11">NOVIEMBRE</option>
                           <option value="12">DICIEMBRE</option>
                        </select>
                     </div>
                  </div>
                  <br />
               </div>
            </fieldset>
         </div>
         <div class="col-md-8" id="pago-resumen-lista"></div>
      </div>
   </div>
</form>
<script src="js/pago_presupuesto.js" type="text/javascript" charset="utf-8"></script>
<script src="js/pago_detalle_filtro.js" type="text/javascript" charset="utf-8"></script>