<div class="col-md-12">
    <form class="form-horizontal siom-form-tiny" role="form"  action="#/pago/resumen/lista" method="POST" id="FormPagoResumenLista">
        <fieldset class="siom-fieldset" style="padding-bottom:0px; margin-top:0px; margin-left:10px; shape-margin:10px;  ">
            <legend>Resumen Costeo</legend>
            <div class="col-md-12">
                <div class="col-md-12" style="float: left; padding-top: 0px;  padding-bottom: 0px;padding-right: 0px;padding-left: 0px;">
	                <table id="tabla" data-toggle="table" data-sort-order="desc">
	                    <thead>
	                        <tr>
	                        	<th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Numero Costeo</th>
	                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Periodo</th>
	                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Tipo</th>
	                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Estado</th>
	                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Q</th>
	                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">P</th>
	                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Capex</th>
	                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Opex</th>
	                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Derivada</th>
	                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">HEM</th>
	                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true" style="display: none">Linea Presupuestaria</th>
								<th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Tipo Cambio</th>
								<th colspan="1" class="col-md-2" data-align="right" data-sortable="true"></th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        {{#monto}}
	                            <tr>
	                            	<td class="col-md-2">
										<a href="#/pago/costeo/periodo/{{copa_periodo}}/actividad/{{copa_tipo_ot}}/{{copa_id}}" class="click">{{copa_id}}</a>
										<input id="copa_id" name="copa_id"  value="{{copa_id}}" type="hidden"/>
	                                </td>
	                                <td class="col-md-2" >
										<input id="copa_periodo" name="copa_periodo" onkeypress="return validaNumericos(event)" value="{{copa_periodo}}" />
	                                </td>
	                                <td class="col-md-2" >
	                                	<input id="copa_tipo_ot" name="copa_tipo_ot"  value="{{copa_tipo_ot}}" />
	                                </td>
	                                <td class="col-md-2">
	                                	<input id="copa_estado" name="copa_estado"  value="{{copa_estado}}" />	
	                                </td>
	                                <td class="col-md-2">
	                                	<input id="copa_cantidad_total" name="copa_cantidad_total" onkeypress="return validaNumericos(event)" value="{{copa_cantidad_total}}"/>
	                                </td>
	                                <td class="col-md-2">
	                                	<input id="copa_monto_total" name="copa_monto_total" onkeypress="return validaNumericos(event)" value="{{copa_monto_total}}"/>
	                            	</td>
	                                <td class="col-md-2">
	                                	<input id="copa_monto_capex" name="copa_monto_capex" onkeypress="return validaNumericos(event)" value="{{copa_monto_capex}}"/>       	
	                                </td>
	                                <td  class="col-md-2">
	                                	<input id="copa_monto_opex" name="copa_monto_opex" onkeypress="return validaNumericos(event)" value="{{copa_monto_opex}}"/>
	                            	</td>
	                                <td class="col-md-2">
	                                    <input type="text" id="derivada" name="derivada"  onkeypress="return validaNumericos(event)" value="{{copa_derivada}}" value maxlength="20" />
	                                </td>
	                                <td class="col-md-2">
										<input type="text" id="hem" name="hem" onkeypress="return validaNumericos(event)" value="{{copa_hem}}" value maxlength="20" />
									</td>
									<td style="display: none" class="col-md-3">{{papr_nombre_servicio}}
										<input id="papr_id" name="papr_id"  value="{{papr_id}}" type="hidden"/>
									</td>
									<td class="col-md-2">
										<input type="text" id="tipo_cambio" name="tipo_cambio" onkeypress="return validaNumericos(event)" value="{{copa_tipo_cambio}}" value maxlength="20"  />
	                                </td>
									<td>
										<button id="guardarResumenCosteo" name="guardarResumenCosteo" type="button" class="btn btn-default guardarResumenCosteo" data-copa_id="{{copa_id}}" style="padding:2px;height:20px">
											<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
										</button>

										<a id="botonEditar" href="#/pago/editar/costeo/copa_id/{{copa_id}}" class="glyphicon glyphicon-pencil" style="display:{{copa_visibilidad_editar}}"></a>

										<a id="botonAsignar" href="#/pago/asignar/usuario/validador/copa_id/{{copa_id}}" class="glyphicon glyphicon-plus"></a>
									</td>
	                            </tr>
	                        {{/monto}}
	                    </tbody>
	                </table>
            	</div>
            </div>
        </fieldset>
    </form>
    <div class="col-md-4 text-left">
        <button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}">Descargar en Excel</button>
    </div>

</div>
<style>
    .fixed-table-container thead th .th-inner, .fixed-table-container tbody td .th-inner{
        line-height: 15px;
        text-align: center;
    }
</style>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/pago_resumen_lista_admin.js" type="text/javascript" charset="utf-8"></script>