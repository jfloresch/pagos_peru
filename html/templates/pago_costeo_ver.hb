<link href="css/bootstrap.css" rel="stylesheet" media="screen">
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/content.css" rel="stylesheet" media="screen">

<form role="form" action="#/pago/subir/archivo" method="POST" id="form-subir-archivo">
	<table data-toggle="table" border="0px" width="100%">
		<tr height="10px">
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%"></td>
		</tr>
		<tr height="10px">
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%">
				<a type="button" style='width:90px;' class="btn btn-primary btn-default pull-right btn-xs" href="#/pago/regresar/resumen">Regresar</a>
			</td>
		</tr>
		<tr>
			<td colspan="5" height="10">
			</td>
		</tr>
		<tr>
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%">
				<div id="botonera_validar" style="display:{{botonera_acta_visible}}">
					<button type="button" style='width:90px;' class="btn btn-primary btn-default pull-right btn-xs" id="validaracta" data-periodo="{{periodo}}"
					 data-copa-id="{{copa_id}}" data-actividad="{{actividad}}">Validar Acta</button>
				</div>
				<div id="botonera_validar" style="display:{{botonera_costeo_visible}}">
					<button type="button" style='width:90px;' class="btn btn-primary btn-default pull-right btn-xs" id="validarcosteo" data-periodo="{{periodo}}"
					 data-copa-id="{{copa_id}}" data-actividad="{{actividad}}">Validar Costeo</button>
				</div>
			</td>
		</tr>
	</table>
	<fieldset class="siom-fieldset" style="padding-bottom:10px; margin-top:1px;">
		<legend>Costeo {{copa_id}} para Periodo:{{periodo}}</legend>
		<input id="periodo" name="periodo" value="{{periodo}}" type="hidden" />
		<input id="actividad" name="actividad" value="{{actividad}}" type="hidden" />
		<input id="copa_id_valor" name="copa_id_valor" value="{{copa_id}}" type="hidden" />

		<table data-toggle="table" data-sort-order="desc" border="0px" width="100%" class="panel panel-default">
			<thead class="form-group">
				<tr>
					<th class="control-label-th-sin-top">NÚMERO </th>
					<th class="control-label-th-sin-top">TIPO OT </th>
					<th class="control-label-th-sin-top">EST.PROCESO </th>
					<th class="control-label-th-sin-top">SAP LPU </th>
					<th class="control-label-th-sin-top">ITEM LPU </th>
					<th class="control-label-th-sin-top">VALOR LPU </th>
					<th class="control-label-th-sin-top">CANT LPU </th>
					<th class="control-label-th">SUB TOTAL LPU</th>
					<th class="control-label-th-sin-top">F.EJECUCIÓN </th>
					<!--th  data-align="right">Mes Ejecución</th-->
					<th class="control-label-th-sin-top">Z.MOVISTAR </th>
					<th class="control-label-th-sin-top">Z.CONTRATO </th>
					<!--th  data-align="right">Hem</th-->
					<th class="control-label-th-sin-top"></th>
					<th class="control-label-th-sin-top"></th>
				</tr>
			</thead>
			<tbody class="form-group cambio">
				{{#pago_os_periodo}}
				<tr class="panel panel-default cambio">
					<td width="5%">{{id_ot}}
						<input id="id_ot" name="id_ot" value="{{id_ot}}" type="hidden" />
						<input id="monto_total" name="monto_total" value="{{monto_total}}" type="hidden" />
						<input id="capex_ot" name="capex_ot" value="{{capex_ot}}" type="hidden" />
						<input id="opex_ot" name="opex_ot" value="{{opex_ot}}" type="hidden" />
					</td>
					<td width="6%" class=" control-label-th">{{tipo_ot}} </td>
					<td width="6%">{{estado_ot}} </td>
					<td width="8%"> {{sap_lpu}} </td>
					<td width="24%"> {{item_lpu}} </td>
					<td width="8%" class="text-rigth peso-right">{{parsePeso valor_lpu}} </td>
					<td width="8%" class="text-rigth peso-right">{{parsePeso cant_lpu}} </td>
					<td width="8%" class="text-rigth peso-right">{{parsePeso subtotal_lpu}}</td>
					<td width="9%"> {{fecha_aprobacion_ot}} </td>
					<!--td width="15%">{{mes_ejecucion}}</td-->
					<td width="8%" class="control-label-th"> {{zona_movistar}} </td>
					<td width="7%" class="control-label-th"> {{zona_contrato}} </td>
					<td></td>
					<td width="3%" align="right">
						<div id="botonera_eliminar" style="display: {{display_eliminar}}">
							<button id="eliminarRegistroCosteo" style="padding:2px;height:20px;" name="eliminarRegistroCosteo" type="button" class="btn btn-default eliminarRegistroCosteo"
							 data-id_ot="{{id_ot}}">
								<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							</button>
						</div>
					</td>
				</tr>
				{{/pago_os_periodo}}
			</tbody>
		</table>
		<div class="panel-footer siom-paginacion">
			<div class="row">
				<div class="col-md-8 text-right">
					<div class="pagination-info">Página {{pagina}}/{{paginas}} ({{total}} registros)</div>
					<div id="pagination" class="pagination-buttons" data-page="{{pagina}}" data-total="{{paginas}}" data-actividad="{{actividad}}"
					 data-periodo="{{periodo}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
				</div>
			</div>
		</div>
	</fieldset>
	<table data-toggle="table" data-sort-order="desc" border="0px" width="100%">
		<td>
			<div id="botonera_os" style="display: {{botonera_os_visible}};">
				<button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..." data-processing-text="Generando..."
				 data-periodo="{{periodo}}" data-actividad="{{actividad}}">Exportar Actividad de Costeo OS</button>
				<button type="button" class="btn btn-default btn-xs" id="downloadActaos" data-loading-text="Descargando..." data-processing-text="Generando..."
				 data-periodo="{{periodo}}" data-actividad="{{actividad}}">Generar Acta OS</button>
				<a href="#/pago/costeo/documentacion/periodo/{{periodo}}/costeo/{{copa_id}}" class="dropdown-toggle mas_opciones">Descargar Documentación </a>
			</div>
		</td>
		<div align="right" style="padding:-50px;height:0px">
			<span class="btn btn-default btn-xs btn-file">
				Agregar archivo(s)
				<input type="file" name="archivos[]" id="archivo-0" data-id="0">
			</span>
			<button type="button" class="btn btn-default btn-xs subirArchivoCosteo" id="subirArchivoCosteo" name="subirArchivoCosteo"
			 data-loading-text="Subiendo..." data-periodo="{{periodo}}" data-actividad="{{actividad}}" style="padding:2px;height:20px">
				<span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
			</button>
			<ul id="listado-archivos" class="list-group siom-file-list"></ul>
		</div>
		<div id="botonera_mnt" style="display:{{botonera_mnt_visible}}">
			<button type="button" class="btn btn-default btn-xs" id="downloadMNT" data-loading-text="Descargando..." data-processing-text="Generando..."
			 data-periodo="{{periodo}}" data-actividad="{{actividad}}">Exportar Actividad Costeo MNT</button>
			<button type="button" class="btn btn-default btn-xs" id="downloadActamnt" data-loading-text="Descargando..." data-processing-text="Generando..."
			 data-copa_id="{{copa_id}}" data-periodo="{{periodo}}" data-actividad="{{actividad}}">Generar Acta MNT</button>
			<a href="#/pago/costeo/documentacion/periodo/{{periodo}}/costeo/{{copa_id}}" class="dropdown-toggle mas_opciones">Descargar Documentación </a>
		</div>

	</table>
</form>
<style>
	.fixed-table-container thead th .th-inner,
	.fixed-table-container tbody td .th-inner {
		line-height: 15px;
		text-align: center;
	}
</style>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-fileinput.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/pago_costeo_ver.js" type="text/javascript" charset="utf-8"></script>