
<link href="css/bootstrap.css" rel="stylesheet" media="screen">
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/content.css" rel="stylesheet" media="screen">
<form class="form-horizontal siom-form-tiny" role="form"  action="#/pago/resumen/lista" method="POST" id="FormPagoResumenLista">
	<table id="tabla" border="1px" width="100%"  class="panel panel-default" >
		<thead class="form-group">
            <tr class="panel panel-default" >
				<th width="12%" align="center" class="control-label-th"><small>HEM</small></th>
				<th width="12%" align="center" class="control-label-th"><small>DERIVADA</small></th>
				<th width="12%" align="center" class="control-label-th"><small>TIPO DE CAMBIO</small></th>
				<th width="12%" align="center" class="control-label-th"><small>OBSERVACIÓN</small></th>
			</tr>
		</thead>
		 <tbody  class="form-group cambio">
			{{#hemyderivada}}
			<tr>
			    <td width="12%" class="control-label-td">{{hem}}</td>
			    <td width="12%" class="control-label-td">{{derivada}}</td>
			    <td width="12%"  value="{{parsePeso tipo_cambio}}"  class="control-label-td">{{parsePeso tipo_cambio}}</td>
			    <td width="12%" class="control-label-td">{{observacion}}</td>
			</tr>
			{{/hemyderivada}}  
		</tbody> 
	</table>
   <!-- /fieldset-->
    </form>
<script src="js/bootstrap-wizard.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-fileinput.min.js"  type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/detalle_hemyderivada.js" type="text/javascript" charset="utf-8"></script>
