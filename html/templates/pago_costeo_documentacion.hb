<link href="css/bootstrap.css" rel="stylesheet" media="screen">
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/content.css" rel="stylesheet" media="screen">
    <form class="form-horizontal siom-form-tiny" role="form"  action="#/pago/resumen/lista" method="POST" id="FormPagoResumenLista">
        <fieldset class="siom-fieldset" style="padding-bottom:10px; margin-top:0px;">
            <legend>Documentación costeo {{copa_id}}</legend>
                <table border="0px" width="100%" class="panel panel-default">
                    <thead class="form-group">
                        <tr class="panel panel-default cambio">
                            <th></th>
                            <th  class=" control-label-th">NOMBRE</th>
                            <th  class=" control-label-th">DESCRIPCÍON</th>
                            <th ></th>
                          
                        </tr>
                    </thead>
                        <tbody class="form-group cambio">
                        {{#documentacion}}  
                                <tr height="20px"class="panel panel-default cambio" style="height: 52px;">
                                    <td  class="col-md-2 text-center"></td>
                                    <td  class="col-md-2 text-left">{{repo_nombre}}</td>
                                    <td  class="col-md-2 text-left">{{repo_descripcion}}</td>
                                    <td class="col-md-2 text-center">
                                        <button type="button" class="btn btn-primary btn-default pull-right btn-xs descargarDocumento" id="descargarDocumento " data-periodo="{{periodo}}" data-periodo="{{periodo}}" data-repo-id ="{{repo_id}}" data-actividad="{{actividad}}">Descargar</button>
                                    </td>
                            </tr>
                        {{/documentacion}} 
                        </tbody>
                    </table>
        </fieldset>
    </form>
<style>
    .fixed-table-container thead th .th-inner, .fixed-table-container tbody td .th-inner{
        line-height: 15px;
        text-align: center;
    }
</style>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-fileinput.min.js"  type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/pago_costeo_documentacion.js" type="text/javascript" charset="utf-8"></script>
