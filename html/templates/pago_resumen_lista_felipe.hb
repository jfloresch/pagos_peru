<div class="col-md-12">
    <form class="form-horizontal siom-form-tiny" role="form"  action="#/pago/resumen/lista" method="POST" id="FormPagoResumenLista">
        <fieldset class="siom-fieldset" style="padding-bottom:10px; margin-top:0px;">
            <legend>Resumen Presupuesto por Periodo</legend>
            <div class="col-md-12">
                <div class="col-md-12" style="float: left; padding-top: 0px;  padding-bottom: 0px;padding-right: 0px;padding-left: 0px;">
                <table data-toggle="table" data-sort-order="desc">
                    <thead>
                        <!--<td> 
                            <button type="submit" class="btn btn-primary btn-default" data-presupuesto="{{presupuesto}}" id="guardarPresupuesto">Guardar</button>
                        </td>-->
                        <tr>
                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Periodo</th>
                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Tipo</th>
                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Monto</th>
                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Monto Capex</th>
                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Monto opex</th>
                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Derivada</th>
                            <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">HEM</th>
							<th colspan="1" class="col-md-2" data-align="right" data-sortable="true"></th>
                        </tr>
                        <tr>
                            <th ></th>
                            <th ></th>
                            <th ></th>
                            <th ></th>
                            <th ></th>
                            <th ></th>
                            <th ></th>
							<th ></th>
							<th ></th>
                        </tr>
                    </thead>
                        <tbody>
                           
                            {{#monto}}  
                                <tr>
                                    <td class="col-md-3">
                                       <a href="#/pago/costeo/periodo/{{copa_periodo}}/actividad/{{copa_tipo_ot}}" class="click">{{copa_periodo}}</a> 
                                    </td>
                                    <td class="col-md-2">{{copa_tipo_ot}}</td>
                                    <td class="col-md-2">{{copa_monto_total}}</td>
                                    <td>{{copa_monto_capex}}</td>
                                    <td>{{copa_monto_opex}}</td>
                                    <td> <input id="copa_id" name="copa_id"  value="{{copa_id}}" value maxlength="4" type="hidden"/>
                                    </td>
                                    <td>
                                        <input type="text" id="derivada" name="derivada"  placeholder="{{copa_derivada}}" value maxlength="4" />
                                    </td>
                                    <td><input type="text" id="hem" name="hem" placeholder="{{copa_hem}}" /></td>
									<td>
										<button type="button" class="btn btn-default btn-xs BotonSLACronogramaValidacionEditar clase-editar" data-mant_id="{{mant_id}}" data-sla_cronograma_margen="{{sla_cronograma_margen}}" style="padding:2px;height:20px"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>

										<button type="button" class="btn btn-xs download" data-info-id="{{info_id}}" style="padding:2px;height:20px"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span></button>
									</td>
                                </tr>
                            {{/monto}} 
                        </tbody>
                    </table>
                </div>
            </div>
        </fieldset>
    </form>
    <div class="col-md-4 text-left">
        <button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}">Descargar en Excel</button>
    </div>

   <!--div class="col-md-12">
      <button type="button" class="btn btn-primary btn-default pull-right" id="GuardarCosteo" data-saving-text="Guardando..."data-processing-text="Guardando..." >Guardar Costeo</button>
   </div-->
</div>
<style>
    .fixed-table-container thead th .th-inner, .fixed-table-container tbody td .th-inner{
        line-height: 15px;
        text-align: center;
    }
</style>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script> 
<script src="js/pago_resumen_lista.js" type="text/javascript" charset="utf-8"></script>