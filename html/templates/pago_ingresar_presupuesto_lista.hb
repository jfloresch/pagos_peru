<div class="col-md-12">
   <fieldset class="siom-fieldset" style="padding-bottom:10px; margin-top:0px;">
      <legend>Prespuesto Pago</legend>
      <div class="col-md-12">
         <div class="col-md-12" style="float: left; padding-top: 0px;  padding-bottom: 0px;padding-right: 0px;padding-left: 0px;">
            <table data-toggle="table" data-sort-order="desc">
               <thead>
                  <tr>
                     <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Empresa</th>
                     <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Sociedad</th>
                     <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">ID OPEX/CAPEX</th>
                     <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Nombre Servicio</th>
                     <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Proveedor</th>
                     <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Año</th>
                     <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Monto</th>
                     <!--  <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Saldo</th>
                     <th colspan="1" class="col-md-3" data-align="right" data-sortable="true">Monto Comprometido </th> -->
                  </tr>
                  <tr>
                     <th ></th>
                     <th ></th>
                     <th ></th>
                     <th ></th>
                     <th ></th>
                     <th ></th>
                     <th ></th>
                     <!-- <th ></th>
                     <th ></th> -->
                  </tr>
               </thead>
               <tbody>
                  {{#monto}}  
                     <tr>
                        <td class="col-md-3">
                           <a href="#/pago/costeo/periodo/{{copa_periodo}}" class="click">{{copa_periodo}}</a> 
                        </td>
                        <td class="col-md-2">{{copa_tipo_ot}}</td>
                        <td class="col-md-2">{{copa_monto_total}}</td>
                        <td>{{copa_monto_capex}}</td>
                        <td>{{copa_monto_opex}}</td>
                        <td>
                           <input type="text" id="derivada" name="derivada"  placeholder="Derivada" value maxlength="4" />
                        </td>
                        <td><input type="text" id="hem" name="hem"/></td>
                        <!-- <td class="col-md-2" >{{saldo}} </td>
                        <td class="col-md-2"><a href="#/pago/detalle_os_periodo/contrato/periodo/{{periodo}}" class="click">{{monto_comprometido}}</a> 
                        </td> -->
                     </tr>
                  {{/monto}} 
               </tbody>
            </table>
         </div>
      </div>
   </fieldset>
   <div class="col-md-4 text-left">
      <button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}">Descargar en Excel</button>
   </div>
   <div class="col-md-12">
      <button type="button" class="btn btn-primary btn-default pull-right" id="GuardarCosteo" data-saving-text="Guardando..."data-processing-text="Guardando..." >Guardar Costeo</button>
   </div>
</div>
<style>
   .fixed-table-container thead th .th-inner, .fixed-table-container tbody td .th-inner{
   line-height: 15px;
   text-align: center;
   }
</style>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script> 
<script src="js/pago_resumen_lista.js" type="text/javascript" charset="utf-8"></script>
