<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Asignación Validadores de Costeo</h5>
  </div>
  <div class="col-md-7">

  </div>
</div>

{{#if data.status}}
	<form class="form form-horizontal siom-form-tiny" role="form" action="#/pago/asignar/validador/costeo/copa_id/{{copa_id}}/add" method="POST" id="siom-form-asignacion">
		<fieldset class="siom-fieldset"  style="margin-top:20px;">
			<legend>Costeo Pago {{copa_id}}</legend>
			<div class="row form-group">
				<div class="col-sm-2  col-md-offset-1">
					<label class="control-label">VALIDADORES</label>
				</div>
				<div class="col-sm-8">
					<div class="row">
						<div class="col-sm-6">
							<input type="text" class="form-control input-sm" id="filtro_validador" placeholder="Buscar Validador" />
							<ul class="list-group siom-asignacion-list" id="lista_validadores">
								{{#validadores}}
									<li class="list-group-item" data-id="{{usua_id}}" data-nombre="{{usua_nombre}}">
										<span>{{usua_nombre}}</span>
										<i class="glyphicon glyphicon-chevron-right pull-right"></i>
									</li>
								{{/validadores}}
							</ul>
						</div>
						<div class="col-sm-6">
							<input type="text" class="form-control input-sm" id="filtro_validadores_seleccionados"  placeholder="Buscar validadores  seleccionados" />
							<ul class="list-group siom-asignacion-list" id="lista_validadores_seleccionados">
								{{#usuariosvalidadores}}
									<li class="list-group-item" data-id="{{usua_id}}" data-nombre="{{usua_nombre}}">
										<span>{{usua_nombre}}</span>
										<i class="glyphicon glyphicon-trash pull-right eliminar"></i>
									</li>
								{{/usuariosvalidadores}}
							</ul>

						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row text-center siom-form-actions">
				<div class="col-md-12">
					<a href="#/pago/regresar/resumen" class="btn btn-default">Cancelar</a>
					<button type="submit" class="btn btn-primary" data-loading-text="Asignando..." autocomplete="off" id="submit_asignar">Asignar Validador</button>
				</div>
			</div>
		</fieldset>
	</form>

{{else}}
	<div class="alert alert-danger" role="alert">
  	{{data.error}}
  	{{#if data.debug}}
 	<br><small>{{data.debug}}</small>
	{{/if}}
	</div>
{{/if}}

<script src="js/pago_costeo_asignacion.js" type="text/javascript" charset="utf-8"></script>
