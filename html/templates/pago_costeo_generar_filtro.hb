<link href="css/bootstrap.css" rel="stylesheet" media="screen">
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/content.css" rel="stylesheet" media="screen">
<script src="js/pago_costeo_generar_filtro.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<table border="0px" width ="100%" style="/*margin-left: 5px; */margin-top:20px;">
	<tr class="row">
		<td width ="15%" valign="TOP">
			<form class="form-horizontal siom-form-tiny" role="form" action="#/pago/detalle/costeo/filtro" method="POST" id ="siom-form-costeo-generar" >
				<fieldset class="siom-fieldset">
				<legend>Filtros de Búsqueda</legend>            
				<table border="0px">
					<tr class="row">	
						<td class="col-md-6 text-center">
							<button type="button" class="btn btn-primary btn-default Limpiarfiltro" id="Limpiarfiltro">Limpiar</button>
						</td>
						
						<td class="col-md-6 text-center">
							<button type="button" class="btn btn-primary btn-default buscarFiltro" id="buscarFiltro">Buscar</button>
						</td>
					</tr>
					<tr class="row">
						<td colspan="2">
							<hr>
						</td>
					</tr>
					<tr class="row">
						<td class="col-md-2 text-right">
							<label class="control-label ">Periodo </label>
						</td>	
						<td class="col-md-10 text-left">
							<input type="text"  class="form-control " onkeypress="return validaNumericos(event)" maxlength="6" id="periodo" name="periodo" value="{{periodo}}" />
						</td>	
					</tr>
					<tr class="row">
						<td width="10%" class="col-md-2 text-right">
							<label for="actividad" title='Actividad' class="control-label ">Actividad</label>
						</td>
						<td width="10%" class=" text-left ">
							<select class="selectpicker " id="actividad" name="actividad">
							   <option pull-left value="OS"> OS-Correctivo</option>
							   <option pull-left value="LMT"> OS-L.Media Atención</option>
							   <option pull-left value="MNT"> MNT-Mantenimiento </option>
							</select>
						</td>
					</tr>
					<tr class="row">
						<td width="10%" class="col-md-2 text-right">
							<label class="control-label ">Numero OT </label>
						</td>	
						<td width="10%" class="col-md-10 text-left">
							<input type="text"  class="form-control " onkeypress="return validaNumericos(event)" maxlength="6" id="numero_ot" name="numero_ot" value="{{numero_ot}}" />
						</td>	
					</tr>
					<tr class="row">
						<td width="10%" class="col-md-2 text-right" >
							<label class=" control-label">F.Ini Aprobacion</label>
						</td>	
						<td width="10%" class="col-md-10 text-left">
							<input  autocomplete="off" id="fecha_inicio"  class="form-control" size="14" type="text" data-date-autoclose="true" name="f_ini_programada" value="{{#if data.f_ini_programada}} {{formatDate data.f_ini_programada '%d-%m-%Y'}} {{/if}}"/>
						</td>
					</tr>
					<tr class="row">
						<td width="10%" class="col-md-2 text-right">
							<label class="control-label">F.Fin Aprobacion</label>
						</td>	
						<td width="10%" class="col-md-10 text-left">
							<input  autocomplete="off" id="fecha_fin" class="form-control" size="14" type="text" data-date-autoclose="true" name="f_fin_programada" value="{{#if data.f_fin_programada}}{{formatDate data.f_fin_programada '%d-%m-%Y'}}{{/if}}"/>
						</td>
					</tr>
					<tr class="row">
						<td width="10%" class="col-md-2 text-right">
							<label for="region" title='Region' class="control-label">Region</label>
						</td>
						<td width="10%" class="col-md-10 text-left">
							<select class="selectpicker" style="width: 70%;" multiple id="region"  name="region"  >
							<option value="0"> Todos </option>		
								{{#regiones}}
								<option value="{{regi_nombre}}" {{regi_selected}} >{{regi_nombre}}</option>
								{{/regiones}}
							</select>
						</td>
					</tr>
					<tr class="row">
						<td width="10%" class="col-md-2 text-right">
							<label for="zcontrato" title='Zona Contrato' class="control-label">Zona Contrato</label>
						</td>
						<td width="10%" class="col-md-10 text-left">
							<select class="selectpicker"  style="width: 70%;" multiple id="zcontrato" name="zcontrato" >
							<option value="0"> Todos </option>		
								{{#zona_contrato}}
								<option value="{{zona_nombre}}">{{zona_nombre}}</option>
								{{/zona_contrato}}
							</select>
						</td>
					</tr>
					<tr class="row">
						<td width="10%" class="col-md-2 text-right">
							<label for="zmovistar" title='Zona Movistar' class="control-label">Zona Movistar</label>
						</td>
						<td width="10%" class="col-md-10 text-left">
							<select class="selectpicker" style="width: 70%;"  id="zmovistar" multiple  name="zmovistar" >
							<option value="0"> Todos </option>		
								{{#zona_movistar}}
								<option value="{{zona_nombre}}">{{zona_nombre}}</option>
								{{/zona_movistar}}
							</select>
						</td>
					</tr>
					<tr class="row">
						<td width="10%" class="col-md-2 text-right">
							<label for="zcluster" title='Zona Cluster' class="control-label ">Zona Cluster</label>
						</td>
						<td width="10%" class="col-md-10 text-left">
							<select class="selectpicker"  style="width: 70%;"  id="zcluster" multiple name="zcluster">
							<option value="0"> Todos </option>	
								{{#zona_cluster}}
								<option value="{{zona_nombre}}">{{zona_nombre}}</option>
								{{/zona_cluster}}
							</select>
						</td>
					</tr>					
				</table>
				</fieldset>		
			</form>
		</td>
		<td width ="85%" valign="TOP">
			<div class="col-md-12" id="pago-costeo-generar-lista" style="border: 0px solid #990000;"></div>
		</td>
	</tr>
</table>
<!--script src="js/pago_costeo_generar_filtro.js" type="text/javascript" charset="utf-8"></script-->