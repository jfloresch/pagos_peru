    <form class="form-horizontal siom-form-tiny" role="form"  action="#/pago/resumen/lista" method="POST" id="FormPagoResumenLista">
       <h5 style="margin-left: 20px;">Resumen Costeo</h5>
	               <table id="tabla" style="margin-left: 20px;" border="1px" width="100%"  class="panel panel-default" >
	                    <thead class="form-group" >
	                    <tr class="panel panel-default" >
				               <th width="5%" class=" control-label-th"><small>N.COSTEO</small></th>
				               <th width="5%" class=" control-label-th"><small>PERIODO </small></th>
				               <th width="5%" class=" control-label-th"><small>TIPO    </small></th>
				               <th width="11%"class=" control-label-th"><small>ESTADO  </small></th>
				               <th width="5%" class=" control-label-th"><small>CANTIDAD</small></th>
				               <th width="7%" class=" control-label-th"><small>M.TOTAL	</small></th>
				               <th width="7%" class=" control-label-th"><small>CAPEX 	</small></th>
				               <th width="7%" class=" control-label-th"><small>OPEX		</small></th>
				               <!--th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Linea Presupuestaria Capex</th-->
				               <!--th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Linea Presupuestaria Opex/th-->
				               <!--th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Tipo Linea</th-->
				               <th width="9%" class=" control-label-th"><small>OBSERVACIÓN</small>  </th>
				               <th width="3%" class=" control-label-th">			                </th>
				               <th width="7%" class=" control-label-th">                            </th>
        				</tr>
	                    </thead>
	                    <tbody class="form-group cambio">
	                        {{#monto}}
	                            <tr class="panel panel-default cambio">
	                            	<td   width="5%" class="text-center" >
										<a  href="#/pago/costeo/periodo/{{copa_periodo}}/actividad/{{copa_tipo_ot}}/{{copa_id}}" <small>{{copa_id}}</small></a>
										<input id="copa_id" name="copa_id"  value="{{copa_id}}" style="display:none">
										<input id="periodo" name="periodo"  value="{{copa_periodo}}" style="display:none">
										<input id="actividad" name="actividad"  value="{{copa_tipo_ot}}" style="display:none">
	                                </td>
	                                <td class="control-label-th" >

	                                	<small>{{copa_periodo}} </small>
										<!--a href="#/pago/costeo/periodo/{{copa_periodo}}/actividad/{{copa_tipo_ot}}/{{copa_id}}" class="click">{{copa_periodo}}</a-->
										<!--input id="copa_id" name="copa_id"  value="{{copa_id}}" type="hidden"/-->
	                                </td>
	                                <td id="tipo_ot" class="control-label-th"      >
	                                	<small>{{copa_tipo_ot}} </small>
	                                </td>
	                                <td id="estado" class="text-left "  >
	                                	<small >{{copa_estado}}</small>
	                                </td>
	                                <td id="cantidad_total" class="control-label">
	                                	<small style="padding-right: 3px;">{{copa_cantidad_total}}</small>
	                                </td>
	                                <td id="monto_total"  class="control-label" >
	                                	<small style="padding-right: 3px;">{{copa_monto_total}}</small>
	                                </td>
	                                <td id="monto_capex"  class="control-label" >
	                                	<small style="padding-right: 3px;">{{copa_monto_capex}}</small>
	                                </td>
	                                <td id="monto_opex"   class="control-label" >
	                                	<small style="padding-right: 3px;">{{copa_monto_opex}}</small>
	                                </td>
									<td id="observacion_resumen" class="text-left">
										<small style="margin-left: 19px;">{{copa_observacion}}</small>
									</td>
									<td width="8%">
										<a role="menuitem" id="botonhemyderivadaver{{copa_id}}" onclick="abrirModalhemyderivada({{cont_id}},{{copa_id}},'{{copa_tipo_ot}}');" class="glyphicon glyphicon-search"tabindex="-1" href="#" data-toggle="modal" data-target="#modalMantHistoricos"data-title="Historico Mantenimientos" ></a>
									</td>
									<td >
										<div id="botonera_validar"  style="display:{{copa_validar_acta}}">
											<button type="button" class="btn btn-primary btn-default " id="validarActa" data-periodo="{{periodo}}" data-copa-id="{{copa_id}}" data-actividad="{{actividad}}">Validar Acta</button>
										</div>
										<div id="botonera_validar"  style="display:{{copa_validar_costeo}}">
											<button type="button" class="btn btn-primary btn-default " id="validarCosteo" data-periodo="{{periodo}}" data-copa-id="{{copa_id}}" data-actividad="{{actividad}}">Validar Costeo</button>
										</div>
									</td>
								</tr>
	                        {{/monto}}
	                    </tbody>
	                </table>
			                      <!-- inicio modal historico -->
		<div class="modal fade" id="modalMantHistoricos">
		    <div class="modal-dialog" style="width: 800px">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                    <span aria-hidden="true">&times;</span>
		                </button>
		                <h4 class="modal-title">Detalle Costeo.</h4>
		            </div>
		            <div class="modal-body">
		            <div class="row">
		              <div class="col-md-12" id="detalle-hemyderivada">
		            </div>
		                </div>
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-primary" data-dismiss="modal">Salir</button>
		            </div>
		        </div>
		    </div>
		</div>
		<!-- fin modal historico -->
    </form>
      
    <div class="col-md-4 text-left">
        <button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}">Descargar en Excel</button>
    </div>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/pago_resumen_lista_cerrada.js" type="text/javascript" charset="utf-8"></script>