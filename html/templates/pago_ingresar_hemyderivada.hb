
<link href="css/bootstrap.css" rel="stylesheet" media="screen">
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/content.css" rel="stylesheet" media="screen">
<script src="js/pago_ingresar_hemyderivada.js" type="text/javascript" charset="utf-8"></script>
<form role="form" action="#/pago/detalle/hemyderivada/filtro" method="POST" id ="siom-form-costeo-generar" >
    <div class="col-md-12">
        <fieldset class="siom-fieldset" style="padding-bottom:10px; margin-top:0px;">
            <legend>Detalle Registros Por Periodo</legend>            
			<table border="0px" width="100%" >
				<tr>
					<td width="5%" align="right">
						<label class="control-label" >Periodo</label>
					</td>	
					<td width="5%" class="control-label-th">
						<input type="text" class="text-left" onkeypress="return validaNumericos(event)" readonly maxlength="6" id="periodo" name="periodo" value="{{periodo}}" />
					</td>
					<td width="5%" align="right">
						<label class="control-label" for="actividad" title='Actividad' >Actividad</label>
					</td>
					<td width="10%" class="control-label-th">
						<input type="text" class="text-left"  readonly  id="actividad" name="actividad" value="{{actividad}}" />
						<!--select class="selectpicker" data-width="100%"  id="actividad" name="actividad">
						   <option value="OS"> Orden Servicio </option>
						   <option value="MNT"> Mantenimiento </option>
						</select-->
					</td>
					<td width="10%">
						<!--button type="button" class="btn btn-primary btn-default pull-right" id="buscarFiltro">Buscar</button-->
					</td>
					<td width="60%"></td>
					<td width="10%">
						<div border="0px">
							<a type="button"   class="btn btn-primary btn-default pull-right btn-xs" id="id_papre_boton_regresar" data-tipo="Regresar" href="#/pago/regresar/resumen">Regresar</a>
						</div>
					</td>
				</tr>
			</table>
		</fieldset>			
	</div>			
</form>		
<form role="form" action="#/pago/hemyderivada/add" method="POST" id="siom-form-costeo" name="siom-form-costeo">			
    <div class="col-md-12">
        <fieldset class="siom-fieldset" style="padding-bottom:10px; margin-top:0px;">
            <legend>Registros Pendientes de Costeo</legend>            
			<table border="0px" width="100%">
				<tr>
					
					<td	 width="20%"></td>
					<td	 width="20%"></td>
                    <td width="20%"></td>
                   <!--  <td width="20%"  align="right">
                    	 <div border="0px">
							<button type="button" style="background:green" class="btn btn-primary  btn-xs pull-right" onclick="saveSession()">Guardar Sesión</button>
						</div>
                    </td> -->
					<td width="20%">
						<div border="0px">
							<button type="button" class="btn btn-primary btn-default btn-xs pull-right" id="GuardarCosteo" data-saving-text="Guardando..."data-processing-text="Guardando..." >Guardar</button>
							<input type="text"   id="copa_id" name="copa_id" value="{{copa_id}}" style="display:none;" />
						</div>
					</td>

				</tr>
				<tr height="10px" ></tr>
				<tr>
					<td width="5%" align="left">
						<label  class="control-label " for="derivada" title='derivada' >TODO</label>
						<input type="checkbox" id="check_hemyderivada_all" name="check_hemyderivada_all">					
					</td>

					<td width="5%">
						<label  class="control-label" for="derivada" title='derivada' >DERIVADA</label>
						<input type="text" class="text-rigth" id="imputderivadaall"  maxlength="10" onkeypress="return validaNumericos(event)" name="imputderivadaall">
					</td>
					<td width="5%">
						<label  class="control-label" for="Hem" title='Hem' >HEM</label>
						<input type="text"  class="text-rigth" id="imputhemall" maxlength="10" onkeypress="return validaNumericos(event)" name="imputhemall">
					</td>
					  <td Width="5%" >
					  	<label  class="control-label" for="Hem" title='Hem' >TIPO DE CAMBIO</label>
                  		<input type="text" style='width:75px;' class="text-rigth"  onkeypress='return validaIngresoComa(this.value,event) ' id="imputtipocambioall" name="imputtipocambioall"  maxlength="20"  placeholder="{{parsePeso copa_tipo_cambio}}" value="{{parsePeso copa_tipo_cambio}}" />
               		</td>
					<td width="5%" >	
						<label  class="control-label" for="observacion" title='observacion' >OBSERVACIÓN</label>
						<textarea id="observacionall" name="observacionall" onkeypress='return validaObservacion(this.value,event)'  rows="4"cols="40" maxlength="50"></textarea>
					
					</td>	
					
								
				</tr>
				
				<tr>
					<td colspan="5" width="100%">
						<table width="100%" border="1px" data-toggle="table" data-sort-order="desc"  class="panel panel-default"  id="lista_os_periodo" >
							<thead class="form-group">
								<tr class="panel panel-default">
									<th class=" control-label-th" data-sortable="true">
										<label  class="control-label" for="numeroot" title='numeroot' >NÚMERO OT</label>
									</th>
									<th class=" control-label-th" data-sortable="true">
										<label  class="control-label" for="desclpu" title='desclpu' >DESCRIPCION DE LPU</label>
									</th>
									<th class=" control-label-th" data-sortable="true">
										<label  class="control-label" for="mtotal" title='mtotal' >MONTO TOTAL</label>
									</th>
									<th class=" control-label-th">
										<label  class="control-label" for="derivada" title='derivada' >DERIVADA</label>
									</th>
									<th class=" control-label-th">
										<label  class="control-label" for="hem" title='hem' >HEM</label>
									</th>
									<th class=" control-label-th" data-sortable="true">
										<label  class="control-label" for="cambio" title='cambio'>TIPO CAMBIO</label>
									</th>
									<th class=" control-label-th" data-sortable="true">
										<label  class="control-label" for="observacion" title='observacion'>OBSERVACIÓN</label>	
									</th>
								</tr>
								<tr>
									<th class=" control-label-th" data-sortable="true">
										<input type="text"   id="periodo" name="periodo" value="{{periodo}}" style="display:none;" />
										<input type="text"   id="actividad" name="actividad" value="{{actividad}}" style="display:none;" />
									</th>
									
									<th class=" control-label-th">
										
									</th>

									<th class=" control-label-th" data-sortable="true">
										<input  class="text-rigth" id="monto_total_cabecera" name="monto_total_cabecera" readonly value="{{parsePeso copa_monto_total}}">
									</th>
									<th class=" control-label-th">
										
									</th>

									
									
								</tr>
								
							</thead>
		                    <tbody class="form-group cambio">
								{{#pago_os_periodo}}   
									<tr class="panel panel-default">
										<td width="14px">{{id_ot}}
											
											<input type="text" id="id_ot" name="id_ot" value="{{id}}" style="display:none;"/>

										</td>
										<td width="14px">{{desc_item_lpu}}

										</td>
										
										<td width="16px" class=" control-label-th">
											<input readonly="readonly" id="monto_total_{{id_ot}}" class="text-rigth" value="{{monto}}">
										</td>
										<td width="14px" id="derivada" class="control-label-th"> 
											<input   class="text-left" id="imputderivada{{id}}" onkeypress="return validaNumericos(event)" value="{{derivada_id}}" maxlength="10" name="imputderivada">
											
										</td>
										<td width="14px" id="hem" class="control-label-th" > 
											<input   class="text-left" id="imputhem{{id}}"  value="{{hem_id}}" maxlength="10" onkeypress="return validaNumericos(event)" name="imputhem"/>
										</td>
										  <td Width="14%" >
                  							<input type="text" style='width:75px;' class="text-rigth"  onkeypress='return validaIngresoComa(this.value,event)'  id="imputtipocambio{{id}}" name="imputtipocambio"  maxlength="20" placeholder="{{copa_tipo_cambio}}" value="{{tipo_cambio}}" />
               							</td>
										<td width="14px" > 
											<textarea id="observacion{{id}}" name="observacion" onkeypress='return validaObservacion(this.value,event)' value="{{observacion}}" rows="4"cols="50" maxlength="50">{{observacion}}</textarea>
										</td

										
									</tr>
								{{/pago_os_periodo}}   
							</tbody>
                		</table>
						<div class="panel-footer siom-paginacion">
	                    <div class="row">
	                        <div class="col-md-8 text-right">
	                            <div class="pagination-info">Página {{pagina}}/{{paginas}} ({{total}} registros)</div>
	                            <div id="pagination" class="pagination-buttons" data-page="{{pagina}}" data-total="{{paginas}}" data-actividad="{{actividad}}" data-periodo="{{periodo}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
	                        </div>
	                    </div>
	                </div>	
					</td>
				</tr>
			</table>
        </fieldset>
    </div>
</form>
<style>
    .fixed-table-container thead th .th-inner, .fixed-table-container tbody td .th-inner{
        line-height: 15px;
        text-align: center;
    }
</style>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>