<script src="js/bootstrap-table.js" type="text/javascript" charset="utf-8"></script>    
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<script src="js/bootstrap-select.min.js" type="text/javascript" charset="utf-8"></script>
	<table border="0px" width ="100%" style="/*margin-left: 5px; */margin-top:20px;">
		<tr>
			<td width ="20%">
            <form class="form-horizontal siom-form-tiny" role="form" action="#/pago/resumen/filtro" method="POST" id="pago-resumen-filtro">
               <fieldset class="siom-fieldset" style="padding-bottom:0px; margin-top:0px; margin-left:10px; shape-margin:10px;  ">
                  <legend>Seleccione Filtros</legend>
                     <div class="form-group" style="display:block">
							<div class="col-sm-2" border="0px">
								<button type="button" class="btn btn-primary btn-default" id="Limpiarfiltro">Limpiar</button>
                        </div>
							<div class="col-md-2" border="0px">
                           <button type="button" class="btn btn-primary btn-default" id="BuscarResumenPago" data-tipo="OS">Buscar</button>
                        </div>
                     </div>
                     <hr />
                     <div class="form-group" style="display:block">
							<label for="id_papre_seleccion_contrato" title='Año'  class="col-sm-2 control-label">Año</label>
							<div class="col-md-5" style="border: 0px solid #990000;">
								<input type="text" class="form-control" id="pafi_anio" name="pafi_anio" maxlength="4" data-width="100%" value="{{data.pafi_anio}}" />
                        </div>
                     </div>
                     <br />
                     <div class="form-group" style="display:block">
							<label for="pafi_mes" title='mes del contrato solicitado' class="col-sm-2 control-label">Mes</label>
							<div class="col-md-5" style="border: 0px solid #990000;">
                           <select class="selectpicker" data-width="100%"  name="pafi_mes">
                              <option value="00">TODOS</option>
                              <option value="01">ENERO</option>
                              <option value="02">FEBRERO</option>
                              <option value="03">MARZO</option>
                              <option value="04">ABRIL</option>
                              <option value="05">MAYO</option>
                              <option value="06">JUNIO</option>
                              <option value="07">JULIO</option>
                              <option value="08">AGOSTO</option>
                              <option value="09">SEPTIEMBRE</option>
                              <option value="10">OCTUBRE</option>
                              <option value="11">NOVIEMBRE</option>
                              <option value="12">DICIEMBRE</option>
                           </select>
                        </div>
                     </div>
                     <br />
               </fieldset>
            </form>
			</td>
			<td width ="70%">
				<div class="col-md-10" id="pago-resumen-lista" style="border: 0px solid #990000;"></div>
			</td>
		</tr>
	</table>
<script src="js/pago_presupuesto.js" type="text/javascript" charset="utf-8"></script>
<script src="js/pago_detalle_filtro.js" type="text/javascript" charset="utf-8"></script>