<link href="css/bootstrap.css" rel="stylesheet" media="screen">
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/content.css" rel="stylesheet" media="screen">
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/pago_costeo_generar.js" type="text/javascript" charset="utf-8"></script>
<script src="js/detalle_os_periodo.js" type="text/javascript" charset="utf-8"></script>
<form role="form" action="#/pago/costeo/add" method="POST" id="siom-form-costeo" name="siom-form-costeo">
	<fieldset class="siom-fieldset" style="padding-bottom:10px; margin-top:0px;">
		<legend>Registros Pendientes de Costeo</legend>
		<table border="0px" width="100%">
			<tr>
				<td width="20%">
				</td>
				<td width="20%">
				</td>
				<td width="20%" align="left"></td>
				<td width="20%">
					<button type="button" style="background:green" class="btn btn btn-primary btn-default pull-right btn-xs" onclick="saveSession();AlertaGuardado()">Guardar Sesión</button>

					<input type="text" id="str_id_ot" name="str_id_ot" style="display:none;" />
					<input type="text" id="str_capex_ot" name="str_capex_ot" style="display:none;" />
					<input type="text" id="str_check_capex_value" name="str_check_capex_value" style="display:none;" />
					<input type="text" id="str_opex_ot" name="str_opex_ot" style="display:none;" />
					<input type="text" id="str_check_opex_value" name="str_check_opex_value" style="display:none;" />
				</td>
				<td width="20%">
					<button type="button" class="btn btn-primary btn-xs btn-default pull-right" id="GuardarCosteo" data-saving-text="Registrando..."
					 data-processing-text="Registrando...">Registrar Costeo</button>
				</td>
			</tr>
			<tr height="10px"></tr>
			<tr>
				<td width="20%">
					<label class="control-label">Linea presupuestaria Capex</label>
				</td>
				<td width="20%" class="control-label">
					<select class="botones" id="lineacapex" name="lineacapex" width="100%">
						<option value="0"> --Seleccione Linea-- </option>
						{{#linea_presupuestocapex}}
						<option value="{{papr_id}}">{{papr_nombre_servicio}}</option>
						{{/linea_presupuestocapex}}
					</select>

				</td>
				<td width="20%">
					<label class="control-label">Linea presupuestaria Opex</label>
				</td>
				<td width="20%" class="control-label">
					<select class="botones" id="lineaopex" name="lineaopex" width="100%">
						<option value="0"> --Seleccione Linea-- </option>
						{{#linea_presupuestoopex}}
						<option value="{{papr_id}}">{{papr_nombre_servicio}}</option>
						{{/linea_presupuestoopex}}
					</select>

				</td>
				<td width="20%">

				</td>
			</tr>
			<tr>
				<td colspan="6" width="100%">
					<table width="100%" border="0px" data-toggle="table" data-sort-order="desc" class="panel panel-default" id="lista_os_periodo">
						<thead class="form-group">
							<tr class="panel panel-default">
								<th class=" control-label-th" data-sortable="true">NÚMERO
								</th>
								<th class=" control-label-th" data-sortable="true">TIPO</th>
								<th class=" control-label-th" data-sortable="true">MONTO TOTAL
								</th>
								<th class=" control-label-th">
									<input type="checkbox" id="check_capex_all" name="check_capex_all"> CAPEX
								</th>
								<th class=" control-label-th">
									<input type="checkbox" id="check_opex_all" name="check_opex_all"> OPEX
								</th>
								<th class=" control-label-th" data-sortable="true">TIPO MONEDA</th>
								<th class=" control-label-th" data-sortable="true">F. APROBACIÓN SIOM</th>
							</tr>
							<tr class="panel panel-default">
								<th class=" control-label-th" data-sortable="true">
									<input type="text" id="periodo" name="periodo" value="{{periodo}}" style="display:none;" />
									<input type="text" id="actividad" name="actividad" value="{{actividad}}" style="display:none;" />
								</th>
								<th class=" control-label-th" data-sortable="true"></th>
								<th class=" control-label-th" data-sortable="true">
									<input class="text-rigth" id="monto_total_cabecera" name="monto_total_cabecera" readonly value="0">
								</th>
								<th class=" control-label-th">
									<input class="text-rigth" id="monto_total_capex" name="monto_total_capex" readonly value="0">
								</th>
								<th class=" control-label-th">
									<input class="text-rigth" id="monto_total_opex" name="monto_total_opex" readonly value="0">
								</th>
								<th class=" control-label-th" data-sortable="true"></th>
								<th class=" control-label-th" data-sortable="true"></th>
							</tr>

						</thead>
						<tbody class="form-group cambio">
							{{#pago_os_periodo}}
							<tr class="panel panel-default">
								<td width="14px">{{id_ot}}
									<input type="text" id="id_ot" name="id_ot" value="{{id_ot}}" style="display:none;" />
								</td>
								<td width="14px" class=" control-label-th">{{tipo_ot}}
									<input type="hidden" id="tipo_ot" name="tipo_ot" value="{{tipo_ot}}" />
								</td>
								<td width="16px" class=" control-label-th">
									<input readonly="readonly" id="monto_total_{{id_ot}}" class="text-rigth" value="0">
								</td>
								<td width="14px" id="capex">
									<input type="checkbox" id="check_capex" class="text-rigth" name="check_capex" value="{{id_ot}}" onclick="checkPagarCapex({{id_ot}})"
									/> {{parsePeso capex_ot}}
									<input type="hidden" id="capex_ot" name="capex_ot" value="{{capex_ot}}" />
									<input type="hidden" id="check_capex_value" name="check_capex_value" value="NO" />
								</td>
								<td width="14px" id="opex">
									<input type="checkbox" id="check_opex" name="check_opex" value="{{id_ot}}" onclick="checkPagarOpex({{id_ot}})" /> {{parsePeso opex_ot}}
									<input type="hidden" id="opex_ot" name="opex_ot" value="{{opex_ot}}" />
									<input type="hidden" id="check_opex_value" name="check_opex_value" value="NO" />
								</td>
								<td width="14px" id="tipo_moneda" class=" control-label-th">{{tipo_moneda}}
									<input type="hidden" id="tipo_moneda" name="tipo_moneda" value="{{tipo_moneda}}" />
								</td>
								<td width="14px" class=" control-label-th">{{fecha_aprobacion_ot}}
									<input type="hidden" id="fecha_aprobacion_ot" name="fecha_aprobacion_ot" value="{{fecha_aprobacion_ot}}" />
								</td>
							</tr>
							{{/pago_os_periodo}}
						</tbody>
					</table>
					<div class="panel-footer siom-paginacion">
						<div class="row">
							<div class="col-md-8 text-right">
								<div class="pagination-info">Página {{pagina}}/{{paginas}} ({{total}} registros)</div>
								<div id="pagination" class="pagination-buttons" data-page="{{pagina}}" data-total="{{paginas}}" data-actividad="{{actividad}}"
								 data-periodo="{{periodo}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
							</div>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</fieldset>
</form>
<!--</td-->
<tr>
	</table>
	<style>
		.fixed-table-container thead th .th-inner,
		.fixed-table-container tbody td .th-inner {
			line-height: 15px;
			text-align: center;
		}
	</style>
	<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
	<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>