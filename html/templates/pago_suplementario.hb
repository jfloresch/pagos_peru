<script src="js/bootstrap-table.js" type="text/javascript" charset="utf-8"></script>    
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<script src="js/bootstrap-select.min.js" type="text/javascript" charset="utf-8"></script>
<form class="form-horizontal siom-form-tiny" role="form" action="#/pago/pago_suplementario/add" method="POST" id="form_pago_suplementario">
   <div class="col-md-9">
      <div class="row" style="/*margin-left: 5px; */margin-top:20px;">
         <fieldset class="siom-fieldset" id="siom-usuario-info">
            <legend>Ingreso Suplementario</legend>
            <table colspan="4"  width="90%">
               <tr> 
                     <td width="15%" border='1' > 
                        <label for="pasu_anio" title='Año de ingreso suplemnto' class="col-sm-4 control-label  pull-right ">Año</label>
                     </td border='1'>
                     <td width="30%" > 
                        <input type="text" id="pasu_anio" name="pasu_anio" class="form-pasu_anio" value maxlength="4" placeholder="Año">
                     </td>
               </tr>
               <tr> 
                  <td width="15%" border='1' > 
                     <label for="pasu_monto" title='Monto a inyectar/quitar al presupuesto ' class="col-sm-4 control-label pull-right ">Monto</label>
                  </td border='1'>
                  <td> 
                     <input type="text" id="pasu_monto" name="pasu_monto" class="form-pasu_monto" placeholder="Monto">
                  </td>
                  <td width="15%"> 
                     <label for="pasu_moneda"  title='Ingresar el tipo de moneda correspondiente al presupuesto ' class="pull-right ">Tipo Moneda</label>
                  </td>
                  <td>  
                     <select id="pasu_moneda" class="selectpicker" value="{{pasu_moneda}}" data-width="100%" name="pasu_moneda" title="Moneda"   data-container="body" >
                        <option value="CLP">CLP</option>
                        <option value="UF">UF</option>
                     </select>
                  </td>
               </tr>
               <tr> 
                     <td> 
                        <label for="pasu_gastoop"  title='Ingresar el tipo de gasto operacional ' class="pull-right ">Gasto OP</label> 
                     </td>
                     <td colspan="3">  
                        <select id="pasu_gp" class="selectpicker" value="{{pasu_gastoop}}" name="pasu_gastoop"  data-width="100%" name="pasu_gp" title="gastoOP"   data-container="body" >
                        <option value="CAPEX">CAPEX</option>
                        <option value="OPEX">OPEX</option> 
                     </td>
               </tr>
               <tr >
                  <td>
                     <label for="pasu_observacion"  title='Ingresar la boservacion correspondiente' class="pull-right ">Observación</label> 
                  </td>
                  <td colspan="3">
                      <textarea class="col-sm-5 siom-value" id="pasu_observacion" placeholder="Observación" name="pasu_observacion" ></textarea>
                  </td>
                  <td>
                   <button type="button" class="btn btn-primary btn-default pull-right" id="id_pasu_boton_ingresar_suplemento" data-tipo="MNT"> Ingresar</button>
                </td>
               </tr>
            </table>
         </fieldset>
      </div>
   </div>
</form>
 <script src="js/pago_suplementario.js" type="text/javascript" charset="utf-8"></script>
