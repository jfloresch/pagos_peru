<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap-select.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/detalle_pago.js" type="text/javascript" charset="utf-8"></script> 
<form class="form-horizontal siom-form-tiny" role="form" action="#/pago_presupuesto_lista" method="POST" id="form-filtro_detalle_Pago">
   <div class="col-md-4">
      <fieldset class="siom-fieldset" style="padding-bottom:0px; margin-top:0px; margin-left:10px; shape-margin:10px;  ">
         <legend>Selección Contrato</legend>
         <div class="col-md-12">
            <div class="col-md-12" style="float: left; padding-top: 0px;padding-right: 0px;padding-left: 0px;">
               <div class="form-group"  id="id_papre_seleccion_contrato" style="display:block">
                  <label for="id_papr_seleccion_contrato" title='Lista de contratos activos '  class="col-sm-4 control-label">Año</label>
                  <div class="col-md-6">
                       <input type="text" id="papr_anio" name="papr_anio" class="form-papr_anio" placeholder="Año" value maxlength="6">
                  </div>
               </div>
               <br>
               <div class="form-group" id="papre_anio" style="display:block">
                  <label for="papreanio" title='Año del contrato solicitado' class="col-sm-4 control-label">Mes</label>
                  <div class="col-md-6">
                     <input type="text" id="papr_mes" name="papr_mes" class="form-papr_mes" placeholder="Mes" value maxlength="6">
                  </div>
               </div>
               <div class="col-md-13">
                  <br>
                  <button type="button" class="btn btn-primary btn-default pull-right" id="id_papr_boton_consultar" data-tipo="MNT">Consultar</button>
               </div>
            </div>
         </div>
      </fieldset>
   </div>
   <div class="col-md-8" id="lista_detalle_pago">
   </div>
</form>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>

