	
<link href="css/bootstrap.css" rel="stylesheet" media="screen">
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/content.css" rel="stylesheet" media="screen">
	<fieldset class="siom-fieldset" style="padding-bottom:10px; margin-top:0px;">
        <legend>Resumen Presupuesto {{anio}}</legend>
        <table border="0px" width ="100%" class="panel panel-default">
			<thead class="form-group">
				<tr class="panel panel-default">
					<th   class=" control-label-th" ><label class="control-label">EMPRESA</label></th>
					<th   class=" control-label-th" ><label class="control-label">SOCIEDAD</label></th>
					<!--th colspan="1" class="col-md-2" data-align="right" data-sortable="true">ID_Opex/Capex</th-->
					<th  class=" control-label-th" ><label class="control-label">NOMBRE SERVICIO</label></th>
					<th  class=" control-label-th" ><label class="control-label">TIPO LINEA</label></th>
					<th  class=" control-label-th"  ><label class="control-label">PROVEEDOR</label></th>
					<!--<th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Año</th>-->
					<th  class=" control-label-th" ><label class="control-label">$ <span class="span_tipo_moneda_pago"></span></label></th>
					<th  class=" control-label-th"><label class="control-label">CONSUMIDO</label></th>
					<th  class=" control-label-th" ><label class="control-label">SALDO</label></th>
					<th  class=" control-label-th" ><label class="control-label"></label></th>
					<th  class=" control-label-th" ><label class="control-label"></label></th>
				</tr>
			</thead>
				<tbody class="form-group cambio"> 
					{{#pago_pre_tabla}}  
						<tr height="30" class="panel panel-default">
							<td width="11%">
								<input autocomplete="off" class="text-left"  id="empresa" name="empresa"  value="{{empresa}}"/>
								<input autocomplete="off" id="papr_id" hidden="none" name="papr_id"  value="{{papr_id}}" />
							</td>
							<td width="11%">
								<input autocomplete="off" class="text-left"  id="sociedad" name="sociedad" onkeypress="return validaNumericos(event)" value="{{sociedad}}"/>   
							</td>
							<!--td class="col-md-2" >
								<input id="id_capex_opex" name="id_capex_opex" onkeypress="return validaNumericos(event)" value="{{id_capex_opex}}"/>  
							</td-->
							<td width="12%" >
								<input autocomplete="off" class="text-left"  id="nombre_servicio" name="nombre_servicio"  value="{{nombre_servicio}}"/> 
							</td>
							<td width="10%">
								<input autocomplete="off" class="text-left"  id="papr_tipo" name="papr_tipo"  value="{{papr_tipo}}"/> 
							</td>
							<td width="10%">
								<input autocomplete="off" class="text-left" id="proveedor" name="proveedor"  value="{{proveedor}}"/> 
							</td>
							<td width="12%">
								<input autocomplete="off" class="text-rigth" id="monto" name="monto" onkeypress="return validaNumericos(event)"  onblur="parsePesoJs(this);" value="{{parsePeso monto}}"/>   
							</td>
							<td width="12%">
								<input autocomplete="off" class="text-rigth"  id="consumido" name="consumido" readonly value="{{parsePeso consumido}}"/>  
							</td>
							<td width="12%">
								<input autocomplete="off" class="text-rigth" id="saldo" name="saldo" readonly value="{{parsePeso saldo}}"/>    
							</td>
							<td width="5%">
								<button id="guardarpresupuesto" name="guardarpresupuesto" type="button" class="btn btn-default guardarpresupuesto " data-copa_id="{{copa_id}}" style="padding:2px;height:20px">
										<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
								</button>
							</td>
							<td width="5%">
								<button id="eliminarpresupuesto" name="eliminarpresupuesto" type="button" class="btn btn-default eliminarpresupuesto" data-papr_id="{{papr_id}}" style="padding:2px;height:20px">
									<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								</button>
							</td>       
						</tr>    
					{{/pago_pre_tabla}}                
				</tbody>
            </table>
		</fieldset>
		<div class="col-md-4 text-left">
			<button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}">Descargar en Excel</button>
		</div>
<style>
    .fixed-table-container thead th .th-inner, .fixed-table-container tbody td .th-inner{
        line-height: 15px;
        text-align: center;
    }
</style>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script> 
<script src="js/pago_presupuesto_admin.js" type="text/javascript" charset="utf-8"></script> 

    