
<link href="css/bootstrap.css" rel="stylesheet" media="screen">
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/content.css" rel="stylesheet" media="screen">
<script src="js/bootstrap-table.js" type="text/javascript" charset="utf-8"></script>    
<script src="js/bootstrap-select.min.js" type="text/javascript" charset="utf-8"></script>
	<table border="0px" width ="100%" style="/*margin-left: 5px; */margin-top:20px;">
		<tr>
			<td width ="20%" valign="TOP" >
				<form class="form-horizontal siom-form-tiny" role="form" action="#/pago/pago_presupuesto/add" method="POST" id="form-pago_presupuesto">
					<fieldset class="siom-fieldset" style="padding-bottom:0px; margin-top:0px; margin-left:0px; shape-margin:10px;  ">
					<legend>Ingreso Linea Presupuestaria</legend>
					<table border="0px" width ="100%" valign="TOP">
						<tr>
							<td  align="right">
								<label for="pago_anio" title='pago_anio' class="control-label" >Año</label>
							</td>
							<td  class="control-label-th">
								<input class="form-control" style=" width: 140px; margin-left: 20px" type="text" id="papr_anio"  
								 onkeypress="return validaNumericos(event)" name="pago_anio" placeholder="{{anio_actual}}" maxlength="4" value="{{anio_actual}}" />							
							</td>
						</tr>
						<tr>
							<td align="right">
								<label for="pago_anio" title='pago_anio' class="control-label" >Monto <span class="span_tipo_moneda_pago"></span></label>
							</td>
							<td  class="control-label-th">
								<input  class="form-control " style=" width: 140px; margin-left: 20px" type="text" id="papr_monto" name="pago_monto" onkeypress="return validaNumericos(event)" placeholder="Monto" maxlength="20" />
							</td>
						</tr>
						<tr>
							<td  align="right">
								<label for="pago_empresa" title='empresa'  class="control-label" >Empresa</label>
							</td>
							<td class="control-label-th">
								<select class="botones text-ancho" data-width="100%"  id="papr_empresa" name="pago_empresa">
								{{#empresa}}
									<option value="{{soci_nombre}}">{{soci_nombre}}</option>
								{{/empresa}}
								</select>							
							</td>
						</tr>
						<tr>
							<td  align="right">
								<label for="pago_sociedad" title='sociedad'  class="control-label" >Sociedad</label>
							</td>
							<td  class="control-label-th">
								<select class="botones text-ancho" data-width="100%" id="papr_sociedad" name="pago_sociedad">
								{{#sociedad}}
									<option value="{{soci_codigo}}">{{soci_codigo}}</option>
								{{/sociedad}}
								</select>
							</td>
						</tr>
						<tr>
							<td  align="right">
								<label  for="pago_linea" title='pago_linea' class="control-label" >Tipo linea</label>
							</td>
							<td width ="75%" class="control-label-th">
								<select class="botones text-ancho" data-width="100%"  id="tipo_linea" name="tipo_linea">
									<option value="CAPEX">CAPEX</option>
									<option value="OPEX">OPEX</option>
									<option value="AMBAS">AMBAS</option>
								</select>
							</td>
						</tr>
						<tr>
							<td align="right">
								<label for="pago_proveedor" title='pago_proveedor' class="control-label" >Proveedor</label>
							</td>
							<td width ="75%" class="control-label-th">
								<select class="botones text-ancho" data-width="100%"  id="papr_proveedor" name="pago_proveedor">
								{{#proveedor}}
									<option value="{{empr_nombre}}">{{empr_nombre}}</option>
								{{/proveedor}}
								</select>
							</td>
						</tr>
						<tr>
							<td   align="right">
								<label for="pago_servicio" title='pago_servicio' class="control-label" >Nom. Servicio</label>
							</td>
							<td class="control-label-th">
								<textarea  class="form-control text-ancho" style=" width: 140px;margin-left: 20px" type="text" name="pago_servicio"  id="papr_servicio" maxlength="150"></textarea>
							</td>
						</tr>
						<tr style="display:none">
							<td  align="right">
								<label for="pago_capex_opex" title='capex_opex' class="col-ingreso" control-label">ID_Opex / Capex</label>
							</td>
							<td  class="control-label-th">
								<input type="text" id="papr_capex_opex" name="pago_capex_opex" placeholder="ID_Opex / Capex" maxlength="20"/>
							</td>
                        </tr>
						<tr>
							<td colspan="2" width ="25%" class="control-label-th">
								<button type="button" class="btn btn-primary btn-default pull-right" id="id_papr_boton_consultar" > Ingresar </button>
							</td>
						</tr>
					</table>
					</fieldset>
				</form>
			</td>
			<td width ="80%" valign="TOP">
			    <form class="form-horizontal siom-form-tiny" role="form" action="#/pago/presupuesto_tabla" method="POST" id="form-pago-presupuesto-tabla">
					<div>
						<div class="col-md-12" id="pago-presupuesto-tabla"></div>
					</div>
				</form>
			</td>
		</tr>
	</table>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script> 
<script src="js/pago_presupuesto.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>