<div class="col-md-12">
   <fieldset class="siom-fieldset" style="padding-bottom:10px; margin-top:0px;">
      <legend>Detalle Costeo del Periodo Seleccionado</legend>
      <div class="col-md-13">
         <a type="button" class="btn btn-primary btn-default pull-right" id="id_papre_boton_consultar" data-tipo="MNT" href="#/pago/pago_detalle_filtro" >Regresar </a>
      </div>
      <div class="col-md-12">
         <div class="col-md-12" style="float: left; padding-top: 0px;  padding-bottom: 0px;padding-right: 0px;padding-left: 0px;">
            <table data-toggle="table" data-sort-order="desc">
               <thead>
                  <tr>
                     <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Número</th>
                     <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Tipo</th>
                     <th colspan="2" class="col-md-2" data-align="right" data-sortable="true">Monto</th>
                     <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Fecha Aprobación</th>
                     <th colspan="1" class="col-md-3" data-align="right" data-sortable="true">Periodo</th>
                  </tr>
                  <tr>
                     <th ></th>
                     <th ></th>
                     <th >Capex</th>
                     <th >Opex</th>
                     <th ></th>
                     <th ></th>
                  </tr>
               </thead>
               <tbody>
                  {{#pago_os}}   
                  <tr>
                     <td class="col-md-2" >{{id_ot}}</td>
                     <td class="col-md-2" >{{tipo_ot}}</td>
                     <td id="capex">{{capex_ot}}</td>
                     <td id="opex">{{opex_ot}}</td>
                     <td class="col-md-2" >{{fec_aprobacion_ot}}</a> </td>
                     <td class="col-md-3" >{{hem_id}}</td>
                     <td class="col-md-3" >{{periodo_pago_ot}}</td>
                  </tr>
                  {{/pago_os}}   
               </tbody>
            </table>
         </div>
      </div>
   </fieldset>
   <div class="col-md-4 text-left">
      <button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}" data-periodo="{{periodo}}">Generar Acta</button>
   </div>
</div>
<style>
   .fixed-table-container thead th .th-inner, .fixed-table-container tbody td .th-inner{
   line-height: 15px;
   text-align: center;
   }
</style>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/detalle_costeo_periodo.js" type="text/javascript" charset="utf-8"></script>