var _isLogged = false;
var _appVersion = "1.0.0";

var _tipo_moneda_pago = "";
var _tipo_moneda = "";

String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

(function ($) {
    window.app = $.sammy('#main', function () {
        this.use('Handlebars', 'hb');
        this.helpers({
            showMain: function (context, template) {
                context.user = window.user;
                context.client = client;
                context.sections = sections;
                context.contract = contract;
                context.contracts = contracts;
                context.tasks = window.tasks || [];
                context.version = _appVersion;

                $.each(context.sections, function (index, s) {
                    l = '#/' + template.replace("_", "/");

                    if (s.sections) {
                        $.each(s.sections, function (index, ss) {
                            if (l.indexOf(ss.link) == 0) {
                                ss['default'] = true;
                            }
                            else {
                                ss['default'] = false;
                            }
                        });
                    }
                    else {
                        if (l.indexOf(s.link) == 0) {
                            s['default'] = true;
                        }
                        else {
                            s['default'] = false;
                        }
                    }
                });
                context.partial('templates/main.hb?' + _appVersion);
            },
            showSection: function (context, template, data, callback) {
                var that = this;
                this.load('templates/' + template + '.hb?' + _appVersion)
                    .then(function (partial) {
                        context.data = data;
                        context.partials = { section_template: partial };
                        context.partial('templates/main.hb?' + _appVersion, data, function () {
                            if (callback) {
                                callback(data);
                            }
                        });

                    })
            },
            showUI: function (context, template, template_data, url, url_params, callback) {
                try {
                    var that = this;
                    if (!url_params) {
                        url_params = {}
                    }

                    this.showMain(context, template);
                    $.get(url, url_params, function (data) {
                        data = $.extend(data, template_data);
                        if (!data.status) {
                            if (data.isSessionExpired) {
                                that.showError("Sesión expirada", data.error, "", "#/logout");
                                return;
                            }
                        }

                        that.showSection(context, template, data, callback);
                    }, 'json').fail(function () {
                        alert("(UI) La busqueda realizada no posee datos de respuesta");
                        that.showSection(context, template, data, callback);
                    });
                }
                catch (e) {
                    data = { status: false, error: e.message };
                    that.showSection(context, template, data, callback);
                }
            },
            updateNotifications: function (data) {
                if (data.status) {
                    actualizar_tareas = true;
                    if (window.tasks) {
                        if (JSON.stringify(window.tasks) === JSON.stringify(data.tareas)) {
                            actualizar_tareas = false;
                        }
                    }

                    if (actualizar_tareas) {
                        window.tasks = data.tareas;
                        num_tareas = data.tareas.length;

                        lista = $("#siom-lista-tareas");
                        if (num_tareas == 0) {
                            $("#siom-numero-tareas").text("0");
                            $("#siom-numero-tareas").removeClass("label-danger").addClass("label-success");

                            lista.html("<li><div class='sin-tarea'>Sin tareas pendientes</div></li>");
                        }
                        else {
                            $("#siom-numero-tareas").text(num_tareas);
                            $("#siom-numero-tareas").removeClass("label-success").addClass("label-danger");

                            html = "";
                            for (i = 0; i < num_tareas; i++) {
                                tarea = data.tareas[i];
                                t = window.ProcesarTarea(tarea);

                                html += "<li>";
                                html += "<div class='row'>";
                                html += "<div class='col-xs-8 text-left'>";
                                html += "<div class='tarea'><a href='" + t.link + "'>" + t.texto + "</a></div>";
                                html += "<div class='emplazamiento'>" + tarea.empl_nombre + "</div>";
                                html += "<div class='direccion'>" + tarea.empl_direccion + "</div>";
                                html += "</div>";
                                html += "<div class='col-xs-4 text-right'>";
                                html += "<div class='id_relacionado'><b>" + tarea.tare_modulo + "</b> Nº " + tarea.tare_id_relacionado + "</div>";
                                html += "<div class='fecha'>" + $.timeago(tarea.tare_fecha_despacho) + "</div>";
                                html += "</div>";
                                html += "</div>";
                                html += "</li>";
                            }
                            lista.html(html);
                        }
                    }

                    if (0 < data.notificaciones.length) {
                        var stack_bottomright = { "dir1": "up", "dir2": "left", "push": "top", "firstpos1": 25, "firstpos2": 0 };

                        for (i = 0; i < data.notificaciones.length; i++) {
                            noti = data.notificaciones[i];
                            n = window.ProcesarNotificacion(noti);

                            if (!window.notificaciones) {
                                window.notificaciones = {};
                            }
                        }
                    }
                } else {
                    console.log(data.error);
                }
            },
            updateElement: function (element, template, url_data, params_data, options) {
                try {
                    var that = this;
                    var msg = (options && options.loading_message) || "Cargando datos...";

                    element.text(msg);
                    $.post(url_data, params_data, function (data) {
                        if (data.status) {
                            data.options = options;
                            $.get("templates/" + template + ".hb").then(function (src) {

                                template = Handlebars.compile(src);
                                element.html(template(data));
                                window.resizeElements();
                            }).fail(function () {
                                that.showError(element, "Error al leer template", "template:" + template)
                            });
                        }
                        else {
                            that.showError(element, data.error, "ruta:" + url_data)
                        }

                    }, 'json');
                }
                catch (e) {
                    this.showError(element, e.message);
                }
            },
            showError: function (title, message, debug_info, nextRoute) {
                var that = this;
                modal = $("#errorModal");
                modal.find(".modal-title").text("SIOM " + title);
                body = "<strong>ERROR </strong>" + message;
                if (debug_info) {
                    body += "<br><small>";
                    body += debug_info;
                    body += "</small>";
                }
                modal.find(".modal-body").html(body);
                modal.modal('show');
                modal.on('hidden.bs.modal', function (e) {
                    if (nextRoute) {
                        that.redirect(nextRoute);
                    }
                })
            },
            showSuccess: function (title, message, nextRoute) {
                var that = this;
                modal = $("#successModal");
                modal.find(".modal-title").text("SIOM " + title);
                modal.find(".modal-body").text(message);
                modal.modal('show');
                modal.on('hidden.bs.modal', function (e) {
                    if (nextRoute) {
                        that.redirect(nextRoute);
                    }
                })
            },
            formatDate: function (dt) {
                try {
                    if (dt == undefined || dt == "") {
                        return "";
                    }
                    //formato yyyy-mm-dd
                    if (typeof dt == 'string' || dt instanceof String) {
                        dt = dt.split("-");

                        if (31 < parseInt(dt[0])) {
                            return dt[0] + "-" + dt[1] + "-" + dt[2];
                        } else {
                            return dt[2] + "-" + dt[1] + "-" + dt[0];
                        }
                    } else {
                        var y = dt.getFullYear();
                        var m = (dt.getMonth() > 8) ? (dt.getMonth() + 1) : ('0' + (dt.getMonth() + 1));
                        var d = (dt.getDate() > 9) ? (dt.getDate()) : ('0' + dt.getDate());
                        //return d.toString()+"-"+m.toString()+"-"+y.toString();
                        return y.toString() + "-" + m.toString() + "-" + d.toString();
                    }
                } catch (e) {
                    console.log(e.message);
                    return "";
                }
            },
            checkProfile: function (verb, path) {
                if (verb == "get") {
                    if (path.indexOf("#") != 0) {
                        path = path.substr(path.indexOf("#"));
                    }
                    path = path.replace(/(\d+)/g, "*");

                    return window.profile.indexOf(path) != -1;
                }
                return true;
            },
            getReadableSize: function (size) {
                var i = -1;
                var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
                do {
                    size = size / 1024;
                    i++;
                } while (size > 1024);
                return Math.max(size, 0.1).toFixed(1) + byteUnits[i];
            }
        });

        this.before({ except: { path: ['#/login', '#/login/post', '#/logout', '#/no_permitido', '#/contrato/cambiar', '#/descargarapk', '#/descargarapkbeta'] } }, function () {
            if (!_isLogged) {
                that = this;
                $.ajax({url: "rest/islogged", type: 'GET', async:false,
                    success: function(json) {
                        if(json.status){
                            _isLogged = json.logged;
                            window.user_id = json.user_id;
                            window.user = json.user_name;
                            client = json.client;
                            contract = json.contrato;
                            sections = json.sections;
                            contracts = json.contracts;
                            window.config = json.config;
                            window.profile = json.profile;
                            window.perfiles_usuario = json.user_level;

                            if (!_isLogged) {
                                app.setLocation('#/login');
                                return false;
                            } else {
                                if (!that.checkProfile(that.verb, that.path)) {
                                    console.log(that.path + ": NO PERMITIDO")
                                    app.setLocation('#/no_permitido')
                                    return false;
                                }
                            }
                        } else {
                            app.setLocation('#/login');
                            return false;
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log(xhr, textStatus, errorThrown);
                        app.setLocation('#/login');
                    }
                });
                $.get("rest/pago/tipos_monedas", function( data ) {
                    _tipo_moneda  = data[0]; 
                    _tipo_moneda_pago = data[1];
                });
                
            } else {
                if (!this.checkProfile(this.verb, this.path)) {
                    console.log(this.path + ": NO PERMITIDO")
                    this.redirect('#/no_permitido')
                    return false;
                }
            }
        });

        this.after(function () {
            if (!this.app.getLocation().endsWith("#/login")) {
                context = this;
                setTimeout(function () {
                }, 500);
            }
        });

        this.get('#/', function () {
            this.redirect('#/login');
        });


        this.get('#/login', function () {

            this.partial('templates/login.hb');
        });

        this.post('#/login/post', function (context) {
            if (this.params.user == "") {
                context.error = "Debe indicar usuario";
                context.partial('templates/login.hb');
            } else if (this.params.pass == "") {
                context.user = this.params.user;
                context.error = "Debe indicar clave";
                context.partial('templates/login.hb');
            } else {
                context.user = this.params.user;
                context.validating = true;
                context.partial('templates/login.hb');
                $.post("rest/login", { "user": this.params.user, "pass": this.params.pass }, function (json, textStatus) {
                    context.validating = false;
                    if (json.status) {
                        isLogged = true;
                        window.user_id = json.user_id;
                        window.user = json.user_name;
                        client = json.client;
                        sections = json.sections;
                        contract = json.contrato;
                        contracts = json.contracts;
                        window.config = json.config;
                        window.profile = json.profile;
                        def_section = 0;

                        if (0 < json.sections.length) {
                            if (json.sections[def_section].sections) {
                                if ($.inArray(json.sections[def_section].sections[0].link, window.profile) < 0) {
                                    context.redirect(window.profile[0]);
                                } else {
                                    context.redirect(json.sections[def_section].sections[0].link);
                                }
                            } else {
                                if ($.inArray(json.sections[def_section].link, window.profile) < 0) {
                                    context.redirect(window.profile[0]);
                                }
                                else {
                                    context.redirect(json.sections[def_section].link);
                                }
                            }
                        }
                    } else {
                        context.error = json.error;
                        context.partial('templates/login.hb');
                    }
                }, "json")
                    .fail(function (xhr, textStatus, errorThrown) {
                        console.log(xhr, textStatus, errorThrown);
                        context.validating = false;
                        context.error = "Error " + xhr.status + ": " + xhr.statusText;
                        context.partial('templates/login.hb');
                    });
            }
        });

        //Interfaz Movil
        //======================================================================
        this.get('#/movil_base', function (context) {
            this.showUI(context, 'movil/movil_template', {}, '');
        });
        //===============================================================================================================
        //Usuario
        this.get('#/usuario', function (context) {
            this.showMain(context, "usuario");
            this.showSection(context, "usuario", { usua_id: window.user_id, usua_nombre: window.user });
        });

        this.post('#/usuario', function (context) {
            $.post("rest/core/usuario/" + this.params.usua_id + "/cambio_clave", { "usua_password": this.params.usua_password, "usua_password_nuevo": this.params.usua_password_nuevo }, function (json, textStatus) {
                if (json.status) {
                    that.showSuccess("Configuración de usuario", "Contraseña cambiada exitosamente");
                } else {
                    that.showError("Configuración de usuario", json.error)
                }
            }, 'json').fail(function () {
                that.showError("Configuración de usuario", "No se pudo guardar cambio de contraseña")
            });
        });

        //logout
        this.get('#/logout', function () {
            _isLogged = false;
            $.post("rest/logout");
            window.SyncStop();
            window.emplazamientosFiltros = null;
            window.osBandejaFiltros = null;
            window.mntBandejaFiltros = null;
            localStorage.clear();
            window.localStorage.clear();

            this.redirect('#/login');
        });

        this.get('#/no_permitido', function (context) {
            this.showMain(context, "no_permitido");
            this.showSection(context, "no_permitido", {});
        });

        //======================================================================
        //Cambiar contrato
        this.post('#/contrato/cambiar', function (context) {
            cont_id = this.params["cont_id"];
            if (!cont_id) {
                alert("Id de contrato inválido");
                return;
            }
            if (cont_id != contract) {
                contract = cont_id;
                window.app.refresh();
            }
        });

        /*Regresa a la pagina principal del perfil correspondiente */
        this.get('#/pago/regresar/resumen', function (context) {
            if (window.perfiles_usuario.includes("SYS_FINANCIERO") || window.perfiles_usuario.includes("ADM_FINANCIERO")) {
                this.redirect('#/pago/resumen');
            } else {
                if (window.perfiles_usuario.includes("VALIDADOR_PRESUPUESTO")) {
                    this.redirect('#/pago/pago_presupuesto');
                } else {
                    this.redirect('#/pago/resumen/cerrado');
                }
            }
        });

        /*pago presupuesto */
        this.get('#/pago/pago_presupuesto/perfil', function (context) {
            if (window.perfiles_usuario.includes("SYS_FINANCIERO") || window.perfiles_usuario.includes("ADM_FINANCIERO")) {
                this.redirect('#/pago/pago_presupuesto/admin');
            } else {
                this.redirect('#/pago/pago_presupuesto');
            }
        });

        /*pago presupuesto */
        this.get('#/pago/presupuesto/perfil', function (context) {

            if (window.perfiles_usuario.includes("SYS_FINANCIERO") || window.perfiles_usuario.includes("ADM_FINANCIERO")) {
                this.redirect('#/pago/pago_presupuesto/admin');
            } else {
                this.redirect('#/pago/pago_presupuesto');
            }

        });

        /*====================visualiza el pago_presupuesto ===================================================*/
        this.get('#/pago/pago_presupuesto/admin', function (context) {
            this.showUI(context, 'pago_presupuesto_admin', {}, 'rest/contrato/' +contract +'/ingresoLinea', {}, function () {
                setTimeout(function () {
                    window.app.runRoute('post', '#/pago/presupuesto/tabla/admin');
                    $("#form-pago-presupuesto-tabla").submit();
                }, 200);
            });
        });

        /*Visualiza  el hb pago_presupuesto_tabla y pago_presupuesto*/
        this.post('#/pago/presupuesto/tabla/admin', function (context) {
            page = this.params['page'];
            /*page = (page)?(page.substring(1)):(1);*/
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            this.updateElement($("#pago-presupuesto-tabla"), "pago_presupuesto_tabla_admin", "rest/contrato/" + contract + "/pago/presupuesto/lista/admin");

        });

        /* Esta funcion es utilizada para mostrar y rescatar los datos en el formulario respectivo con permisos normales*/
        /*this.get('#/pago/pago_detalle_filtro', function(context) */
        this.get('#/pago/resumen', function (context) {
            that = this;
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            this.showUI(context, "pago_resumen_filtro", window.pagoResumenFiltro, "rest/contrato/" + contract + "/pago/resumen/filtros", {}, function () {
                setTimeout(function () {
                    window.app.runRoute('post', '#/pago/resumen/admin/filtro/1', window.pagoResumenFiltro);
                    $("#pago-resumen-filtro").submit();
                }, 200);
            });
        });

        /* visualiza pago resumen filtro */
        this.post('#/pago/resumen/filtro(/:page)?', function (context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);

            data = this.params.toHash();
            delete data.splat;
            delete data.page;

            this.updateElement($("#pago-resumen-lista"), "pago_resumen_lista", "rest/contrato/" + contract + "/pago/resumen/filtro/" + page, data);
            setTimeout(function () {
                window.app.runRoute('post', '#/pago/presupuesto/tabla');
                $("#form-pago-presupuesto-tabla").submit();
            }, 200);
        });

        /*====================visualiza pagina principal cerrado  ===================================================*/
        this.get('#/pago/resumen/cerrado', function (context) {
            that = this;
            data = this.params.toHash();
            delete data.splat;
            delete data.page;

            if (window.pagoResumenFiltro) {
                window.pagoResumenFiltro.pafi_anio = this.formatDate(window.pagoResumenFiltro.pafi_anio);
            } else {
                window.pagoResumenFiltro = {}
            }
            this.showUI(context, "pago_resumen_filtro_cerrado", window.pagoResumenFiltro, "rest/contrato/" + contract + "/pago/resumen/filtros", {}, function () {
                setTimeout(function () {
                    window.app.runRoute('post', '#/pago/resumen/cerrado/filtro/1', window.pagoResumenFiltro);
                    /*$("#pago-resumen-filtro-cerrado").submit();*/
                }, 200);
            });
        });

        /* visualiza pago resumen cerrado */
        this.post('#/pago/resumen/cerrado/filtro(/:page)?', function (context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);

            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            copa_id = this.params['copa_id'];
            this.updateElement($("#pago-resumen-lista-cerrado"), "pago_resumen_lista_cerrada", "rest/contrato/" + contract + "/pago/resumen/cerrado/filtro/" + page, data);
            setTimeout(function () {
            }, 200);
        });

        /*Visualiza  el hb pago_presupuesto_tabla y pago_presupuesto*/
        this.post('#/pago/presupuesto/tabla', function (context) {
            page = this.params['page'];
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            this.updateElement($("#pago-presupuesto-tabla"), "pago_presupuesto_tabla", "rest/contrato/" + contract + "/pago/presupuesto/lista");

        });

        /*====================visualiza el pago_presupuesto ===================================================*/
        this.get('#/pago/pago_presupuesto', function (context) {
            this.showUI(context, 'pago_presupuesto', {}, 'rest/contrato/'+contract +'/ingresoLinea', {}, function () {
                setTimeout(function () {
                    window.app.runRoute('post', '#/pago/presupuesto/tabla');
                    $("#form-pago-presupuesto-tabla").submit();
                }, 200);
            });

        });

        this.get('#/pago/costeo/documentacion/periodo/:periodo/costeo/:copa_id', function (context) {
            that = this;
            data = this.params.toHash();
            copa_id = this.params['copa_id'];
            periodo = this.params['periodo'];
            this.showUI(context, 'pago_costeo_documentacion', {}, "rest/costeo/contrato/" + contract + "/visualizar/documentacion/periodo/" + periodo + "/costeo/" + copa_id, {}, function () {
                setTimeout(function () {
                }, 8000000);
            });

        });

        /* visualiza pantalla de usuario validador costeo */
        this.get('#/pago/asignar/usuario/validador/costeo/copa_id/:copa_id', function (context) {

            that = this;
            data = this.params.toHash();
            copa_id = this.params['copa_id'];

            this.showUI(context, 'costeo_asignacion_validacion_costeo', {}, "rest/contrato/" + contract + "/pago/asignar/usuario/validador/costeo/copa_id/" + copa_id, {}, function () {
                setTimeout(function () {
                }, 8000000);
            });

        });
        /* visualiza pantalla de usuario validador acta */
        this.get('#/pago/asignar/usuario/validador/acta/copa_id/:copa_id', function (context) {

            that = this;
            data = this.params.toHash();
            copa_id = this.params['copa_id'];
            this.showUI(context, 'costeo_asignacion_validacion_acta', {}, "rest/contrato/" + contract + "/pago/asignar/usuario/validador/acta/copa_id/" + copa_id, {}, function () {
                setTimeout(function () {
                }, 8000000);
            });

        });

        /*===============================visualiza el detalle de hem y derivada ==========================*/
        this.post('#/hemyderivada/detalle', function(context) {
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            this.updateElement($("#detalle-hemyderivada"), "detalle_hemyderivada","rest/contrato/"+contract+"/detalle/hemyderivada", data);
        });

        /*============================ Visualizar editar hem generar filtro==============================*/ 
        this.get('#/pago/ingresar/hemyderivada/copa_id/:copa_id', function(context){
            that = this;
            data = this.params.toHash();
            copa_id = this.params['copa_id'];
            this.showUI(context, 'pago_ingresar_hemyderivada_filtro',window.pagoCosteoFiltro,"rest/contrato/"+contract+"/pago/ingreso/hemyderivada/copa_id/"+copa_id,{},function(){
                setTimeout(function() {
                    window.app.runRoute('post','#/pago/detalle/hemyderivada/filtro/1',window.pagoCosteoFiltro);
                }, 200);
            });
        });

        /*se enlaza con el filtro de ingreso hem y derivada  para visualizar los datos solicitados */
        this.post('#/pago/detalle/hemyderivada/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page)?(page.substring(1)):(1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            periodo = this.params['periodo']; 
            actividad = this.params['actividad'];
            //actividad = this.params['xxactividad'];
            region = this.params['region'];
            f_ini_programada = this.params['f_ini_programada'];
            f_fin_programada = this.params['f_fin_programada'];
            zmovistar = this.params['zmovistar'];
            zcontrato = this.params['zcontrato'];  
            zcluster = this.params['zcluster']; 
            numero_ot = this.params['numero_ot']; 
            lpresupustaria = this.params['lpresupustaria']; 
            if('OS'!=actividad && 'MNT' != actividad && 'LMT'!=actividad){
                actividad = "OS";
            }            
            data = {    periodo:periodo
                        ,actividad:actividad
                        ,region:region
                        ,f_ini_programada:f_ini_programada
                        ,f_fin_programada:f_fin_programada
                        ,zmovistar:zmovistar
                        ,zcontrato:zcontrato
                        ,zcluster:zcluster
                        ,numero_ot:numero_ot
                        ,lpresupustaria:lpresupustaria
            };
            this.updateElement($("#pago-costeo-ingresar-hemyderivada-lista"),"pago_ingresar_hemyderivada","rest/contrato/"+contract+"/pago/ingreso/hemyderivada/"+copa_id+"/copa_id/"+page,data);
             setTimeout(function() {
                },200);

        });

        /*====================Visualiza pago costeo editar generar filtro ===================================================*/
        this.get('#/pago/editar/costeo/copa_id/:copa_id', function (context) {
            that = this;
            data = this.params.toHash();
            copa_id = this.params['copa_id'];
            this.showUI(context, 'pago_costeo_editar_generar_filtro', window.pagoCosteoFiltro, "rest/contrato/" + contract + "/pago/costeo/copa_id/" + copa_id, {}, function () {
                setTimeout(function () {
                    window.app.runRoute('post', '#/pago/detalle/editar/costeo/filtro/1', window.pagoCosteoFiltro);
                }, 200);
            });
        });

        /*se enlaza con el filtro para visualizar los datos solicitados */
        this.post('#/pago/detalle/editar/costeo/filtro(/:page)?', function (context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            periodo = this.params['periodo'];
            actividad = this.params['actividad'];
            //actividad = this.params['xxactividad'];
            region = this.params['region'];
            f_ini_programada = this.params['f_ini_programada'];
            f_fin_programada = this.params['f_fin_programada'];
            zmovistar = this.params['zmovistar'];
            zcontrato = this.params['zcontrato'];  
            zcluster = this.params['zcluster']; 
            numero_ot = this.params['numero_ot']; 
            if('OS'!=actividad && 'MNT' != actividad && 'LMT'!=actividad){
                actividad = "OS";
            }
            data = {    periodo:periodo
                        ,actividad:actividad
                        ,region:region
                        ,f_ini_programada:f_ini_programada
                        ,f_fin_programada:f_fin_programada
                        ,zmovistar:zmovistar
                        ,zcontrato:zcontrato
                        ,zcluster:zcluster
                        ,numero_ot:numero_ot
            };
            this.updateElement($("#pago-costeo-editar-generar-lista"), "pago_costeo_editar_generar", "rest/contrato/" + contract + "/pago/costeo/editar/generar/" + copa_id + "/copa_id/" + page, data);
            setTimeout(function () {
            }, 200);

        });

        /*====================visualiza pagina principal de generar costeo  ===================================================*/
        this.get('#/pago/costeo/generar', function (context) {
            that = this;
            data = this.params.toHash();
            this.showUI(context, "pago_costeo_generar_filtro", window.pagoCosteoFiltro, "rest/contrato/" + contract + "/pago/costeo", {}, function () {
                setTimeout(function () {
                    window.app.runRoute('post', '#/pago/detalle/costeo/filtro/1', window.pagoCosteoFiltro);
                }, 200);
            });
        });

        /*visualiza generar costeo*/
        this.post('#/pago/detalle/costeo/filtro(/:page)?', function (context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            periodo = this.params['periodo'];
            actividad = this.params['actividad'];
            region = this.params['region'];
            f_ini_programada = this.params['f_ini_programada'];
            f_fin_programada = this.params['f_fin_programada'];
            zmovistar = this.params['zmovistar'];
            zcontrato = this.params['zcontrato'];
            zcluster = this.params['zcluster'];
            numero_ot = this.params['numero_ot'];

            if('OS'!=actividad && 'MNT' != actividad && 'LMT'!=actividad){
				actividad = "OS";
            }

            data = {    periodo:periodo
                        ,actividad:actividad
                        ,region:region
                        ,f_ini_programada:f_ini_programada
                        ,f_fin_programada:f_fin_programada
                        ,zmovistar:zmovistar
                        ,zcontrato:zcontrato
                        ,zcluster:zcluster
                        ,numero_ot:numero_ot
           };

            this.updateElement($("#pago-costeo-generar-lista"), "pago_costeo_generar", "rest/contrato/" + contract + "/pago/generar/costeo/" + actividad + "/" + page, data);
            setTimeout(function () {
            }, 200);

        });


        /* =====================realiza el ingreso de un presupuesto==================================================*/
        this.post('#/pago/pago_presupuesto/add', function (context) {

            that = this;
            form = $(context.target);
            data = this.params.toHash();
            delete data.splat;
            var formData = new FormData();
            $.each(data, function (key, value) {
                if ($.isArray(value)) {
                    formData.append(key, JSON.stringify(value));
                } else {
                    if (key == "archivos_descripciones") {
                        formData.append(key, JSON.stringify([value]));
                    } else {
                        formData.append(key, value);
                    }
                }
            });

            $.each(form.find("input:file"), function (i, file) {
                if ($(file)[0].files[0]) {
                    that.showError(" 4 Error")
                    formData.append($(this).attr("cont_id"), $(file)[0].files[0]);
                }
            });
            url = 'rest/pago_presupuesto/add/contrato/' + contract;
            $.ajax({
                url: url,
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (json, textStatus) {
                    if (json.status) {
                        that.showSuccess("Se ha creado correctamente ", "El presupuesto se ha ingresado satisfactoriamente");
                    } else {
                        that.showError("No se pudo ingresar nuevo presupuesto", "no se puede ingresar un presupuesto con el mismo año")
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    that.showError("  Error al ingresar presupuesto", "No se pudo ingresar nuevo presupuesto con el mismo año (" + textStatus + ")", "ruta:" + url, "#/pago/pago_presupuesto")
                }
            });
        });

        /*realiza la asignacion de validadores costeo*/

        this.post('#/pago/asignar/validador/costeo/copa_id/:copa_id/add', function (context) {
            that = this
            copa_id = this.params['copa_id'];
            validadores = this.params['validadores'];
            data = this.params.toHash();
            delete data.splat;
                                        
            url = 'rest/contrato/'+contract+'/asignar/validador/costeo/copa_id/'+copa_id+'/add';
            $.post(url,data,function(json,textStatus) {
                form = $(context.target);
                if (json.status) {
                    form.find('.siom-form-actions').hide();
                    form.find(':input').prop('disabled', true);
                    that.showSuccess("Agregando validador", "La asignación de validadores se efectuo correctamente", "#/pago/regresar/resumen")
                }
                else {
                    form.find(':submit').button('reset');
                    that.showError("Error agregando validadores", json.error, "ruta:" + url, "#/pago/regresar/resumen")
                }
            }, 'json').fail(function () {
                form = $(context.target);
                form.find(':submit').button('reset');
                that.showError("Error agregando validadores", "No se pudo agregar validador", "ruta:" + url, "#/pago/regresar/resumen")
            });
        });

        /*realiza la asignacion de validadores acta*/
        this.post('#/pago/asignar/validador/acta/copa_id/:copa_id/add', function (context) {
            that = this
            copa_id = this.params['copa_id'];
            validadores = this.params['validadores'];
            data = this.params.toHash();
            delete data.splat;
            
            url = 'rest/contrato/' + contract + '/asignar/validador/acta/copa_id/' + copa_id + '/add';
            $.post(url, data, function (json, textStatus) {
                form = $(context.target);
                if (json.status) {
                    form.find('.siom-form-actions').hide();
                    form.find(':input').prop('disabled', true);
                    that.showSuccess("Agregando validador", "La asignación de validadores se efectuo correctamente", "#/pago/regresar/resumen")
                }
                else {
                    form.find(':submit').button('reset');
                    that.showError("Error agregando validadores", json.error, "ruta:" + url, "#/pago/regresar/resumen")
                }
            }, 'json').fail(function () {
                form = $(context.target);
                form.find(':submit').button('reset');
                that.showError("Error agregando validadores", "No se pudo agregar validador", "ruta:" + url, "#/pago/regresar/resumen")
            });
        });

        /*Realiza el ingreso de un suplemnto*/
        this.post('#/pago/pago_suplementario/add', function (context) {

            that = this;
            form = $(context.target);
            data = this.params.toHash();
            delete data.splat;

            var formData = new FormData();
            $.each(data, function (key, value) {
                if ($.isArray(value)) {
                    formData.append(key, JSON.stringify(value));
                } else {
                    if ("archivos_descripciones" == key) {
                        formData.append(key, JSON.stringify([value]));
                    } else {
                        formData.append(key, value);
                    }
                }
            });

            $.each(form.find("input:file"), function (i, file) {
                if ($(file)[0].files[0]) {
                    that.showError(" 4 Error")
                    formData.append($(this).attr("cont_id"), $(file)[0].files[0]);
                }
            });
            url = 'rest/pago_suplementario/add/contrato/' + contract;
            $.ajax({
                url: url,
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (json, textStatus) {
                    if (json.status) {
                        that.showSuccess("Se ha creado correctamente ", "El suplemento  se ha ingresado satisfactoriamente");
                    } else {
                        that.showError("No se pudo ingresar nuevo suplemento ", "no se puede ingresar un suplemento")
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    that.showError("  Error al ingresar suplemento", "No se pudo crear nuevo suplemento (" + textStatus + ")", "ruta:" + url, "#/pago/regresar/resumen")
                }
            });

        });

        /*   =======================================================================
           traer contratos actuales a pago_presupuesto indicando el formulario dirigido y el php con las funcionalidades */
        this.get('#/pago_detalle_filtro', function (context) {
            this.redirect('#/pago/regresar/resumen');
        });

        /*Filtro de pago costeo generar*/
        this.post('#/pago/detalle/filtro', function (context) {

            actividad = this.params['actividad'];
            periodo = this.params['periodo'];
            fecha_inicio = this.params['fecha_inicio'];
            region = this.params['region'];
            zcontrato = this.params['zcontrato'];
            zmovistar = this.params['zmovistar'];
            zcluster = this.params['zcluster'];
            data = { periodo:periodo
                 ,actividad: actividad
                 ,fecha_inicio:fecha_inicio 
                 ,fecha_fin:fecha_fin
                 ,region:region
                 ,zcontrato:zcontrato
                 ,zmovistar:zmovistar
                 ,zcluster:zcluster
                };
            delete data.splat;
            delete data.page;
            this.showUI(context, "pago_costeo_generar", data, "rest/contrato/" + contract + "/pago/costeo/filtro/" + actividad + "/actividad", {}, function () {
            });
        });

        /* =====================realiza el ingreso de la hem y la derivada ==================================================*/
        this.post('#/pago/resumen/lista', function (context) {

            that = this;
            form = $(context.target);
            data = this.params.toHash();
            delete data.splat;

            url = 'rest/contrato/' + contract + '/pago/actualizar/presupuesto';
            $.post(url, data, function (json, textStatus) {
                if (json.status) {
                    that.showSuccess("Guardar costeo ", "Costeo guardado existosamente ", "#/pago/regresar/resumen")
                } else {
                    that.showError("Error al guardar el costeo ", json.error, "ruta:" + url, "#/pago/regresar/resumen")
                }
            }, 'json').fail(function () {
                that.showError("Error guardando  costeo", "No se pudo guardar el costeo", "ruta:" + url, "#/pago/regresar/resumen")
            });
        });

        /*Guardar costeo  actividad FAZ */
        this.post('#/pago/costeo/actividad/add', function (context) {
            that = this;
            form = $(context.target);
            data = this.params.toHash();
            var formData = new FormData();
            $.each(data, function (key, value) {
                if ($.isArray(value)) {
                    formData.append(key, JSON.stringify(value));
                } else {
                    if (key == "archivos_descripciones") {
                        formData.append(key, JSON.stringify([value]));
                    } else {
                        formData.append(key, value);
                    }
                }
            });

            url = 'rest/contrato/'+contract+'/pago/costeo/actividad/add';
        			
            send_data= {"str_id_ot":data.str_id_ot,"precio_lpu":data.precio_lpu,"check_penalidad":data.check_penalidad,
            "cantidad_lpu":data.cantidad_lpu,"str_capex_ot":data.str_capex_ot,"str_opex_ot":data.str_opex_ot,
            "str_check_penalidad_value":data.str_check_capex_value,"str_check_opex_value":data.str_check_opex_value,
            "periodo":data.periodo, "actividad":data.actividad, "tipo_ot":data.tipo_ot,
            "tipo_moneda":data.tipo_moneda, "opex_ot":data.opex_ot, "monto_total":data.monto_total_opex, 
            "monto_total_capex":data.monto_total_capex, "monto_total_cabecera":data.monto_total_cabecera,
            "linea":data.linea, "id_ot[]":data.id_ot, "fecha_aprobacion_ot": data.fecha_aprobacion_ot, 
            "check_opex_value":data.check_opex_value, "check_opex":data.check_opex, "capex_ot[]":data.capex_ot,
            "check_capex":data.check_capex, "check_capex_value":data.check_capex_value,"opex_ot":data.opex_ot};
            $.ajax({
                url: url,
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (json, textStatus) {
                    if (json.status) {
                        that.showSuccess("Guardar costeo ", "Costeo guardado existosamente", "#/pago/regresar/resumen")
                        //this.redirect();
                    } else {
                        that.showError("el costeo no se guardo correctamente", json.error, "ruta:" + url, "#/pago/regresar/resumen")
                    }

                },
                error: function (xhr, textStatus, errorThrown) {
                    that.showError("Error al guardar el costeo ", "no se pudo guardar el costeo(" + textStatus + ")", "ruta:" + url, "#/pago/regresar/resumen")
                }
            });
        });

        /*Boton para  guardar en la base de datos el costeo generado*/
        this.post('#/pago/costeo/add', function(context) {
			that = this;
            form = $(context.target);
            data = this.params.toHash();
            actividad = this.params['actividad'];
            periodo = this.params['periodo'];
            
            var formData = new FormData();
            $.each(data, function (key, value) {
                if ($.isArray(value)) {
                    formData.append(key, JSON.stringify(value));
                }
                else {
                    if (key == "archivos_descripciones") {
                        formData.append(key, JSON.stringify([value]));
                    }
                    else {
                        formData.append(key, value);
                    }
                }
            });

            url = 'rest/contrato/' + contract + '/pago/costeo/add';
            $.ajax({
                    url: url,
                    data: formData,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(json, textStatus ){
                        if(json.status){
                            if("OS"==actividad || "LMT"==actividad)
                            {   
                               
                                that.showSuccess("Guardar costeo ","Costeo guardado existosamente","#/pago/generar/costeo/actividad/"+actividad+"/periodo/"+periodo+"/filtro")
                                //this.redirect();
                            }else{
                                 that.showSuccess("Guardar costeo ","Costeo guardado existosamente","#/pago/generar/costeo/actividad/"+actividad+"/periodo/"+periodo+"/filtro")
                                //this.redirect();
                               } 
                            
                        } else {
                            that.showError("El costeo no se guardo correctamente",json.error,"ruta:"+url,"#/pago/regresar/resumen")
                        }

                    },
                    error: function(xhr, textStatus, errorThrown){
                       that.showError("Error al guardar el costeo ","no se pudo guardar el costeo("+textStatus+")","ruta:"+url,"#/pago/regresar/resumen")
                    }
                });
        });


        /*Filtro de pago costeo actividad generar*/
        this.get('#/pago/generar/costeo/actividad/:actividad/periodo/:periodo/filtro', function(context) {
            actividad=this.params['actividad'];
            copa_id=this.params['copa_id'];
            periodo=this.params['periodo'];

            this.showUI(context,"pago_generar_costeo_actividad",{},"rest/contrato/"+contract+"/pago/generar/costeo/filtro/"+actividad+"/actividad/periodo/"+periodo,{},function(){
            });
        });

        /*Boton para  guardar hem ,derivada y tipo de cambio por OT*/
        this.post('#/pago/hemyderivada/add', function(context) {
            that = this;
            form = $(context.target);
            data = this.params.toHash();
            actividad=this.params['actividad'];
                    
            var formData = new FormData();
            $.each(data, function( key, value ) {
                if($.isArray(value)){
                    formData.append(key,JSON.stringify(value));
                }
                else{
                    if(key=="archivos_descripciones"){
                        formData.append(key,JSON.stringify([value]));
                    } else {
                        formData.append(key,value);
                    }
                }
            });

            url = 'rest/contrato/'+contract+'/pago/hemyderivada/add';
            send_data= {"id_ot":data.id_ot,
            "imputderivada":data.imputderivada,
            "imputhem":data.imputhem,
            "imputtipocambio":data.imputtipocambio,
            "observacion":data.observacion,
            };
            $.ajax({
                    url: url,
                    data: formData,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(json, textStatus ){
                        if(json.status){
                            that.showSuccess("Guardar Hem y Derivada"," hem y derivada guardados exitosamente ");                         
                        } else {
                            that.showError("El costeo no se guardo correctamente",json.error,"ruta:"+url,"#/pago/regresar/resumen")
                        }

                },
                error: function (xhr, textStatus, errorThrown) {
                    that.showError("Error al guardar el costeo ", "no se pudo guardar el costeo(" + textStatus + ")", "ruta:" + url, "#/pago/regresar/resumen")
                }
            });
        });

        /*Filtro de pago costeo actividad generar*/
        this.get('#/pago/generar/costeo/actividad/:actividad/periodo/:periodo/filtro', function (context) {
            actividad = this.params['actividad'];
            copa_id = this.params['copa_id'];
            periodo = this.params['periodo'];
            this.showUI(context, "pago_generar_costeo_actividad", {}, "rest/contrato/" + contract + "/pago/generar/costeo/filtro/" + actividad + "/actividad/periodo/" + periodo, {}, function () {
            });
        });

        /*Guarda el  costeo por actividad*/
        this.post('#/pago/generar/costeo/actividad/add', function (context) {
            that = this;
            form = $(context.target);
            data = this.params.toHash();

            //delete data.splat;
            str_id_ot = this.params['str_id_ot'];
        
            var formData = new FormData();
            $.each(data, function (key, value) {
                if ($.isArray(value)) {
                    formData.append(key, JSON.stringify(value));
                } else {
                    if (key == "archivos_descripciones") {
                        formData.append(key, JSON.stringify([value]));
                    } else {
                        formData.append(key, value);
                    }
                }
            });

            url = 'rest/contrato/'+contract+'/pago/costeo/actividad/add';
                    
            send_data= {"str_id_ot":data.str_id_ot,"str_capex_ot":data.str_capex_ot,
            "str_opex_ot":data.str_opex_ot,"str_check_capex_value":data.str_check_capex_value,
            "str_check_opex_value":data.str_check_opex_value,"periodo":data.periodo, 
            "actividad":data.actividad, "tipo_ot":data.tipo_ot, "tipo_moneda":data.tipo_moneda, 
            "opex_ot":data.opex_ot, "monto_total_opex":data.monto_total_opex, "monto_total_capex":data.monto_total_capex,
            "monto_total_cabecera":data.monto_total_cabecera, "linea":data.linea, "id_ot[]":data.id_ot,
            "fecha_aprobacion_ot": data.fecha_aprobacion_ot, "check_opex_value":data.check_opex_value, 
            "check_opex":data.check_opex, "capex_ot[]":data.capex_ot, "check_capex":data.check_capex,
            "check_capex_value":data.check_capex_value,"opex_ot":data.opex_ot,"check_penalidad_value":data.check_penalidad_value,
            "check_penalidad":data.check_penalidad,"penalidad":data.penalidad_ot,"str_penalidad":data.str_penalidad_ot,
            "monto_total_penalidad":data.monto_total_penalidad,"str_check_penalidad_value":data.str_check_penalidad_value
            };

            $.ajax({
                url: url,
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (json, textStatus) {
                    if (json.status) {
                        that.showSuccess("Guardar costeo ", "Costeo guardado exitosamente", "#/pago/regresar/resumen")
                    } else {
                        that.showError("el costeo no se guardo correctamente", json.error, "ruta:" + url, "#/pago/regresar/resumen")
                    }

                },
                error: function (xhr, textStatus, errorThrown) {
                    that.showError("Error al guardar el costeo ", "no se pudo guardar el costeo(" + textStatus + ")", "ruta:" + url, "#/pago/regresar/resumen")
                }
            });
        });

        this.post('#/pago/costeo/ver/filtro/ot', function (context) {

            that = this;
            form = $(context.target);
            data = this.params.toHash();
            delete data.splat;
            data.tipo_ot = this.params['tipo_ot'];
            data.periodo = this.params['periodo'];
            copa_id = 0;

            this.redirect("#/pago/costeo/periodo/" + data.periodo + "/actividad/" + data.tipo_ot + "/" + copa_id);
            //$("#siom-form-costeo").submit();

        });


        this.get('#/pago/costeo/periodo/:periodo/actividad/:actividad/:copa_id', function (context) {

            that = this;
            periodo_valor = this.params['periodo'];
            actividad_valor = this.params['actividad'];
            copa_id_valor = this.params['copa_id'];
            data = { periodo:periodo_valor,
                     actividad:actividad_valor,
                     copa_id:copa_id_valor};
            this.showUI(context, "pago_costeo_ver_filtro", window.pagoCosteoVer, "rest/contrato/" + contract + "/periodo/" + this.params['periodo'] + "/actividad/" + actividad_valor + "/costeover", {}, function () {
                window.app.runRoute('post', "#/pago/costeo/periodo/" + periodo_valor + "/actividad/" + actividad_valor + "/copa/" + copa_id_valor + "/ver/filtro/1", data);
            });
        });
        this.post('#/pago/costeo/periodo/:periodo/actividad/:actividad/:copa_id_valor', function (context) {

            that = this;            
            periodo_valor = this.params['periodo'];
            actividad_valor = this.params['actividad'];
            copa_id_valor = this.params['copa_id'];
            this.redirect("#/pago/costeo/periodo/" + periodo_valor + "/actividad/" + actividad_valor + "/copa/" + copa_id_valor + "/ver/filtro/1", data);

        });
        /*Visualiza  la pagina de costeo periodo*/
        this.post('#/pago/costeo/periodo/:periodo/actividad/:actividad/copa/:copa_id_valor/ver/filtro(/:page)?', function (context) {

            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            periodo = this.params['periodo'];
            actividad = this.params['actividad'];
            copa_id_valor = this.params['copa_id'];
            copa_id = this.params['copa_id_valor'];
            if(copa_id_valor == null || copa_id_valor == 'undefined' || copa_id_valor == '') {
                copa_id_valor = copa_id;
            }

            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            usuario = window.user_id;
            this.updateElement($("#pago-costeo-lista"), "pago_costeo_ver", "rest/contrato/" + contract + "/usuario/" + usuario + "/periodo/" + periodo + "/actividad/" + actividad + "/copa/" + copa_id_valor + "/ver/filtro/" + page, data);
        });


        //=======detalle os pagada
        /*NO EN USO*/
        this.get('#/pago/detalle_costeo_periodo/contrato/periodo/:periodo', function (context) {
            this.showUI(context, 'detalle_costeo_periodo', {}, 'rest/pago/detalle_costeo_periodo/contrato/' + contract + '/periodo/' + this.params['periodo']);            
        });

        /*Detalle costeo*/
        this.get('#/pago/detalle_os_periodo/contrato/periodo/:periodo', function (context) {
            this.showUI(context, 'detalle_os_periodo', {}, 'rest/pago/detalle_os_periodo/contrato/' + contract + '/periodo/' + this.params['periodo']);
        });

        this.post('#/pago/pago_presupuesto', function (context) {

            that = this;
            form = $(context.target);
            data = this.params.toHash();

            delete data.splat;
            var formData = new FormData();
            $.each(data, function (key, value) {
                if ($.isArray(value)) {
                    formData.append(key, JSON.stringify(value));
                } else {
                    if (key == "archivos_descripciones") {
                        formData.append(key, JSON.stringify([value]));
                    }
                    else {
                        formData.append(key, value);
                    }
                }
            });

            $.each(form.find("input:file"), function (i, file) {
                if ($(file)[0].files[0]) {
                    that.showError(" 4 Error")
                    formData.append($(this).attr("cont_id"), $(file)[0].files[0]);
                }
            });

            url = 'rest/buscar_aprobado';
            $.ajax({
                url: url,
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (json, textStatus) {
                    if (json.status) {

                    } else {
                        that.showError("Error al realizar la consulta ", json.error, "ruta:" + url, "#/pago_presupuesto")
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    that.showError("Error al realizar la consulta", "no se puedo realizar la nueva consulta(" + textStatus + ")", "ruta:" + url, "#/pago_presupuesto")
                }
            });
        });

        this.post('#/pago/subir/archivo', function (context) {
            that = this;
            form = $(context.target);
            data = this.params.toHash();
            delete data.splat;
            var formData = new FormData();
            $.each(data, function (key, value) {
                if ($.isArray(value)) {
                    formData.append(key, JSON.stringify(value));
                }
                else {
                    if (key == "archivos_descripciones") {
                        formData.append(key, JSON.stringify([value]));
                    }
                    else {
                        formData.append(key, value);
                    }
                }
            });


            $.each(form.find("input:file"), function (i, file) {
                if ($(file)[0].files[0]) {
                    formData.append($(this).attr("name"), $(file)[0].files[0]);
                }
            });

            url = 'rest/pago_costeo_ver/contrato/' + contract + '/archivo/add', data;
            $.ajax({
                url: url,
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (json, textStatus) {
                    if (json.status) {
                        that.showSuccess("", "El archivo se subio correctamente ", "#/pago/costeo/periodo/" + periodo + "/actividad/" + actividad + "/" + copa_id_valor + "");
                    }
                    else {
                        that.showError("Error al subir el archivo", json.error, "ruta:" + url, "#/pago/costeo/periodo/" + periodo + "/actividad/" + actividad + "/" + copa_id_valor + "");
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    that.showError("Error creando orden de servicio", "No se pudo guardar O.S. (" + textStatus + ")", "ruta:" + url, "#/pago/costeo/periodo/" + periodo + "/actividad/" + actividad + "/" + copa_id_valor + "")
                }
            });
        });
        this.notFound = function () {
        }
    });
    window.app.raise_errors = true;
    window.app.run('#/login');
})(jQuery);
