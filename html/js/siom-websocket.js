(function($) { 
	var socket = null;
	var loginInterval = 3000;

	window.WebsocketConnect = function(callback){
		socket = io.connect("http://localhost:5000");
		
		socket.on("connect", function(){
		    console.log("Socket: conectado");

		    if(window.user){

		    	socket.emit("login",window.user);
		    }
		    else{
		    	var loginTimer = setInterval(function(){
		    		if(window.user){
				    	socket.emit("login",window.user);
				    	clearInterval(loginTimer);
				    }
		    	},loginInterval);
		    }
		});

		socket.on("logged", function(data) {
			console.log("logged",data);

			if(callback){
				callback("onConnected");
			}
		});

		 socket.on('disconnect', function(){
		      if(callback){
				callback("onDisconnected");
			}
		 });
	}

	window.WebsocketIsConnected = function(){
		return (socket &&  socket.socket.connected);
	}

	window.WebsocketDisconnect = function(){
		console.log(socket);
		if(socket &&  socket.connected){
			socket.emit("logout");
			socket.disconnect();
		}
	}

})(jQuery);

