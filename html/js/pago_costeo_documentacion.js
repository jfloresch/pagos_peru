(function($) {

$("button.descargarDocumento").on("click",function(e){

		e.stopImmediatePropagation();
	  	$btn = $(this);
	  	$btn.button("Descargando");
	  	repo_id=$btn.data("repo-id");

	  	$.fileDownload("rest/costeo/documentacion/id_reporte/"+repo_id+"/descargar",{httpMethod:"POST",data:"",
		    prepareCallback:function(url) {
		        $btn.button("processing");
		    },
		    successCallback: function(url) {
		        $btn.button('reset')
		    },
		    failCallback: function(responseHtml, url) {
		        $btn.button('reset')
		    }
	  	});		
	});	

})(jQuery);