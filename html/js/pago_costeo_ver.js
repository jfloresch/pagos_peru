
function validaNumericos(event) {
	if (event.charCode >= 48 && event.charCode <= 57) {
		return true;
	} else {
		if (8 == event.charCode) {
			return true;
		}
	}
	return false;
}


(function ($) {

	$("#form-subir-archivo").on('change', '.btn-file :file', function () {
		var input = $(this),
			id = input.data("id");
		label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		nextId = parseInt(id) + 1;
		size = input[0].files[0].size;

		if (size != null && size > window.config.max_file_upload) {
			alert("Tama�o de archivo supera maximo soportado");
			return;
		}

		var html = '<li class="list-group-item">';
		html += '<a class="btn btn-default btn-xs" data-id="' + id + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
		html += '<div>';
		html += '<div><textarea class="form-control" name="archivos_descripciones" rows="1"></textarea></div>';
		html += '<small>' + label + '</small>';
		html += '</div>';
		html += '</li>';
		$(html).appendTo($("#listado-archivos"));

		$('<input type="file" name="archivos[]" id="archivo-' + nextId + '" data-id="' + nextId + '">').insertAfter($(this));
	});
	$("#listado-archivos").on('click', 'a.btn', function (e) {
		var id = $(this).data("id");
		$("#archivo-" + id).remove();
		$(this).parent().remove();
	});


	$('#pagination').bootpag().on("page", function (event, num) {
		page = num;
		data = $.extend($('#pagination').data("filters"), { page: page });
		periodo = $('#pagination').data("periodo");
		actividad = $('#pagination').data("actividad");
		copa_id = $('#pagination').data("copa_id_valor");
		copa_id = copa_id_valor;

		/* window.app.runRoute('post','#/pago/costeo/periodo/'+periodo+'/actividad/'+actividad+'/pago/costeo/ver/filtro/'+page,data);*/
		window.app.runRoute('post', '#/pago/costeo/periodo/' + periodo + '/actividad/' + actividad + '/copa/' + copa_id_valor + '/ver/filtro/' + page, data);
	});

	$("button#download").on("click", function (e) {



		e.stopImmediatePropagation();
		$btn = $(this);
		$btn.button("loading");


		//alert("download2");
		//Flight::route('POST /contrato/@cont_id:[0-9]+/pago/periodo/@periodo:[0-9]+/actividad/@actividad/costeo/ver/descargar', function($cont_id,$periodo,$actividad){
		//$.fileDownload("rest/contrato/"+window.contract+"/pago/periodo/"+$btn.data("periodo")+"/costeo/ver/descargar",{httpMethod:"POST",data:"",
		$.fileDownload("rest/contrato/" + window.contract + "/pago/periodo/" + $btn.data("periodo") + "/actividad/" + $btn.data("actividad") + "/costeo/ver/descargar", {
			httpMethod: "POST", data: "",
			prepareCallback: function (url) {
				$btn.button("processing");
			},
			successCallback: function (url) {
				$btn.button('reset')
			},
			failCallback: function (responseHtml, url) {
				$btn.button('reset')
			}
		});
	});

	$(".eliminarRegistroCosteo").click(function () {

		form = $(context.target);
		var id_ot = "";
		var capex_ot = "";
		var opex_ot = "";
		var monto_total = "";

		$(this).parents("tr").find("#id_ot").each(function () {
			id_ot = $(this).val();
			/*console.log(id_ot);*/
		});

		$(this).parents("tr").find("#capex_ot").each(function () {
			capex_ot = $(this).val();
			/*console.log(capex_ot);*/
		});

		$(this).parents("tr").find("#opex_ot").each(function () {
			//console.log($(this).val());
			opex_ot = $(this).val();
			/*console.log(opex_ot);*/
		});
		$(this).parents("tr").find("#monto_total").each(function () {
			//console.log($(this).val());
			monto_total = $(this).val();
			/*console.log(monto_total);*/
		});


		data = {
			capex_ot: capex_ot
			, opex_ot: opex_ot
			, monto_total: monto_total
		};

		url = 'rest/contrato/' + contract + '/pago/actividad/' + actividad + '/copa_id/' + copa_id_valor + '/ot/' + id_ot + '/eliminar';
		/*	console.log(url);*/
		$.post(url, data, function (json, textStatus) {
			if (json.status) {

				that.showSuccess("Eliminar orden de trabajo ", "Orden de trabajo procesada correctamente ", location.reload());
			} else {
				that.showError("no se quito la orden de servicio ", json.error, "ruta:" + url, "#/pago/regresar/resumen")
			}
		}, 'json').fail(function () {
			that.showError("Error al quitar la orden de servicio ", "No se pudo quitar la orden de servicio", "ruta:" + url, "#/pago/regresar/resumen")
		});
	});

	$("button#downloadMNT").on("click", function (e) {

		e.stopImmediatePropagation();
		$btn = $(this);
		$btn.button("loading");
		//$.fileDownload("rest/contrato/"+window.contract+"/pago/periodo/"+$btn.data("periodo")+"/costeo/ver/mnt/descargar",{httpMethod:"POST",data:"",
		$.fileDownload("rest/contrato/" + window.contract + "/pago/periodo/" + $btn.data("periodo") + "/actividad/" + $btn.data("actividad") + "/costeo/ver/mnt/descargar", {
			httpMethod: "POST", data: "",
			prepareCallback: function (url) {
				$btn.button("processing");
			},
			successCallback: function (url) {
				$btn.button('reset')
			},
			failCallback: function (responseHtml, url) {
				$btn.button('reset')
			}
		});
	});

	$("button#downloadActamnt").on("click", function (e) {

		e.stopImmediatePropagation();
		$btn = $(this);
		$btn.button("loading");
		console.log("el copa_id  =========" + copa_id_valor);
		$.fileDownload("rest/contrato/" + window.contract + "/periodo/" + $btn.data("periodo") + "/pago/copa/" + copa_id_valor + "/acta/mnt", {
			httpMethod: "POST", data: "",
			prepareCallback: function (url) {
				$btn.button("processing");
			},
			successCallback: function (url) {
				$btn.button('reset')
			},
			failCallback: function (responseHtml, url) {
				$btn.button('reset')
			}
		});
	});
	/*direcciona la descarga al php de  generar acta Orden de servicio  */
	$("button#downloadActaos").on("click", function (e) {

		//alert("downloadActaos");

		e.stopImmediatePropagation();
		$btn = $(this);
		$btn.button("loading");

		console.log("el copa_id  =========" + copa_id_valor);
		$.fileDownload("rest/contrato/" + window.contract + "/periodo/" + $btn.data("periodo") + "/pago/copa/" + copa_id_valor + "/acta/os", {
			httpMethod: "POST", data: "",
			prepareCallback: function (url) {
				$btn.button("processing");
			},
			successCallback: function (url) {
				$btn.button('reset')
			},
			failCallback: function (responseHtml, url) {
				$btn.button('reset')
			}
		});
	});

	$("button#validaracta").on("click", function (e) {
		form = $(context.target);

		var copa_id = $(this).data("copa-id");

		confirm("�Esta seguro que desea validar el acta?", function (status) {
			if (status == true) {
				url = 'rest/contrato/' + contract + '/periodo/' + periodo_valor + '/copa_id/' + copa_id_valor + '/actividad/' + actividad_valor + '/validar', data;
				$.post(url, data, function (json, textStatus) {
					if (json.status == 0) {
						that.showError("Error al validar los cambios ", json.error, "ruta:" + url);
					} else {
						alert("Validaci�n ejecutada correctamente ", window.location.replace("#/pago/regresar/resumen"));
						return true;
					}

				}).fail(function () {
					that.showError("Error Al validar acta", "Error en POST", "ruta:" + url);
				});
			} else {
				that.redirect("#/pago/regresar/resumen");
			}
		});
	});
	$("button#validarcosteo").on("click", function (e) {
		form = $(context.target);

		var copa_id = $(this).data("copa-id");

		confirm("�Esta seguro que desea validar el acta?", function (status) {
			if (status == true) {
				url = 'rest/contrato/' + contract + '/periodo/' + periodo_valor + '/copa_id/' + copa_id_valor + '/actividad/' + actividad_valor + '/validar/costeo', data;
				$.post(url, data, function (json, textStatus) {
					if (json.status == 0) {
						that.showError("Error al validar los cambios ", json.error, "ruta:" + url);
					} else {
						alert("Validaci�n ejecutada correctamente ", window.location.replace("#/pago/regresar/resumen"));
						return true;
					}

				}).fail(function () {
					that.showError("Error Al validar acta", "Error en POST", "ruta:" + url);
				});
			} else {
				that.redirect("#/pago/regresar/resumen");
			}
		});
	});

	$(".subirArchivoCosteo").on("click", function (e) {

		archivos_descripciones = $(this).find(':input[name="archivos_descripciones"]');
		for (i = 0; i < archivos_descripciones.length; i++) {
			t = archivos_descripciones[i];
			if ($(t).val() == "") {
				alert("Debe ingresar una descripcion para cada archivo");
				return false;
			}
		}
		$("#form-subir-archivo").submit();
		subirArchivoCosteo.disabled = true;
	});


})(jQuery);