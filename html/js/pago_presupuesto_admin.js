$(".span_tipo_moneda_pago").text(_tipo_moneda_pago);
/*Valida numericos */
function validaNumericos(event) {
    if(event.charCode >= 48 && event.charCode <= 57){
        return true;
    }
    return false;        
}   

function parsePesoJs(valor) {
    var value = valor.value;
    if(null!=value){

        value = value.replace(".", ",");  
        var parts = value.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        valor.value = parts.join("."); 
        return valor.value;       
    }       
}   


(function($) { 


	$("button#download").on("click",function(e){
		e.stopImmediatePropagation();
	  	$btn = $(this);
	  	$btn.button("loading");						
	  	$.fileDownload("rest/contrato/"+window.contract+"/pago/detalle/resumen/descargar",{httpMethod:"POST",data:$.param($btn.data("filters")),
		      prepareCallback:function(url) {
		          $btn.button("processing");
		      },
		      successCallback: function(url) {
		          $btn.button('reset');
		      },
		      failCallback: function(responseHtml, url) {
		          $btn.button('reset')
		          alert(responseHtml);
		      }
	  	});
	});

    $(".eliminarpresupuesto").click(function(){

        form = $(context.target);
        var id_ot="";
        var capex_ot="";
        var opex_ot="";
        var monto_total="";

        $(this).parents("tr").find("#papr_id").each(function(){
            papr_id= $(this).val();
            
        });

        $(this).parents("tr").find("#consumido").each(function(){
            consumido= $(this).val();
           
        });
    
        data = {
                 papr_id:papr_id
                ,consumido:consumido
        };
        if ("0"<consumido || 0 < consumido) {
           
            alert("El presupuesto se encuentra en gesti�n , no se puede eliminar este presupuesto "); 
            return true;
        }       
        url ='rest/contrato/'+contract+'/pago/presupuesto/eliminar';
        $.post(url,data,function(json,textStatus) {
            if(json.status){
                that.showSuccess("Eliminar presupuesto","Presupuesto eliminado correctamente ",'#/pago/presupuesto/perfil');
            } else {
                that.showError("no se pudo eliminar el presupuesto ",json.error,"ruta:"+url,"#/pago/regresar/resumen")
            }
        },'json').fail(function(){
            that.showError("Error al eliminar presupuesto ","no se pudo eliminar el presupuesto contactar con soporte siom","ruta:"+url,"#/pago/regresar/resumen")
        });
    });

	$(".guardarpresupuesto").click(function(){

	$(this).parents("tr").find("#papr_id").each(function(){
        papr_id = $(this).val();
    });

    $(this).parents("tr").find("#empresa").each(function(){
        empresa = $(this).val();
    });
    
    $(this).parents("tr").find("#sociedad").each(function(){
        sociedad = $(this).val();
    });

   /* $(this).parents("tr").find("#id_capex_opex").each(function(){
        //console.log($(this).val());
        id_capex_opex = $(this).val();
        
    });*/

    $(this).parents("tr").find("#nombre_servicio").each(function(){    
        nombre_servicio = $(this).val();  
    });

    $(this).parents("tr").find("#papr_tipo").each(function(){  
        papr_tipo = $(this).val();  
    });

    $(this).parents("tr").find("#proveedor").each(function(){
        proveedor = $(this).val(); 
    });

    $(this).parents("tr").find("#monto").each(function(){   
        monto = $(this).val(); 
    });

    $(this).parents("tr").find("#consumido").each(function(){    
        consumido = $(this).val();   
    });

    $(this).parents("tr").find("#saldo").each(function(){   
        saldo = $(this).val();  
    });

  	if(papr_id == null || papr_id == ""){
        alert("Campo papr_id esta vacio");
    }
    if(empresa == null || empresa == ""){
        alert("Campo empesa esta vacio");
    }
    if(sociedad == null || sociedad == ""){
        alert("Campo sociedad esta vacio");
    }
   /* if(id_capex_opex == null || id_capex_opex == ""){
        alert("Campo capex y opex  esta vacio");
    }*/
    if(nombre_servicio == null || nombre_servicio == ""){
        alert("Campo nombre_servicio esta vacio");
    }
    if(papr_tipo == null || papr_tipo == ""){
        alert("Campo papr_tipo esta vacio");
    }
    if(proveedor == null || proveedor == ""){
        alert("Campo proveedor esta vacio");
    }
    if(monto == null || monto == ""){
        alert("Campo monto esta vacio");
    }else{

        data = {  papr_id:papr_id
          		 ,empresa: empresa
                 ,sociedad:sociedad 
                /* ,id_capex_opex:id_capex_opex*/
                 ,nombre_servicio:nombre_servicio
                 ,papr_tipo:papr_tipo
                 ,proveedor:proveedor
                 ,monto:monto
                 ,consumido:consumido
                 ,saldo:saldo
        };

        url = 'rest/contrato/'+contract+'/pago/presupuesto/actualizar/admin';
            $.post(url,data,function(json,textStatus) {
                    if(json.status){
                          that.showSuccess(" Presupuesto actualizado ","El presupuesto se actualizo correctamente","#/pago/presupuesto/perfil")
                    } else {
                        that.showError("No se actualizo el presupuesto ",json.error,"ruta:"+url,"#/pago/presupuesto/perfil")
                    }
            },'json').fail(function(){
                that.showError("Error al actualizar presupuesto","No se pudo actualizar  el presupuesto ","ruta:"+url,"#/pago/presupuesto/perfil")
            });
    }
});
})(jQuery);