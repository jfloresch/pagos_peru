(function($) { 
	var updateTimer = null;
	// El numero original es 15000. Se cambia para evitar sincronizaciones tan seguidas en DESARROLLO
	var updateInterval = 432000000;


	window.SyncIsRunning = function(){
		return updateTimer!=null;
	}

	window.SyncStop = function(){
		if(updateTimer!=null){
			clearInterval(updateTimer);
			updateTimer = null;
		}
	}

	
})(jQuery);