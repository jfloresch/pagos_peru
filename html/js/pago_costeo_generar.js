(function($) { 

	$("button#download").on("click",function(e){
		e.stopImmediatePropagation();
	  	$btn = $(this);
	  	$btn.button("loading");
	  	$.fileDownload("rest/contrato/"+window.contract+"/pago/os/periodo/"+$btn.data("periodo")+"/descargar",{httpMethod:"POST",data:$.param($btn.data("filters")),
		      prepareCallback:function(url) {
		          $btn.button("processing");
		      },
		      successCallback: function(url) {
		          $btn.button('reset')
		      },
		      failCallback: function(responseHtml, url) {
		          $btn.button('reset')
		          alert(responseHtml);
		      }
	  	});  		
	});

	$('#fecha_inicio,#fecha_fin').datepicker({
	    format: "dd-mm-yyyy",
	    viewMode: "days",
	    minViewMode: "days",
	    language: "es"
	})


/*	$(".buscarFiltro").click(function(){

	
	 console.log('esta aqui');
        data = {  periodo:periodo
          		 ,actividad: actividad
                 ,fecha_inicio:fecha_inicio 
                 ,fecha_fin:fecha_fin
                 ,region:region
                 ,zcontrato:zcontrato
                 ,zmovistar:zmovistar
                 ,zcluster:zcluster
        };

        url = "rest/contrato/"+contract+"/pago/costeo/filtro/"+actividad+"/actividad";
            $.post(url,data,function(json,textStatus) {
                    if(json.status){
                          that.showSuccess(" Presupuesto actualizado ","El presupuesto se actualizo correctamente","#/pago/costeo/generar")
                    } else {
                        that.showError("No se actualizo el presupuesto ",json.error,"ruta:"+url,"#/pago/presupuesto/perfil")
                    }
            },'json').fail(function(){
                that.showError("Error al actualizar presupuesto","No se pudo actualizar  el presupuesto ","ruta:"+url,"#/pago/presupuesto/perfil")
            });
    
  });
*/
})(jQuery);
