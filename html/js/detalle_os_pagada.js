(function($) { 


	$("button#download").on("click",function(e){
		e.stopImmediatePropagation();
	  	$btn = $(this);
	  	$btn.button("loading");	
	  	$.fileDownload("rest/pago/descargar/detalle/os/contrato/"+window.contract+"/periodo/"+$btn.data("periodo")+"/lista",{httpMethod:"POST",data:$.param($btn.data("filters")),
		      prepareCallback:function(url) {
		          $btn.button("processing");
		      },
		      successCallback: function(url) {
		          $btn.button('reset')
		      },
		      failCallback: function(responseHtml, url) {
		          $btn.button('reset')
		          alert(responseHtml);

		      }

	  	});
	  		
	});

})(jQuery);
