(function($) { 
	//inicializar componentes
    $('.selectpicker').selectpicker();
    window.scrollToElement($('#siom-form-asignacion'));

    //Eventos
    $('#filtro_validador').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#lista_validadores li').hide();
            $('#lista_validadores li').filter(function () {
                return rex.test($(this).text());
            }).show();
        })
    $('#filtro_validadores_seleccionados').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#lista_validadores_seleccionados li').hide();
            $('#lista_validadores_seleccionados li').filter(function () {
                return rex.test($(this).text());
            }).show();
        })


    $("#lista_validadores li").click(function(e){
        if($(this).hasClass("disabled")){
            return;
        }

    	id     = $(this).data("id");
    	nombre = $(this).data("nombre");

    	html = '<li class="list-group-item" id="'+id+'">'+
    		   '<span>'+nombre+'</span>'+
    		   '<i class="glyphicon glyphicon-trash pull-right eliminar" data-id="'+id+'"></i>'+
    		   '<input type="hidden" name="validadores" value="'+id+'">'+ // FAZ  tengo que cambiar esto  type='hidden'
			   '</li>';

        $(this).addClass("disabled");
    	$("#lista_validadores_seleccionados").append(html);
    });

    $(document).on('click','#lista_validadores_seleccionados i',function(e){
    	id  = $(this).data("id");
    	$("#lista_validadores_seleccionados #"+id).remove();	
        $("#lista_validadores #"+id).removeClass("disabled");
    })

	// CAMBIO MOMENTANEO SOLO PARA PEXT
    //$('#siom-form-devolver').submit(function() {
    //    $("#submit_devolver").button('loading');
    //    return true;
    //});

    $('#siom-form-asignacion').submit(function() {

		validadores = $("#lista_validadores_seleccionados li").length;	
        if(validadores==0){
        	alert("Debe agregar al menos un validador.");
        	return false;
        }

        $("#submit_asignacion").button('loading');
        return true;
    });
    
})(jQuery);