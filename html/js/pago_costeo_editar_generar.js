/*Valida numericos */
function validaNumericos(event) {
    if(event.charCode >= 48 && event.charCode <= 57){
    return true;
    }
    return false;        
}	


/*Funcion invocada cuando se hace click sobre alguno de los checkbox de pagar*/
function checkPagar() {
	UpdateTotal();
	return true;
}

function checkPagarCapex(valueOT) {
	
	form   = $("#siom-form-costeo");
	
	id_ot  = form.find(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	
	capexPagoSel = form.find(':input[name="check_capex"]').map(function(){ return $(this); }).get();
	opexPagoSel = form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
	
	for(i=0;i<id_ot.length;i++){
		if (capexPagoSel[i].val() == valueOT) {
			if (opexPagoSel[i].is(':checked')) {
				opexPagoSel[i].prop('checked',false);
			}
			break;
		}

	}
	UpdateTotal();
	return true;
}

function checkPagarOpex(valueOT) {
	
	form   = $("#siom-form-costeo");
	id_ot  = form.find(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	
	capexPagoSel = form.find(':input[name="check_capex"]').map(function(){ return $(this); }).get();
	opexPagoSel = form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
	
	for(i=0;i<id_ot.length;i++){
		//alert(opexPagoSel[i].val());
		if (opexPagoSel[i].val() == valueOT) {
			if (capexPagoSel[i].is(':checked')) {
				capexPagoSel[i].prop('checked',false);
			}
			break;
		}
	}
	UpdateTotal();
	return true;
}
/*
	Chequea 
*/
$("#check_capex_all").on("click",function(e){
	form   = $("#siom-form-costeo");
	var c = this.checked;
	$("input[name='check_capex']").prop('checked',c);
	
	id_ot  = $(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	opexPagoSel = form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
	
	for(i=0;i<id_ot.length;i++){
		if (opexPagoSel[i].is(':checked')) {
			opexPagoSel[i].prop('checked',false);
			break;
		}
	}
	UpdateTotal();
	return true;
});

$("#check_opex_all").on("click",function(e){
	form   = $("#siom-form-costeo");
	var c = this.checked;
	$("input[name='check_opex']").prop('checked',c);
	
	id_ot  = $(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	capexPagoSel = form.find(':input[name="check_capex"]').map(function(){ return $(this); }).get();
	
	for(i=0;i<id_ot.length;i++){
		if (capexPagoSel[i].is(':checked')) {
			capexPagoSel[i].prop('checked',false);
			break;
		}
	}
	UpdateTotal();
	return true;
});

/*Funcion que actualiza el total de costeo tanto para el monto CAPEX como el monto OPEX*/
function UpdateTotal(){
	form   = $("#siom-form-costeo");
	id_ot  = form.find(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	check_capex = form.find(':input[name="check_capex"]').map(function(){ return $(this); }).get();
	check_opex = form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
	capex_ot = form.find(':input[name="capex_ot"]').map(function(){ return $(this); }).get();
	opex_ot =form.find(':input[name="opex_ot"]').map(function(){ return $(this); }).get();
	
	totalCapex=0;
	totalOpex=0;
	
	totalCabecera=0;
	totCabeceraCapex=0;
	totCabeceraOpex=0;

	for(i=0;i<id_ot.length;i++){
		totalCapex=0;
		totalOpex=0;
		
		valor = id_ot[i].attr("value");
		if(check_capex[i].is(':checked')){
			totalCapex = totalCapex + parseFloat(capex_ot[i].val());
			totalCabecera=totalCabecera+totalCapex;
			totCabeceraCapex=totCabeceraCapex+totalCapex;
		};

		if(check_opex[i].is(':checked')){
			totalOpex = totalOpex + parseFloat(opex_ot[i].val());
			totalCabecera=totalCabecera+totalOpex;
			totCabeceraOpex=totCabeceraOpex+totalOpex;
		}

		$("#monto_total_"+valor).val(totalCapex+totalOpex);
		
	}
	$("#monto_total_cabecera").val(totalCabecera);
	$("#monto_total_capex").val(totCabeceraCapex);
	$("#monto_total_opex").val(totCabeceraOpex);
}



(function($) { 

	/*$('#pagination').bootpag().on("page", function(event, num){
        page = num;
        data = $.extend($('#pagination').data("filters"),{page:page});
        periodo = $('#pagination').data("periodo");
        actividad = $('#pagination').data("actividad");
        window.app.runRoute('post','#/pago/detalle/periodo/'+periodo+'/actividad/'+actividad+'/detalle/ot/'+page,data);
    });*/
	$("button#download").on("click",function(e){
		e.stopImmediatePropagation();
	  	$btn = $(this);
	  	$btn.button("loading");	
	  	$.fileDownload("rest/pago/descargar/detalle/costeo/contrato/"+window.contract+"/periodo/"+$btn.data("periodo")+"/lista",{httpMethod:"POST",data:$.param($btn.data("filters")),
		    prepareCallback:function(url) {
		        $btn.button("processing");
		    },
		    successCallback: function(url) {
		        $btn.button('reset')
		    },
		    failCallback: function(responseHtml, url) {
		        $btn.button('reset')
		    }
	  	});		
	});


	$("#buscarFiltro").click(function(){
	
		$("#siom-form-costeo-generar").submit();
		return true;

	});

	$("#GuardarCosteo").click(function(){
	
		button   	= $(this);
		form     	= $("#siom-form-costeo");
		id_ot  = form.find(':input[name="id_ot"]').map(function(){ return $(this); }).get();
		check_capex = form.find(':input[name="check_capex"]').map(function(){ return $(this); }).get();
		check_opex = form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
		capex_ot = form.find(':input[name="capex_ot"]').map(function(){ return $(this); }).get();
		opex_ot = form.find(':input[name="opex_ot"]').map(function(){ return $(this); }).get();
		check_opex_value = form.find(':input[name="check_opex_value"]').map(function(){ return $(this); }).get();
		check_capex_value = form.find(':input[name="check_capex_value"]').map(function(){ return $(this); }).get();
		periodo = document.getElementById("periodo").value;
		actividad = document.getElementById("actividad").value;

		
		
		//monto_total_cabecera = $("#monto_total_cabecera").val();
		monto_total_cabecera =document.getElementById("monto_total_cabecera").value;
		if (0==monto_total_cabecera) {

			alert("Debe seleccionar al menos una orden de trabajo, para registrar costeo."); 
			return true;
		}

		monto_total_capex =document.getElementById("monto_total_capex").value;
		lineapresupuestariacapex =document.getElementById("lineacapex").value;
		if ("0"==lineapresupuestariacapex && 0 < monto_total_capex) {
		/*	console.log("paso por validador capex");*/
			alert("Debe seleccionar al menos una linea de presupuesto capex, para registrar costeo."); 
			return true;
		}

		monto_total_opex =document.getElementById("monto_total_opex").value;
		lineapresupuestariaopex =document.getElementById("lineaopex").value;
		if ("0"==lineapresupuestariaopex && 0 < monto_total_opex) {
			/*console.log("paso por validador opex");*/
			alert("Debe seleccionar al menos una linea de presupuesto opex, para registrar costeo."); 
			return true;
		}

		monto_total_opex =document.getElementById("monto_total_opex").value;
		lineapresupuestariaopex =document.getElementById("lineaopex").value;
		if ("0"<lineapresupuestariaopex && 0 == monto_total_opex) {
			/*console.log("paso por validador opex cantidad");*/
			alert("Debe seleccionar al menos una orden de trabajo tipo Opex, para registrar costeo."); 
			return true;
		}

		monto_total_capex =document.getElementById("monto_total_capex").value;
		lineapresupuestariacapex =document.getElementById("lineacapex").value;
		if ("0"<lineapresupuestariacapex && 0 == monto_total_capex) {
		/*	console.log("paso por validador capex");*/
			alert("Debe seleccionar al menos una orden de trabajo tipo Capex, para registrar costeo."); 
			return true;
		}

		
		string_id="";
		string_capex_valor="";
		string_capex_selec="";
		string_opex_valor="";
		string_opex_selec="";
		
		for(i=0;i<id_ot.length;i++){
			string_id=string_id + ";" + id_ot[i].val();
			string_capex_valor=string_capex_valor +";"+ capex_ot[i].val();
			string_opex_valor=string_opex_valor +";"+ opex_ot[i].val();
			
			if(check_opex[i].is(':checked')){
				check_opex_value[i].val("SI");
				string_opex_selec=string_opex_selec + ";SI";
			}else{
				check_opex_value[i].val("NO");
				string_opex_selec=string_opex_selec + ";NO";
			}
			if(check_capex[i].is(':checked')){
				check_capex_value[i].val("SI");
				string_capex_selec=string_capex_selec + ";SI";
			}else{
				check_capex_value[i].val("NO");
				string_capex_selec=string_capex_selec +";NO";
			}
		}
		document.getElementById("str_id_ot").value = string_id;
		document.getElementById("str_capex_ot").value = string_capex_valor;
		document.getElementById("str_check_capex_value").value = string_capex_selec;
		document.getElementById("str_opex_ot").value = string_opex_valor;
		document.getElementById("str_check_opex_value").value = string_opex_selec;
		
		form     	= $("#siom-form-costeo");
		confirm("El costeo será guardado <b></b><br>Desea continuar?",function(status){
			if(status){
				button.button("Guardando...");
				form.submit();
				//return true;
			}
		});
	});
	
})(jQuery);