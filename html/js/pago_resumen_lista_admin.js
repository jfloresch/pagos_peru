
function validaNumericos(event) {
    if(event.charCode >= 48 && event.charCode <= 57){
        return true;
    } else {
        if(8==event.charCode){
            return true;
        }
    }
    return false;
}

(function($) {
	$("button#download").on("click",function(e){
		e.stopImmediatePropagation();
	  	$btn = $(this);
	  	$btn.button("loading");
	  	$.fileDownload("rest/contrato/"+window.contract+"/pago/detalle/descargar",{httpMethod:"POST",data:$.param($btn.data("filters")),
            prepareCallback:function(url) {
                $btn.button("processing");
            },
            successCallback: function(url) {
                $btn.button('reset');
            },
            failCallback: function(responseHtml, url) {
                $btn.button('reset')
                alert(responseHtml);
            }
	  	});
	});

  $("tbody tr").each(function(){
    var elm = $(this).find("td");
    var estado = elm.filter(":eq(2)").text();
    if( "CREADO" == estado || 
		"APROBADO" == estado ||
		"PEND_VALIDAR" == estado ||
		"VALIDANDO" == estado ){
        $(this).find("input").attr('disabled',true);
        $(this).find("select").attr('disabled',true);
        $(this).find("button").attr('disabled',true);
    }
	if(estado == "VALIDADO" ){
        
      //$(this).find("a").remove();

    }
  });

  $(".guardarResumenCosteo").click(function(){

    $(this).parents("tr").find("#copa_id").each(function(){
        //console.log($(this).val());
        copa_id = $(this).val();
        console.log(copa_id);
    });
    $(this).parents("tr").find("#copa_periodo").each(function(){
        //console.log($(this).val());
        copa_periodo = $(this).val();
        console.log(copa_periodo);
    });

    $(this).parents("tr").find("#copa_tipo_ot").each(function(){
        //console.log($(this).val());
        copa_tipo_ot = $(this).val();
        console.log(copa_tipo_ot);
    });
    $(this).parents("tr").find("#copa_estado").each(function(){
        //console.log($(this).val());
        copa_estado = $(this).val();
        console.log(copa_estado);
    });
    $(this).parents("tr").find("#copa_cantidad_total").each(function(){
        //console.log($(this).val());
        copa_cantidad_total = $(this).val();
        console.log(copa_cantidad_total);
    });
    $(this).parents("tr").find("#copa_monto_total").each(function(){
        //console.log($(this).val());
        copa_monto_total = $(this).val();
        console.log(copa_monto_total);
    });
    $(this).parents("tr").find("#copa_monto_capex").each(function(){
        //console.log($(this).val());
        copa_monto_capex = $(this).val();
        console.log(copa_monto_capex);
    });
    $(this).parents("tr").find("#copa_monto_opex").each(function(){
        //console.log($(this).val());
        copa_monto_opex = $(this).val();
        console.log(copa_monto_opex);
    });

  
    $(this).parents("tr").find("#derivada").each(function(){
        //console.log($(this).val());
        derivada = $(this).val();
        console.log(derivada);
    });
    $(this).parents("tr").find("#hem").each(function(){
        //console.log($(this).val());
        hem = $(this).val();
        console.log(hem);
    });
    $(this).parents("tr").find("#tipo_cambio").each(function(){
        //console.log($(this).val());
        tipo_cambio = $(this).val();
        console.log(tipo_cambio);
    });
    $(this).parents("tr").find("#papr_id").each(function(){
        //console.log($(this).val());
        papr_id = $(this).val();
        console.log(papr_id);
    });

    if(derivada == null || derivada == ""){
        alert("Campo derivada esta vacio");
    }
    if(hem == null || hem == ""){
        alert("Campo hem esta vacio");
    }
    if(tipo_cambio == null || tipo_cambio == ""){
        alert("Campo tipo_cambio esta vacio");
    }else{
      data = {copa_id: copa_id,
             derivada: derivada, 
             hem: hem, 
             tipo_cambio:tipo_cambio, 
             papr_id:papr_id
    };
    url = 'rest/contrato/'+contract+'/pago/actualizar/presupuesto';
    $.post(url,data,function(json,textStatus) {
        if(json.status){
              that.showSuccess("Enviado costeo ","Costeo enviado exitosamente","#/pago/resumen")
        } else {
            that.showError("No enviado el costeo ",json.error,"ruta:"+url,"#/pago/resumen")
        }
    },'json').fail(function(){
        that.showError("Error enviando costeo","No se pudo enviar el costeo","ruta:"+url,"#/pago/resumen")
    });
    }
  });

})(jQuery);
