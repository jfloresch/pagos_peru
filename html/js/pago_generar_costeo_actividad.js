
/*Valida numericos */
function validaNumericos(event) {
	
	if(event.charCode >= 48 && event.charCode <= 57 || event.charCode==46 ){
    return true;
    }else{
    		
		if(8==event.charCode){

			return true;
		}else{
    	if(9==event.charCode || 13==event.charCode)
    		
    		actualizarMonto();
   		}
    }
    return false;
}	

function parsePesoJs(valor) {

    var value = valor;
    if(null!=value){
       
        value = value.toString().replace(".", ",");  
        var parts = value.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        value= parts.join("."); 
        return value;       
    }       
}   
function actualizarMonto(){

	form   = $("#siom-form-costeo");
	id_ot  = form.find(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	cantidad_lpu = form.find(':input[name="cantidad_lpu"]').map(function(){ return $(this); }).get();
	precio_lpu= form.find(':input[name="precio_lpu"]').map(function(){ return $(this); }).get();
	
	totalcantidad_lpu=0;
	totalprecio_lpu=0;
	totalCabecera=0;

	for(i=0;i<id_ot.length;i++){
		valor = id_ot[i].attr("value");
		
		str_cantidad_lpu = cantidad_lpu[i].val();
		str_precio_lpu = precio_lpu[i].val();
		totalcantidad_lpu =  parseFloat(str_cantidad_lpu.replace(/,/gi, "."));
		totalprecio_lpu = parseFloat(str_precio_lpu.replace(/,/gi, "."));
		totalCelda=totalcantidad_lpu*totalprecio_lpu;
		

		totalCabecera= totalCabecera + (totalprecio_lpu*totalcantidad_lpu);

		totalCelda=parsePesoJs(totalCelda.toFixed(3));
		$("#monto_total_"+valor).val(totalCelda);
	}

	totalCabecera=parsePesoJs(totalCabecera.toFixed(3));
	$("#monto_total_cabecera").val(totalCabecera);
	return;
}

/*Funcion invocada cuando se hace click sobre alguno de los checkbox de pagar*/
function checkPagar() {
	UpdateTotal();
	return true;
}

function checkPagarCapex(valueOT) {
	
	form   = $("#siom-form-costeo");
	
	id_ot  = form.find(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	
	capexPagoSel = form.find(':input[name="check_capex"]').map(function(){ return $(this); }).get();
	opexPagoSel = form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
	
	for(i=0;i<id_ot.length;i++){
		if (capexPagoSel[i].val() == valueOT) {
			if (opexPagoSel[i].is(':checked')) {
				opexPagoSel[i].prop('checked',false);
			}
			break;
		}
	}
	UpdateTotal();
	return true;
}

function checkPagarOpex(valueOT) {
	
	form   = $("#siom-form-costeo");
	id_ot  = form.find(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	capexPagoSel = form.find(':input[name="check_capex"]').map(function(){ return $(this); }).get();
	opexPagoSel = form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
	
	for(i=0;i<id_ot.length;i++){
		//alert(opexPagoSel[i].val());
		if (opexPagoSel[i].val() == valueOT) {
			if (capexPagoSel[i].is(':checked')) {
				capexPagoSel[i].prop('checked',false);
			}
			break;
		}
	}
	UpdateTotal();
	return true;
}

function checkPenalidad(valueOT) {
	
	form   = $("#siom-form-costeo");
	id_ot  = form.find(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	console.log("paso por el de penalidad")
	penalidad = form.find(':input[name="check_penalidad"]').map(function(){ return $(this); }).get();
	cantidad_lpu = form.find(':input[name="cantidad_lpu"]').map(function(){ return $(this); }).get();
	
	for(i=0;i<id_ot.length;i++){
		if (penalidad[i].val() == valueOT) {
			
			if (penalidad[i].is(':checked')) {
				cantidad_lpu[i].prop('readonly',false);
			}else
			{
				cantidad_lpu[i].prop('readonly',true);
				
			}
			break;
		}
	}

	return true;
}

/*
	Chequea 
*/
$("#check_capex_all").on("click",function(e){
	form   = $("#siom-form-costeo");
	var c = this.checked;
	$("input[name='check_capex']").prop('checked',c);
	
	id_ot  = $(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	opexPagoSel = form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
	
	for(i=0;i<id_ot.length;i++){
		if (capexPagoSel[i].val() == valueOT) {
			if (opexPagoSel[i].is(':checked')) {
				opexPagoSel[i].prop('checked',false);
			}
			break;
		}

	}
	UpdateTotal();
	return true;
});

$("#check_opex_all").on("click",function(e){
	form   = $("#siom-form-costeo");
	var c = this.checked;
	$("input[name='check_opex']").prop('checked',c);
	
	id_ot  = $(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	capexPagoSel = form.find(':input[name="check_capex"]').map(function(){ return $(this); }).get();
	opexPagoSel = form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
	
	for(i=0;i<id_ot.length;i++){
		//alert(opexPagoSel[i].val());
		if (opexPagoSel[i].val() == valueOT) {
			if (capexPagoSel[i].is(':checked')) {
				capexPagoSel[i].prop('checked',false);
			}
			break;
		}
	}
	UpdateTotal();
	return true;
});

/*Funcion que actualiza el total de costeo tanto para el monto CAPEX como el monto OPEX*/
function UpdateTotal(){
	form   = $("#siom-form-costeo");
	id_ot  = form.find(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	check_capex = form.find(':input[name="check_capex"]').map(function(){ return $(this); }).get();
	check_opex = form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
	capex_ot = form.find(':input[name="capex_ot"]').map(function(){ return $(this); }).get();
	opex_ot =form.find(':input[name="opex_ot"]').map(function(){ return $(this); }).get();
	
	totalCapex=0;
	totalOpex=0;
	
	totalCabecera=0;
	totCabeceraCapex=0;
	totCabeceraOpex=0;

	for(i=0;i<id_ot.length;i++){
		totalCapex=0;
		totalOpex=0;
		
		valor = id_ot[i].attr("value");
		if(check_capex[i].is(':checked')){
			totalCapex = totalCapex + parseFloat(capex_ot[i].val());
			totalCabecera=totalCabecera+totalCapex;
			totCabeceraCapex=totCabeceraCapex+totalCapex;
		};

		if(check_opex[i].is(':checked')){
			totalOpex = totalOpex + parseFloat(opex_ot[i].val());
			totalCabecera=totalCabecera+totalOpex;
			totCabeceraOpex=totCabeceraOpex+totalOpex;
		}

		$("#monto_total_"+valor).val(totalCapex+totalOpex);
		
	}
	$("#monto_total_cabecera").val(totalCabecera);
	$("#monto_total_capex").val(totCabeceraCapex);
	$("#monto_total_opex").val(totCabeceraOpex);
}



(function($) { 


	$(".cantidad_lpu").focusout(function(){
		actualizarMonto();

	});
	/*$('#pagination').bootpag().on("page", function(event, num){
        page = num;
        data = $.extend($('#pagination').data("filters"),{page:page});
        periodo = $('#pagination').data("periodo");
        actividad = $('#pagination').data("actividad");
        window.app.runRoute('post','#/pago/detalle/periodo/'+periodo+'/actividad/'+actividad+'/detalle/ot/'+page,data);
    });*/
	$("button#download").on("click",function(e){
		e.stopImmediatePropagation();
	  	$btn = $(this);
	  	$btn.button("loading");

	  	$.fileDownload("rest/pago/descargar/detalle/costeo/contrato/"+window.contract+"/periodo/"+$btn.data("periodo")+"/lista",{httpMethod:"POST",data:$.param($btn.data("filters")),
		    prepareCallback:function(url) {
		        $btn.button("processing");
		    },
		    successCallback: function(url) {
		        $btn.button('reset')
		    },
		    failCallback: function(responseHtml, url) {
		        $btn.button('reset')
		    }
	  	});		
	});


	$("#buscarFiltro").click(function(){
	
		$("#siom-form-costeo-generar").submit();
		return true;

	});

	$("#GuardarCosteo").click(function(){
	
		button   	= $(this);
		form     	= $("#siom-form-costeo");
		id_ot  = form.find(':input[name="id_ot"]').map(function(){ return $(this); }).get();
		/*check_capex = form.find(':input[name="check_capex"]').map(function(){ return $(this); }).get();
		check_opex = form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
		capex_ot = form.find(':input[name="capex_ot"]').map(function(){ return $(this); }).get();
		opex_ot = form.find(':input[name="opex_ot"]').map(function(){ return $(this); }).get();
		check_opex_value = form.find(':input[name="check_opex_value"]').map(function(){ return $(this); }).get();
		check_capex_value = form.find(':input[name="check_capex_value"]').map(function(){ return $(this); }).get();
		cantidad_lpu = form.find(':input[name="cantidad_lpu"]').map(function(){ return $(this); }).get();
		precio_lpu = form.find(':input[name="precio_lpu"]').map(function(){ return $(this); }).get();
		monto_total = form.find(':input[name="monto_total"]').map(function(){ return $(this); }).get();
		periodo = document.getElementById("periodo").value;
		actividad = document.getElementById("actividad").value;*/

		actualizarMonto();
		
		check_penalidad = form.find(':input[name="check_penalidad"]').map(function(){ return $(this); }).get();
		check_penalidad_value = form.find(':input[name="check_penalidad_value"]').map(function(){ return $(this); }).get();
		cantidad_lpu = form.find(':input[name="cantidad_lpu"]').map(function(){ return $(this); }).get();
		monto_total = form.find(':input[name="monto_total"]').map(function(){ return $(this); }).get();
		monto_total_cabecera = form.find(':input[name="monto_total_cabecera"]').map(function(){ return $(this); }).get();

		periodo = document.getElementById("periodo").value;
		actividad = document.getElementById("actividad").value;


		
		/*lineapresupuestariacapex =document.getElementById("linea_presupuestocapex").value;
		if ("0"==lineapresupuestaria) {
			alert("Debe seleccionar al menos una linea de presupuesto, para generar costeo."); 
			return true;
		}*/
		
		//monto_total_cabecera = $("#monto_total_cabecera").val();
		monto_total_cabecera =document.getElementById("monto_total_cabecera").value;
		if (0==monto_total_cabecera) {
			alert("Debe seleccionar al menos una orden de trabajo, para generar costeo."); 
			return true;
		}
		
		
		string_id="";
		string_cantidad_lpu_valor="";
		string_penalidad_selec="";
		string_monto_total_cabecera="";
		string_monto_total="";
		
		for(i=0;i<id_ot.length;i++){
			string_id=string_id + ";" + id_ot[i].val();

			string_monto_total=string_monto_total +";"+ monto_total[i].val();
			string_cantidad_lpu_valor=string_cantidad_lpu_valor+";"+ cantidad_lpu[i].val();
			
			if(check_penalidad[i].is(':checked')){
				check_penalidad_value[i].val("SI");
				string_penalidad_selec=string_penalidad_selec + ";SI";
			}else{
				check_penalidad_value[i].val("NO");
				string_penalidad_selec=string_penalidad_selec + ";NO";
			}
		}
		document.getElementById("str_id_ot").value = string_id;
		document.getElementById("str_cantidad_lpu").value = string_cantidad_lpu_valor;
		document.getElementById("str_check_penalidad_value").value = string_penalidad_selec;
		document.getElementById("str_monto_total_cabecera").value = monto_total_cabecera;
		document.getElementById("str_monto_total").value = string_monto_total;
	
		
		form     	= $("#siom-form-costeo");
		confirm("El costeo será guardado <b></b><br>Desea continuar?",function(status){
			if(status){
				button.button("Guardando...");
				form.submit();
				//return true;
			}
		});
	});
	
})(jQuery);