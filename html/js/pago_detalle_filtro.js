(function($) {

	$("button#Limpiarfiltro").on("click",function(e){
		$('input[name=pafi_anio]').val("");
		$('input[name=pafi_anio]').change();

		$('select[name=pafi_mes]').val("00");
		$('select[name=pafi_mes]').change();
  		
	});

	$("#pafi_anio").on('input', function(){
		$(this).val($(this).val().replace(/[^0-9]/gi,''));
		return true;
	});

	$("button#BuscarResumenPago").on("click", function (e) {
		$("#pago-resumen-filtro").submit();
		return true;
	});

})(jQuery);