
function abrirModalhemyderivada( cont_id, copa_id, copa_tipo_ot){
    data = {};
    data.contId = cont_id;
    data.copa_id = copa_id;
    data.copa_tipo_ot = copa_tipo_ot;
    window.app.runRoute('post','#/hemyderivada/detalle', data);
}

(function($) {

    $("button#validarActa").on("click",function(e){

      
            form = $(context.target);

      
            $(this).parents("tr").find("#periodo").each(function(){
                periodo_valor= $(this).val();
                periodo = periodo_valor;
            });
             $(this).parents("tr").find("#copa_id").each(function(){
                copa_id_valor= $(this).val();
                copa_id = copa_id_valor;
            });
             $(this).parents("tr").find("#actividad").each(function(){
                actividad_valor= $(this).val();
                actividad = actividad_valor;
            });
             console.log("la actividad es"+actividad_valor);
                        
            confirm("¿Esta seguro que desea validar el acta?", function(status){
                if (status == true){
                    url = 'rest/contrato/'+contract+'/periodo/'+periodo_valor+'/copa_id/'+copa_id_valor+'/actividad/'+actividad_valor+'/validar',data;
                    $.post(url,data,function(json,textStatus) {
                        if( json.status==0 ){
                            that.showError("Error al validar los cambios ",json.error,"ruta:"+url);
                        } else {
                            alert("Validación ejecutada correctamente ",window.location.replace("#/pago/regresar/resumen"));
                            return true;   
                        }
                     
                    }).fail(function(){
                        that.showError("Error Al validar acta","Error en POST","ruta:"+url);
                    });
                } else {
                    that.redirect("#/pago/regresar/resumen");
                }
            });
  });

    $("button#validarCosteo").on("click",function(e){

      
            form = $(context.target);

      
            $(this).parents("tr").find("#periodo").each(function(){
                periodo_valor= $(this).val();
                periodo = periodo_valor;
            });
             $(this).parents("tr").find("#copa_id").each(function(){
                copa_id_valor= $(this).val();
                copa_id = copa_id_valor;
            });
             $(this).parents("tr").find("#actividad").each(function(){
                actividad_valor= $(this).val();
                actividad = actividad_valor;
            });
             console.log("la actividad es"+actividad_valor);
                        
            confirm("¿Esta seguro que desea validar el costeo?", function(status){
                if (status == true){
                    url = 'rest/contrato/'+contract+'/periodo/'+periodo_valor+'/copa_id/'+copa_id_valor+'/actividad/'+actividad_valor+'/validar/costeo',data;
                    $.post(url,data,function(json,textStatus) {
                        if( json.status==0 ){
                            that.showError("Error al validar los cambios ",json.error,"ruta:"+url);
                        } else {
                            alert("Validación ejecutada correctamente ",window.location.replace("#/pago/regresar/resumen"));
                            return true;   
                        }
                     
                    }).fail(function(){
                        that.showError("Error Al validar costeo","Error en POST","ruta:"+url);
                    });
                } else {
                    that.redirect("#/pago/regresar/resumen");
                }
            });
  });

    
	$("button#download").on("click",function(e){
		e.stopImmediatePropagation();
	  	$btn = $(this);
	  	$btn.button("loading");
	  	$.fileDownload("rest/contrato/"+window.contract+"/pago/detalle/cerrada/descargar",{httpMethod:"POST",data:$.param($btn.data("filters")),
		      prepareCallback:function(url) {
		          $btn.button("processing");
		      },
		      successCallback: function(url) {
		          $btn.button('reset');
		      },
		      failCallback: function(responseHtml, url) {
		          $btn.button('reset')
		          alert(responseHtml);
		      }
	  	});
	});




})(jQuery);
