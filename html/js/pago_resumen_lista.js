

function validaNumericos(variable,event) {
    
    tipo_cambio =variable.search(",");
    
    if((event.charCode >= 48 && event.charCode <= 57) || (44==event.charCode && -1 ==tipo_cambio)){
    	return true;
    }else{
      if(8==event.charCode){
        return true;
      }
    }

    return false;
}


function validaNumericosSinComa(event) {
    if(event.charCode >= 48 && event.charCode <= 57){
    return true;
    }else{
      if(8==event.charCode){
        return true;
      }
    }
    return false;
}

function parsePesoJs(valor) {
    console.log(valor.value);
    var value = valor.value;
    if(null!=value){
        console.log(value);
        value = value.replace(".", ",");  

        var parts = value.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        console.log(parts.join(".")); 
        valor.value = parts.join("."); 
        return valor.value;       
    }       
}   

function abrirModalhemyderivada( cont_id, copa_id, copa_tipo_ot){
    data = {};
    data.contId = cont_id;
    data.copa_id = copa_id;
    data.copa_tipo_ot = copa_tipo_ot;
    window.app.runRoute('post','#/hemyderivada/detalle', data);
}

(function($) {

    $("button#validaracta").on("click",function(e){

      
            form = $(context.target);

      
            $(this).parents("tr").find("#periodo").each(function(){
                periodo_valor= $(this).val();
                periodo = periodo_valor;
            });
             $(this).parents("tr").find("#copa_id").each(function(){
                copa_id_valor= $(this).val();
                copa_id = copa_id_valor;
            });
             $(this).parents("tr").find("#actividad_valor").each(function(){
                actividad_valor= $(this).val();
                actividad = actividad_valor;
            });
            
                        
           console.log("la actividad es "+ actividad);
          

            confirm("�Esta seguro que desea validar el acta?", function(status){
                if (status == true){
                    url = 'rest/contrato/'+contract+'/periodo/'+periodo_valor+'/copa_id/'+copa_id_valor+'/actividad/'+actividad_valor+'/validar',data;
                    $.post(url,data,function(json,textStatus) {
                        if( json.status==0 ){
                            that.showError("Error al validar los cambios ",json.error,"ruta:"+url);
                        } else {
                            alert("Validaci�n ejecutada correctamente ",window.location.replace("#/pago/regresar/resumen"));
                            return true;   
                        }
                     
                    }).fail(function(){
                        that.showError("Error Al validar acta","Error en POST","ruta:"+url);
                    });
                } else {
                    that.redirect("#/pago/regresar/resumen");
                }
            });
  });
    
    $("button#validarcosteo").on("click",function(e){

      
        form = $(context.target);

  
        $(this).parents("tr").find("#periodo").each(function(){
            periodo_valor= $(this).val();
            periodo = periodo_valor;
        });
         $(this).parents("tr").find("#copa_id").each(function(){
            copa_id_valor= $(this).val();
            copa_id = copa_id_valor;
        });
         $(this).parents("tr").find("#actividad_valor").each(function(){
            actividad_valor= $(this).val();
            actividad = actividad_valor;
        });
         console.log("la actividad es"+actividad);
                    
        confirm("�Esta seguro que desea validar el costeo?", function(status){
            if (status == true){
                url = 'rest/contrato/'+contract+'/periodo/'+periodo_valor+'/copa_id/'+copa_id_valor+'/actividad/'+actividad_valor+'/validar/costeo',data;
                $.post(url,data,function(json,textStatus) {
                    if( json.status==0 ){
                        that.showError("Error al validar los cambios ",json.error,"ruta:"+url);
                    } else {
                        alert("Validaci�n ejecutada correctamente ",window.location.replace("#/pago/regresar/resumen"));
                        return true;   
                    }
                 
                }).fail(function(){
                    that.showError("Error Al validar el costeo","Error en POST","ruta:"+url);
                });
            } else {
                that.redirect("#/pago/regresar/resumen");
            }
        });
    });
    
	$("button#download").on("click",function(e){
		e.stopImmediatePropagation();
	  	$btn = $(this);
	  	$btn.button("loading");
	  	$.fileDownload("rest/contrato/"+window.contract+"/pago/detalle/descargar",{httpMethod:"POST",data:$.param($btn.data("filters")),
		      prepareCallback:function(url) {
		          $btn.button("processing");
		      },
		      successCallback: function(url) {
		          $btn.button('reset');
		      },
		      failCallback: function(responseHtml, url) {
		          $btn.button('reset')
		          alert(responseHtml);
		      }
	  	});
	});

  $("tbody tr").each(function(){
    var elm = $(this).find("td");
    var estado = elm.filter(":eq(3)").text();
    if(     "CREADO" == estado || 
		    "APROBADO" == estado ||
            "PEND_VALIDAR_ACTA" == estado ||
		    "PEND_VALIDAR_COSTEO" == estado ||
            "VALIDANDO_ACTA" == estado ||
            "COSTEO_VALIDADO" == estado ||
		    "VALIDANDO_COSTEO" == estado ){

      $(this).find("input").attr('disabled',true);
      $(this).find("select").attr('disabled',true);
     /* $(this).find("button").attr('disabled',true);*/
    }
	if(estado == "VALIDADO" ){
        
		//$(this).find("a").remove();

    }
    if("CREADO" == estado )
    {
        $(this).find("#observacion_resumen").attr('disabled',false);
    }
  });

  $("button#guardarResumenCosteo").click(function(){
    var copa_id = "";
	  var periodo = "";
    var derivada = "";
    var hem = "";
    var tipo_cambio = "";
    var papr_id = "";
	  var observacion = "";

    $(this).parents("tr").find("#copa_id").each(function(){
     
     copa_id = $(this).val();
   
    });
    //VERIFICAR PERIODO
    $(this).parents("tr").find("#periodo").each(function(){
     
      periodo = $(this).val();
 
    });
    $(this).parents("tr").find("#derivada").each(function(){
    
      derivada = $(this).val();
   
    });
    $(this).parents("tr").find("#hem").each(function(){
     
      hem = $(this).val();
    
    });
    $(this).parents("tr").find("#tipo_cambio").each(function(){
     
      tipo_cambio = $(this).val();
      
    });
	$(this).parents("tr").find("#observacion_resumen").each(function(){
      
      observacion = $(this).val();
    
    });
    $(this).parents("tr").find("#copa_estado").each(function(){
      
      copa_estado = $(this).val();
    
    });
    console.log(copa_estado);

    if('ACTA_VALIDADO'!=copa_estado)
    {
         data = {copa_id:copa_id,observacion:observacion};
          url = 'rest/contrato/'+contract+'/pago/actualizar/observacion';
          $.post(url,data,function(json,textStatus) {
              if(json.status){
                  that.showSuccess("Observacion guardada ","Observacion almacenada correctamente ","#/pago/regresar/resumen")
              } else {
                  that.showError("No se guardo observacion ",json.error,"ruta:"+url,"#/pago/regresar/resumen")
              }
          },'json').fail(function(){
              that.showError("Error al guardar la observacion","No se pudo guardar la observacion","ruta:"+url,"#/pago/regresar/resumen")
          });
    }else
    {
       
        if(null == observacion ||  "" == observacion ){
          alert("Campo observacion  esta vacio para el costeo ID: "+copa_id);
          return   
        }else{
             
              data = {copa_id: copa_id, observacion:observacion, periodo:periodo};
              url = 'rest/contrato/'+contract+'/pago/actualizar/presupuesto';
              $.post(url,data,function(json,textStatus) {
                  if(json.status){
                      that.showSuccess("Enviado costeo ","Costeo enviado exitosamente","#/pago/regresar/resumen")
                  } else {
                      that.showError("No enviado el costeo ",json.error,"ruta:"+url,"#/pago/regresar/resumen")
                  }
              },'json').fail(function(){
                  that.showError("Error enviando costeo","No se pudo enviar el costeo","ruta:"+url,"#/pago/regresar/resumen")
              });
            }
            
    }
  });


   /* $("#listado-archivos").on('click','a.btn', function(e) {
        var id = $(this).data("id");
        $("#archivo-"+id).remove();
        $(this).parent().remove();
    });
*/

})(jQuery);
